---
title: OTZ一百遍
author: gprs2006
date: 2012-03-03 00:00:01
category: tiandao
layout: post
lang: zh-Hant
depth: 1
---

#### 作者：gprs2006

---

一直以來，棄天帝都不相信蒼是一個完美的道士，即使是在當他隻身單挑魔界大軍，只用一招白虹貫日就輕易應付了一招雲龍斬之後，仍然固執的堅持着自己的看法。

蒼嘆了口氣，搭下那本來就合上的眼皮，無語的轉頭走開，只留下了一個很古怪的表情-----囧

棄天帝好奇的問，你這是什麼表情？

蒼再度搖了搖頭，嘆道----愚神不可教也。

棄天帝抓着張紙，鬱悶的沉思了半天，還是米能搞明白蒼的意思，無奈只能展開黑色小翅膀，從天魔池屈駕飄臨到魔界，去找素以只要一聚頭就勝過諸葛亮的三隻臭皮匠-----魔界三巨頭出主意。

棄天帝正巧親眼目睹三巨頭的爭霸戰，從爭奪，到最後勝利，以及伏嬰師和斷風塵只能向兩隻小狗般眼淚汪汪的盯着他在那裏吃得快活的整個幼稚行爲，全程都在他的V8監視中。

襲滅天來手中捧着好不容易才從另外兩巨頭手中騙搶回來的香草冰淇淋，趕在沒有融化之前，全部消滅掉。

同時發自內心暗暗讚歎，不愧是三巨頭的老大，超大號的特大家庭裝，從捧上手到及時被消滅，只短短三分鐘時間。

咳哼一聲，既突顯出大BOSS的地位，也作爲了開場白。

對於BOSS的降臨，三巨頭並沒有表現出爲人臣的受寵若驚，浮現更多的，是被打擾了的不耐煩。

長話短說的說明了來意，並附上草稿以作說明，斷風塵被其餘兩隻很沒義氣的推了出來，眼珠轉了輕快地，刷刷在紙上補上幾筆。

聰明但不絕頂的棄天帝頓時目露精光，茅塞頓開。滿意的飄然而去。

當棄天帝再度出現在眼皮前時，蒼被他臉上叵測的笑意鬧得心裏發毛。

我的皇后原來竟然是外在斯文，內裏OPEN的。

倒退一步，笑得那麼奸詐的棄天帝，靠近是很危險的。

你的暗示我當然明白了，這麼妙的人體藝術，果然還是隻有我的皇后才能完全體會到的，也只有你才能身體力行的完完整整詮釋出來。

蒼考慮着要是把自己丟到異度空間的話，是否能回來，不過，先不管能否回來，當他不小心的瞟到那張被完全誤會曲解了的表情演變圖之後，意識到逃離眼前不懷好意的所謂的神才是當務之急。

當然，松鼠是始終逃不出棄天帝的手掌心的，一陣“......”之後，蒼被棄天帝抓着，親身示範了那從表情分解演變出來動作，從跪趴OTZ到朝天 ○\|＿\|￣式。

一招一式，毫無遺留，全部演練了一遍，棄天帝意猶未盡之餘，重重複復的抓着蒼反反覆覆的又練習了一百多遍。

時間在流逝，溫存在延續，還是該死的無限延長。

蒼全身癱軟在褐與黑交織的發鋪上，好奇的問棄天帝，除了整天不事生產外，還能做些什麼。

結果棄天帝很自豪的列舉了一堆他的豐功偉績：

不辭辛苦的親身上陣，傾巢而出的出動窩裏的所有人，大張旗鼓的鬧上了人間，造成不必要的建築損毀，生靈塗炭，經濟失衡，社會動盪......，損人不利己天翻地覆的一場大亂後，除了造成魔界人口瞬間增長過快，人手調配嚴重不足，物質需求急劇上升，物價指數迅速飆升造成通貨膨脹外，似乎毫無得着......

最後的最後，就是把我的皇后，你--OTZ100遍......

<br/>

-----我是作者的分割線------

咳咳，來點作者的話

可能比較含蓄，大家不是太明白，我這裏說明一下（100遍還含蓄= =）

開頭是因爲棄總一貫的火星思維，蒼給了他一個囧的表情

對於這個表情，棄總不太明白是什麼意思，於是棄總就把這個囧字畫在紙上，拿去問三巨頭

三巨頭忙着香草冰激凌爭奪戰，不理棄總，最後斷風塵被兩外兩隻沒義氣地推出來應付棄總

鑑於斷同學萬花叢中過的輝煌，看到那個囧字就理所當然地按照自己的邏輯一根筋地把囧字進行演變

從囧到囧丁乙到囧TZ到OTZ以此類推到○\|＿\|￣等等五花八門各式各樣（因爲囧表示無語無奈的意思，so囧=OTZ，由OTZ演變到後面○\|＿\|￣那些各種各樣的姿勢，如果還不太明白，可以百度一下囧和OTZ的歷史）

於是，悲劇發生了。。。。。。。

棄總按照那張圖，找到蒼一一實驗，那些表情演變姿勢圖就是h的各種姿勢，OTZ是跪趴式，○\|＿\|￣是仰躺式等等以此類推各式各樣五花八門千變萬化。。。。。。。於是就有了上文的情節。。。。。。。
