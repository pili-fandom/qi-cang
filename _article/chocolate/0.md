---
title: 情人巧克力
date: 2018-01-02 00:00:00
layout: post
lang: zh-Hant
category: chocolate
depth: 1
author: 阿日
---

#### By 阿日

原載於[http://36rain.com/read.php?tid=107690](http://36rain.com/read.php?tid=107690)

---

「今天會議就到這裏爲止，大家散會吧！」坐在會議桌中的男人宣佈開會結束。

一聽開會結束後，大家都站了起來伸伸懶腰，這場會議持續開了2小時以上的時程，原因是爲了接下來的年度業績。

<br/>

等到所有經理人離開會議室後，蒼松了一口氣躺在椅子上，揉着雙眼舒緩壓力。這時候，蒼的二弟·翠山行站在旁邊問道：「哥……要不要先去吃中餐？現在也中午了！」

他眯着眼睛思考着，想一想也好，便對翠山行說：「好！你去備車吧！我馬上下去。」

翠山行立刻離開會議室，而蒼看着面前的筆電，手指移動幾下後就關掉電源，起身離開會議室。

<br/>

現在時間也已經是中午午休時段，所有員工開始約好等等要去員工餐廳還是外面的餐廳吃頓好的，一路上遇到的職員都跟蒼打聲招呼。

「執行長好！」

「執行長好！」

蒼也有禮地回答他們：「嗯！快去吃午餐吧！」

<br/>

走到大樓電梯前，正準備要搭電梯下去時，後方正傳來女性職員的談話：「對了！今天是白色情人節耶！你跟你男朋友今天要怎麼過呢？」

問問題的女職員用他的手肘頂着他的同事，想要套問出什麼，另一名女職員害羞的捂着發紅的臉頰，害羞的說：「討厭啦！問這種問題，才不告訴你咧！除非你先說你男朋友要送什麼？」

「這問題好像不是問我吧！不過啊……我大概知道他會送什麼，最多請我吃吃燭光晚餐會是看看電影而已……」

「嗄……那麼沒情趣啊……虧你情人節那天親手做巧克力給他。」那名女職員爲他同事惋惜着。

<br/>

聽到女職員談論話題中，蒼才知道今天是白色情人節，這讓他想到那個人在情人節時送他的九十九玫瑰、晚上還押走自己到他的別墅吃燭光晚餐，還記得在晚餐結束後，那個人對他說了一句話：『我很期待你在白色情人節時會送我什麼喔！』

那時候他的表情告訴自己『如果不準備沒關係……我會討回來的！』。

自己還很不爽的回他一句『誰理你啊！！』，接着頭也不回地跑了，如今想想似乎有點不妥，但那也不關自己的事……誰叫他不斷對自己獻殷勤，明明自己給他吃了數次閉門羹、冷臉，他還是像打不死的蟑螂死纏爛打地來騷擾。

幸好這幾天他出國了，有好幾天耳根能安靜了……至於白色情人節嘛～不想管了！

<br/>

蒼搭電梯坐到一樓後，走出大樓的旋轉門前等翠山行開車過來，在等車期間，蒼低頭看着手機、看看接下來的行程。

『嘎嘎！！』一陣刺耳的煞車聲傳入耳裏，映入眼簾的是一臺黑色的高級轎車，從車裏下來兩名黑衣人，走到蒼面前。

「蒼先生！失禮了。」一名黑衣人對他道歉。

在蒼感到莫名之刻，一記手刀往頸部劈下來，「呃！！」

悶哼一聲後，蒼失去意識地昏過去，正開車來接蒼的翠山行撞見，立即下車子阻止現行綁架！

「你們要幹什麼？快放開我們的執行長！！」翠山行斥喝道。

一名黑衣人擋在他之前，對他說：「很抱歉！總裁下令一定要帶蒼先生走！」

「你們總裁是誰啊？膽敢在光天化日下綁人！我要報警！」翠山行拿出手機準備打電話。

「我們總裁就是異度集團的棄天帝先生。」黑衣人報上他們老闆的名號。

翠山行一聽，手機差點滑落！錯愕地看着那位黑衣人。

黑衣人見翠山行震驚過大，無法回神，他便一行禮後坐上轎車將人帶走，徒留驚嚇過度的翠山行呆在原地。

「老哥抱歉……不是我不去救你，是怕救了你連累到全公司及家裏的人……」最後翠山行含淚說着，棄天帝他惹不起啊！

<br/>

＊＊＊＊＊

<br/>

不知道昏迷多久，再醒過來之後，蒼覺得自己的脖子非常痛！他想伸手揉着發疼的地方，但卻發現手伸不起來，他向手臂的方向往下看，發現自己身體居然被綁着！

這……這是怎麼回事？他還記得中午要跟小翠一起去吃飯，接下來有兩名黑衣人出現……

黑衣人！對了、是他！是那傢伙搞的鬼嗎？他是什麼時後回國的？

蒼往四周瞧瞧，發現自己身處富麗堂皇的房間內，人還躺在床上！他試着爬起來、卻發現雙腳被綁在床尾的兩邊床柱。

一道開門聲，走入一位黑髮男子，身穿黑西裝、白色襯衫、打着黑色領帶，帶着饒富趣味的表情看着躺在床上的蒼。

「親愛的，你醒啦！」輕挑的語氣，配上那張邪魅的俊容，雙眼有着一藍一金異色瞳孔。

他就是異度集團的總裁——棄天帝，他推着一臺推車進到房間，推車上還有一大盤高級巧克力、巧克力沾醬。

棄天帝將推車停到床邊後，坐在床上看着被綁得像肉糉的蒼，他內心一股慾望不停流竄着。

「棄天帝！你到底要做什麼？？」蒼憤怒地看着他。

看樣子……蒼似乎忘了一個月前的事了，棄天帝心裏盤算着等等該怎麼『料理』這美味的人。

「你忘了啊……一個月前的情人節，我說我很期待你的回禮啊！但你遲遲沒有表示，所以我只好親自來討了！」棄天帝突然靠近蒼，兩人之間的距離只有一線之隔。

蒼不自覺往後退，但雙腳都被綁住，不能移動！他只能讓身體稍微往後移。「那是你自做主張的要我陪你吃那頓飯吧！我可從沒答應你要給你回禮的！」

「是啊……那時是我自做主張要你陪我吃的，那麼……現在我再自做主張向你討回禮也沒差吧！」一說完，棄天帝馬上把人壓制在床上。

蒼被嚇到，但隨即恢復冷靜，他開始奮力掙扎、想把棄天帝甩下身，但棄天帝不爲所動！動手扯掉蒼的領結、襯衫釦子，扒開衣服露出他的胸膛。

身爲男人，蒼當然知道棄天帝要做什麼，但他可沒那種意願！

「放開我！棄天帝！」蒼嘶吼大叫，一邊掙扎着。

棄天帝深深一笑，一手抓住蒼的下顎、吻住他的雙脣，這又讓蒼瞪大了他的單鳳眼。

<br/>

入侵的舌信靈巧地滑過口腔，找到他的舌頭後立即卷在一起、不停吸吮着，交換彼此的唾液。兩人呼吸之間，熱氣全數噴灑在對方的臉頰上，棄天帝享受着他強行侵入的甘甜之吻。

直到他認爲可以後，放開蒼的下顎、讓他喘口氣。被吻到差點不能呼吸的蒼，在深深一吻結束後，立即大口大口吸氣、雙頰明顯漲紅着。

棄天帝看他這樣子也是挺可愛的，他伸手向推車拿起一顆巧克力含在嘴中，接着再低頭吻住蒼、以口對口地把巧克力傳遞到他的口中。巧克力在經過溫度融化後，流進蒼的嘴中、口腔內頓時充滿了香甜味道。

棄天帝再起身，詢問蒼：「巧克力好吃嗎？」他用手指輕撫蒼的嘴脣。

「哼！」蒼別過臉，不理會棄天帝。

不理他嗎？那也沒關係……得到他的『回禮』比較重要！棄天帝的雙手開始往下游移，脣辨輕點在蒼的頸部，讓蒼感覺到一陣一陣的溼熱。

最後雙手停留在白皙的胸膛，搓揉着胸前茱萸，蒼頓時悶哼一聲，一股麻癢刺痛的快感瞬間升起。

「呃……棄天帝你快住手！」感到非常不舒服的蒼，欲喝止身上人的行爲。

只見棄天帝低下頭，伸舌舔舐着一邊的硃紅、一手搓揉着另一邊，溼熱感加上搓揉的快感，弄得蒼受不了、咬緊下脣才忍着不發出丟臉的叫聲。

看着被自己舔到挺立的乳首，棄天帝對成果感到滿意，他擡頭看着蒼此刻的表情，看他一直隱忍着不出聲、緊咬下脣不放，這讓他有點心痛。

「蒼……何必壓抑自己呢？體會情慾本來就是我們人該經歷的事啊！」他撫摸着蒼的臉頰，輕輕點着他緊咬不放的雙脣。

<br/>

在兩人對視之時，棄天帝一手悄悄往下移，用力抓住蒼的下體！

「嗚呃！」一陣劇痛讓蒼叫了出來，他再憤怒地瞪着棄天帝。

「接着……我會再讓你享受至高極樂的！」語畢，他再將身體往下移動。

棄天帝來到蒼的下半身，解開西裝褲的皮帶、拉鍊，接着一鼓作氣往下拉，期間的蒼完全無法抵抗！

褲子脫掉後，下身私密處全一覽無遺，讓蒼羞怯地將頭別到一旁，雙眼緊閉着。棄天帝一手大掌握住蒼的玉莖、溫柔地撫摸着，接着開始擼動爲蒼製造更多的快感。

在下身被棄天帝用微涼的手掌握住時，蒼就感覺到一股電流往身上竄，再開始撫弄時，身體就像數道電流灌進全身至四肢百骸，極高的快感佔據全身，讓蒼再也忍不住叫了出來。

「啊哈……呼嗯……嗯……」蒼不停地扭動身體。

看着蒼在自己手下那麼沉醉的模樣，這讓棄天帝更加高興，感覺到蒼在自己手中起了反應、開始膨脹，也覺得可以先停下，去準備另一件事。

棄天帝停下動作後，讓蒼有稍稍喘息的時間，當他閉上眼睛休憩時，忽然感覺到下身一股冰涼，他睜開眼睛往下移看，棄天帝居然在他的私密處抹上巧克力醬，他氣急敗壞大叫：「棄天帝！！你在幹嘛？快住手、變態！！」

「做什麼？當然是吃『巧克力』啊！」棄天帝一口含下蒼沾滿巧克力醬的玉莖，舌信舔着莖身享受巧克力的滋味。

在身下人吸舔自己的下身，那淫糜的舔舐聲敲蕩着蒼的理智，溼熱的觸感、莖身被舌頭包住挑逗快感，舌頭的纏繞及不時用牙齒輕啃玉莖頂端，搔颳得柱身不時打顫，幾儘讓蒼要失去理智。

在忙着服侍下身同時，棄天帝也不忘了蒼的上半身，伸出一手揉捏着胸膛的茱萸，讓蒼有更多的快樂。

「啊……哼嗯……」發出甜膩的嗓音，蒼的雙眼泛着淚光，在上與下的逗弄下，再也忍不住將達到高潮。

也知道蒼即將要高潮，棄天帝賣力吸着蒼的玉莖，在一陣淫糜吸舔水漬聲下，蒼的欲流宣泄而出，棄天帝抓緊時機、鬆開蒼的玉莖，使得他的玉露濺得滿手都是。

他看着蒼的玉露沾滿整隻手，戲謔地伸舌舔一舔，對蒼說：「嗯……味道挺像白巧力的！」

高潮過後的蒼，根本不想看到棄天帝，他閉上眼、不搭理，而棄天帝看他這樣，也是不在意，便將沾滿玉露的手往後庭探，一根手指開始開拓後穴。

感覺到後庭被入侵的感覺，蒼依然閉着雙眼，但突然的入侵，也使他難受地皺起眉梢。

棄天帝看蒼依然不搭理自己，於是繼續做自己的事，他停止開拓動作，抓住蒼的一隻腳踝、解開綁在腳踝的繩子，這讓蒼成功地注意到自己。

蒼也感覺到棄天帝解開一腳的繩子後，想試着抽回自己的腳、用力踹下這男人，但腳卻被棄天帝死命抓着。

「嘖嘖嘖……想把我一腳踢開，想太美嘍！」棄天帝快速地將蒼的西裝褲脫下一邊，讓另一邊的腳還掛着褲子。

接着將能自由行動的腳掛在肩上、讓後庭顯露在自己面前，手指再繼續做着開拓動作，再次入侵……讓蒼感到不適，他試着用他能動的腳將棄天帝踢開，但無奈棄天帝抓得非常死緊！

「蒼……別再掙扎了……還是乖乖地享受我爲你帶來的快樂吧！」他親吻蒼的大腿內側，用指甲輕刮肌膚、引起蒼的輕顫。

棄天帝再伸入第二根手指，加快速度、整個腸壁開始分泌出潤滑液，讓兩根手指更加暢快，而蒼也感覺到後庭開始有異樣的感覺，那種麻癢感順着脊髓上流至腦頂，他感覺到後穴還想要更多……

「啊哈……」蒼開始輕喘，身體不斷髮着抖。

眼見蒼已經起了反應，察覺後穴的開拓也差不多了……棄天帝抽出手指，蒼頓時感覺到一陣空虛。

棄天帝拿起方才用的巧克力醬，倒在蒼泛起粉色的胸膛上：「現在……我要好好享受了……」棄天帝的眼神流露出旺盛的情慾。

他脫掉自己的褲子、掏出早已壯碩的欲根，腰一挺、碩根長驅直入，已經開拓過的蒼，還是無法承受異物入侵，痛得悶哼一聲：「呃啊！」

蒼的眼角流出淚水來，棄天帝看到後，伸出手指爲他擦拭，「別怕……等等就舒服了……」

「你放過我不是更好！！」蒼氣得大叫！

他的蒼總是如此天真又固執啊……棄天帝會心一笑，他俯低身子、在蒼的耳邊小聲說：「你真的太可愛了！」還故意在耳邊吹氣。

蒼被惹得繃起身體，別過頭避開與棄天帝四目交接，看蒼的反應如此羞怯，棄天帝忍不住心裏的惡趣味，開始舔着蒼沾有巧克力醬的胸膛，舌頭一點一點地舔點着，不時用牙齒啃咬，讓蒼的身體輕顫着：「嗯！」

等到棄天帝舔完胸膛上的巧克力醬後，他再拿顆巧克力含在嘴中、吻住蒼的嘴，讓他再一次享受巧克力的香甜，埋在蒼體內的碩大開始緩動起來。

溫柔又緩慢的律動，想讓蒼慢慢適應他的存在，雙脣交接的蒼感受到下身開始律動的麻癢，忍不住發出鼻音：「嗚……唔……嗯嗯……」

嘴與嘴之間脣舌交纏，發出淫糜的水聲，彼此間的銀絲牽連着，讓蒼的嘴角流出津液，棄天帝停止深深長吻、開始加快腰身的速度，讓蒼忍不住發出呻吟：「啊……啊……哈……」

後庭被加快速度的碩大不斷穿刺，周圍的肉壁開始升起摩擦的快感，使得蒼的理智陷入情慾潮水中，棄天帝看着蒼沉醉在自己帶來的快感下，心裏更加有莫名的得意，加快腰部的律動。

「啊……棄天帝……太快了……不要……停下來……」蒼被折騰得說不出完整的話。

棄天帝聽得懂他的意思，但他內心止不住惡劣地問他：「不要停嗎？好啊！」

他再壓下蒼的玉腿、身體俯下全力衝刺，讓自己的碩大更加深入蒼的內部，讓蒼大叫：「啊啊……不是……你故意的……停……停啊……」

「是你說不要停的……現在說要停下……你說……誰比較故意呢？」棄天帝不停搗弄蒼的頂點。

在碩大不停的穿刺、搗弄下，無意間撞到了蒼的敏感點，使得蒼高亢呻吟：「哈啊……」

聽得出似乎頂到蒼的敏感點，棄天帝再深入一次，「是這裏嗎？」

「啊……不要……」頂着蒼哀求着。

看蒼因自己意亂情迷的模樣，實在迷人，他決定解開綁在蒼身上的繩子。

解下繩子後，看到胸膛以下都出現紅印，讓棄天帝看了心疼起來，他親吻着腹部上的勒痕。從腹肌到肚臍，蒼感受到棄天帝輕柔一點一點地吻着他的腹部，每落下一吻就像是星火燎原一樣，燥熱非常，身體不自覺地還想更多。

「棄天……」蒼不自覺地扭動身體，想要更多。

感受到蒼無言的邀請，棄天帝也無法拒絕這邀請，於是解開了自身的衣物、露出精壯的身材，讓躺在床上已經意識混亂的蒼不禁心動。接着他再解開蒼另外一隻腳的繩子、一併擡起放置肩上，開始律動自己的腰，迎合蒼、給他更多的快樂。

「蒼……」棄天帝賣力擺動腰部，不斷抽插着蒼。

肉壁在碩大不停穿插下分泌出更多的腸液，讓兩人的交合處發出淫糜的水聲，讓蒼更加陷入瘋狂、放聲呻吟：「哈啊……棄天帝……棄天……啊……啊……」

雙手絞緊着床單，口中不停地呻吟着，加快了棄天帝體內血液的奔騰，讓他更加忘我地加速抽撤。

受不了速度太快，蒼開口哀求着：「啊……棄天……太快了……慢……點……呃……」

棄天帝身體再度俯下、讓自己更深入蒼的體內，「停不下來啊……蒼……你太棒了……」

「啊……啊……哈啊……太快了……」無法停止的快感，激得蒼流出淚水，呻吟聲帶着哭腔，讓他格外的楚楚可憐。

「蒼……蒼……」棄天帝不斷叫着蒼，精壯的身體揮發着汗水。

佔有心愛的人與之結合，原來是這麼棒事情！棄天帝完全沉溺在他與蒼交合的極度情慾之下。

數次瘋狂抽撤後，蒼忍不住達到高潮、玉莖挺立地射出第二道激流，後穴的肉壁在感受到高潮後開始收縮，緊縛着棄天帝的欲根，受到肉穴的收縮後，讓他也忍不住他的第一波高潮、全數射進蒼的後庭內。

一道熱流灌入後穴內中，滾燙得讓蒼緊繃身體接受。

<br/>

「呼……呼……」到達高潮後，蒼虛脫無力地躺在床上喘氣着。

而撐着身體由上往下看的棄天帝，看着蒼紅着臉喘氣的模樣，格外迷人，加上他原本白皙的身體泛起粉嫩可口的顏色，引發他另一波情慾。

沒察覺到身上之人用危險的眼神看着他的蒼，眼神迷茫、身體還無法從方才高潮恢復過來，他閉上眼……想試着讓頭腦冷靜下來。

突爾！一雙大手抓住自己的身體，蒼驚醒過來，他看到棄天帝邪俊的面容近在咫尺間，將他的身體翻過去、讓他背對着他。

「你要幹嘛？」蒼無力問着。

「噓……」棄天帝示意他不要再多問。

不明白他意思的蒼，開始感到害怕，就趁身體還能動之時，想逃開棄天帝。但在他移動時，棄天帝馬上看穿他的意圖，大掌緊扣住蒼腰間、擡起，再次挺入他的欲根。

又再次被欲根挺入的蒼，發出細微的單音：「嗯！」

再次進入後穴中，有着先前體液的潤滑，讓棄天帝深深直入頂端，感受到蒼的後穴包住的感覺，他忍不住發出讚歎：「呼嗯！」

棄天帝也注意到推車上的巧克力還有很多，於是拿起一盤在手上，在蒼優美的背脊上放上一顆一顆巧克力、直到尾椎爲止。

感覺到背部的異樣，蒼不敢移動，怕一移動、就會引起身後男人的慾望，但這樣也正好合棄天帝的意，他低下身體，慢慢吃着放在背上的巧克力，因爲蒼剛經歷過高潮、身體體溫稍高，讓在背上的巧克力開始融化了。

棄天帝微微一笑，伸出手指抹了抹融化的巧克力沾在指端，遞向蒼的嘴邊讓他嚐嚐，「蒼……來吃吃看！」

蒼聽話地伸出舌頭舔着，巧克力摻雜着甜味與汗水的鹹味，帶着特殊的口感，蒼一併將手指含入口中舔着。以舌頭攪動着，感受自己的手指與蒼的舌頭攪和交纏，引得棄天帝埋在蒼體內的慾望、開始腫大。

棄天帝再擺動腰身，這次的姿勢讓他能深入蒼的體內，感受到肉壁與欲根溼熱摩擦的快感，忍不住加快速度！

上一波的高潮結束沒多久，又再次迎接另一波極情快感，也讓蒼再度失控呻吟：「哈啊……啊……啊……啊……」

跪趴的姿勢，能看見後庭不停吞吐火紅的欲根，蒼羞怯地閉上眼不敢再看！

「啊……啊……哈啊……」

在交合處的欲根不停地抽撤下，讓穴口中的淫液流了出來、順着蒼的大腿往下流，實在飽足視覺上的快感，讓整個畫面充滿着旖旎的色彩，肉體與肉體間的拍打聲傳遍整間臥室。

淫糜的交合聲、肉體的碰撞聲，實實在在敲擊人的意志，讓情慾掌控一切，不能自拔。

隨後，棄天帝再換個姿勢，將緊錮在腰部的雙手，用力一擡、使得蒼的身體往上揚，靠坐在棄天帝的胸膛上，兩人的交合處又更加緊密。

棄天帝一手嵌住蒼的腰身、一手握住玉莖開始上下擼動，讓蒼不禁發出細膩的呻吟，「嗯……嗚……啊……」

「蒼……」棄天帝在他的耳邊輕喊着。

似被魔音蠱惑着，棄天帝的脣瓣在蒼的耳畔廝摩，想讓他回過頭來、給他一個吻。「蒼……」棄天帝再喊一次，成功地引到蒼轉回頭，棄天帝隨即吻上去！

不同於之前強行侵略，這次的吻異常的溫柔，他輕點着、伸出舌信舔着蒼的嘴脣滋潤着，蒼也很喜歡這樣的吻，緩緩開啓嘴、讓棄天帝進入到他的口中。

舌信一進入蒼的口中後，立即捲上裏面的軟舌、瘋狂纏繞着。在上面的雙嘴激烈的交纏同時，下身交合處也開始緩緩動起來、棄天帝也不忘撫弄着蒼的玉莖，逐漸地……棄天帝開始加快腰部擺動速度、兩人的深吻不得不暫時停下，專注在身下快感中！

「啊啊……棄天……啊……」蒼因爲前有大掌撫弄着、後有欲根不停抽撤自己，在雙重快感加乘下，全身無力地躺在棄天帝身上，任由他擺弄。

聽着蒼在耳邊細膩又高亢的呻吟，大大刺激了棄天帝的情慾，他加快撫弄蒼的玉莖速度，讓蒼達到第三次高潮。

「啊……啊……不行了……」蒼再一次宣泄在棄天帝手裏，白濁的液體沾滿棄天帝的手及自己腹部上。

在蒼達到高潮後，棄天帝也不遑多讓，再抽撤數下後、雙手用力抱住蒼的細腰，將自己所有精華全射在裏面，而蒼弓起身體，迎接身後男人的熱液後，體力不支地昏了過去。

<br/>

＊＊＊＊＊

<br/>

醒過來後，蒼髮現自己躺在溫暖的懷抱內，他擡頭一看居然是躺在棄天帝的懷中，抱住他的人還睡得很熟、沒發現到身旁的人早已醒過來。

蒼想趕緊爬起來快點離開，沒想到身體一動、腰部就傳來強烈的痠痛感，讓蒼忍不住悶哼一聲：「嗚！」

聽到身旁發出聲響，棄天帝也被驚醒，發現蒼已經醒了……也跟着爬起來，「你醒了啊！」

蒼沒好氣地瞪他一眼，便忍着痠痛爬起來，也發現自己身上穿着一件乾淨的浴袍，身上沒有黏膩感，除了腰部及後庭那裏傳來陣陣酸澀、刺痛。

「我的衣服呢？快還給我！」蒼口氣不佳地對棄天帝道。

「我請人拿去洗了……明天才會好！你今天就先住下來吧！」棄天帝慵懶地躺在床上說着。

蒼怒火上提，威脅棄天帝說：「當心我提出控訴！！」

棄天帝當然不怕，他雙手上舉、讓頭枕在手臂中，黑色浴袍似有若無露出健壯的胸膛，看起來帥氣十足、要是有女性在場，一定迷死不少人，但蒼可不是普通人，不吃這一套！

「我現在就要離開！！」蒼再聲明一次，要是再來第三次，絕對宰了棄天帝！

看蒼的雙眼冒出熊熊的怒火，棄天帝也很識相，知道再不答應他的要求，倒黴的肯定是自己。

「好吧！我的衣服先借你穿，等等請司機載你回去！」棄天帝緩緩爬起來，走向房間外招來管事。

<br/>

等穿好衣服後，蒼板着表情走到大宅門外，等着車子接送，他身上穿着棄天帝的白襯衫、深色牛仔褲，因爲棄天帝的身材比他稍壯一點，所以衣服顯得鬆垮、褲子還得用皮帶扣住才不會往下滑。

一擡黑色轎車駛進宅邸後，停在大門的階梯前，蒼直接走下去，打開車門坐進去。正要關上車門時，一隻手擋下來，來人是棄天帝。

「你的衣服我明天親自開到公司送還給你。」他笑容可掬地說着。

「你放到樓下櫃檯就好，明天我會派人去收！」蒼沒好氣說着，他現在正壓抑怒氣。

「那……這禮拜天我們再一起吃午飯如何？」棄天帝不知死活地再問。

蒼再也忍不住怒火，一腳踹開棄天帝，順勢把門關上再叫司機快開車。

被踢倒在地的棄天帝不在乎地在地上大笑，看蒼髮怒的樣子也是挺可愛的，這禮拜天吃不成午飯沒關係，還有下禮拜、下下禮拜，總有天會讓蒼跟他在一起的。

<br/>

《完》

<br/>

<br/>

<br/>

【後續】

<br/>

<br/>

坐上棄天帝的私人轎車回到家後，已經是晚上八點多了，蒼拿出家門鑰匙開門進到屋內後，一股濃郁的甜味充斥家中。

這股味道非常熟悉，但蒼想不出來是什麼東西時，翠山行一看到他回來，便緊張地衝上去，「哥！」

翠山行連忙檢查蒼有沒有受傷什麼的，而蒼看到他這樣的舉動，出手阻止他，「好了，小翠……我沒事啦！」

「今天中午棄天帝把你抓走，我怎能不擔心呢？」翠山行緊張說着。

「我知道你擔心我……也幸好你沒追上來，要是他敢對你怎麼樣的話，我是不會放過他的！」

蒼曾經囑咐過翠山行，要是哪一天他要是被棄天帝單獨約談的話，誰都不可以跟來或是報警，因爲棄天帝不只是一位手段兇狠的商人，還是有黑道勢力的奸商，要是讓自己的弟弟妹妹遇上危險的話，他可不會輕易原諒自己。

<br/>

「對了……今天棄天帝把你帶走是爲了什麼事？」翠山行問蒼。

這問題讓蒼瞬間跌入寒冰地獄似的，內心不斷打寒顫……但故做沒事一般對翠山行說：「沒事……還是老樣子想找我合資某件土地併購案。」

「哥……你的衣服怎麼全換了？！他還做了什麼對吧！」翠山行注意到他的衣服全換成另一套休閒服。

「喔……今天跟他吃中飯時不小心沾到醬料，棄天帝就拿這件給我，沒什麼……」蒼對翠山行說謊。

「可是……」

見翠山行不死心地追問下去，蒼對着他說：「小翠……你不相信我嗎？」

這句話一出，打斷了翠山行所有疑問，他是絕對相信自己的哥哥，但不相信棄天帝，不過蒼都說出這句話來，他也只好作罷。

<br/>

翠山行不再問下去後，蒼直接問他家裏是怎麼回事，「小翠……今天家裏怎麼充斥着濃郁的甜味啊？」

「哦~那是赤雲染在準備明天學校園遊會要賣的東西，好像是……巧克力香蕉吧！」翠山行告訴他。

一聽到巧克力這名詞，蒼想到今天下午的事……胃忍不住翻騰，一股嘔吐感油然而生。蒼捂住鼻口，衝進房間的衛浴，吐了出來。

見蒼有奇怪的行徑，翠山行擔心的跟上去，不料蒼直接鎖上房門，翠山行只好在門外不停拍打房門。

「哥、哥……你沒事吧！哥……」翠山行不停喊着。

「嘔、嘔……」蒼猛烈嘔吐着，無法迴應門外的翠山行。

<br/>

等到嘔吐感稍微停止後，蒼有氣無力地迴應小翠：「小翠，我沒事……你去幫我弄一碗熱湯還有粥，我想吃東西。」蒼製造藉口引開小翠。

「哥，你是不是還沒吃東西所以才餓過頭了嗎？」小翠猜想蒼突然嘔吐是不是這原因，因爲他曾經一整天都沒吃東西，整個精神都投入工作上。

「對……我晚餐還沒吃，你快幫我去弄！」

「好！你等我！」

等小翠離開後，蒼開始脫下衣服，打開浴室的蓮蓬頭清洗自己的身體，隨後，整間浴室充滿着霧氣。清洗完畢後，蒼對着浴室內的鏡子，看着自己身體上，從鎖骨到胸膛間分佈密密麻麻的吻痕，只在鏡子裏看着上半身就這樣了，他不敢再繼續往下看。

蒼深深嘆了一口氣……

<br/>

＊＊＊＊＊

<br/>

隔天早上，蒼自己一人進到公司後，他的祕書突然衝向他、告知：「執行長，異度集團的總裁正在您的辦公室等您。」

一聽到棄天帝人在他的辦公室，心裏無奈起來，「我知道了……你先去忙吧！」

祕書告退後，蒼鼓起勇氣走進辦公室，一進到辦公室後，就看到棄天帝坐在自己的沙發椅上望向辦公室大片落地窗外。

「這窗外的風景很不錯！一眼望去無數的高樓大廈，沿伸至海防線際，晚上時肯定有美麗的夜景可觀賞吧！」棄天帝讚賞說。

看他今天來說了一堆廢話，不趕快把他趕出去，重要工作可是會延宕的，蒼決定開門見山：「有什麼事快說！不要浪費我的時間！」

棄天帝站起來，隨手拿着一袋紙袋地給蒼，笑着說：「你的衣服。」

蒼看了一眼，一手搶過去，直接無視棄天帝走到自己的辦公桌，「你可以走了。」

看他那麼的冷淡，棄天帝輕挑地說：「那麼冷淡啊……好歹我們之間的關係可是非常深啊！」他故意提點昨晚的事。

蒼不以爲意地回他：「如果沒錯的話，我是可以告你的！」

「告我什麼？妨礙自由嗎？你也只能告我這條，但能告成功嗎？」棄天帝雙手抵在辦公桌上，壓低身體靠近蒼。

蒼不躲也不閃的直視他，不畏懼他淫威。棄天帝看着他的雙眼，雙眼表露出憤怒的火光，那樣真是美……就像是第一次見到蒼時，他身上散發初一種光芒，讓人目不暇給。

<br/>

「好吧！我還是說出今天的來意後，馬上就走、不妨礙你辦公了。」棄天帝下一秒語出驚人。

這讓蒼頓時錯愕，這傢伙從不如此坦率，以前說話老是拖泥帶水、浪費他的時間，今天怎麼那麼快表明來意。

「快說吧！我沒時間……」蒼催促着。

「跟我交往吧！」棄天帝馬上說出來意。

蒼一聽，以爲他又在開玩笑，不以爲意的低頭看文件，「你要說的就是這件事。」

「我今天來也是爲了這件事。」棄天帝微笑着。

「那你可以走了。」蒼連頭都沒擡，直接下逐客令。

見他反應冷淡，棄天帝一手伸向蒼的下巴、使力讓他擡頭面向自己，一瞬間吻上。

蒼被突如其來的舉動嚇到，擡起手來想把棄天帝推開，無奈他的手扣住下顎太緊，一推他、捏住下顎的力道變加重，讓蒼無法推開。

<br/>

「嗚……嗚……嗯嗯……」被吻住的嘴不斷髮出鼻音。

棄天帝的舌頭靈巧地撬開蒼的嘴，入侵他的口腔、一下子捲住他的舌信，開始大肆交纏起來。

最後蒼忍不住，咬下棄天帝的下脣，迫使棄天帝放開自己。

「唉……你真粗暴！」棄天帝抱怨說着，但表情似乎很開心。

他摸着被蒼咬傷的下脣，還有血留在口腔中。

蒼厭惡地擦着嘴，並說：「如果這是你對待情人的方式的話，很抱歉……我無福消受！請你馬上出去，否則我只好請警衛了！」

棄天帝快步向前，一手抓住蒼的手、一手摟住他的腰壓制在自己懷中，「蒼……不管你怎麼拒絕我，我一定要得到你的！」

「你真的很煩！快放開我！」蒼奮力掙扎着。

<br/>

「不然這樣吧！我們來打個賭吧！看看一年之內，我能不能贏得你的心！」棄天帝最後提出賭注。

這賭注，昭顯棄天帝的自信，蒼看這男人的神情，一時之間是不會罷手，但也不想跟他浪費時間！

「這賭注聽起來真是不公平！」蒼淡淡地說。

棄天帝笑了起來，他再將臉靠得更進，「這世上不如意的事十之八九，有些事本來就不公平的，身爲商人的你早該知道，不是嗎？」

蒼不語，只是冷冷地看着棄天帝，也認爲他說的沒錯！最後他開口問：「那麼你輸的話，你是否從此不出現在我的面前、不再幹擾我的生活呢？」

蒼提出他的代價，棄天帝一聽，微眯他那雙金藍異瞳，思考一下後就回答：「這嘛……那好吧！我接受！」

「賭約何時開始？」

棄天帝壞心一笑，對蒼說：「就從現在開始。」語畢，他低頭吻住蒼，再次品嚐他口中的芳甜。

至於一年後，賭約在最後如何，也不可預知。

<br/>

完