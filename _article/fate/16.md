---
title: 天命
author: 終朝採藍
category: fate
layout: post
date: 2023-04-28 00:00:16
lang: zh-Hant
depth: 2
---

### 第16章

<br/>

他們在天波浩渺的崖邊心思迥異地站立了很久，後來也沈默了很久。在過去的那三年中也是如此，每次针锋相对的情事後，他們總會有這麼一段不算短也不算很長的停戰時光。

棄天帝很隨便地把蒼拉到身前，手臂環在他的腰間，帶來冰涼的觸感。蒼發現，棄天帝沒有再那麼刻意地與人類的身體保持距離了。

魔神若有若無的冰涼氣息撫過頭頂，蒼不禁再次打了個寒顫。棄天帝低笑一聲，若無其事地把玩起道者的淺色長髮來。

<br/>

堅強的貌似無懈可擊的先天道者，卻有著極其柔軟的長髮，有種似曾相識的溫柔觸感。

這樣的似曾相識感，似乎並非來自那三年，而是追溯到更久遠之前，植根於他的記憶深處。

為什麼？

他沒有答案，而此刻他也不屑於思考這個問題。

<br/>

「又在想什麼？如何贏吾嗎？」漫長的沈默過後，棄天帝問道。

「也許是如何在這三十天內殺你。」蒼若無其事道。

「這不算破壞賭約？」指尖繞過蒼柔軟的長髮，棄天帝玩味道，「人類似乎很喜歡雙標。」

「吾可不曾說過，不會殺你。」蒼淡漠道。

「殺吾嗎？哈。」棄天帝大笑幾聲，似乎全然不在意這種以卵擊石的挑釁。「⋯⋯吾凌駕六天之界，還未曾有神人能傷吾性命。」

「也許，你也不像你想像的那樣無懈可擊。」蒼偏過頭，倔強地掙開棄天帝玩弄頭髮的手，淡漠地望著懸崖下的驚濤拍岸，「盲目的自負將成為你的弱點。」

蒼忽然很想休息了。

可是人世沈淪，妖魔馳天，眾生依舊在苦海掙扎。肩負著除魔衛道的玄宗使命，擔下同修的遺願，「休息」二字，他沒有資格。

<br/>

似乎對蒼的掙脫掌控十分不滿，棄天帝扳起蒼的頭，居高臨下地逼他看著自己.

「倒是越來越有趣了。」瞥見蒼半踏出懸崖的腳尖，魔神嘲諷道，「哦？想跳下去？」

「魔物還未死，蒼自然不會先行一步。」蒼淡淡一笑。

「哈！」棄天帝情緒不明地大笑一聲，手上的力度越來越重，品味著蒼的眉頭緩緩蹙起。

「蒼，作為吾的奴隸，你，沒有決定自己生死的資格。」

「活得太久，是記性不好嗎？」蒼冷漠道，「賭局還未分勝負，吾不是任何人的附屬品。」

「⋯⋯哦？再三忘記自己的處境，需要吾提醒你嗎？」棄天帝一把扯過蒼的衣襟，袖一揚，毫不憐惜地將他扔在身後的空地上。「若你想再好好享受一次，直接說出來⋯⋯或許是更好的選擇。」

蒼被重重摔落在草地上，劇烈的動作再一次牽動了傷口，嘴角再一次滲出血來。

「又想強迫吾嗎？訴諸暴力，棄天帝，你好像也黔驢技窮了。」

掙扎幾下，蒼坐起身，淡漠地地對視著喜怒無常的墮天之神，眼神冰冷而無畏。

「想死嗎？你就是在這裡摔的粉身碎骨，吾依然能讓你生不如死地復生。」

「哈！」蒼擦去嘴角的血跡，「棄天帝，你真以為能主宰所有人的生死？」

棄天帝緩緩走上前，俯下身揪起蒼的長髮，強迫他看著自己。

「為何不能？絕對的力量，才能達成願望。」

喜怒無常的魔神似乎又恢復了暴虐的本質，彷彿方才那一瞬若有若無的溫柔不過是鏡花水月般的假象。

「那蒼祝願你，」蒼淒厲地微微一笑，「祝願你有一天，也會有主宰不了的生死⋯⋯」

想起玄宗諸位同修的血染戰場，來遲一步的無盡遺憾，無能為力的絕望感，讓蒼一時心痛到幾乎無法呼吸。

想到這裡，蒼一字一頓道：「蒼真心祝願你，總有一天，你也會有想救而救不了的人，好好體會那是一種怎樣無能為力的絕望滋味。⋯⋯哈！」

「想救而救不了的人？笑話。」棄天帝不屑道，「人，怎會值得神來拯救？況且，也不存在吾救不了的人。」

蒼只是淒厲地看著他，一言不發。

<br/>

翻手為雲覆手為雨的毀滅之神，眾生性命不過在他翻掌之間。

極端的力量伴隨著極端冰冷無常的性情，真算得上是天地的災難。

<br/>

道者眼眸中那種深可見骨的哀慟卻讓棄天帝不覺心中一悶，不覺鬆開手，轉過身去。「罷了。」

蒼的身體此刻似乎不能承受更多的負荷，他也算興致缺缺，就不如放這個不知進退的人類一次。

「棄天帝，吾要安葬赭杉軍。」明知此刻閉嘴是最好的選擇，但蒼不甘示弱的聲音再一次在背後響起。「他的軀體就在天魔池裏，吾看到了。」

「閉嘴，⋯⋯蒼，你還真是沒完沒了。」棄天帝不耐煩地雙手背在身後。對於惹他心煩，蒼總是不遺餘力。這個愚蠢的道士就不能少說兩句？

「是誰先挑起的話題？是吾嗎？」

「⋯⋯」

「赭杉軍有如今的境地，又有誰的功勞？」

「哈哈哈⋯⋯怎麼，蒼，你是在指責吾嗎？」

「他不能留在你那污穢的血池裏。」

「污穢⋯⋯？哈哈哈！」

「吾要帶他回混沌岩池。」蒼堅持道，「吾要一日的時間安葬赭杉，你不能打擾。怎麼，這你也要為難？」

「隨便你。」棄天帝不耐煩地擺擺手。對人間繁瑣的儀式，他毫無興趣。「但一日太多了。半日寬限，不能更多。」

「⋯⋯不行，半日不夠。」

「作為吾的奴隸，你，有得選嗎？⋯⋯再頂嘴，半天也沒有。」

「⋯⋯」蒼無奈地閉了嘴。雖然半日太緊迫，但也算是⋯⋯聊勝於無嗎。

「還有，時限一到，吾自會回到你的魔界廢墟。混沌岩池不歡迎你，你不能去。」蒼補充道。

「你又想背著吾搞什麼把戲？」棄天帝似乎覺得有些好笑。「蒼，你當吾傻嗎？」

「吾的賭局，你的遊戲啊。」蒼面無表情，「多留點餘地才有趣，不是嗎。」

「隨便你吧。」棄天帝不耐煩地擺擺手。「珍惜吾今日對你的寬容。」

<br/>

***

<br/>

混沌岩池如今立起了四座墳塋。玄宗四奇同葬此地。

试上高峰窥皓月，偶开天眼觑红尘。可怜身是眼中人。

斯人已去，是非恩怨也轉頭皆空。赭杉軍與墨塵音選擇了寬恕，蒼雖尊重他們的選擇，但始終不能全然釋懷，因為一己私利而傷害諸多人命的做法。

他想起不久前和棄天帝的對談。後者的看法雖然極端，但也並非全無道理。三教在有心人手中不過是一種工具，約束人心與欺騙人心，也許只是一念之間的差別。

<br/>

趁這段時間，蒼用銀鴒給劍子傳遞了消息，希望他儘快與三先天其餘的兩位匯合。至少三十天之內，三教頂峰是安全的。而自己一旦失敗，最後一戰很快會到來，而他們是人間最後的一線希望。

至於赭杉軍與墨塵音，還是儘快安置好他們的魂魄，送他們一程，去往那個無苦無悲的極樂世界。

而他自己⋯⋯大概會在不久後的某一日，滯留在久遠前的過去，孤獨的死去。

孤獨像是一種逃不開的詛咒。人世流連一趟，眾生都是這般苦。

<br/>

「墨塵音，赭杉的魂魄交給吾吧。」在混沌岩池外設下乾坤法陣後，蒼掏出那只小小的聚魂瓶。玄宗的法寶之一，用上好的白瓷燒製而成，素白的瓶身沒有一絲瑕疵。

「只能一賭了。」

「絃首是要回到過去修補赭杉的魂魄嗎？」墨塵音睜大眼睛，「絃首找到能修補魂魄的人了？」

「哈，大概吧⋯⋯」蒼苦笑一聲。「沒有別的辦法。」

他打開瓶口，一縷幽幽殘魂被吸入瓶中。

「但是為什麼絃首看起來似乎很愧疚⋯⋯」墨塵衣遲疑道，「雖然你隱藏的很好，但吾能看出來。」

「愧疚嗎？」蒼平靜地閉上眼睛。「他也算是罪魁禍首，吾不需要愧疚。」

雖然這樣說，蒼還是難以遏止地有了一絲歉意。這算是一種利用嗎？

「罪魁禍首⋯⋯？」

「嗯。」蒼緊閉著雙眼，淡淡地點點頭。「若能修好赭杉的魂魄，等吾帶他回來，你們不能再流連人世了。拖的越久，遺忘的就會越多。說到這裡，墨塵音，你有發覺自己遺忘過什麼嗎？」

墨塵音仔細回想了一陣。「似乎⋯⋯沒有。」

「⋯⋯但願如此。」蒼把聚魂瓶小心地收進懷裡，「對了，這次可能要拜託你，每隔一刻鐘向陣中傳音，告知吾過去了多久。開啟術法後的時間尺度會產生變化，時間的流逝似乎要慢上許多，吾因此失去了準確的時間概念。」

「嗯，吾記住了。」

<br/>

將金色的頭飾扔向空中，蒼隨即低喝一聲，催動術法。「伏天王，降天一，玄天陰陽化百氣——」

蒼回想起在棄天帝記憶中感受到的那個滿目暗紅的場景，還有探向胸口時那空蕩蕩的心臟位置。那應當是關鍵的事件之一。

如果回想起某個場景，就能到達相同的位置，那麼首先需要了解，這個關鍵時刻發生了什麼。

<br/>

「啊⋯⋯噗⋯⋯」

道者屏氣凝神，回想著那片暗紅的景象。身體迅速落入那個失重空間後，他又被突然拋擲出來。

望著蒼的身形驟然消失，又突然出現，重重地落在地上，墨塵音擔憂地提高了聲音。「絃首？怎麼回事？」

胸骨盡斷的內傷與下體撕裂的傷痛在摔落之下登時齊齊迸裂，蒼痛得「哇」地一聲吐出一大口鮮血。

「無妨⋯⋯失誤而已。」蒼擦去嘴角血跡，「不必擔憂。」

——看來需要具體的場景才能回到相應的地點。滿目暗紅，沒有具體的景物，無法催動空間術法。

也罷。蒼默默地想，既然如此，還是回到祭天大典分別的那一刻。

畢竟，他還欠武神一個解釋。

<br/>

「伏天王，降天一。玄天陰陽化百氣——」

「絃首，唉⋯⋯」墨塵音嘆了口氣，不及補充幾句，奪目的白光已騰空而起。金色的額鏈飛速地空中旋轉著，蒼已經瞬間不見人影。

<br/>

\-\-\-\-\-

這個「想救也救不了的人」是個巨大的flag

**警告**：前期的武神是黑心肝作者腦補的一個還沒黑（指變成黑色）的老棄，會和後面的老棄有非常大的不同。他會慢慢向老棄的性格轉化。

這篇設定裡，這個時候的他跌入紅塵，不知道自己是天神，是有「人性」的。

雖然吧他性格還是不太好相處，還是個有點傻的，但他沒太跟人類過不去，就是有點孤單。

然後蒼是他的光～～ 在恢復記憶前，這段人世之旅他是真的對蒼動了情的。

如果能接受這一點，再往下看。
