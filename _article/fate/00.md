---
title: 天命
author: 終朝採藍
category: fate
date: 2023-04-27
layout: post
lang: zh-Hant
depth: 1
subdocs:
  - link: /fate/0/
    title: 序章
  - link: /fate/1/
    title: 第1章
  - link: /fate/2/
    title: 第2章
  - link: /fate/3/
    title: 第3章
  - link: /fate/4/
    title: 第4章
  - link: /fate/5/
    title: 第5章
  - link: /fate/6/
    title: 第6章
  - link: /fate/7/
    title: 第7章
  - link: /fate/8/
    title: 第8章
  - link: /fate/9/
    title: 第9章
  - link: /fate/10/
    title: 第10章
  - link: /fate/11/
    title: 第11章
  - link: /fate/12/
    title: 第12章
  - link: /fate/13/
    title: 第13章
  - link: /fate/14/
    title: 第14章
  - link: /fate/15/
    title: 第15章
  - link: /fate/16/
    title: 第16章
  - link: /fate/17/
    title: 第17章
  - link: /fate/18/
    title: 第18章
  - link: /fate/19-0/
    title: 番外  逛窯子
  - link: /fate/19/
    title: 第19章
  - link: /fate/20/
    title: 第20章
  - link: /fate/21/
    title: 第21章
  - link: /fate/22/
    title: 第22章
  - link: /fate/23/
    title: 第23章
  - link: /fate/24-0/
    title: 番外  生悶氣
  - link: /fate/24/
    title: 第24章
  - link: /fate/25/
    title: 第25章
  - link: /fate/26/
    title: 第26章
  - link: /fate/27-0/
    title: 番外  致幻蘑菇
  - link: /fate/27/
    title: 第27章
  - link: /fate/28/
    title: 第28章
  - link: /fate/29/
    title: 第29章
  - link: /fate/30/
    title: 第30章
  - link: /fate/31/
    title: 第31章
  - link: /fate/32/
    title: 第32章
  - link: /fate/33/
    title: 第33章
  - link: /fate/34/
    title: 第34章
  - link: /fate/35/
    title: 第35章
  - link: /fate/36-0/
    title: 番外  姻緣樹
  - link: /fate/36/
    title: 第36章
  - link: /fate/37/
    title: 第37章
  - link: /fate/38/
    title: 第38章
  - link: /fate/39/
    title: 第39章
  - link: /fate/40/
    title: 第40章
  - link: /fate/41/
    title: 第41章
  - link: /fate/42-0/
    title: 棄蒼相性一百問（武神篇）上
  - link: /fate/42-1/
    title: 棄蒼相性一百問（武神篇）下
  - link: /fate/42-2/
    title: 棄蒼相性一百問（棄總篇）上
  - link: /fate/42-3/
    title: 棄蒼相性一百問（棄總篇）下
  - link: /fate/42/
    title: 第42章
  - link: /fate/43/
    title: 第43章
  - link: /fate/44/
    title: 第44章
  - link: /fate/45/
    title: 第45章
  - link: /fate/46/
    title: 第46章
  - link: /fate/47/
    title: 第47章
  - link: /fate/48/
    title: 第48章
  - link: /fate/49/
    title: 第49章
  - link: /fate/50/
    title: 第50章
  - link: /fate/51/
    title: 第51章
  - link: /fate/52-0/
    title: 番外  話本
  - link: /fate/52/
    title: 第52章
  - link: /fate/53/
    title: 第53章
  - link: /fate/54/
    title: 第54章
  - link: /fate/55/
    title: 第55章
  - link: /fate/56/
    title: 第56章
  - link: /fate/57/
    title: 第57章
  - link: /fate/58/
    title: 第58章
  - link: /fate/59/
    title: 第59章
  - link: /fate/60/
    title: 第60章
  - link: /fate/61/
    title: 第61章
  - link: /fate/62/
    title: 第62章
  - link: /fate/63-0/
    title: 番外  情動
  - link: /fate/63/
    title: 第63章
  - link: /fate/64/
    title: 第64章
  - link: /fate/65/
    title: 第65章
  - link: /fate/66/
    title: 第66章
  - link: /fate/67/
    title: 第67章
  - link: /fate/68/
    title: 第68章
  - link: /fate/69/
    title: 第69章
  - link: /fate/70-0/
    title: 番外  黑貓奇談
  - link: /fate/70/
    title: 第70章  尾聲
  - link: /fate/71/
    title: 後記&覆盤
  - link: /fate/72/
    title: 隱藏世界線相關釋疑
  - link: /fate/73/
    title: 番外  道士和親
---

#### 作者：終朝採藍

> ##### 打雷啦
>
> 本文有大量R18元素，很黃很暴力而且OOC，如有不適請立刻退出
{: .block-danger }

##### 本文設定

1. 背景是磐隱神宮最後一根神柱沒守住，棄天再臨。所以接的是磐隱神宮最終戰那一集，後面就和劇情走向不一樣了，其他設定儘量和原劇一致

2. OOC警告：本文對遠古時期的棄總有大量描繪。最早的他如同一張白紙，還沒有生出對人類的厭惡，甚至對人世有點好奇。在這段過程裡，他因蒼而知愛憎別離，是真的體會過人的情感的。能接受這一點再往下看。

3. 蒼對棄總不會有「LOVE」，到最後也一點都不會有。我覺得蒼是有大愛但沒有私情的人。

4. 有非常少的赭墨/龍劍，偏友情向。

5. 這就是個爛俗小黃文，不是宏大的道魔之戰群像，請不要有過多期待><

6. 有強制愛，虐蒼虐得也比較狠，棄蒼由於力量不對等以及理念衝突，他們之間會非常坎坷。

7. 武力值設定上，棄總是天花板。請尊重本文二設，不要跟某藍爭論這一點，謝謝>< （因為連載的時候被杠了）

---
