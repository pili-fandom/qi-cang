---
title: 天命
author: 終朝採藍
category: fate
layout: post
date: 2023-04-28 00:00:17
lang: zh-Hant
depth: 2
---

### 第17章

<br/>

百年時光匆匆而過。

自從人間有了四時更替，秋收冬藏，人世間的物產也日漸豐饒。從村落到城邦，國家的雛形就這樣在人類文明的長河中悄然誕生。

人世流傳，大荒山下有一肅慎之國，而大荒山中有一山神，以山野為鄰，以溪流為伴，為人間斬禍除惡。人們稱他「白衣武神」。

人間每年祭天的傳統還是保留了下來。據說，「白衣武神」每年都會在祭天大典快結束時出現一次，站在大荒之山腳下，面色陰沉地望著遠方。天亮之前，他的身影又會悄無聲息地沒入山峰高聳入雲的輪廓之中。

見過他的人說，白衣武神的手腕上，好像總是繞著一條紫色的紗帶。莊嚴的白衣中帶了一點肅穆的紫，而這位不食人間煙火的「天神」，似乎也同以前有幾分不同了。

至於有哪些不同，人們也說不上來。

<br/>

<br/>

回想著分別的場景，蒼再一次回到了相同的位置。

他再一次降落在黑夜將盡、人潮開始散去的破曉時分。看到不遠處的大荒之山落寞孤寂的輪廓，天邊一輪疏月高懸，蒼這才隱隱放下心來。

依天象判斷，這次回來的時辰距他上次離去並不算太遠，應當不會超過兩個時辰。或許武神依舊在前方的山路上，還未走遠。

胸骨盡斷帶來鮮明的痛楚。蒼站穩腳跟，竭力壓抑住傷勢，正準備月下疾疾而行。然而他抬起頭，一道白衣似雪的落寞身影赫然映入眼簾——

白衣武神就站在不遠處的山腳下，金藍異瞳陰沉晦暗地遙望著他，腕上系著一條紫色袖帶，在清晨微涼的風中翻飛如織。

蒼心中一驚，頓覺幾分歉意。自他離開後，武神一直站在那裡等他回來嗎？

四目相對中，武神的神情中有幾分蒼看不懂的東西，讓蒼一時不知如何言語。

零星的人潮在道者身邊穿梭而過。蒼撥開行人，向那一襲白衣的高大身影走去。武神只是沉著臉，緘默地望著他緩緩走近。蒼愈加迷惘了。

「抱歉，吾⋯⋯」

武神沈默地打量了道者幾眼，異色雙瞳似乎染上一絲山雨欲來的怒意，隨即一言不發地轉身離去。

「等等！⋯⋯」蒼快步追上，心中更加疑惑了。「抱歉，方才有事不得已離開——哇——噗⋯⋯」

正要追上白衣身影的一剎那，突然衝出強大的氣勁把道者掀出了十幾丈的距離。本是重傷的道者毫無防備，突襲而來的劇烈痛楚讓他眼前一黑。

在空中翻滾了幾圈，蒼只能用眼角的餘光勉強捕捉到一個怒氣騰騰的模糊背影，狂大的氣流掀起他的衣襬，盛怒般烈烈作響。蒼一時難忍傷勢，哇地一聲再次嘔出一大口鮮血。

⋯⋯這位大神又在發什麼瘋？

<br/>

落地的瞬間，蒼做好了落地時內傷再次爆發的準備，正要強撐著運功，卻猝不及防地穩落在一個有力的臂彎裏。

「你受傷了。誰傷你？」

熟悉的低沈聲音再次響起，不容置疑的語氣不是詢問而是在陳述一個事實。冷酷之中，卻有一絲若有若無的溫柔。

「無妨。」蒼推了推他，試圖掙脫那條手臂的束縛，掙扎著站起身來。

「不準推開吾。」低沈的聲線緩緩命令道，壓抑不住的怒氣幾乎沸騰。武神隨即霸道地把道者抱在了懷裡。「說！誰傷你。」

「⋯⋯」蒼一時語塞。

「不肯說，是不相信吾之能為嗎？」異色雙瞳危險地瞇了起來。

「不是這個世界的人⋯⋯」蒼還是掙扎著撲騰了幾下，試圖脫出那個懷抱，「無妨，吾能自己解決。」

「不准推開吾，要吾說第二遍嗎！」武神有些惱怒道，隨即手掌覆上蒼的胸腔。

一陣澎湃的力量頓時貫穿四肢百骸，蒼感覺到傷勢在迅速的癒合，熾熱的手掌帶來的暖意也讓痛感減輕了許多。「謝了。」

蒼隨即放棄了掙扎。沒有必要做無謂的抵抗——這並非與這尊大神相處的最好方式。

「為什麼你會受傷？那個世界，你不是說吾也在嗎？」武神語氣一頓，冷冷道，「吾為什麼會放任你受傷？」

「⋯⋯這」

「怎麼不說了？！」

蒼苦笑一聲。「哈，情況複雜，你暫時不在。」

——雖然某種意義上講，這的確是實話。

他不是他，他已經不是他了。

「既然吾不在，為什麼你不去找吾？！」

「⋯⋯」

「吾絕不會放任你受傷。再有下次，到吾的身邊去，聽到沒有？！」

「唔⋯⋯」

「聽到沒有！？」

「唔，好吧。」蒼乖乖地點了點頭。

「哼。」武神不滿地重重一哼，隨後冷冷地逼問道，「還有哪裡受傷了？」

「沒有了。」蒼閉上眼睛。他可不想被發現下身的撕裂傷——不然這位大神指不定又會做出什麼異於常人的舉動來。

武神懷疑地掃視著道者周身。「真的沒有了？」

他的手掌突然覆上蒼的眉頭。沈重的倦意突然襲來，蒼心中一驚，掙扎了幾下努力保持清醒。「做什麼？吾沒⋯⋯」

「閉眼。」武神命令道，隨即手掌順著朱砂流紋蜿蜒的方向緩緩而下，覆上他的雙眼。

「不行⋯⋯」蒼依舊掙扎著。現在並非休息的時候⋯⋯

連日來終究是過於疲憊，蒼陷在黑暗裡，終究沒能抵擋住濃重的倦意，沈沈睡去。

「嗯？怎麼這麼能撐⋯⋯」武神納悶地咕噥一聲，扣住道者的腰，把他牢牢地抱在懷裡，隨後向山裡走去。

<br/>

***

<br/>

蒼又陷入了一個如真似幻的夢境裡。荒涼大漠，殘陽泣血，面前是似曾相識的黑色身影，如瀑布般流瀉的黑髮此時在風中張揚地狂舞著，發梢似乎也沾了些鮮紅的血跡。

「閉上⋯⋯眼睛⋯⋯」

蒼聽到自己的聲音。困在悲傷的夢境裡，他隱約感覺到自己對著面前的身影釋出一個很淺的笑意。

他累了。

身上的衣袍似乎被鮮血染透了，視野越來愈模糊，像是隔著水霧，什麼都看不真切了。淚水滲進嘴角，又鹹又苦的味道。夢中的他疲倦地閉上了雙眼——

他終於可以不再壓抑自己，痛快地流淚了。

為什麼？他在為什麼而悲傷？

下一瞬，蒼猛然發覺自己的手掌貫穿了面前身影的胸膛。此刻，整拳沒入在那空蕩蕩的胸腔裡，而他手心中，似乎還緊緊攥著什麼。

蒼心中一驚。被自己的動作嚇了一跳，蒼顫抖著抽回手，然而手心中除了殷紅的鮮血，空無一物。

發生了什麼？他在做什麼？

「道門欠你的⋯⋯吾還你⋯⋯」

蒼聽到自己氣若游絲的聲音。

「人類欠你的⋯⋯吾還你⋯⋯」

生命在快速地流逝，蒼已經無力辨別，夢中的自己還說了些什麼。

「蒼⋯⋯蒼⋯⋯」

天際傳來遙遠的呼喚。是誰在喚他？

「下一次相遇⋯⋯吾會救你⋯⋯」

蒼聽見自己斷斷續續的氣音。他用盡最後的力氣，對面前的身影釋出一個最後的笑意。

「蒼⋯⋯蒼！」

「哈哈哈⋯⋯」面前的身影爆發出一串低沈淒涼的笑聲。「哈哈哈，哈哈哈哈⋯⋯哈哈哈哈哈哈⋯⋯」

他是誰？是棄天帝嗎？蒼昏昏沈沈地想，意識漸漸在沈重的疼倦意中消散⋯⋯

他真的累了。

「哈哈哈哈⋯⋯哈哈哈哈哈哈⋯⋯」

低沈淒厲的狂笑響徹茫茫天地，大地應聲而裂，滾滾黃沙四起，在山脈異變中悽愴地悲鳴。

「⋯⋯哈哈哈⋯⋯哈哈哈哈哈哈⋯⋯哈哈哈哈哈哈哈⋯⋯」

「蒼⋯⋯蒼！蒼！！」喚他的聲音越來越大。「蒼！」

<br/>

——蒼猛地驚醒。

此刻他正躺在一處山洞裡，身下鋪墊了許多厚厚的柔軟稻草，像是剛烘乾似的，還殘留著火焰炙烤過後的一絲餘溫。

武神正站在他身側，俯著身，微蹙著眉頭在喚他。

「你怎麼了？你好像夢到了什麼。還流淚了。」他指尖拂走蒼眼角的水光，金藍異瞳有些瞋怒。「怎麼回事？」

「無妨⋯⋯」

「說！」

「吾⋯⋯」蒼本能地躲開了他的手，掙扎著爬起身來。「惡夢而已。」

「⋯⋯你又躲開吾。」武神不滿道。

「這裡是⋯⋯你住的地方？」環顧四週，蒼問道。

蒼不記得上次來過這個山洞。武神似乎也極少休息，只有在極度運功之後才會靠著樹幹閉眼小憩片刻。

「嗯？住的地方？」

「嗯⋯⋯就是日常起居所在。」

「⋯⋯日常起居？」

「哈，沒什麼。」

「吾不需要這個。方才開出的山洞而已，你，需要休息。」武神簡潔地命令道。

「⋯⋯」蒼一時語塞。

他因此開了一個山洞嗎？石床的邊緣，刀刻斧鑿般平整，那嶄新的劃痕，看來也是新開鑿的痕跡。

本是別有目的而來，蒼一時不知該如何回應這樣純粹而笨拙的好意。「你⋯⋯不必如此。」

武神聽了他的話，皺了皺眉頭。

「⋯⋯謝了。」蒼話鋒一轉，輕聲道謝。

「嗯。」武神輕輕嗯了一聲。眼中的溫柔散去，金藍異瞳寒芒一閃，又變得有些傲慢起來。他背手轉過身，留給道者一個心思難辨的背影，不知在想些什麼。

「⋯⋯吾離開了多久？」蒼問道。

「哼！」武神不滿地重重一哼，轉過頭來有些惱怒地望著蒼。「你真不知道？」

「抱歉，吾確實不知。」蒼歉意道。

「哈哈哈！很好！⋯⋯那就不必知道了！」

「⋯⋯抱歉。」蒼聲音漸漸低了下去。他開始有了一種不安的預感。「吾真的不知。」

「哼！」

「吾到底離開了多久？」

「一百年！」

「⋯⋯」波瀾不驚的藍紫色雙眸猛地一顫。

「⋯⋯百年過去，你還回來做什麼？！」

「抱歉⋯⋯」

「抱歉兩字，吾不想聽。」武神回過頭來，金藍異瞳神色複雜。

「吾⋯⋯」

「夠了！⋯⋯過去的事，吾，可以既往不咎。這次回來，吾不准，你就不許走。聽清楚沒有？！」

「⋯⋯抱歉，這個，恕蒼目前做不到⋯⋯」蒼抱歉地對著武神笑了笑，望著異色雙瞳漸漸變冷，急忙補充。「但⋯⋯總有一天，吾會不走了，留在這個世界⋯⋯陪你。這個承諾，可以嗎？」

「總有一天？⋯⋯總有一天是什麼時候？」

「也許，不需要太久。」蒼勉強露出一個淡淡的笑意。

武神雙手背在身後，傲慢地轉過身去。「⋯⋯隨便你，吾，無所謂。你留與不留，你以為吾會在意嗎？」

「⋯⋯」

「哼。」武神冷哼一聲，慵懶地擺擺手。「罷了，過去的事，吾不追究。下次記得，吾准許之後，你才能走。」

「吾儘量。」蒼很淡地笑了笑。他坐起身，雙腳踏在山洞的地面上，緩緩站起身來。再生之力貫穿經脈，傷勢都已癒合，劇痛也散去了，只是仍稍感疲憊。

「走下來做什麼？回去。你需要休息。」

「無妨。」蒼淡淡道。現在不是休息的時候。

「回去。」依舊是命令般的低沈語氣，異色雙瞳帶著不容抗拒的神之威儀，狠狠地瞪了道者一眼。「聽不懂吾的話？！」

對視中，蒼堅持了幾秒，敗下陣來，只好又坐回了厚厚的稻草上。

「這些日子，你一直在山⋯⋯」

話音未落，蒼猛地一頓。他才意識到一個問題——

每年祭天大典，武神難道都會去當初分別的地點⋯⋯等他歸來嗎？

所以才會在山下相同的地點遇到他。

蒼茫然地收攏五指。

<br/>

蒼知道或許自己對於武神是特別的，也因此總是小心地避免承受他更多的好意。他還不起，甚至⋯⋯也不能還。

但這種「特別」，也不過是源於一種後者流落人世的短暫陪伴，在後者無窮無盡的生命裡，簡直渺小得不值一提。

他們的命運注定只是短暫地交匯，他有一天會孤獨地在這個世界死去，而武神會回歸六天之界開啟他漫長而無感無情的真正宿命，日復一日的俯瞰人間，不會再記得他。

——直到再次相遇。

而那時，他是自降為魔的棄天帝，而自己是六絃之首，他們之間會橫亙著萬千人命，血海深仇。

除非能改變什麼。可是能否真的改變什麼？

想到這裡，蒼有些痛苦地閉上眼睛。

<br/>

「在想什麼？」武神走上前來，不由分說地掰開道者攥緊的手掌。「這麼用力做什麼？以後不許了。」

蒼柔軟的淺色長髮流瀉在肩頭，撩撥似地划過他的指尖。他不由得在發梢纏繞了幾圈。這種柔軟順從的質地，雖然和蒼的氣質截然不同，卻讓他莫名很喜歡。

「你還是適合紫色。黑色不適合你。」金藍異瞳瞇了起來，打量著道者身上的紫色道袍。「你這樣穿，好看。」

「⋯⋯哈。」

拋卻了身為天神的記憶，這位大神竟也有單純直率的一面⋯⋯

看到蒼的袖口不知為何多了很多條紫色的袖帶。武神索性扯過蒼的雙手，繼而勾起幾條袖帶，有些好奇地把玩起來。「怎麼多了好幾條？」

「⋯⋯」

「不錯，吾中意。」武神品味著。

「⋯⋯」蒼一臉無奈地半伸著雙臂，任由他把玩著袖口散開的袖帶。儘管他與棄天帝那麼不同，無意間流露出的一絲孩子氣卻極為相似。

⋯⋯現在開口請他修復赭杉的魂魄，會是合適的時機嗎？

一番權衡之後，蒼正要開口。武神此刻卻突然放下了道者的雙袖，站起身來。

「你，好好休息。不許亂跑。」

「你去做什麼？」蒼問道，再一次站起身來。「也許，吾可以幫⋯⋯」

「回去！又要吾說第三遍。」一聲低喝打斷了蒼的話音，「聽不懂吾的話嗎。吾，不准。」

「你要做什麼，讓吾助你。吾傷勢已復。」

「不行！」武神冷冰冰地打斷。然而當他轉過頭來，微微低頭注視著道者時，此刻眉眼之中又凝上了一絲最初那若有若無的低頭的溫柔。

「你要去做什麼？」蒼問。

「你好好休息。」

「還剩下一道濁氣，對嗎？」

「⋯⋯唔。」武神不滿地嘀咕，「你很會猜嘛。」

<br/>

蒼知道那是『眾生濁』，也叫『有情濁』，貪嗔痴恨愛惡慾，六欲七情八苦，人類一切惡念的源頭，執著的源頭，也是五濁惡氣中最強大的一道。百年前，武神驅除另外四道濁氣，只用了不到半年的時間。如今百年匆匆而過，而這五濁惡氣的最後一道竟然還在人世。

想到這裡，蒼有些擔憂：「眾生濁，是很難驅除嗎？」

「哼，有何難。無非就是多花些時間而已⋯⋯」

「讓吾助你。」蒼堅持道。

「吾，需要你的幫助嗎？」武神再一次背手轉身，倨傲地說。「而且，⋯⋯這種污穢的氣息，不需要你來沾。」

蒼應該是乾乾淨淨的，他不需要接觸到這些污穢的東西。

<br/>

「⋯⋯」蒼聞言一怔，這種質樸得甚至有些笨拙的好意，讓他再次陷入了沈默。

「讓吾助你。」蒼的聲音很輕，卻清晰可聞。「這是吾的堅持。」

「有完沒完！」武神氣呼呼地轉過頭，金藍異瞳瞪了道者一眼。「真是固執得愚蠢！」

「你不也是？」蒼淡淡地問。

「⋯⋯？！」

「你若不同意，吾就打爛你這座山，然後一聲不響地消失，而且下次不會再出現在你面前。」蒼不急不慢道。

「⋯⋯沒完沒了！」武神悻悻道，最終還是柔軟下來。「真拿你沒辦法。」

「那就是同意了？」蒼微微一笑。

「哼⋯⋯勉強算吧。」武神氣鼓鼓地一把將道者攔腰撈起，抱在胸前，「走吧。」

「⋯⋯吾自己能走。」蒼無奈道。

「你有傷。吾不准。」

「哈。」蒼不禁莞爾一笑。拋卻了冷血的性格，這樣的固執竟有幾分孩子氣的可愛。邊想著，道者一邊倔強地試圖掙脫那個懷抱。

「吾之前的確有傷，但是已經恢復。你的好意吾心領會，但是這種保護對吾實屬多餘。」

「你怎麼這麼不乖！⋯⋯」

蒼依舊在他懷抱裡掙扎著，一人一神滑稽地扭打在一起。武神堅持了一會，最終還是悻悻地松了力度，放任蒼站在地上。他依然扣著道者的後腰，像是有些不想放手。

「⋯⋯走吧。」蒼對著他笑了笑。

「哼，隨你了。」金藍異瞳悶悶不樂地打量著道者周身，像是確認蒼所言「傷勢恢復」是否屬實。

他知道蒼是少有的強者，能替他接引風雷的世間又有幾人。強大堅韌如蒼，的確不需要強加的好意。

可是這個人有時候像一陣轉瞬即逝的風，匆匆而來，又匆匆而去，蜻蜓點水般擾亂了他的生活，卻又一聲不響的離他而去。

他對於蒼的世界一無所知，而蒼也始終不願意談起，始終小心謹慎地與他保持著距離。

這個人如果不緊緊抱在懷裡，好像就倏忽會化作一只青鳥，隨來時的風輕飄而去，再也留不住了。

上次放蒼離開，再回首是百年身。下次呢？會不會是千萬年？

僵持了一會，武神最後還是鬆開了手，放任蒼站在了一側，低聲黯然道：「隨便你。走吧。」

「嗯。」

白紫相間的兩道莊重沈穩的身影最終還是並肩而行，緩緩走出幽暗的山洞，踏入這廣袤天地、萬丈紅塵之中。

那是千萬年前他們的起點。

也會是千萬年後他們的歸途。

<br/>

> ##### 小劇場
>
> 武神貓貓：（站在山下，擦眼抹淚）蔥花寶貝來了沒有⋯⋯嗚嗚嗚⋯⋯
>
> 某藍：還得再等等
>
> 武神貓貓：（貓爪亂撓）嗚嗚嗚，本貓的蔥花寶貝就這樣不見了⋯⋯
>
> 棄貓貓：（皺眉）聽說有人要搶本貓的蔥？！想得美！本貓打斷他的腿！
{: .block-tip}
