---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:05:41
lang: zh-Hant
depth: 2
---

### 第41章

<br/>

「⋯⋯為什麼不要？就這麼不想同吾交歡，蒼？」輕咬吸吮著道者胸前的紅纓，武神低聲地逼問。

面前的人類已經是第幾次在他手中失魂落魄地高潮了？面頰被情慾染成緋色，清冷的雙眸中泛起瑩晶潤水光，早已無力掙扎，癱倒在他的臂彎裡。即便如此，那抗拒隱忍的神情始終不減，自始至終遙不可及，勾起他的怒火愈燃愈烈。

「就這麼抗拒吾嗎，蒼？」

在夢魘中回過神來，蒼依舊低低地喘息著。

蒼沒有再默念清心訣。放任自己在武神懷抱裡低低地呻吟著，被情慾的浪潮推上高峰，這樣的感覺，他並不適應。

事實上，蒼已經射不出任何東西了。靠在他的肩頭，蒼輕輕地搖了搖頭，「抱歉。吾還不太習慣這樣⋯⋯」

「夠了？」武神扳起道者低垂的頭，強迫他看向自己，狠狠道，「那，換吾了！」

燭火搖曳中一眼對視，那雙金藍異瞳格外地剔透，怒意後像是有某種痛苦的底色，隨著倒映在眼中的火苗一起哀傷地躍動著。

「要吾⋯⋯怎樣做？」蒼輕聲地問，「怎樣做⋯⋯能讓你好受些？」

溫暖的手掌將道者的頭緩緩按下去，按在胯間的硬物上。低沈沙啞的聲線緩緩命令道，「含進去。」

蒼毫不猶豫地照做了。

他承了武神的情，又間接將武神推向了這樣的境遇，兩百年的虧欠，日復一日的空候，蒼著實不知道自己還能補償武神些什麼。

<br/>

雙手仍然被束縛在背後，俯身的動作顯得有些笨拙。武神攔著他的腰，把他的上身輕輕托住。「⋯⋯別用牙。」

「嗯⋯⋯」

緩緩將灼熱的硬刃吞進口中，蒼含糊地回應道。硬物直抵喉嚨深處，險些讓他再一次乾嘔起來。武神冷笑一聲，死死地將他的頭按在胯間。

道者隱忍的神情隱沒在黑暗裡，柔軟的淺色長髮流瀉開來，披散在武神的胯間，幽幽燭光中閃著綢緞般的光澤。牆上的人影交疊晃動，有種鏡花水月般的親暱假象。

強抑住本能的抗拒，蒼閉上眼睛，試探性地舔了舔口中堅硬的異物。

「⋯⋯還是不會？」

「唔⋯⋯吾⋯⋯試⋯⋯」

「夠了！」武神不耐煩地說，指尖插進道者的發梢，開始自顧地猛烈抽送起來。

「唔⋯⋯」

面對突如其來的攻勢，道者毫無防備，喉嚨間發出咕噥的響聲。

那種語氣⋯⋯是痛苦嗎？蒼茫然地想。之前的武神不曾有過，後來的棄天帝更不曾。也許兩百年孤獨太漫長，長到無處消磨，長到任何健全的心智都會被孤獨帶來的痛苦侵襲到發瘋。

不記得被抽插了多少下，雙腮只覺麻木的酸痛。咸腥的濃稠液體激射而出，全數澆灌在道者的喉嚨深處。蒼再也忍不住，掙扎著脫出按著他的手掌，吐出口中的硬物，趴在榻邊不住地幹嘔起來。

黏稠的白色液體，沿著道者精緻的下顎輪廓緩緩滴落，在空中連成絲線，泛著晶亮的光澤。

方才射在喉嚨中的都吐完了，但蒼仍止不住地乾嘔，眼角瞥見武神坐在榻上神色晦暗地望著，一言不發，沒有阻止，也沒有上前。空氣中像是有某種壓抑的情緒在瘋狂地翻捲。

待乾嘔終於平息，一隻有力的手臂將道者攔腰勾了起來。背後溫暖的胸膛緊緊相貼，武神靠在他耳邊很輕地說，「就這麼⋯⋯厭惡吾的東西？」

蒼望著地上的一片狼藉，撲騰了幾下。「不是⋯⋯這樣。」

「就這麼⋯⋯不想要⋯⋯吾的東西？」

耳畔擦過溫熱而危險的氣息，像是廝磨，更像是暴風雨的前奏。

輕柔而痛苦的語調之後，蒼被粗暴地摔在了榻上，雖然厚厚的稻草鋪墊，並沒有讓他覺得疼。

武神並沒有像過去那樣，把他翻過身去後入他，而是正面欺了上來，手指撫去他下巴上的白色污濁，塗抹在他的後庭上。道者兩條修長的雙腿被強制拉開，隨後被高高架起，搭在了對面的肩頭。

「既然這麼不想要⋯⋯」耳鬢廝磨的輕柔語氣，掩蓋不住山雨欲來的滾滾怒意。「那就好好看著⋯⋯上你的，是誰。」

灼熱的硬物抵在了下體的穴口，彷彿盛怒之下的蓄勢待發。有那麼一瞬，蒼以為武神要蠻橫地撞進來，像後來的棄天帝無數次對他所做的那樣。

閉上眼睛，等待著直衝而上的脹痛，卻只聽見一聲落寞的嘆息。

熟悉的硬物，終究還是緩慢地廝磨著，一寸一寸地開拓著挺了進來，直到把他的身體完全侵佔，試探性地緩緩抽送了幾下。

蒼睜開眼，金藍異瞳正晦暗不明地望著他。

「疼嗎？」

「不疼。」蒼搖搖頭。雙手依然被束縛在背後，硌得他有些酸疼，無意識地輕輕晃動了幾下。武神見狀，探到他的背後解開了束縛。嘆息著撫摸過道者腕間勒出的紅痕，隨即插進他的指縫間扣住十指，把道者的雙手抵在了面頰兩側。

「好好看著⋯⋯吾是如何進入你的。」危險地一聲呢喃，武神微微退出了道者的身體，隨後一個猛衝，頂到了底。「抬起頭，看到了嗎⋯⋯」

道者深海沈沈的雙眸輕抬望去，沈默地看著自己被粗暴地插入著，身體隨著律動而收縮，順從地吞吐著那根熟悉的硬刃，伴隨著黏膩的水聲，被一插到底時發出肢體撞擊時的輕響。黑暗中所有感知都會被無限地放大，無數細碎嘈雜的聲響撞擊著耳中的鼓膜，道者平靜地閉上了眼睛。

「誰准你閉眼？睜開眼睛，看著⋯⋯」危險的氣息越靠越近，武神緩緩俯下身，把道者牢牢壓在身下，埋進他的頸窩。「說，上你的，是誰⋯⋯」

「⋯⋯」蒼喉嚨間發出一聲含糊不清的嗚咽。對面的壓制讓他動彈不得，艱難地低聲喘息著。

「不肯說⋯⋯？」危險的耳鬢廝磨。不同顏色的發梢裹挾著沈重灼熱的呼吸，緊密交纏在一起⋯⋯

「還是不說？」又是狠狠地一插。蒼無意識地悶哼了一聲。

「既然不說⋯⋯就讓你好好感受一番⋯⋯你是誰的人。」武神危險地在道者耳畔耳語道。

瘋狂的佔有中，道者的身軀也隨之沈沈浮浮，隨著抽送的節奏很輕地哀傷呻吟著。武神扳起道者的下顎，蠻橫地撬開他的牙齒。溫熱的舌尖探了進來，在道者口中貪婪地攻城掠地。

唇齒纏綿間是苦澀的味道。他竟不知，遲來了兩百年的一個吻，竟然那麼苦。

⋯⋯

吾很想你。

吾真的很想你。

日日夜夜盼著你入夢，反覆回味著相處的點滴，眼中所見心中所想全是你。

你怎能不來？你怎能不來？

來一趟人間，為何這麼苦。在那些無數個求之不得輾轉反側的日夜裡，有時他甚至覺得，苦得他都想要忘記這一切了。

你怎能不來？怎能不來？

⋯⋯

蒼用力地絞住了他的長髮，卻依舊很安靜，只是眉間的朱砂流紋因情慾和痛苦而格外地鮮紅。黑暗中他對時間失去了準確的感知，這場瘋狂而壓抑的情事持續了多久？

一刻鐘，一個時辰，一日，還是一個世紀？抑或是，幾千萬年？

「腿，夾緊，手，抱著吾⋯⋯」武神低聲道。

蒼沈默地照做了，伸出手去環在對面的肩頭，雙腿牢牢勾在武神的後腰。「對不起⋯⋯」

後者隨之猛地一頂，一股強悍的力量撞進道者身體，黏稠的液體隨之澆灌在腹腔深處。

「唔⋯⋯」

蒼一聲悶哼，突然的劇痛與魔氣貫體之感讓道者眼前瞬間失去了色彩。

「蒼？蒼？⋯⋯」

道者的道門功體無法相容創世之初這般強大的魔氣，痛得幾乎失去了意識。「吾⋯⋯」

望著眼前幾乎昏迷的道者，武神慌忙將他抱在懷裡，拍了拍他的臉頰。「蒼？蒼？⋯⋯」

幾乎豁盡畢生氣力來遮掩住痛苦的神色，蒼以過人的意志強撐著清醒了過來。他深吸一口氣，眼前終於開始緩緩恢復清明，身軀因劇痛和魔氣入體仍在不住地發抖。「無，無妨⋯⋯」

「弄疼你了？」生冷的目光頓時柔和下來，金藍異瞳有些無措，連忙將再生之力灌入道者體內。「疼？⋯⋯還疼嗎？吾，吾弄疼你了。」

「⋯⋯無妨。」蒼搖搖頭，顫抖著握住了他的手，努力平復著體內氣息衝突，疼痛讓他止不住地瑟瑟發抖。

<br/>

痛⋯⋯太痛了⋯⋯

兩百年前，他們的情事之後，這種魔氣侵襲之感還沒有這般激烈。望著武神湛墨般流瀉的漆黑長髮和英氣挺拔的黑色長眉，蒼終於明白，眼前的軀體即使再強大，承接了創世之初全部五濁惡氣的那一刻，倒計時已經開始，只能被所謂「魔氣」漸漸侵蝕，無可逆轉地走上即將衰敗的宿命。

他的血既然已經成為黑色，所謂「魔氣」早已貫通他的四肢百骸了。

<br/>

「真的無妨？」武神皺起眉頭。

「嗯。已經不疼了。」蒼虛弱地笑了笑。

「⋯⋯哼，這就承受不住吾了嗎。」武神悶悶地說，「蒼，你以前身體沒這麼差。」

蒼溫和地望著金藍異瞳，握著他的手漸漸用力，悄悄運功壓制住體內的氣息衝突。「吾好多了⋯⋯真的。」

「唔⋯⋯好吧。過來，」武神不滿地哼了一聲，貼在道者身後，把他攔腰勾在懷裡，「躺下。」

蒼有些茫然地任由他攬在懷裡，一同緩緩躺在榻上。

<br/>

難以想像他們有一天也會像普通的戀人那樣，前後相擁著，安靜地躺在一起。股間感受到武神胯間的巨物仍是硬的，蒼蜷縮了一下，被生硬地喝止。「別亂動！」

「⋯⋯」

「再亂動，吾不保證⋯⋯」溫熱的危險氣息擊打在道者耳畔，「不會再來一次。」

蒼只好一動不動，任由身後溫暖的胸膛緊緊擁著他。

「這次，就讓吾的東西⋯⋯留在你身體裡，如何？」武神撥弄著柔軟的淺色長髮，在道者耳後很輕地說，「⋯⋯知道錯了嗎？」

「⋯⋯」

「知道錯了嗎！問你話呢！」

「⋯⋯」蒼小聲地說，「嗯。」

「說，錯哪了？！」

「⋯⋯」

「看來是剛才做得不夠了？」武神一聲冷笑，勾住道者的腰，把他往自己胯間狠狠一按。

蒼微微一抖，輕聲道，「抱歉。」

「哼。」武神輕柔地把他翻過身來，扳起他的下巴。「說吧。棄天帝，是誰？」

「⋯⋯」

「同吾在一起，還敢想著別的人？！」武神危險地頂了頂道者的下身，酸酸地說，「哼，你還大聲喊出了他名字。」

「⋯⋯總有一天，吾會告訴你。」蒼有些歉意地望著悶悶不樂的金藍異瞳，「但不是現在。」

「⋯⋯有你討價還價的份？！」

「抱歉。吾⋯⋯」

「蒼⋯⋯」武神突然打斷了他，「說起來，你從來沒有叫過吾的名字。」

「⋯⋯」蒼一時語塞。

「未來的吾，叫什麼？」

金藍異瞳晦暗地望著道者微顫的深藍色雙眸，溫暖的手掌輕輕撫過道者精緻的腰線。

「⋯⋯」蒼歉意地一笑，閉上眼睛，「吾會告訴你，但也不是現在⋯⋯」

「這也不能說，那也不能說！！這次回來，怎麼不來找吾，是不是也要留到以後再解釋？！」

「⋯⋯是。」蒼低低地回應道。

「誰給你的有恃無恐？！」

「抱歉，蒼⋯⋯有難處。」

「難處。哼，什麼難處？你的難處，吾給你解決。」金藍異瞳傲慢地瞇了起來。「不用你自己解決。」

「蒼⋯⋯只能自己一人承擔。」

「又是這種什麼都要自己擔的愚蠢！不知進退！」

「⋯⋯總之，以後吾會把一切都告訴你。」蒼睜開眼，眸色如海，凝重地承諾道，「但不是現在。」

「哼⋯⋯」手指輕輕摩挲過道者秀致的面龐，武神悶悶道，「蒼⋯⋯」

「嗯？」

「關於你的未來⋯⋯吾一無所知。」武神頓了頓，輕輕吻了吻道者眉間的朱砂流紋，一聲嘆息。「也許你有所謂的『同修摯友』⋯⋯可吾一無所有⋯⋯」

「⋯⋯對不起。」蒼低聲道。

「蒼。吾的生命裡，只有你⋯⋯心裡想的也都是你⋯⋯」

輕柔的吻漸漸滑落，在道者雙唇上蜻蜓點水般地輕輕一點。「吾每天都在等你⋯⋯」

「⋯⋯」蒼黯淡地垂下眼睛。

武神語氣倏忽加重，狠狠道，「⋯⋯你怎能不來？你怎能不來？！！」

「⋯⋯其實，吾也一無所有了。」長睫低垂，蒼平靜地說。

「哼，你一直都有吾。」勾在道者腰間，武神猛地翻了個身，把他壓在身下，撩撥般地輕咬了咬他的下唇。「只要你回來，回到吾身邊⋯⋯什麼樣的心願，吾都會為你達成。你的擔子，吾想給你擔。吾不要你累，也不要你疼。」

被圈禁在武神身下逼仄的一方空間裡，蒼緩緩睜開眼睛。他們的胸膛貼的很近，近得蒼能聽見清晰有力的心跳。「哈⋯⋯」

「笑什麼？你不信？⋯⋯你就是想要天上的星星，吾也會給你摘下來。」

「⋯⋯你從哪裡學的這些話？」蒼問。

<br/>

這次回來，蒼察覺到武神明顯有些不同了。最大的變化，是他對情感沒有那麼懵懂了。

轉念一想，武神的兩百年，不過是他的幾日。時間跨度的差別之大，總是讓蒼恍惚間忘記這兩百年已經足夠長，長到可以讓任何情感都醞釀得更加醇厚濃烈。

<br/>

「話本上看的⋯⋯」金藍異瞳罕見地眨了眨，似乎有點吞吞吐吐。

「話本？哪裡來的話本？」蒼追問道。

「人間撿的。」金藍異瞳緩緩閉上，武神悶悶不樂道。

「撿的？」蒼難以置信地重複了一遍。這樣拙劣的藉口讓道者忽然覺得有些好笑——對於從不出山的他，哪有那麼容易就能撿到這種話本？

更何況他看過的恐怕不止一本兩本。

「哼，問那麼多做什麼。」

「⋯⋯你應當多閱讀一些修身養性的書目。若你感興趣，蒼可以介紹給你聽。」

「修身養性？無趣。」武神固執地說，「話本好看。吾偏要看。」

「⋯⋯好吧。」蒼只好由他去了。

——畢竟這也不是此行的重點。

<br/>

「蒼⋯⋯」武神坐起身來，把道者也拉了起來攬在懷裡。搖曳的燭火在石壁上投下纏綿親暱的身影，一時間竟有種溫馨的假象。

「嗯。」溫和地靠在武神肩頭，蒼靜靜等待下文。

「吾想⋯⋯愛你。」武神正色道，「吾想學會⋯⋯如何愛你。」

道者平靜的雙眸陡然一顫，收攏了五指。

武神認真地補充道：「吾，不太懂。你可以⋯⋯教吾⋯⋯如何愛你。吾，願意聽。」

「⋯⋯」蒼不語，只是安靜地垂下了眼睛。

「摘星星⋯⋯你不喜歡⋯⋯那就換一種說法。」武神想了想，握住道者的手，牽引到自己胸前，緊緊按在心口的位置。

「你就是想要吾的心，吾也會把它掏出來給你⋯⋯」

與兩百年前相同的位置，卻原因不明地格外灼熱。蒼顫抖著想要抽回手。「⋯⋯」

「不許放開⋯⋯」武神低頭看著懷裡的人，牢牢地按住他的手，緊貼在心口的位置。「你感受到⋯⋯吾的心跳了嗎？」

<br/>

咚咚，咚咚，咚咚。

<br/>

感受著沈穩有力的心跳，道者在他的懷抱中緩緩抬起頭。

「這裡，都是你⋯⋯吾想，學會愛你⋯⋯」

青石燈中的燭光微弱地搖晃著。牆上對視的兩個影子凝固一般，彷彿靜止了一個世紀。

<br/>

「吾這次來⋯⋯是來找一把劍⋯⋯」

半晌之後，蒼率先打破了沈默。

「嗯？」

蒼小心地斟酌著措辭，近乎一句一頓。「那把劍，能傷你。所以，絕不能落入『其他人』手裡。」

「哦？」武神扳起他的頭，強迫他再一次與自己對視，趣味道，「那把劍，在哪裡？」

金藍異瞳閃爍著興奮的光澤，彷彿又尋到了遊戲的樂趣。

「答應吾，無論發生什麼，都不要靠近那個地方。」蒼淡定道，「吾就會告訴你。」

「為什麼？」

「在那裡，你會有危險。」注視著興味勃勃的金藍異瞳，蒼凝重道。

「哈哈哈⋯⋯」武神低聲大笑起來，美目傲慢地垂下，「笑話。」

「你信也好，不信也罷。」蒼不置可否，「把這件事交給吾處理，不要牽涉其中。」

「哈哈哈⋯⋯」金藍異瞳倏忽睜開，武神罕見地有點竊喜，「蒼，你是在擔心吾嗎？」

「⋯⋯」

「不說話，就是默認了？」武神笑吟吟地望著道者低垂的眼睛。交錯的睫影遮住了眸中情緒的色彩，道者並未看他。

「⋯⋯總之，這把劍不能落在其他人手裡。拜託你，交給吾處理吧。」蒼小心地迴避了回答，轉而正色道。

「⋯⋯這就是沒來找吾的原因嗎？蒼，你擔心吾，所以你自己偷偷去找那把劍？」

「⋯⋯不全是。」

「不全是？！」

蒼有些歉意地說，「嗯，吾不能騙你。有些複雜⋯⋯剩下的，等到合適的時機再講給你聽。總之，吾要去找那把劍。但你，不能去。」

「哼⋯⋯你要找什麼，吾幫你去找便是，還需要你親自出手嗎？」

蒼沒有理會，而是嚴肅地注視著此時有些興奮的金藍異瞳，「答應吾，把這件事全部交給吾處理。無論發生什麼，你永遠都不要靠近那個地方。」

「不可能。蒼，你是覺得你能瞞得了吾嗎？快點說。」武神斬釘截鐵地拒絕，「如果有危險，那吾不可能放你一個人去。」

「對吾不會有危險，但對你有。那裡設下了專剋你的陣法⋯⋯」

「哈哈哈！笑話！有什麼陣法能真正克制吾嗎？」武神傲慢地大笑起來。蒼有些無語地看著，這種花枝亂顫的傻笑簡直和後來的他如出一轍。「哦～城牆上那種小陣嗎？」

「總之，答應吾。無論發生任何事，都不要靠近那個地方。這件事，交給吾處理。」蒼再次鄭重地重複道。

「哼，不去就不去。」武神悶悶不樂地應道。

「拜託，吾是認真的。」蒼緊緊抓住他的手，語氣真切近乎懇求。「吾來自未來。你若去，會發生很嚴重的後果。」

——雖然這句話是猜測。蒼回憶起在天波浩渺時被棄天帝燒毀的書卷，道門一脈久遠前的歷史。想到魔神那下意識地痛苦皺眉，所謂「道門先祖在崑崙山獲得誅魔之力」一定暗藏著重要的信息。

是不是阻止他去崑崙山，就能阻止一切的發生？

<br/>

「哼，允你了。」

蒼嘆了口氣。事已至此，即使不說，他也不可能瞞得過了。

「在崑崙山。吾這次時間不多，不能再耽誤了。」

「⋯⋯時間不多了？」武神黯然地望著他，「這次你回來，能陪吾多久⋯⋯」

蒼抱歉地一笑，「⋯⋯不太久。」

武神一言不發地低下頭，緊緊擁著他，下巴緊緊貼著道者的眉頭。不同顏色的髮絲交疊垂落，牆上的影子輕輕搖晃著。

感受著那心口處強有力的心跳，蒼低聲道：「總有一天，吾再也不走了⋯⋯你，再等等。再等等吾回來的那天。」

「蒼⋯⋯」武神輕輕喚道。

<br/>

⋯⋯

⋯⋯下次？下次又要到何時⋯⋯

⋯⋯蒼，你一走就是百年，也許吾沒有多少個百年可以消磨了⋯⋯ 

⋯⋯吾的生命，也許快到盡頭了⋯⋯

⋯⋯

<br/>

「嗯？」

武神想了想，終究沒有說出口。千般言語，萬般思緒，只化作一聲平淡的「好。」

「好。吾⋯⋯等你。」

蒼無聲地嘆了口氣，再一次閉上眼睛。

武神沒有說出口的話，他又豈會不明白。

塵世五百年，强大如他，也只剩下最後一百年的生命了。在那之後，他會成為無感無情的天界第一武神，位列創世眾神之間，開啟他真正的宿命。

自己一走就是百年，那下次歸來，又能陪他多久。還要在他生命的終點，將一切全盤托出。

<br/>

幸好⋯⋯他會忘記。

幸好⋯⋯只是五百年的惡夢。

<br/>

<br/>

再一次被猛地撲倒在榻上，灼熱的硬物頂在緊緻的入口。有了之前的潤滑，此刻那裡濕黏黏的。

「吾⋯⋯還想要你。」武神悶悶地說，「吾，不夠。」

「嗯。」蒼輕聲應道，溫和地一笑。

「那吾⋯⋯進來了。」武神輕輕一吻道者的眉間。

「嗯。」

伴隨著黏膩的水聲的，一聲極輕微的呻吟，他再一次毫不留情地撞進溫暖的深處，將緊緻的身體盡數佔有。

<br/>

「你走以後，吾有時會夢到你⋯⋯」武神輕聲道，把道者壓在身下，緩慢溫柔地抽送著。

蒼安靜地聽著。

「吾夢到，你帶吾去了一個有山有海的地方。你臨海撫琴，海面上有很好的月。你說吾可以一直留在那裡⋯⋯」

「嗯⋯⋯」

「夢到⋯⋯你給吾泡茶喝。吾不知怎的愣了很久，茶都涼了⋯⋯你要泡新的給吾，但是吾還是把冷的喝完了。蒼給吾泡的茶，冷的味道也很好。」

「哈。」

「還夢到吾不小心把你的東西打翻了，你很生氣⋯⋯其實吾也很生氣，生氣你為什麼總是不肯回應，不肯看見吾的真心⋯⋯」

「⋯⋯」

「夢到你有一張很小的木床⋯⋯在那張床上，吾進入你⋯⋯你不知怎的突然哭了。看到你哭，吾真的很心疼。你的手垂落在地面上，吾接住了⋯⋯」

「⋯⋯嗯。你夢到了你的未來。」蒼淡淡地一笑。

「人類怎麼稱呼這種，這種可以長期居住的所在？」

「叫作『家』⋯⋯」

「家。家。」武神有點黯然神傷地重複著，「那是你吾居住的地方嗎？」

「⋯⋯嗯，算是吧。」

「好。吾⋯⋯等你。等到⋯⋯那一天。既然你說，那是吾的未來⋯⋯吾可以等，等到那一天來臨」

「也許，需要很久⋯⋯」蒼很輕地說。

「多久？一百年？兩百年？⋯⋯一千年，一萬年？」

「也許，需要更久⋯⋯」

武神臉色暗了下來。

「抱歉。」

「一百年，吾等你⋯⋯幾千萬年，吾也會等你⋯⋯吾會等你，等到你說的，再次相遇的那一天⋯⋯」武神低聲道。

「哈。好。」

「一起去有山有海的地方？」武神輕輕地問。

「嗯，一起去有山有海的地方。」

<br/>

> ##### 小剧场
>
> （大貓貓兩百年間偷看了很多話本，開竅啦，會說情話了）
>
> 武貓：（翻話本ing）愛一個人，他要天上的星星，也要給他摘下來（趕緊小本本記下來）。（若有所思）蒼會想要天上的星星嗎？咦，（地上撿到某藍故意扔的《戀人要做的100件事》）這是什麼？嗯（繼續若有所思）要一條一條和蒼一起做一遍。要送花花，要摘星星，要看海，要成親，要一起旅行，要一起做飯，要⋯⋯
>
> 某藍：喂，傻貓，蒼可沒說過和你是戀人啊！
>
> 武貓：住口！你懂什麼？聽到沒有，蔥花寶貝說「那把劍，不能落在其他人手裡」，說明蔥花寶貝愛我，關心我，他心裡有我！
>
> 某藍：（OS：囧，那句話是「只能落在蒼手裡」的意思⋯⋯等你知道真相會眼淚掉下來）
{: .block-tip}

\-\-\-\-\-

武神貓貓從此以後化身急色貓~
