---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:09:51
lang: zh-Hant
depth: 2
---

### 第51章

<br/>

不出意外地，朱武再一次被棄天帝關了緊閉。正發愁如何消磨漫長無聊的緊閉生活，突然從天而降兩道身影，砸在了他的頭上，砸得他險些眼冒金星。「哎唷！」

等朱武回過神來，赫然只見異度魔界昔日的兩位死敵，此時正齊齊望著他⋯⋯

<br/>

「呃？你們怎麼也被抓過來了？」朱武清了清嗓子。

「蒼落在棄天帝手裡⋯⋯」

赭杉軍深吸一口氣，憂心忡忡去地簡述了經過。朱武一邊聽著，表情漸漸凝重起來。

棄天帝竟然會對這兩位死敵留手，他其實十分詫異。但當下不是思考這種問題的時候。

<br/>

「蒼⋯⋯棄天帝，棄天帝會殺了蒼嗎？銀鍠朱武，你可有辦法？」

「⋯⋯」朱武面色沈重，緩緩地搖了搖頭。「既然他對你們留手，那想必他也不會傷害蒼的性命⋯⋯靜觀其變吧。」

「這吾如何能放心，這吾如何能放心！」

「相信蒼會有應對之策⋯⋯」朱武試圖寬慰道。

以棄天帝的性子，蒼必然難逃殘酷的折磨。想到這裡，朱武長嘆了一口氣。

幾百年的死敵，他相信以蒼的能為和心性，一定能為自己找出退路。只是蒼已經到了極限，在棄天帝的折磨之下又能撐得了多久。

他轉過頭，望著另一側溫潤的灰藍身影，真誠道：「還有⋯⋯一直欠你一句⋯⋯抱歉。」

「你有你的苦衷，都過去了。」墨塵音平靜地打斷，「往事不必再提。當下還是為絃首想想辦法吧。」

「銀鍠朱武，你可有辦法離開這裡？」赭杉軍問道。

朱武苦笑著搖搖頭。

「那，那能否找到幫手？」

「這裡是棄天帝的武神殿⋯⋯未經他允許，任何人不能擅闖⋯⋯」

三人隨後陷入了沈默。時間一分一秒地流逝，焦慮的空氣讓每一秒都顯得無比漫長。

<br/>

<br/>

直到禁閉室的整扇門隨著一聲巨響被掀飛出去，棄天帝突然氣勢洶洶地踏了進來，轉瞬逼到三人面前。

「蒼呢！」棄天帝一把扼住面前紅衣道者的咽喉，將他重重地摔在牆上，「蒼呢！」

<br/>

他是想過讓這兩個礙眼的道士灰飛煙滅的。只是最後一刻為什麼留手了，他也不明白。

<br/>

「呃⋯⋯唔⋯⋯」力度之大，赭杉軍只能發出斷斷續續的氣音。棄天帝這才微微放鬆力度，「吾問你，蒼呢！」

「棄天帝，住手！」朱武與墨塵音衝上前來。棄天帝不耐煩地揮了揮袖擺，二人還未站穩，再一次被掀飛出去，摔在牆面上。

「吾耐心有限。蒼呢？！」

「蒼在哪裡，你問吾做什麼！？你還嫌傷他不夠嗎？！」赭杉軍手中蓄力，意圖強行開陣，「伏天王，降天一——」

「他消失了。」棄天帝眯起眼睛，不耐煩地打斷，「說，怎麼回事。」

「啊⋯⋯」赭杉軍睜大眼睛，結印的手突然脫力般垂了下來，「蒼⋯⋯消失了？」

墨塵音與他無聲地交換了一下神色，面色凝重。

「蒼去了哪裡？！」

赭杉軍垂下的手緩緩收攏五指，雙拳攥起，關節因過度用力而變得青白。

「蒼呢⋯⋯」棄天帝話音甫落，赭杉軍突然一拳打了上來。

「自不量力。」棄天帝冷聲嘲諷。但他沒有躲，也沒有還手，只是固執地重複著，「到底是怎麼回事，說。」

<br/>

「你怎麼能這麼對他！！你怎麼能這麼對他！！」赭杉軍清明的眸子隱隱泛起淚光。蒼不在了，他甚至沒能與好友好好地告別，蒼獨自一人留在了那個久遠前的世界，會發生什麼，蒼要獨自面對什麼，赭杉軍不敢想。

棄天帝微闔著雙眼，面色陰沉。

「他對你是怎樣予取予求⋯⋯又是怎樣孤注一擲地要救你⋯⋯你怎麼能這樣對他！！！」赭杉軍幾乎是用盡全力嘶吼出聲，「明明是你自己不記得了，你要這樣對他！！」

<br/>

金藍異瞳倏忽睜開，目光凌厲地審視著紅衣道者。棄天帝簡潔地命令道，「說清楚。」

赭杉軍冷笑一聲，「吾無權替好友決定。」

「說清楚！」

「他沒有對你說謊！」

「⋯⋯」棄天帝突然鬆開了手。赭杉軍靠在牆面上，墨塵音連忙上前扶住他。

「棄天帝。」朱武突然走上前來，擋在了紅衣道者面前。

「怎麼，吾兒，你也有話想對父皇說？」棄天帝冷酷地掃視了一眼，「讓開。」

「是啊，吾有話要說。」朱武淡定地說。

「哦？」

「你是不是一定要逼死蒼才甘心？」

「⋯⋯」

「吾真替蒼感到不值。」朱武冷冷地看著他。

「住口！」棄天帝低喝一聲，「輪得到你來評價？你又知道些什麼？」

「⋯⋯棄天帝，你的問題，你該最清楚，誰能回答。」

<br/>

父子劍拔弩張，怒目而視，誰都沒有退讓的意思  。

空氣安靜得可怕。僵持片刻，棄天帝突然妥協了下來。陰沉著臉，他一言不發地轉身離去了。

<br/>

朱武鬆了一口氣，轉過身來，只見赭杉軍神色黯然地望著他，聲音有些發顫。「銀鍠朱武，你方才的話是什麼意思？」

「⋯⋯」朱武突然語塞。

「逼死蒼是什麼意思？」赭杉軍喃喃道，「⋯⋯替蒼感到不值又是什麼意思？他會怎樣？」

「赭杉⋯⋯」墨塵音溫柔地握住他的手，「相信絃首，他一定做了最好的安排。他一定不會就這樣輕易離開同修摯友。赭杉，相信他一定會回來，給你吾一個交代⋯⋯」

「吾，明白⋯⋯」赭杉軍低聲道，「可是，讓他獨自面對那些，吾怎麼能放心，吾怎能放心⋯⋯銀鍠朱武，你知道些什麼？」

朱武深吸了口氣，「只是一些猜測⋯⋯」

<br/>

他著實有些懷念這種全心信任，彼此托付的同袍之誼。只可惜，他也什麼都失去了。

<br/>

「蒼，也許做好了萬全的安排⋯⋯」朱武緩緩道，聲音愈來愈低，「⋯⋯卻唯獨沒有為他自己留下一條生路。」

<br/>

***

<br/>

「你傷勢已復。休息好了，就走吧。」武神鬆開手，再一次背過身去，「吾不想再看到你。」

蒼站了起來，「吾不走⋯⋯」

<br/>

武神的情，他還不起，也一度覺得不能還。此刻蒼望著腕上纏繞的流光溢彩的金色流蘇，心緒潮水般翻湧，突然做了一個重大的決定。

如果註定有所虧欠，至少⋯⋯至少在最後一刻到來之前，可以陪他實現那些未竟的心願？

像戀人那樣，一同度過餘生。

<br/>

「這又算什麼？」武神冷酷地嘲諷道。

「⋯⋯」

「吾的心意不容人類踐踏。既然不愛，就走吧。以後，別讓吾再看到你。」

「⋯⋯吾願意」蒼頓了頓，「願意陪你做戀人之間做的事。這個承諾，可以嗎。」

<br/>

武神突然回過頭來看著他，金藍異瞳再一次湧動著蒼看不懂的情緒。蒼靜靜地等著他回話，但武神只是晦暗地盯著他，一言不發。

見武神沒有鬆動的跡象，蒼小心地斟酌起來，「吾⋯⋯」

「走吧。」武神突然打斷，又背過身去，似乎再不願多看他一眼。「吾說過，吾只有一顆心。你既然不要就罷了，沒有反悔的餘地。」

「⋯⋯對不起」蒼低聲道。

事已至此，只好另作打算。至少知道了武神的棲身之地，不至於在接下來的局勢中太被動。

<br/>

「穿過這扇門，走到地道盡頭，你就能回到地面了。」

「多謝⋯⋯」蒼深吸一口氣，向那扇門走去，沒有注意到武神突然轉身，狠狠地瞪了他一眼。「那你⋯⋯保重。」

「站住！」一聲低喝。

「⋯⋯」蒼停下腳步，茫然地回頭。

「讓你走你就走？！你什麼時候這麼聽話了！！」

「⋯⋯」

「吾說什麼你都不聽，為什麼這時候倒是聽話了？！」武神冷笑一聲。

「⋯⋯」

「給吾滾過來！」武神環抱雙臂，不滿地逼視著道者。

金藍異瞳的壓迫之下，蒼慢吞吞地走上前去。「抱歉。吾⋯⋯」

「吾，之前，很生氣。」武神手指戳了戳道者清秀的面頰。

「對不起⋯⋯」

「吾，很生氣！不能輕易便宜了你。」

「⋯⋯好吧。」

「你，哄吾高興。吾，才會原諒你。」武神傲慢地說。

「？」蒼微微抬起頭，有點難以置信地望著他。

『哄』？怎麼哄？

無所不通的先天高人，對此一片空白。

<br/>

「生氣了，要哄的。」見蒼不語，武神忽然覺得有點理虧，磕巴了一下，「而且，而且，話本上也是這麼寫的。」

「又是撿來的？」望著武神一本正經的樣子，有模有樣地說著違和的話，蒼一不小心笑出了聲。

「笑什麼笑？！不許笑！」

「好。」蒼正色起來。「吾⋯⋯盡力。」

武神掃了他一眼，頭生硬地扭到一邊。「哼！」

蒼只好跟著走到一邊，望著悶悶不樂的異色雙瞳。「別生氣了⋯⋯好嗎。」

「哼！」武神又把頭轉到了另一邊，像個生悶氣的彆扭小孩。

蒼只好跟著走到另一邊，「你想讓吾怎麼做？」

「蒼，你只會出一張嘴嗎？！」

「⋯⋯」

<br/>

僵持了一會，蒼緩緩向腰間伸出手去，淡定地解開了腰帶。

武神轉回頭來，眯起眼睛打量著。「這不是會嗎？」

「⋯⋯」蒼回望著，目光平靜，隨後解開了領口的衣襟。那件白底交領的深紫翻面外袍沿著柔韌挺拔的曲線悄然滑落，墜在了腳邊的空地上。他隨後停了下來。

「脫啊，怎麼不脫了？」武神眯著眼睛問。

「⋯⋯」蒼閉上眼睛，慢慢扯開了樸素的灰色內袍。

武神突然一把將他按在了床上，語氣中有一絲難以覺察的興奮。

「這可是你自願的。」

「嗯。」蒼輕輕應道，「你，躺下。」

武神眯著眼睛坐下身來。

蒼深吸了一口氣，跨坐在他的胯間。灼熱的硬物隔著衣物，抵住了他的下體。蒼緩慢地解開他那條綴滿寶石的腰帶。

「磨磨蹭蹭的，幹什麼呢！」

硬物彈了出來，灼熱的皮膚似乎要將他燙傷。下體緩緩剮蹭著那硬物的頂端，找到入口的位置，蒼深吸一口氣，緩緩坐下去，將那碩大的硬物一寸一寸吞沒進身體中。

「唔⋯⋯等等⋯⋯」武神皺了皺眉，扯了一下道者的手臂。

蒼猝不及防地撲倒在武神胸前。鼻尖相貼，金藍異瞳距離很近，彼此的呼吸緊密地纏繞在一起。武神突然翻了個身，把他壓在身下。

一根手指蠻橫地插了進來，替他做起擴張。手指模仿著抽送的動作，反覆剮蹭著那處隱密的位置。電流般的酥麻感傳遍四肢百骸，蒼閉上眼睛。

「不許⋯⋯壓抑自己⋯⋯」武神在道者耳邊輕聲地說。

入口稍許鬆軟，第二根手指猝不及防地插了進來，蒼低喘了一聲。分身漸漸挺立，頂端分泌出晶亮的清液。

「你知不知道，方才你的樣子有多誘人⋯⋯」危險的氣息擦過耳畔，武神另一隻手揉搓起道者胸前粉嫩的花蕾來。「看，你有反應了⋯⋯不許壓抑自己⋯⋯」

「嗯⋯⋯好。」

抽送的手指狠狠地戳了一下，蒼劇烈地一抖，輕吟出聲。頂端分泌的清液愈來越多，伴隨著心底隱秘的、對於到達頂點的期待。

手指插入的很深，後穴早已鬆軟。武神隨後握住了他的分身，感受著道者的身軀再次劇烈地一抖。

「哦？⋯⋯」貼在道者耳畔，他低聲地呢喃，「這就想射出來了？⋯⋯」

手指抽送的節奏愈來愈快，蒼弓起身子，情慾的巨浪席捲著他推向頂點。

武神嗤笑了一聲，手指猝不及防地堵在了慾望的出口。蒼不由得悶哼了一聲。「嗯⋯⋯」

「坐上來。」武神命令道，「吾要看著你⋯⋯在被吾上的時候⋯⋯射出來。」

「⋯⋯」蒼有些無可奈何。

「是你自己答應，要哄吾高興的。」

「是吾⋯⋯先說的嗎⋯⋯？」蒼很輕地呻吟著，斷斷續續地說。

「哼⋯⋯」武神有點理虧地冷哼一聲。

<br/>

再一次跨坐在灼熱的硬物上，蒼深吸一口氣，緩緩坐了下去，直到整根吞沒。被硬物侵入的異樣感並不好受；微微的痛楚散去，生理上的本能又讓他產生了某種對於到達頂點的、隱秘的期待。

「不許壓抑自己⋯⋯」武神雙手握在了道者的腰間，不容置喙地命令道，「吾要你叫出來，聽到沒有。」

「嗯⋯⋯」坐在武神胯間晃動腰肢，蒼試圖說服自己去接受它，在自己身體中緩緩抽動的灼熱硬物。

除了予取予求，他真的不知還能再彌補些什麼。

道者額頭滲出細汗，仰起頭，露出修長白皙的脖頸，續續地低聲呻吟著。武神眯起眼睛，「魔，喜歡激烈的東西！」忽然猛地向上一頂，「來，叫大聲點。」

「啊⋯⋯」一聲嬌吟漏出齒間，蒼被自己發出的聲音嚇了一跳，淡定的臉忽然泛起一絲緋紅來。

武神開始向上頂弄起來，道者的身軀被頂得上下沈沈浮浮。「說，喜不喜歡，被吾上。」

「⋯⋯」

這位大神的惡趣味，還真的是很一致。蒼昏昏沈沈地想。

<br/>

「說！」武神握著道者的腰，狠狠向下一按。蒼低喘一聲，終於達到頂點，白色的濁液激射出來。

「看到沒有，這樣，你就能射⋯⋯」武神突然挺身坐了起來，緩緩逼近，與他交叉而坐，牢牢嵌合。他環住道者的後腰，胸前的衣襟撩撥著道者裸露的肌膚。「抱住吾。」

蒼順從地勾住他的後背，後者開始更加用力地頂弄起來。

⋯⋯

道者的五指收攏又放開，如此循環往復。重傷初癒又反覆高潮，他脫力般地靠在武神的手臂上才沒有倒下去。

突然間天旋地轉，他被撲倒在床上。武神將他壓在了身下，扣住他的十指舉過頭頂。

「夠了？那，換吾了！」

危險的話音消散在吻中。在蒼的身體中疾風驟雨般進進出出，堵住他的唇，與他緊密結合，感受著他體內的溫度。一個清冷的人類，身體深處卻像烈焰般灼熱。

道者在他身下斷斷續續地輕聲呻吟著，撩撥起情慾的火愈燃愈烈——他終於等到他了，終於等到他了。五百年的守候，他終於等到了一個叫「蒼」的人，再也不用失去他了。

金藍異瞳舒服地咪起來。「啊⋯⋯你⋯⋯好緊。快要被你⋯⋯夾斷了。」

昏昏沈沈的道者蹙起了眉頭。他又在哪裡學了這麼多葷話？

<br/>

「蒼⋯⋯」

「嗯⋯⋯」

「吾好想你⋯⋯吾以為，你再也不會回來了⋯⋯可吾，只能一直等⋯⋯一直等⋯⋯」

把他的東西宣洩在蒼的身體深處，蒼就徹底是他的了。還差一點，還差一點⋯⋯

慾望傾瀉而出的一剎那，金藍異瞳驟然睜開。他忽然清醒，退了出來。白色的液體澆灌在了道者的腿間。

蒼已經承受不了他了。承受不了他這具污穢的軀體了。

<br/>

蒼突然抓住他的手，「無妨，吾，承受得了。」

「說什麼傻話。」武神悶悶地說。他還硬著，但是蒼看起來已經十分疲憊了。「罷了，這次就⋯⋯原諒你。」

「⋯⋯不生氣了？」蒼微微一笑。

「哼哼，不會那麼便宜你。」武神在蒼身邊躺了下來，勾在他的腰間，強迫蒼側過身來與他四目相對。呼吸漸漸平靜，蒼安靜地望著他。金藍異瞳罕見地笑咪咪的。

<br/>

「上次你走之後，吾做了好多惡夢⋯⋯」武神將他的長髮撩到耳後，摩挲著道者清秀的側臉。

「嗯？」

「夢到在一個很潮濕的山洞裡，吾把你打傷了⋯⋯」

「⋯⋯哈，很有趣的夢。」蒼淡淡地說。

「吾以為你背叛了吾，氣昏了頭，竟然對你動手⋯⋯」

「⋯⋯」

⋯⋯你怎能背叛吾，你怎能背叛吾。在吾為你付出一切之後，你怎能背叛吾。邊想著，武神悶悶地說，「其實吾立刻就後悔了。夢裡的你也是那麼傻，不知進退⋯⋯」

「哈⋯⋯」

「其實你只要對吾服軟，吾就⋯⋯吾就會原諒你。可你就是不肯，偏要自討苦頭⋯⋯你怎麼那麼傻。蒼，你怎麼那麼傻。」

「⋯⋯後來呢？」

「後來你消失了，吾，吾被嚇醒了⋯⋯你就在山洞裡，對吾悲傷地笑了笑，突然消失了。吾，吾嚇壞了。」

<br/>

那夢境如此真實。困在夢中的他，驚慌失措地站在原地，似乎真切地感受到，他永遠地失去了他，再也找不回那個叫「蒼」的人了。

幸好，只是一場惡夢。幸好，蒼回來了。

<br/>

「惡夢已過，就忘了吧。」蒼淡淡地說，由著武神撥弄著他的頭髮。

「嗯。」武神應了一聲。他的夢境很稀少，每夜入睡前都苦苦企盼著那人能入他的夢，好讓他再次一瞥那人音笑容貌。只是不知怎地，和蒼有關的大都是些惡夢。天意為何要這樣作弄他？

<br/>

「吾經常做惡夢⋯⋯」

「嗯？」

「太多了。夢到有次吾不知怎的往你身體裡面打了五枚釘子，你流了一地的血，吾突然被嚇醒了⋯⋯」

「⋯⋯」

「夢到你吾有一次回到了北海，你要與吾同歸於盡。蒼，吾真的很傷心。」

「⋯⋯」

「結果後來有人站在你背後，為你贊掌，當下了吾的一擊。蒼⋯⋯吾真的很傷心。當初，明明是吾站在你身後，是吾先站在你身後的，為什麼夢裡換成了別人⋯⋯你看吾的目光那麼冷，像刀子似的，割得吾很疼。」

「⋯⋯」

「上次你說，吾夢到了吾的未來⋯⋯那這些又是什麼？」

「⋯⋯」蒼淡淡地一笑，「惡夢既然已過，就忘了吧。」

「哼⋯⋯」武神漫不經心地撥弄著道者的長髮，突然手指一僵。

<br/>

——蒼的長髮短了一截，像是被削斷的。

他記得夢裡他也削過蒼的長髮。切口如出一轍。

<br/>

「怎麼了？」望著武神的臉色突然一沉，蒼問道。

「⋯⋯罷了。沒什麼。」

他想問，卻又不敢問，彷彿害怕聽到一個不願意接受的回答。他逃避式地強迫自己想些別的什麼。「對了。那群人類稱吾『魔』你們人類，還有沒有類似的字眼？」

「你問這作什麼？」蒼有些納悶地望著他，「魔與人，其實並無分別，不過是不同的稱呼罷了。吾與一魔物曾是死敵，後來成為了很好的朋友。」

武神皺了皺眉，「你到底有多少朋友是吾不知道的！」

蒼笑了笑。——你兒子。

「問你話呢！」

「以後都講給你聽⋯⋯」

「哼，吾不感興趣。」武神悶悶地說，「總之，還有沒有類似的稱呼？」

「『鬼』，『邪』⋯⋯怎麼了？」

「鬼，邪？」武神重複道，「這兩個稱呼不錯。」

「⋯⋯？」

「吾這段時間對五濁惡氣，哦，你們人類所謂的『魔氣』，更加運用自如了。最近以魔氣煉造了兩個新的種族，不如就叫鬼族和邪族吧。」

道者的淡淡笑意突然凝固在臉上。

「蒼？你怎麼了？怎麼不說話？」

蒼強迫自己迅速地冷靜下來，聲音仍止不住地微微顫抖。

「你這段時間⋯⋯做了什麼？能否都講給吾聽？」

武神皺了皺眉，「還養了條龍，用吾的血。可惜長得很慢，近一百年才長到人類那麼大。」

「⋯⋯」蒼陡然睜大了眼睛。

「不過⋯⋯哼⋯⋯總有一天，他會成為吾之魔界的載體。就是要多花點時間罷了。」

「⋯⋯為什麼？」蒼顫抖著問，「告訴吾，發生了什麼？」。

<br/>

命運的齒輪緩緩轉動，一刻也不曾停歇，將所有人推向那個那個既定的結局，在深海沈沈中急速下墜。

<br/>

「哼。他們竟然以你的安危相威脅⋯⋯魔化天下，是人類應得的代價。」

<br/>

> ##### 小剧场
>
> 武貓：戀人身分開始的第一天，歐耶✌️！
>
> 蒼：今天你想做什麼
>
> 武貓：（掏出之前某藍扔的《戀人之間要做的100事》，嘩嘩地翻著）嗯⋯⋯很難選
>
> 蒼：那就從第一條開始吧
>
> 武貓：（突然又掏出了某藍悄悄扔的另一本）更想先做這上面的
>
> 蒼：這又是什麼
>
> 武貓：《戀人xx的100種姿勢》
>
> 蒼：？？？
>
> 武貓：你自己同意說願意一起做戀人做的事，（貓爪戳戳蔥花寶貝）難道你忘了？
>
> 蒼：（嘆氣）好吧
>
> （一隻追更道友十月出場，架起了📹，放進錄像帶）
>
> 某藍：（壓低聲音）大家好，今天歡迎一直以來的道友十月來到演播室現場，為我們全方位無死角錄制最新貓片
>
>（此時棄貓仍舊在猛貓落淚，望穿秋水）
{: .block-tip}
