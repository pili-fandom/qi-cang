---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:03:36
lang: zh-Hant
depth: 2
---

### 番外  姻緣樹

<br/>

> KUSO向的小（？）劇場，不影響正文，可以當成是棄總大翅膀夾著（？）蔥花在六天之界閒逛了一下

<br/>

六天之界的某處仙洲之上，有一棵參天桃樹，一樹花海終年不敗。萬千銅鈴盈滿枝頭，被層層疊疊的絲線相連，輕搖間發出泠泠脆響。

姻緣樹上，神和人都能找到代表自己命牌的一串銅鈴，每一根絲線都代表了一段緣。若是良緣，那便是紅線；若是孽緣，那便是黑線。紅黑交疊的絲線，錯綜複雜地纏繞著，輕垂在淡粉色的花海中。

<br/>

天界武神是從來不會來這種地方的。

但是今天是個例外。

掌姻緣的神官——姑且就叫他神官吧——遠遠地看到了兩道身影，一黑一紫，緊緊相靠——啊不，準確說那個紫色的是被拎著，踏雲而至。

他今日多喝了酒，腦子變得有點不清醒，連忙揉了揉眼睛。那的確是武神殿下同一個道士裝扮的人類。

稀客啊，神官心想。武神是創世之初就存在的神祇，比他年歲大得多。從他在姻緣司任職起，武神從來沒有來過這裡，姻緣樹下。

不過他隱約記得，他的前輩，月和老人，在這裡任職的時候，曾說武神在幾千萬年前來過一次。傳言中，那時的武神殿下手中握著一條紫色的飄帶，在樹下一言不發地站了很久。後來像是下了某種決心，把它拋入了雲端，看著它在雲霄中安靜地墜落，隨後轉身緩緩離去了。從此以後再也沒來過這裡。

千萬年的時光對於六天之界的時間尺度也過於遙遠，以至於傳言聽起來十分失真。更何況，武神看起來實在不像會為情所擾的那種神。再後來，武神背離天道，自降為魔，自號棄天，就更不可能來這裡了。

心中一驚，神官連忙打起精神。

<br/>

「帶吾來這裡做什麼？」紫衣道者淡淡地問，聽不出什麼情緒。

「對你的姻緣，不好奇嗎？」唇邊似笑非笑，棄天帝緩緩道，「你那天煞孤星的命格。」

「不好奇。」紫衣道者乾脆地回答。

「⋯⋯真的不好奇？」棄天帝又重複問了一遍，「吾只會帶你來這一次。」

「不好奇。」

「⋯⋯」

<br/>

神官納悶地看了一會兒。看起來明明是武神殿下比較好奇吧⋯⋯

他似乎突然明白了什麼，腦子一熱，連忙開口，「來來來，良緣孽緣一看便知，姻緣樹下通通現形！過了這個村就沒這個店了！這位紫衣的道長，敢問尊姓大名？」

「六絃之首·蒼。」棄天帝回答道。

<br/>

神官：「好勒！」

趁著酒勁爬上樹去，在花海中一頓翻找，「找到了！殿下和道長，來這裡！」

<br/>

「如何？是不是天煞孤星？」棄天帝不由分說，一把扯住紫衣道者的手腕，將他拖拽到屬於他的那串銅鈴下。

隔著頭頂風華繁盛的桃花，蒼看到一隻小巧的銅鈴，形單影隻，沒有任何絲線相連。

紫衣道者眸色平靜深沉，毫無波瀾。事實上，他一點都不意外。

「哈哈哈哈⋯⋯」棄天帝優雅地緩緩笑起來。笑著笑著，他忽然覺得好像自己也不怎麼開心。

有什麼不開心的呢，想到他的魔界兵馬多少折損在這個不知進退的道士手裡，天煞孤星的命格都是便宜了他。

⋯⋯但好像還是不怎麼開心。

所以笑著笑著，魔神乾脆不笑了。

<br/>

「笑完了？該走了？」蒼淡淡地說。

「哼⋯⋯」拽過紫衣道者的手臂，棄天帝正打算離開，突然聽到神官在樹冠上喊了一聲，「欸！二位等等！」

一頓翻找，神官陪笑道，「這位道長不是天煞孤星，不過⋯⋯」

「不過什麼？」棄天帝饒有興致地問。

「這位道長的命牌和樹頂的神之命牌相連。他的姻緣不在凡間，所以一開始沒有看到絲線⋯⋯」

蒼忽然有了一種不祥的預感。

「說。」棄天帝命令道。他說話一向很簡潔。「囉囉唆唆的，吾耐心有限。」

「是是是！」神官連忙應道，「這位道長和武神殿下的命牌連在了一起⋯⋯」

「神也有命牌？」棄天帝皺了皺眉。

「對哇對哇，殿下，你不記得嗎？」

「吾怎麼會記得這種東西？」棄天帝望了一眼身邊默不作聲的紫衣道者，嫌棄地重重一哼，「和人類連在一起，晦氣。」

<br/>

「啊⋯⋯」神官有點茫然地琢磨起來。

也許是酒喝多了，腦子實在不清醒？但是武神殿下看起來並不像不高興的樣子。

<br/>

「蒼也難得和你想法一致。」紫衣道者再次抬頭望了望他的那串銅鈴，另一端確實隱隱約約有線纏繞的痕跡。

「哈哈哈⋯⋯」聞言，棄天帝陰惻惻地冷笑了幾聲。

蒼向神官的方向看去，「勞煩閣下告知，吾與這位⋯⋯這位⋯⋯尊貴的殿下，是紅線相連還是黑線相連？」

「欸？好奇怪，半紅半黑⋯⋯」神官自言自語道，「以前沒出現過這種情況⋯⋯」

「能斬斷嗎？」蒼問道。

棄天帝霎時臉色鐵青。

神官一時摸不著頭腦。武神殿下到底是因為和人類連在一起而惱怒，還是因為這個紫衣道者的話惱怒？⋯⋯真是喝酒誤事，喝酒誤事。

「問你呢！」見神官不答話，棄天帝冷笑道，「能斬斷嗎？」

神官嚇了一跳，「能能能，當然能⋯⋯我馬上就剪！」

棄天帝臉色更黑了：「⋯⋯」

蒼淡淡一笑，「有勞了。」

<br/>

神官掏出鴛鴦剪，顫顫巍巍地向那半黑半紅的絲線伸去⋯⋯

武神殿下的眼神盯得他有點發毛。這到底是讓他剪還是不讓他剪⋯⋯

看起來是不想剪的，可又是那種不容置疑的命令語氣⋯⋯

利刃觸及絲線的那一剎那，神官手中的鴛鴦剪突然灰化了。

「欸⋯⋯」

「怎麼？」蒼疑惑道，「可有不妥？」

「不妥倒是沒有，就是專門用來斬斷緣分的鴛鴦剪突然⋯⋯哎唷！」

神官突然整個人從樹上掉了下來，摔了個嘴啃泥。

「壞了？」望著狼狽地摔在地上的神官，棄天帝居高臨下地問。

「呃，壞了⋯⋯」金藍異瞳的威壓讓神官打了個寒顫。

——哪裡是壞了，明明是被灰化了！這下損耗支出又要算在自己頭上。

「什麼破銅爛鐵？」棄天帝傲慢地說。

「是是是⋯⋯」神官連忙賠罪。他方才是怎麼從樹上摔下來的？⋯⋯真是喝酒誤事，心思不敏捷，動作也不靈便⋯⋯

「沒有鴛鴦剪，就不能斬斷緣分？」蒼突然開口。

「不能。」神官答道，「斬斷情緣只能用姻緣司特製的鴛鴦剪⋯⋯」

「請問是否有備用？」蒼問。

神官愈發摸不清狀況了。武神殿下的臉色看起來更黑了。「沒有⋯⋯」

「哼，今天真是晦氣。」魔神優雅傲慢地一揮衣袖，袖風吹落枝頭的繁花，銅鈴泠泠作響間紛紛揚揚地落下來，把他們罩在淡粉色的花雨中。「蒼，走吧。」

「等等！」神官突然爬起來開口，「我忽然想起來，沒有鴛鴦剪也可以⋯⋯」

「哦？」棄天帝冷笑。

「武神殿下的開天神斧也可以！」

蒼轉過頭來望著棄天帝：「及時斬斷咱們之間的緣分，對你吾都好，只好委屈你的武器屈尊降貴了。」

「⋯⋯吾的武器能用來斬這種污穢的東西嗎！？」

<br/>

神官總算看明白了什麼⋯⋯

武神殿下明擺著就是對這個人類有意思嘛！

趁著酒勁，他大膽獻策「殿下，道長，來都來了，要不要考慮一下⋯⋯」

「考慮什麼？」棄天帝不耐煩道。

「這棵姻緣樹的種子在久遠前曾經飄落到人間，在人間也長出了相似的姻緣樹。人間的傳言，戀人只要在樹下親吻，就能獲得長久的幸福⋯⋯ 雖然是人間的傳言，不過六天之界大家也都信這個。殿下和道長要不要考慮在這裡親一口⋯⋯唉唷！」

「你可以滾了。」低沈的聲線在同一時刻響起，似乎更加惱怒了。

飛出去的神官在空中納悶著，他難道又搞錯了什麼？但是武神殿下看起來明明就很在意的嘛⋯⋯

喝酒誤事，喝酒誤事⋯⋯

<br/>

<br/>

「你為難他做什麼？」蒼嘆了口氣。

「哼，講出這種話，他也污穢了！」棄天帝陰惻惻地冷哼一聲。

「⋯⋯」蒼不想再起爭執，轉移了話題，「走吧。」

道者正要轉身，卻猝不及防地被一把按在了身後桃花樹的枝幹上，力度之大震落了一樹繁花，淡粉色的花雨再一次紛紛揚揚地落在肩頭。一個熱烈的吻不由分說堵了上來，帶著一點點暴躁的怒意，一點點落寞的孤獨，無數深埋心底難以言說的洶湧情緒，蠻橫地撞進了道者唇齒之間。

<br/>

可笑的人間傳言，戀人在姻緣樹下親吻，就能得到長久的幸福？

只有卑微的人類，才會渴求所謂長久的幸福。

世間唯有他才是永垂不朽。

他不曾記得，而蒼也永遠不會知道的是，千萬年前，千萬年前他也站在同一位置，獨自一人望著六天之界的雲海彼端——

<br/>

<br/>

第一個百年，你沒有來。

第二個百年，你沒有來。

第三個百年，你還是沒有來。

⋯⋯

塵世五百年一晃而過，轟然倒地的那一剎那，握著手中那條依舊輕透的紫紗袖帶，穿透硝煙望著血紅的天空。

他想起他不是什麼人間的白衣武神，他是天界的第一武神，從開天闢地起與天地同在。

多可笑啊，也曾眷戀人間，也曾想過擁有人間的幸福。多可笑啊。

回歸六天之界，站在風華正盛的桃花樹下，望著那條紫色袖帶雲間墜落。好像有什麼遠去了，卻又始終未遠。

吾情願⋯⋯忘記你。

馬車上未完成的吻⋯⋯

月光下淡淡的苦澀⋯⋯

幸福是什麼？他沒有，也不會有的東西。

<br/>

<br/>

像是等待了許久的一個未完成的吻，瘋狂而濃烈。在道者唇齒間吸吮著，盡情感受他的味道。

幸福？幸福是什麼？尋常人間的幸福，他不屑一顧的東西。

既然不屑一顧，不曾希冀的東西，為何又覺得心中那麼苦。

多可笑啊，多可笑啊。

<br/>

「棄天帝，你很清楚，你吾之間，不需要這個。」棄天帝回過神來時，蒼已經不知何時推開了他，冷冷道。

「⋯⋯哈哈哈，」魔神一時落寞地大笑幾聲，「怕什麼？人間的傳言，你不會當真了吧！」

「魔神既然沒有當真，蒼自然也不會當真。」

「哈哈哈⋯⋯更何況，傳言說的是戀人，蒼，你不過是吾之奴隸。」棄天帝傲慢地說，語氣有一絲他自己都不曾察覺的酸澀。

「是啊，不是戀人。」蒼淡淡道，「走吧，這裡不適合你與吾這樣的關係。」

「哼。」語氣不明地低哼了一聲，棄天帝目光望向了遠處的雲海。

<br/>

以前來過這裡嗎？

肯定沒有，他是不會來這種地方的。
