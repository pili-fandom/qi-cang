---
title: 天命
author: 終朝採藍
category: fate
layout: post
date: 2023-04-28 00:00:10
lang: zh-Hant
depth: 2
---

### 第10章

<br/>

一句平靜的話，卻如同一道驚雷，在魔界舊址死寂的黑夜裡劈空而下。天魔池水蕩起層層漣漪，像是水底潛伏的深淵巨獸，在黑暗中窺伺著，伺機而動。

<br/>

「什麼？」墨塵音睜大了眼睛，「絃首給自己留下的線索？」

「準確地說，是吾之推測。」蒼平靜地解釋道，「這道空間術法，曾被吾親自開啟過。所以吾猜測，這條額鍊是吾留給自己的線索，指引吾找尋術法另一端的關鍵所在。」

「這太過匪夷所思。」墨塵音思考了片刻，道。「絃首，開啟術法的當真是你本人？」

蒼淡淡道：「嗯，是吾本人無誤。此外，這條額鍊上也確有棄天帝的一絲氣息。」

墨塵音聞言點了點頭，不再懷疑。六絃之首向來殺伐果決，他的判斷鮮有出錯。

「但吾不記得曾經接觸過它，遑論開啟過上面的空間術法術法。」蒼道。

「這⋯⋯」

「所以吾第一個猜測，是記憶出現了差池。於是這段時間，吾回溯且細細推敲了全部記憶，確認了吾不曾接觸過此物，吾之記憶也全無破綻，所以吾否定了記憶有誤這種可能。」蒼略一停頓，取出那條額鍊握在手心。「吾繼續推測，若這件事非是發生在過去，便是發生在未來。」

「⋯⋯」墨塵音沒有應聲，像是在細細思考著蒼這些話中的信息。

蒼繼續道：「這是一種異常強大的空間術法，能穿透時間的限制，也不無可能。⋯⋯或許，未來有一天，吾回到了過去，得到了這條額鍊，開啟了這上面的空間術法，才留下了施術的痕跡。出於⋯⋯不可知的原因，過去的吾沒能回到現在，所以不得不以這種方式把它留給現在的吾，從而指引吾去發現關鍵所在。」

<br/>

自從棄天帝臨世以來，蒼不可避免地回想起了那個年少時在月華之鄉聽過的傳說。

傳說中，未來的某一天，天界第一武神降禍人間，四根神柱全部折斷，塵世會再現遠古的災厄。當時蒼本以為自己是多心，不曾想傳說中的災厄一一應驗，和當下的經歷巧妙的聯繫起來。

<br/>

被囚禁的時間裡，面對著萬年牢黑暗的虛空，蒼反覆地嘗試推敲其中的關竅。

那說書夫子口中的「瘋子」，是否就是為自己擋下一劫的老人、棄天帝口中的「五衰之人」？

若傳說是真，那「五衰之人」口中的「恩公」是誰？「去往大荒」又是何意？

那間祭奠「白衣武神」和「六絃之首」的靈堂又意味著什麼？是否是同死的結局？

不，這不可能是巧合。當所有線索羚羊掛角、草蛇灰線般地串連在一起，只剩下唯一的可能——

<br/>

也許注定有一天，他會被困在遙遠的過去，無法回到現在，所以只能以這般曲折的方式，指引當下的自己——開啟這條額鍊上的空間術法，去時間的另一端，尋找救世之答案。

「天時已來⋯⋯」面向晦暗的池水，蒼輕聲地自語。

⋯⋯好友一步蓮華，吾也終於等到這一天了，蒼淡淡地想。

當下、過去、未來，互為因果。未來的因造就現在的果，終於輪到他，去面對真正的「天命」了。

<br/>

「絃首⋯⋯」墨塵音沉思片刻，略帶隱憂地喚出聲。

「若吾之猜測成立，那未來的吾，必然能預判吾當下之預判。」蒼平靜地說，藍紫色的雙眸深海沈沈，幾乎不帶一絲感情色彩。「所以吾近來反覆思索，未來的吾，傳達這些線索，到底希望現在的吾如何做？」

意識到這句話的分量，墨塵音心一沉。

「——吾想，棄天帝的弱點，就在這空間術法的另一端。未來的吾，希望當下的吾能開啟它，去往另一端，尋找他的弱點，也是救世的契機。」

「⋯⋯絃首，墨塵音只剩最後一個問題，希望絃首切莫隱瞞。」

「嗯？」

「逆天而行，必有代價。此舉，⋯⋯絃首會付出怎樣的代價？」墨塵音一針見血地問道。

「⋯⋯」蒼一瞬的沈默。「吾⋯⋯」

在打開那只桃木匣的一瞬間，他便知道了自己將會付出何種代價，面臨他的會是何種結局。

木匣上那屬於他自己的氣息，蒼再熟悉不過。那是回光返照之象，身形俱殞之氣。

——若開啟術法，也許他會一步一步重蹈覆轍，直到覆水難收，在那個久遠前的過去孤獨地死去。

<br/>

墨塵音敏捷地捕捉到了蒼這一瞬的沈默，苦笑道：「哈，果然墨塵音料想的不錯，絃首已經知道，自己將付出何種代價了。」

「墨塵音⋯⋯」蒼嘆了口氣。

「⋯⋯有去無回，墨塵音說的對嗎？」

「也許還有變數⋯⋯目前，蒼也只是猜測罷了。」

「⋯⋯絃首，尋找棄天帝的弱點，只能通過這樣極端的方式嗎？」

「吾想，這是當下唯一的救世之機。在棄天帝如今絕對的力量面前，人類太渺小了⋯⋯」蒼無聲地閉上雙眼，淡淡地一笑。「大概，『逆天而行』，才是吾之天命吧。」

除了孤注一擲地回到過去，他別無選擇。

當下的因鑄種下未來的果，造就了久遠前的因。而久遠前的因種下現在的果，是注定的天命，也是逃不開的劫。

<br/>

「如果赭杉知道⋯⋯他一定會很難過⋯⋯」墨塵音黯然道。

「墨塵音，多餘的話就省下吧⋯⋯」蒼溫和地輕聲打斷，「在吾之處境，若換作是赭杉，換作是你，也會是同樣的選擇。每個人都有必死的覺悟，玄宗的使命，眾生生命，蒼不能有負諸位同修所托。」

「唉⋯⋯」灰藍的遊魂一聲嘆息。

<br/>

墨塵音忽然察覺，蒼和過去有些不同了。

過去的絃首，那個永遠沈穩從容，一肩挑起大局的大師兄，總能力挽狂瀾，是令人安心的存在。而如今，他的眉目間似乎覆上了一層若有若無的倦意。

那個總是為眾人遮風擋雨的他，也會累嗎？

人，總有極限。

<br/>

「⋯⋯既然絃首已經下定決心，那吾便不再贅言了。無論你做出何種決定，墨塵音都會全力支持。」

「多謝你。」蒼溫和地一笑。

「吾尊重絃首的決定，也希望絃首尊重吾之決定。」

「嗯？」

「若有吾能為之事，請絃首務必告知。」灰藍的遊魂試圖打趣道，「最壞不過魂飛魄散，回歸天地，道法自然，也是不差的選擇嘛～」

「墨塵音⋯⋯」蒼微微蹙眉，「你為眾人付出的，已經夠多了。」

「絃首，多餘的話也請省下吧。因為，吾的位置，若換作是絃首，換作是赭杉，也會是一樣的選擇。」

「哈。」蒼淡淡一笑，「蒼能有你們，深感幸運。」

二人再次相視一笑。

他們已經很久沒有這樣笑過了。

<br/>

「對了，蒼還有一事想問。」蒼看了看手上的鎖鏈，沈吟道，「棄天帝是否會時不時地離開這裡？」

「吾大多數時間都躲在湖底，在赭杉的身體附近，對外界發生的事並不全然了解。⋯⋯不過，」墨塵音一邊想著，一邊緩緩道。「棄天帝的魔氣過於強大，所以吾或多或少總能有所察覺。他大約每隔四五個時辰，會離開這裡一段時間，但未必準確。」

「他每次離開多久？」蒼問。

「吾想想⋯⋯時間不等，但都不少於三個時辰。」

「如果吾判斷無誤，棄天帝應該是回到六天之界了。或許六天之界依舊對他有某種限制——但也許只是暫時而已。」蒼沈吟道，「三個時辰⋯⋯墨塵音，眼下有一事，可否請你幫忙？」

「絃首請講。」

「吾現在要嘗試啟動這條額鍊上的空間術法。」蒼閉上雙眼，指尖輕輕撫過綻放的金色流蘇。「兩個時辰後，若吾還沒有回來，就強行中斷吾之施術，將吾拉回。以你現在的狀態，能做到嗎？」

「哈，放心。吾雖只是一魂，但術法之類還是不在話下。」

「辛苦你了。若兩個時辰內發生任何變故，以你自己為重，不要在意吾。」蒼鄭重地叮囑。「切記，在棄天帝回來之前，回到赭杉身邊，隱藏好你自己。」

「絃首，那你呢⋯⋯」

「哈，放心，六絃之首不會那麼容易失敗。」

蒼緩緩站起身來。墨塵音不再言語，安靜地靜待著。蒼並不適合黑色，但縱然一襲黑袍，也遮不去一身的氣勢凌雲、高遠出塵。

循著殘留的一絲術法痕跡和心中的隱隱猜測，蒼深吸一口氣。藍紫色的雙眸驟然睜開，耀目的紫色光華無聲地綻放。

「伏天王，降天一，蒼音乾坤轉無量，化天地陰陽，轉定一乾坤⋯⋯」

<br/>

<br/>

接觸棄天帝的意識時，看到了什麼？蒼屏氣凝神，集中意念，努力地回想著當時的場景。

一條荒涼的山路，一片茫茫的雪地，在亙古的星空下，寂靜無聲。

額鏈緩緩升起，在空中奪目地旋轉著，閃爍著細碎的金色微光，與道者周身的紫色光華交相輝映。隨著道者默念口訣，鮮豔的紅色寶石突然迸發出奪目的紅光，穿雲裂石般四散而去——

浩蕩而耀目的聖光讓一旁的遊魂不由自主地微微閉上雙眼。電光火石間，待他再次睜開眼時，法陣內已空無一人。鎖鏈下空蕩蕩的，只餘金色的額鏈流光溢彩，在半空無聲地盤桓著。

<br/>

***

<br/>

人間所見的茫茫夜空，是橫跨六天之界的一處古老棋盤。交錯的星軌是那棋盤上縱橫的紋路，芸芸眾生皆是那棋盤上的滿天星斗，按照命定的軌跡，錯綜複雜又有條不紊地運行著。

那是每個人屬於自己的天命。

天命終了，命星會行駛到星軌的盡頭，在浩瀚無垠的夜空化作一道天際流火，燃燒者向西殞落。

世間萬物都逃不開天道，掙不脫星軌的運行規律，萬千命運，息息相關地發生著，關聯著，八千萬年來，從未有變數，平穩而沈悶。

而如今，沈寂了八千萬年後，這塊星軌組成的巨大棋盤彷彿感應到某種變數，驟然一顫。萬千星辰在既定的軌跡上開始微微抖動，交錯的星軌發出雄渾悠遠的共鳴。

<br/>

「太陽神殿下！失落八千萬年的『輪迴一念』，現世了⋯⋯」

茫茫星斗中，被稱作太陽神的身影立於棋盤中央，手執金色權杖，沈默地俯下身去，俯瞰著芸芸眾生。

他在搜尋著某一條星軌的痕跡。

<br/>

「武神呢？」片刻之後，太陽神緩緩起身，紛飛的淡金色長髮間，一雙冷感無情的金色瞳孔銳光乍現，竟是絲毫不遜於棄天帝的赫赫神威。

「目前還在武神殿。」掌星軌的小星官答道。

「嗯⋯⋯你，退下吧。」

轟鳴過後，古老的棋盤又恢復了平靜。太陽神背手轉身，望向棋盤深處的虛空，對著恆常流轉的星辰輕聲道：「原來如此。你，先手太多了。」

棋盤深處，只有星軌的隱隱低鳴，除此之外，寂靜無聲。

然而他似乎聽到了某種回答。太陽神閉上眼睛，像是輕聲的自語。

「那便拭目以待。人間的絃首，吾且看你如何⋯⋯一手回天。」
