---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:04:36
lang: zh-Hant
depth: 2
---

### 第36章

<br/>

蜷縮在神座上醒轉時，蒼已經無法起身了。稍微一動就會牽動下身的痛楚，讓他眼前一陣發黑，好久才恢復清明。果然棄天帝早前對他的折磨還是留了幾分餘地，而當魔神真的發起怒來，這樣的程度他的確難以招架。

六天之界的空氣有些稀薄，空曠的涼意讓蒼一時禁不住有些微微發抖。嘆了口氣，蒼一動不動地躺在神座上，閉上了眼睛，試圖給自己運功療傷。

<br/>

「醒了？」

聞聲蒼緩緩睜開眼，棄天帝不知什麼時候來到了他跟前，居高臨下地打量著他。

金藍異瞳中怒意依舊未散，卻多了一絲難以覺察的妥協意味。冰涼的手突然蓋在他的小腹，隨後一陣與魔神極不相符的暖意緩緩匯入他身體中，下身的撕裂痛霎時減輕了許多。

「謝了。」蒼低聲道，聲音喑啞得讓他自己也驚了一下。

「是吾不能滿足你了？」棄天帝緩緩收回手，傲慢地背在身後，轉過身去背對著他。

「⋯⋯」

棄天帝大度地一揮手，「吾不會為你全部療復，這是觸怒吾的代價。」

<br/>

蒼輕輕動了動，發現他已經能起身了，於是從神座上爬了起來，小心地踩在了琥珀質感的光潔地面上。下身依舊是撕裂般的痛楚，蒼深吸一口氣，緩慢地直起身子。幸好，已經是可以承受的範圍了。

「棄天帝，你以為都和你一樣？吾與朱武只是朋友。」

「哦？幾百年的對立，都能成為朋友？」棄天帝冷笑一聲。

「是啊，為了對付你。」蒼淡淡地說，喑啞的嗓音卻依舊擲地有聲。「是非之前，蒼一向只論立場，不談私仇。」

「⋯⋯」棄天帝陰沉地望了道者一眼，只是冷冷一哼，大度地沒再發怒。以蒼現在的狀態，也經不起其他的摧折了。

——幾百年的對立，都能放下私仇，成為朋友⋯⋯？

不知怎麼心中有些莫名的酸澀，棄天帝酸酸地轉移了話題。「哼，吾兒，那不爭氣的魔界君主，竟然同人類交朋友！」

「朱武？不爭氣的魔界君主？」蒼難以置信地望著魔神，「魔界到底是被誰玩到如今境地的？你真的有在意過魔界嗎？」

——他好意思這樣評價朱武嗎？

「這話從你口中說出，倒是新鮮。」棄天帝漫不經心地品評道。

「不說這些了。」蒼嘆了口氣，話鋒一轉，「答應吾去見朱武，尊貴的魔神不會反悔吧。」

「反悔？哈哈哈⋯⋯就讓你見他一面又如何。這種事，不值得吾在意。」

「那你生什麼氣？」蒼反問道。

「⋯⋯吾是生氣吾兒，那不成器的魔界君主！」

「哦，是嗎？」對於魔神的急於解釋，蒼不置可否。

「你在質疑吾？」棄天帝不滿地一把拽過道者，懲罰式地用力，緊緊扣住他的腰。

「蒼一介貧道，怎敢質疑尊貴的魔神呢。」蒼挖苦道。對於這種明晃晃的宣示「所有權」的動作，他著實有些無奈。這位大神表面看起來可以什麼都不在乎，無所謂，但有的時候真的像個彆扭的幼稚小孩。「吾自己能走。」

「有你選擇的餘地嗎！」

「⋯⋯」蒼不再言語，他不做無意義的反抗。由著棄天帝這樣摟著他，四周的空間突然變換，他們似乎在武神殿中緩緩下沉。

不知下降了多久，六天之界那種四處瀰漫的清聖金光突然消失。沒有了聖光的清暉，陷入在黑暗裡，這裡冷得讓蒼有種重返魔界的錯覺。

——這位大神的口味，還真的很一致。

<br/>

雖然在這裡沒有尋常的空間感，但蒼勉強可以覺察到他們來到了武神殿底部的某個位置。穿過一條陰暗的過道，一扇牢門乍現眼前。牆壁上不只是什麼燃料的火把投下微弱的光，勉強可以看到昔日的紅髮王者坐在地上無聊地划拉著什麼。

「吾兒。」棄天帝緊緊扣住身邊的紫衣道者，惡質地湊上前去，「還未放棄抵抗父皇嗎？」

對於這種話，銀鍠朱武一向懶得搭理他，自顧自地在地上畫著圈。突然感覺到一絲不尋常的氣息，他突然抬起頭，瞳孔巨震：「蒼？！」

「朱武。」蒼對著他淡淡一笑。

朱武尚未從震驚中回神，目光從那雙清冷的雙眸開始緩緩向下⋯⋯長髮披散在肩頭⋯⋯脖頸間盡力遮擋但仍難以掩飾的曖昧痕跡⋯⋯腰間環繞的手臂⋯⋯還有那喑啞的聲音⋯⋯

「棄天帝！！！你為難蒼做什麼？！？！毀掉聖魔元胎是吾的主意，你為難蒼做什麼？！？！」愣了一秒，朱武回過神來，怒聲道。

「吾兒，這就是你來到這裡的第一句話？」棄天帝冷笑一聲。

<br/>

於是蒼敏捷地捕捉到了隱藏的信息。朱武自從被關在這裡，再也沒有同棄天帝說過話了。

<br/>

「棄天帝，吾還是低估了你。」朱武有些憤怒。

「哼，這就是觸怒吾的代價。怎麼，吾兒，還沒有覺悟嗎？」

「棄天帝，」蒼適時打斷，「別忘了你答應吾的事。吾要和朱武單獨一談。」

「⋯⋯」鐵青著臉，棄天帝似乎極欲想說些什麼，但還是忍住了。

「哼。」他隨即冷哼了一聲，鬆開了箝制著道者的手，優雅地背過身去。「珍惜吾給予你們的一刻鐘。」

正當蒼以為他要轉身離去時，魔神像是突然想起了什麼，氣態高貴地轉回身來，雲袖輕輕一揮。蒼腳尖的地面上突然出現一道光芒奪目的白線，似乎是某種結界在緩緩升起。

「敢邁過這條線，吾會讓你知道代價。」棄天帝冷笑道。輕揮的手背在身後，他隨後優雅地轉身離開，只剩下目瞪口呆的昔日王者，望著紫衣道士一臉平靜地對著他，淡淡一笑，略顯無奈。

「蒼！你⋯⋯他⋯⋯」朱武一時不知道該說些什麼，尷尬地咳嗽了一下。

⋯⋯雖然咳嗽的有點假。

「那一劍⋯⋯抱歉。」蒼毫無痕跡地轉移了話題，歉意道，「吾⋯⋯」

「那是吾自己的決定，同你無關。蒼，不必再提了。」

「⋯⋯」停頓片刻，蒼喑啞道，「好。你在這裡過的⋯⋯如何？」

雖然這句話，蒼問出口就有些後悔了。嚮往自由的人，淪落到如今的境地，又能過的如何。

「還⋯⋯不錯。」朱武微微一頓，「至少吾不理他，他也不能拿吾怎樣。蒼，你呢。」

這話問出口，朱武立刻也後悔了。蒼這樣落在棄天帝手中，只比他更慘。

「還⋯⋯不錯。」蒼淡淡一笑。

「⋯⋯哈哈哈」兩人忽然相視大笑起來。

他們很久都沒有這樣暢快地笑過了。

笑聲停止，蒼正色道：「吾會想辦法，讓他答應放你自由。」

「⋯⋯啊？」朱武有些詫異。這話聽起來怎麼和出賣色相差不多⋯⋯

趕緊把這樣的想法從腦海中剔除，朱武繼續道，「蒼。別勉強自己⋯⋯」

「吾會想辦法，讓他放你自由。」蒼鄭重地重複道，「而且，抱歉，朱武，吾來此有事相求。」

「蒼，你說。只是以吾目前的境地，怕是能為有限⋯⋯」

蒼向身後望了一眼。棄天帝當真不在這裡嗎？

「棄天帝不在這裡。」看穿了蒼的疑惑，朱武確認道，「蒼，你說。」

蒼點點頭。棄天帝的確算得上守信諾的神——雖然這話朱武未必同意——繼續道：「朱武，戒神寶典中是否提到，異度魔界初臨在道境的原因？」

「初臨？蒼，你是指道境被毀，只剩方寸的那一次？」看著對面道者平靜的眼眸中似有波瀾，朱武頓了一頓，「抱歉⋯⋯」

蒼點點頭，「嗯。不是幾百年前的道魔之戰。應當在更久遠前，異度魔界初創的時期。道境被毀，只剩下以封雲山為中心，周遭百里的方寸之地。」

「那時吾還沒有出生。棄天帝應當是降臨在了第一代聖魔元胎，吾父皇身上⋯⋯」朱武躊躇片刻，「為什麼選在道境，抱歉，蒼，吾亦不知。」

「無妨。」蒼淡淡道。朱武的回答，他並不意外，那段過去太久遠了。「朱武，時間有限，吾長話短說。那並非棄天帝第一次降臨人間。」

「⋯⋯啊？」

「而是更早，那時他還沒有自號『棄天』。」蒼頓了頓，「那時他的活動範圍，屬於如今的苦境。而四境以苦境為基，如果毀滅人間是他最直接的目的，那再臨人間的第一站，為何不是選在苦境？」

「⋯⋯以他那種性子，也許是投骰子決定的？」朱武諷刺道。

「哈⋯⋯也許。」蒼淡淡一笑，「不過吾近來有一些猜測。他並沒有初臨人間的記憶，但吾想，那段經歷也許給他的潛意識中留下了什麼，所以選擇道境作為毀滅人間的第一站。吾猜測，此舉引起天界的反對，引發了第一次天界戰爭。最終他選擇背離太陽神所代表的天道，開始自號『棄天』，在道境創下了『異度魔界』。但『魔』的存在，應該要追溯到更久遠以前了⋯⋯」

朱武點點頭，示意他在聽，靜靜地等待紫衣道者把話說完。

「為什麼要選擇道境？既然要滅道境，為何留下了封雲山所在的位置？吾相信，草蛇灰線，一切事物都是有跡可循的。朱武，吾需要找到原因，才有希望阻止他繼續毀滅人間。」

「這⋯⋯與他毀滅人間有何關聯？」

「因為吾回到了過去，見到了第一次降臨人間的那個他。」蒼淡淡地說，「吾需要知道，他的那段經歷中後來發生了什麼，才不至於太被動。如果吾能及時阻止，從而改變現在的他，也許一切都還有轉圜的餘地。」

「⋯⋯什麼？」朱武愣了片刻，才消化了這句話的含義，「你回到了過去？！」

蒼點點頭，「說來話長。朱武，戒神寶典中是否有道境被毀前的路觀圖？」

「這⋯⋯」朱武略一沈思，「抱歉，吾暫時不能確定，但可以想辦法。為何需要路觀圖？」

「道境被毀的土地已經歸於混沌，沒有氧氣，生靈不能生存。吾想進入其中，必須速戰速決，所以路觀圖必不可少。」

「蒼，這聽起來很危險。」朱武不無擔憂地說，「就為了弄清發生了什麼？為了⋯⋯改變他？」

「也不僅僅是為他。吾想，也許『道』與『魔』的真相，就被埋藏在那裏。無論真實的過去是什麼，都不該被遺忘。作為道門一脈的後人，吾有責任找到道與魔的真相。」

「唉⋯⋯」朱武嘆了口氣，「吾能做什麼？」

「吾會想辦法讓棄天帝放你自由。至少，先讓你能在六天之界自由活動。」

聽起來還是和出賣色相差不多⋯⋯朱武及時阻止了自己沿這個角度繼續想下去，「嗯？」

「如果戒神寶典中沒有遠古道境的路觀圖，那麼六天之界也許有。」蒼閉上眼睛。

他想起了那些如真似幻的夢境，星官模稜兩可的指示。從第一次回到過去以來，自己冥冥之中似乎一直受到了某種指引。

無論指引他的神或人到底是誰，既然走到了這一步，那蒼判斷，這裡一定有找到線索所必須的東西。

「即便沒有路觀圖，六天之界也一定有吾需要的其他線索。蒼能否托你找尋，和第一次天界戰爭有關的那段過去？」

<br/>

『天地陰陽，自成三界。天有神，人有識，地有靈。逆者闇氣加身。天神有魔道，人間有修羅，地靈有餓鬼。天界產生爭戰，第一武神悖離天道，封號棄天帝，自創魔道，就此與天界抗衡⋯⋯』

『⋯⋯棄天帝創立異度魔界，意圖毀滅人間。佛陀降下龍神之燄，一擋棄天帝野心。天為阻止魔，雙方立下誓約，就以人間為賭注。天神授命三教，抵擋魔界之禍；派出聖獸，守護天地支柱。若三柱盡毀，需三名默契無間的三教高手藉神柱聖氣，驅逐五濁惡氣，共修天極聖光。風雷引動，闇遮天地之刻，共擋魔禍。』

道者不由得想起了磐隱神宮刻下的古老文字。真真假假，假假真真，被篡改的歷史掩埋的真相，也許已經很近了。

<br/>

朱武點點頭表示會意：「好。」

雖然聽起來像是蒼要他大鬧六天之界⋯⋯不過這個想法，朱武並不討厭。也許有機會還可以拆一拆棄天帝的武神殿⋯⋯

「抱歉⋯⋯」想到朱武肉身已經死於自己劍下，如今還不讓他的靈識安生，蒼有些愧疚。

「吾欠中原的太多了。蒼，別多想。而且，能遊覽一下六天之界也不錯。」

「好。」蒼很淺地一笑，「那麼只剩下最大的問題，朱武，若能找到路觀圖，你與吾之間怎樣才能傳遞消息？」

「這個簡單！」朱武爽快道。

「嗯？」

「吾可以托夢給你。這還是吾最近發現的，在六天之界，靈識可以托夢給人間，挺好玩的⋯⋯」見蒼突然陷入了沈思，朱武頓了頓，「蒼，怎麼了？」

蒼回過神，「沒什麼，突然想起最近幾件事。」

——如果六天之界可以托夢給人間，那他夢到的到底是什麼？是否有誰故意引導，甚至誤導他的選擇？會是太陽神嗎？還是另有他者？

暫時按下心中猜測，蒼繼續道：「另外，還有兩件事，如果有機會，能否勞煩你幫蒼順便打聽？」

「蒼，你說。」

「第一件事，關於棄天帝第一次降臨人間。吾猜測，那段時間太陽神也以某種方法去了人間。吾需要知道，他那時的身份是誰。」

對此，蒼雖然有所猜測，卻不能確定。他有一種不安的預感——也許他遺漏了非常重要的、決定成敗的關鍵。

「啊⋯⋯？」朱武微愣了一下。自從磐隱神宮一別，蒼那邊似乎發生了很多變故，讓他一時難以消化。

雖然困惑，但朱武並沒有多問。

「第二件事，關於一把劍⋯⋯」

<br/>

<br/>

牢房的正上方，武神殿空曠的地面上，百無聊賴的毀滅之神正望著雪山發呆。

一刻鐘有這麼長？早知道，應該只允諾那個愚蠢的道士半刻鐘的寬限。

但是他是高貴的神嘛，怎麼可能反悔，只好繼續無聊地看著雪山。

<br/>

終於等到一刻鐘結束，高貴的魔神一腳踩進了禁閉室所在的空間裡。「說完沒有？」

蒼正坐在結界的一側安靜地打坐。

朱武靠在禁閉室的角落裡閉目養神，似乎懶得看他。

「⋯⋯哈哈哈」棄天帝有點沒趣地冷笑幾聲，轉向了蒼，「在這你都能打坐？」

「道心堅定，何處不能修行。」聽到聲音，蒼睜開眼睛，緩緩站起身來。深吸一口氣，下身的撕裂痛讓他不得不放緩動作，才不至於身形搖晃。棄天帝走上前去，不由分說把他攬在懷裡。

「走吧。」蒼平靜地說，「吾說完了。」

「哼。」魔神面色陰沉地再次看了角落裡的朱武一眼，後者依舊頭轉向另一邊，絲毫沒有看過來的意思。冷哼了一聲，他帶著蒼離開了。

直到優雅的毀滅之神和他身邊的紫衣道者身影消失不見，紅髮王者終於在禁閉室中轉過頭來。望著他們離去的方向，他悲傷地嘆了一口氣，鬆開了攥緊的雙拳。

——蒼，值得嗎？

閉上眼睛，朱武無聲地問。

<br/>

> ##### 小剧场
>
> 朱武：（憤怒地拔出斬風月）棄天帝！！！蒼比我還小幾百歲！！！你禽獸不如！！！
>
> 棄貓：（傲慢地）哈哈哈，傻孩子，驚了嗎？絕對的力量，才能達成願望～
>
> 朱武：（斬風月從手中掉了下來）（內心os：願⋯⋯望⋯⋯？？？等等，這話聽起來不太對啊）棄天帝，你要不要認真想想，你到底在說什麼？
>
> 棄貓：（傻裡傻氣的大笑）哈哈哈哈哈，傻兒子，還沒有覺悟嗎？等你有絕對的力量，再來質疑父皇吧，哈哈哈哈哈哈哈哈
>
> 朱武（目瞪口呆）：別笑了！重點是「力量」二字嗎？啊？！你知不知道你自己在說什麼？！啊？！？！
{: .block-tip}
