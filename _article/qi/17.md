---
title: 家有仙七
author: Lamour
date: 2008-02-17
category: domestic_qi
layout: post
lang: zh-Hant
depth: 2
---

### 17.神奇的畫
 

自從發生了那樁捉姦在床(？)的事情之後，龍之夢集團的疏樓龍宿先生覺得這群人的工作態度很成問題，於是他採取的態度是——人盯人戰術——簡單說就是他親自過來盯梢劍子，以及他那個“有作風問題”的道門朋友。

 

其實蒼真是很無辜，除了僱他來的龍宿之外，每個人都瞭解他的無辜，但是關鍵是龍大少爺不瞭解……

 

因此他總是不放心讓蒼和劍子呆在一起。

 

開工頭兩天，蒼、一步蓮華和劍子三下五除二登記完了大廳和過道的物品以及估值，然後蒼提出一起清理最重要的古董收藏室。結果被一旁叉著腰什麼也不幹的龍宿一口否決。

 

“這裡你一個人就夠了吧。劍子和一步蓮華還是先去清理圖書館比較好吧？”說完，拉著劍子就往樓上走。

 

一邊走，劍子心理就在嘀咕：清理個鬼的圖書館，這根本不是清單所列項目好吧？

 

蒼也不以為意，拿了兩雙乾淨的白色手套放在旁邊，一雙使用，一雙備用。用於檢測、留底的工具，比如寶麗來一次成像相機、超高像素專業檢測相機、軟刷、試劑瓶等放了一堆。準備等一下查看物品的時候用。

 

這次要估價的最大標的就是古董收藏室裡的這幅畫。

 

畫作的年代有點久遠了。但是油畫保存的很好，細節什麼還算清楚。

 

畫上是夕陽中的城堡。仔細看會發現，畫中的城堡就是他們現在所處的城堡，只不過，是若干世紀前的樣子。

火燒雲熱烈地燃燒著，城堡的輪廓被細膩地描摹。近處的高原上，一個清瘦的剪影烙在畫面中。

 

蒼看著這幅畫，忽然有一瞬的失神，但他說不清那究竟是一種什麼樣的情緒。於是，他帶上手套，開始近距離地端詳起這幅畫的落款和時間。

 

畫的右下角，一行很小的法語字寫著：Q emperor /紀元803年霜月/第一次的風景

 

蒼早在出發前就做過功課，Q emperor就是當年馳騁霹靂大陸的差點統一全大陸的某君主的名字，冒似也叫棄天帝來著。

 

嗯？這個名字有點耳熟，好像還在哪裡聽到過……

 

正在這時，門外忽然響起了禮貌的敲門聲。

 

蒼脫下自己的手套，搭在一旁的桌子上，轉身過來開門。卻原來是襲滅天來站在門外。

 

“忙麼？”

蒼有些迷糊。

“還好，正在看東西。有事麼？”

襲滅天來露出一個為難的表情：“我哄不住他了，他要找你。”

說完，他往後退了一步。

一張眼淚汪汪的臉就露了出來，然後……

 

“啊！”

“嗚……嗚……”

“小七乖……你抱得太緊，我喘不過氣來了。”

 

棄天帝於是後退一步，稍稍放開他。

蒼剛想喘兩口氣，忽然就覺得天旋地轉，整個人被橫了過來。

“嗯？你要幹什麼？？放我下來！！！”

“砰”得一聲，門關上了。

門外的襲滅天來帶著溫和的笑容，在門把手上掛上了一塊牌子：施工進行中，請勿進入。

<br/>

此時此刻，龍宿正悠閒地坐在圖書館的絲絨墊圈椅上玩IPAD250，一旁勞作著的一步蓮華看了他一眼，對身邊的劍子仙蹟說：“看，都是你招來的。”

 

“喂，你是他招來的。”劍子補充說明道。

“可他是來盯你的。”

“那還不是因為你們沒幹好事兒麼？”

“哦，所以他是怕你出軌？”

 

“啪！”得一聲響，書架上掉下了幾本書。

 

龍宿嚇了一跳循聲望去，只見劍子大聲道：“你才出軌呢！你們全寺廟都坐動車！”