---
title: 質子
author: 西陵歌者
date: 2010-05-10
category: hostage
layout: post
lang: zh-Hant
depth: 2
---

### 終章

　　“伏嬰！你！朕把老師託付給你，你……”

　　看到僅一日之間，就變得空空如也的房間，棄天帝只覺得一股急火衝上。他昨日忙碌一日，得到蒼殺死金鎏影的稟報，急急趕來時已是入夜了。

　　“臣死罪。”跪倒在地，見魔帝已經急急起身，便欲衝出，趕緊從懷中取出一紮布帛，道：“陛下，蒼老師心情激盪過度，此時正在一處清淨隱祕所在休養……”緩緩抹下一把冷汗，伏嬰師心知，同棄天帝對峙多次，唯有此次當真是危如累卵了，“蒼老師有書信在此，請陛下御覽。”將書信雙手奉上，卻已經緊張到不顧禮數，直勾勾看著自己主君將書信展開……

　　“……”緩緩坐下，展開書帛，臉上神情漸漸緩和，“既如此，讓老師先歇歇吧。”隨後擡起頭來，道：“伏嬰，此事應當如何解決呢？”

　　“啊……”訝異一封書信竟能讓魔帝重歸平靜，伏嬰師愣了片刻，才道：“這……蒼老師殺死金鎏影之事，尚未傳揚，陛下若想息事寧人，何妨言說近日大亂，金鎏影被黃商子、九方墀所殺……”

　　棄天帝緩緩搖頭，道：“老師信內言道，金鎏影已自承玄天子雖然傷重，然卻是為他所殺……”

　　“啊？！”

　　“嗯。”

　　“這……”遲疑一下，望著魔帝，有看看桌上書信，“陛下是，想將此事公諸於世？”

　　“此乃老師心願。”

　　“……那陛下又要如何處置弦首？雖是罪有應得，畢竟金鎏影乃是歸降諸侯，而老師他夥同朱武謀反一事……尚未得到陛下赦令……”

　　“吾明日稱帝，大赦天下！”

　　“臣遵旨！”

<br/>

<br/>

　　棄天帝初年二月，魔侯棄天稱帝，國號為魔，改元天隆。

<br/>

<br/>

　　玄陵距離封雲城僅有半日的路途，天下大亂，守陵軍士早已逃散，舊有房舍倒還依舊矗立。

　　蒼到達之時，已是黃昏。緩緩下車，看看伏嬰師先行派遣而來的工匠僕從已將此院稍作修整清掃，雖然還有些破落，然而安睡一晚，已無問題。

　　安頓下來，已經天黑，走入屋內，竟是一愣：怒滄琴蓋著輕綃，安安靜靜放在屋內琴案之上。

　　“這……”記得早已將此琴贈與棄天，只是到了魔國之後，一直都是自己在使用，如今竟被僕人們理所當然一併收來了，低頭看看右手，四指尾指雖然已經漸漸能夠伸直，然而戰火一起，停了治療，只怕再難起色了。

　　“……罷了，棄兄今後，只怕也再無閒情逸致撫弄此琴了吧？”想到這裡，不由自主坐在琴案之後，雙手按上琴絃，雖不成調，卻仍是瞑目撥弄起來，天色昏暗，無人點燈，萬籟俱寂之間，斷斷續續的琴音悠悠響徹。

　　山鳥驚飛，必有人至，蒼停了琴音，擡起左手，眼中萬物早已失去了色彩，門外一片月光瀉地，一個熟悉人影，映上門窗的一瞬，便破門而入！

<br/>

　　“蒼！”

　　棄天帝一身冕旒凌亂，仍是今晨稱帝祭天之時的裝束。

　　“你又騙我！”看著朦朧月色中披散頭髮，一襲黑衣，背對自己而坐之人，棄天帝心中急怒，再難壓抑。

　　“棄兄……”本想辯解，然而聽到那個隨口而出的“又”字，心間頓時一抖，脖頸僵住，本已轉過一半的臉孔再度慢慢扭回，輕聲道：“蒼滿身罪孽，無顏再留。”

　　“吾已登基，大赦天下，今日神州無罪人！”

　　“陛下可赦蒼之魔之罪，然而蒼之大罪在玄，天子不在，陛下又怎能赦之……”

　　“你說過，只要替玄天子報仇，一切便聽由吾之安排。”

　　“天子雖為金鎏影所殺，然而……天子之崩，其罪在蒼，請陛下為天子報仇，蒼莫敢不從。”

　　“豈有此理，他咎由自取，與你有何關係！”

　　“……蒼當初應允來做質子，便是私心了。”究竟是何時明白，竟難追溯，然而如今，心內已經清楚明白——蒼心心念念想要阻止者：非是玄朝覆滅，乃是此人違了天，悖了命。

<br/>

　　一陣突如其來的靜默。

　　兩人皆不言語，過往之事，電光火石一般閃過腦海。

　　“既如此，那又何妨再私心一次！”痛極怒極，跨步登堂，一把抓著黑暗中那人瘦骨伶仃的手腕，向外便拖，“跟我回去！”

　　“哼。”一瞬間已被拖著出了內室，腰眼撞上外屋桌案一角，瘦弱身體，痛徹骨髓。

　　“蒼……”此時才又驚覺，慌忙放手。

　　“棄兄……”肩頭被扯得彷彿脫臼，蒼撐起身體，微微搖頭，“蒼不欲生，然棄兄不捨；生死已不能自主，棄兄便放蒼在此贖罪吧。”

　　“……”雙手攥成拳頭，道了一聲“好！”然而，就在對方長出口氣之時，又是踏步而上，將人打橫抱起，

<br/>

　　“弦首可為玄朝之忠臣！然而你，必為我棄天之良人！”

<br/>

　　說著，大踏步，向著月光熹微的臥室而去。

　　“……”閉了眼，從對方身上傳來的熱力彷彿要將全身溶化，蒼沒用動作——掙扎抗拒已經太多太久了。吾累了……今夜，便讓蒼再自私一次吧。

<br/>

　　身體被放在剛剛鋪好的窄榻之上，剝去層層衣衫，便似去除了那說不清看不見的阻礙，兩具身軀終於相融，微微的呻吟與粗重的喘息交織，將亂髮貼在臉上的有汗也有淚。手掌輕輕搭上身前聳動汗溼的赤丄裸肩頭，彷彿一個信號一般，讓那人加劇了動作。

　　“嗯……”一股熱流，從身體的中心直衝進來，暖了身也暖了心。微微睜目，瞥見月光之下的靜靜的怒滄琴與凌亂地上的冠冕袞服：如果你我只是初遇該有多好；如果一生僅此一天，蒼更無悔……可惜……

　　沉醉的棄天，察覺懷中軀體止不住的輕顫，又趕緊加快了動作。

<br/>

　　“啊喲！”劇烈的撞擊，讓也有些乏累的棄天帝驚醒過來，才察覺自己身上僅裹著半條被子，人已摔到了塌下。看看一旁睡榻上垂下的一條裸露小臂，棄天帝臉上先是一愣，隨後又露出淡淡滿足微笑，想起昨夜，竟似如飲醇酒，熏熏若醉。拎著被角，緩緩撐起半邊身子，本想爬回榻上，然而一眼望去，竟是榻上人，灑遍枕蓆，如同一張絲網鋪展的雪色長髮！

　　“蒼！”

　　“嗯？”聽到呼喚，微微睜眼。

　　“頭髮……你的頭髮怎會如此？”還未看清對方惶急面孔，聲音已是陣陣催心。

　　“……棄兄？難道此時才發覺？”突然想起初入魔國那日，魔侯明察秋毫，舉手擇取自己一絲華髮的情形，蒼本想嗤笑，然而嘴角一抖，竟讓是露出了戚容。

　　“啊？”一時懵懂，然而仔細回想，自己眼中之人從來都是一般完美。

　　“蒼之髮色，在重傷初醒，得知……棄兄已然攻下封雲城之時，便是如此了……”將頭偏過，雖無埋怨，然而自己親口說出總覺殘忍。

　　異色雙眸滿是不信……不信自己所為竟給對方如此傷害？為何不信！心中一動，竟是連連後退。

　　“……棄兄，得蒼生者得天下……魔帝離宮太久，請回吧。”無需回頭也知此時對方臉上何等表情，攥緊身下皺做一團的潮溼被褥，輕輕說道：“蒼生有幸，亦蒼之幸，陛下……保重。”

<br/>

<br/>

　　棄天帝天隆元年三月，耀侯六禍蒼龍遇刺身亡，太子千流影擊敗玉蟬宮一黨，繼任耀侯；

　　棄天帝天隆元年四月，弦都明玥城兵盡糧絕，開城請降。

　　棄天帝天隆元年六月，耀侯千流影紫耀城請降。

<br/>

<br/>

　　棄天帝天隆三年六月，帝遷都天魔城。

<br/>

<br/>

　　四年後。

　　新都天魔城，選址長河之陽，與玄朝故都封雲城及其不遠處的玄陵遙遙相望，此時正是早春三月，去年遷都時，魔帝親自下令在城外所種的一片矮桃第一次開出了絢爛繁花。

　　“卻說苦命人昨日所說回目‘斷風塵義釋孟白雲，二十六出兵明玥城’講的乃是北線之爭，今日再講講棄天帝南線之戰，”路邊一眉清目秀的乞丐正說的眉飛色舞，頓了一頓道：“這回目嘛，便是‘蕭關城兩狼初相認，斬風月血戰問天譴’……呵呵，無地容身的苦命人，不求其他，唯求一餐之飽。”

　　乞者滿意而去，人群漸漸散開，微服的棄天帝隨手彈去殘留在肩頭的一片桃花瓣，轉身看看旁邊的伏嬰師，道：“伏嬰，走吧。”

　　“是。”兩人牽著馬，轉過幾條街道，路過一片府邸，聽得從後院之內傳出的滿是稚氣的朗朗讀書之聲，不覺同時將步履停下了。

　　“伏嬰，赭老師現下身體如何？”

　　“……”身體不動聲色的一震，竟是遲疑了一下，並未回答。

　　“回宮！”心中瞭然，心念已決，翻身上馬，向著城北新落成的王宮而去。

　　……

　　落座御書房內，棄天帝等到跟著進入的伏嬰師站穩，才沉聲道：“伏嬰，你當年曾向朕保證，等赭老師身體痊癒，便勸他歸順我朝……朕耐心有限。”

　　“臣……”

　　擡手指了指放在一旁矮几之上被紅綾覆蓋的托盤，道：“明日乃是赭老師生辰，這份禮物，乃是朕預備的，伏嬰你就代朕轉交吧。”

　　“臣，替老師謝過。”雙手捧定了托盤，兩臂微微一沉，好不容易才託穩了，躬身退出。

<br/>

<br/>

　　傍晚時分，從宮中回到府內，伏嬰師換過衣服，看看在一邊陳設良久的魔帝壽禮，臉上仍是一如既往的從容淡定，起身走上，並不將紅綾掀開，便手捧托盤，來到後花園內書齋之外。

　　“小碧，一鴻，吾有事同師爺爺商量，今天散學了，你們外面玩耍吧。”

　　“啊？”探頭看看裡在檐下的父親，又擡頭看著坐在身後，手把手教自己寫字的赭杉軍，雖然眼中已經露出歡喜，然而等了許久，還不得到師爺爺許可，年僅四歲的伏嬰碧又有些失望的低下頭，攥著竹管的小手不停的摩挲著光滑的筆桿。此時坐在一旁，已被看過今日作業的斷一鴻早就雀躍而起，道：“赭夫子，我娘說今晚要親自下廚，讓我帶小碧回家吃飯可好。”

　　“……”赭杉軍偏頭，看看突然闖入的伏嬰師，緩緩點頭道：“也罷，你們去吧。”

　　“哦！”一聲歡呼，斷一鴻已經拉起還有些發呆的伏嬰碧，跳下院內，穿好鞋子，順著花園內白石小路，推開院牆一腳的小門，回到了其實僅一牆之隔的斷風塵大司馬府。

　　“……上來坐。”一面頷首，一面隨手收拾被斷一鴻碰亂的桌案。

　　“老師……魔帝陛下為賀老師生辰，賜下壽禮。”脫鞋登堂，跪坐赭杉軍對面，等到他將面前几案收拾乾淨，雙手捧定了那托盤，輕輕一放。

　　“……打開吧。”隔壁院內傳來孩童嬉笑之聲，赭杉軍聽得悠然，嘴角竟然微微翹了起來。

　　紅綾掀起，一身嶄新朝服整齊疊放，看其上花紋，位極人臣；旁邊，尚有一壺一盞。

　　“……老師，學生不孝，然而魔帝旨意不能不尊，請老師成全！”輕輕提起托盤之上，朝服之旁的青銅酒壺，將其中液體斟入青銅盞內，復又退後，跪倒，額頭緊緊貼著地上軟席。

　　“……”低頭看看桌上兩物，赭杉軍面不變色，平靜出手端起酒盞，一飲而盡！

　　飲下杯中之酒，腦中竟是有了從未經歷過的長時間的空白，隨後，種種往事剎那之間湧上心頭，而最終另留在眼前，歷歷在目的，竟是同對面這人朝夕相處的十年。看著對方緩緩擡起的平靜而專注的面容，赭杉軍竟覺得出離恍惚，而終於下定決心，欲將那從不回顧的歲月最後緬懷之時，一股從未體驗過的灼燒熱流已從腹內急速竄起，似乎只是隨著短短一聲心跳，便已經遍佈全身，四肢骨骼彷彿融化一般，知覺雖在，勁力卻失。

　　“罷了！”雙脣翕動，赭杉軍終於放鬆了精神心思，身體亦軟倒在不知何時湊到身邊的伏嬰師懷中。

　　“老師，今日乃是一個望日，學生服侍老師沐浴。”伏嬰師說罷，橫抱赭杉軍身軀，站起身來，向後走去，同時一聲輕笑道：“老師消瘦不少啊，若是當年，只怕學生尚不能如此抱著老師啊。”

　　……

　　“伏嬰！”水流波動聲中，混扎著赭杉軍微弱的哼聲，隨後又是一聲隱忍不發的呻吟，“這究竟是什麼藥……”

　　水聲再次響起，伏嬰師亦擡腿坐入浴桶之內，“呵呵，學生謝過老師成全。”微微探身，將手邊之物一把握著之時，伏嬰師竟有些想笑了，“溺於淵尤可遊也，溺於人不可救也，當日老師問起，溺於桶中，又可奈何，伏嬰之回答便是，願與老師同溺啊。君之所在即為國，親之所在即為家，孑然一身，如今有老師在的地方，才是伏嬰師的家園啊。”

　　“唔……”赭杉軍只覺得全身更軟，癱在浴桶之內動彈不得，無奈輕輕閉了眼，任由對方壓在自己身上隨意擺佈，突然嘆息一聲，道：“伏嬰……”

　　“老師……”聽到聲音有異，伏嬰師動作一停，“……”

　　“事已至此，顧忌無用……你便……盡興吧。”

<br/>

　　……

<br/>

　　翌日清晨，雖渾身痠痛勞累非常，然而當陽光射在眼瞼之上，赭杉軍出於習慣，還是不得不睜開眼睛。

　　“老師……”微微側頭，看著不知何時已經起身的伏嬰師衣冠楚楚立在榻邊。頭腦中一團混亂，想要理清思緒卻又不敢回想前事，赭杉軍思量半晌，臉露苦笑道：“伏嬰，此時，可以把紫霞之濤給吾了吧。”

　　“老師！”撲通一聲跪倒在地，“老師……弟子罪該萬死，然而恕難從命……”

　　“無妨……吾自取便是……”輕輕閉上眼睛，本想不再說話，卻聽到外面一個急促腳步聲匆匆跑來。

　　“師爺爺！一鴻哥又被劍靈大哥揍了……”稚嫩聲音聽不出是幸災樂禍還是著急忙慌，然而才跑進臥室，就被眼前一幕嚇住，一聲不吭了。

　　“小碧……”再次張開眼睛——那日自己醒來，在伏嬰師懷中熟睡的嬰兒，此時已長這麼大了。

　　“……師爺爺，父親他也默不出書了麼？”不知怎麼嘴裡冒出這麼一句話，小碧怯生生走到榻邊，小手探入被中，抓起赭衫軍的大手，輕輕搖晃，“師爺爺別生氣，父親沒貪玩，就是太忙……”

　　“……小碧，吾……”剛想解釋什麼，面前孩童卻已經“哇”得一聲哭了出來，“小碧……”看見眼淚，赭杉軍心中一急，早忘了身上難受，立刻坐起，將幼童攬在懷中，輕輕哄著“小碧莫哭……”

　　“師爺爺別怪父親，……”小碧一面抽泣，一面還要替跪在一邊的伏嬰師求情，最後竟已抽噎得說不出話來。

　　“莫哭，莫哭……”此子體弱，動輒生病，此時看見那張清秀可人的小臉已經漲得通紅，赭杉軍索性將他抱在自己腿上，輕聲安慰，“……吾……不怪你父親……便是。”

　　“真的？”哭聲頓停，一對晶瑩眸子看著對方，“師爺爺也有白頭髮啦……”突然又冒出一句。

　　“哈……”苦笑一聲，“是啊。”

　　“父親也有，那天我還給他拔了一根……”懷中幼童似乎又想起了什麼，突然掙扎下地，“父親，老師不怪你了，快起來！一鴻又被劍靈大哥打了！你快去找他爹來罵他！你官職比吞佛叔叔大，一鴻說，吞佛叔叔聽你的，你跟他說，叫他罵劍靈，不許他打一鴻！不，要找黥武大表哥，讓他罵劍靈和吞佛叔叔！”

　　“……好……”已忘記被這麼拜託過幾百次了，伏嬰師趁勢站起，澀澀回答一句，隨後瞥了一眼坐在榻邊無奈搖頭的赭杉軍，望定自己兒子，道：“小碧，師爺爺要起身了，給師爺爺拿衣服去！”

　　“是！”蹦蹦跳跳跑向外面，一眼瞥見書房几案之上，一套嶄新華麗的衣服疊放得整整齊齊，“哈，這個好。”孩童偷懶，就近抱起衣服，轉身跑回去了。

<br/>

<br/>

　　玄陵外圍，古樹參天，今年已算暖春，然而黃昏時分，在這山間行走，仍是有些溼涼了。

　　棄天帝早早下馬，徒步走進，靜謐林間，偶有一兩聲斷斷續續捉摸不定的琴音傳來，轉過幾處山坳，雖然浩淼居再度落成之後，他從未來過，然而見到那熟悉宅院，卻也知道這一次自己並未走錯。

<br/>

　　“你……來了……”身後院門吱嘎一響，雖然不在一起生活已是四年之久，但那腳步聲不僅依舊熟悉，更能聽出來人心緒。

　　“蒼……”聽到對方聲音，通身竟是一震。

　　“棄兄……”

　　“赭老師他昨日向吾稱臣了。”

　　“……”撫琴的動作終於一停，沉默良久，道：“大哥治國賢才，更能與伏嬰師兩相配合，互補不足，天下之幸。”

　　“蒼，吾想你也……”

　　“蒼之所為，於魔於玄，並無一利於蒼生百姓，再無面目立於朝堂之上了。”

　　“……無有你，便無今日之吾啊！”

　　“天命有數，魔帝切莫妄自菲薄，蒼在此守陵，只為贖罪，別無他想了。”

　　“要到何時，才能贖清？”

　　“……待到百姓安樂，天下生平之際。”

　　“好，那時吾再來找你！”

　　“棄兄……”

　　“吾曾立誓，帶你重返封雲城顛，看來此願難遂；然而吾對你還有一諾，今生勢必達成！”棄天帝說罷，轉身而去。

<br/>

　　“嗯？”微微回頭，看著尚在微微晃動的院門，滿臉盡是詫異之色。

<br/>

　　緩緩抹去算天河送來請自己審閱的《戒神寶典?棄天帝本紀》最後一段話，伏嬰師略微沉吟，嗤笑一聲在一旁留白處寫上：

<br/>

　　“天隆十年，魔太祖棄天帝退位，不知所蹤，諡桓。純皇后蒼氏無子，故留詔傳位皇侄黑羽恨長風，改元承運。”

<br/>

　　之後，伏嬰師看看大雪飄飛的庭院之內，又被赭杉軍罰跪廊下的斷一鴻與伏嬰碧，無奈一嘆，再度將案頭辭呈丟入了火爐。

<br/>

　　“……卻不知，神無之後又是何地啊？”

　　“哈，待天下安定，棄願與老師前往一探。”

<br/>

（全文完）
