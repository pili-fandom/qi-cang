---
title: 十戒
layout: post
lang: zh-Hant
category: ten
depth: 2
date: 2011-03-21 00:00:34
author: 文心玲
---

### 回三十四
蒼在棄天帝的浴室裏洗澡，棄天帝能聽見蓮蓬頭沖水的聲音，他回想起水珠在蒼身上緩緩流下的樣子，不免臉紅心跳。

對棄天帝而言，爲了性愛而臉紅心跳，是青澀少年的專利。

十七歲後，他早已不會爲了此事而心生悸動。

但每當面對蒼時，他感覺他心底最真實的感情都泉涌而出，他希望讓蒼看到他最完美的一面與最脆弱的一面，如果可能不留一絲隱瞞。

他知道愛情是看似堅強的水晶玻璃，經不起任何一絲不安定的縫隙，他必須小心翼翼地保護不使成長中的愛情破碎。

蒼拉開噴霧玻璃門，棄天帝的笑臉正等着他，蒼接過棄天帝遞來的大浴巾，將全身包裹起來。 

他想找吹風機吹乾頭髮，手上的梳子卻被棄天帝搶走。

『歡迎光臨棄天帝理髮廳，客官請讓我爲您服務。』

棄天帝似模似樣的管家樣半鞠躬，蒼想了想便順水推舟，樂得享受他的服務。

『五星級服務？』 

棄天帝微笑。

『只要您想要，特別服務也沒問題。』順道輕舔他的脣瓣吃豆腐。 

蒼開始思考愛舔人的棄天帝是貓科動物還是溫馴的大狗？ 

他看着棄天帝的黑髮，忽然聯想到德國狼犬，但德國狼犬會傻笑嗎？ 

會不會是哈士奇？

有着異色雙瞳的棄天帝哈士奇……

蒼瞄着正專心爲他吹乾頭髮的棄天帝，幻想在墨發加上狗狗耳朵的樣子，不由得輕笑出聲。 

『你想到什麼？』 

棄天帝疑惑的看着蒼，蒼卻轉開視線不讓棄天帝發現他的胡思亂想。 

不知爲何，與棄天帝單獨相處時，蒼感覺自己的思考模式不再嚴肅。

就好象找到了個能安心停泊的港灣，不需再強迫自己成熟與堅強，能放縱平日隱藏着的孩子心性恣意胡鬧。

蒼想着想着，表情不自覺的柔和起來。

棄天帝手捧着蒼的栗色髮絲，吹風機轉到最小風量的刻度，自發根到發稍輕柔吹乾，他放開手裏的長髮，披散而下的栗色長髮閃耀着令他迷醉的光彩。

蒼想起身卻被棄天帝按下，棄天帝的手掌不輕不重的按摩着蒼的肩膀，蒼閉目感受他的額外服務。

棄天帝的手指如同有魔力般，按壓的每一個點力道都剛好到位，不痛但足以令蒼舒服得輕喊出聲。

棄天帝把蒼的表現當作對他的稱讚，微笑着吻上蒼的脣瓣索取回報。

然而程咬金總在不恰當的時間破壞浪漫。

門外輕輕響起敲門的聲音，棄天帝只得起身開門。

『等我。』

棄天帝對着拿起衣物正要走到浴室更衣的蒼說道。

蒼隨意回了聲嗯，便走進浴室。

一直到浴室的噴霧玻璃門關上，棄天帝才打開房門，門前站着的卻不是他料想的伏嬰師，而是蒼的弟弟翠山行。 

翠山行將劇本塞到棄天帝懷中，對房內喊道。

『哥，你的劇本我拿給棄天帝，你到片場前一定要再讀一遍背熟臺詞。』 

蒼在浴室裏急忙穿好衣服，衝到門口。 

『你不跟我一起回片場？』 

翠山行瞄到蒼未來得及扣好的衣領，隱約可見鎖骨處深深淺淺的紅痕。

『伏嬰師說爲了安全起見讓我坐斷風塵的車子走，剛好斷風塵就是我上回跟你說過的網遊朋友，我也想跟他多聊一些，而且—』

翠山行神祕兮兮地在蒼耳邊悄聲說道：

『太陽神開車技術太糟了，我還不想暈車。等會你最好別讓太陽神開車，棄天帝的技術可能比較好。』

蒼亦小小聲說道： 

『他們兄弟倆技術都一樣糟糕，要不我與你一起逃走好了。』

棄天帝原本想裝作聽不見很大聲的悄悄話，但聽見這句真的忍耐不了。

他一把圈住蒼的腰。

『我允許你逃跑了嗎？』

蒼陪笑。『我開玩笑罷了，別太認真。』

說是這樣說，但蒼不排除捉住棄天帝手臂然後以擒拿手反制住他的可能，只不過這也需要時機配合。

但時機就是相當的不湊巧，翠山行的手機正在此時響起，蒼看着翠山行接電話時笑得很溫柔，他頓時懂了自己不該當電燈泡。

弟弟也是有長大的一天，或者說，總有被拐走的一天。

蒼感慨地看着翠山行，翠山行卻被他一臉感動的表情看的莫名其妙，他知道他的哥哥又開始亂想一堆有得沒有的事情，他轉向棄天帝說道：

『好好照顧我哥，如果你的開車技術不佳讓我哥暈車，你就仔細點你的皮！』 

交待完畢後翠山行的手機又響起，接着戴着淺藍蝴蝶帽子的斷風塵就出現在他們眼前。 

『你來了，這是我哥，蒼。棄天帝你認識吧，就不必介紹了。』 

斷風塵微笑。

『蒼，雖然我不是第一次見到你，不過你是第一次見到我，幸會幸會。』 

蒼猛地從弟弟身披婚紗的幻想中醒來，他快速打量眼前據說是水果日報總編的爾雅男子。

他憑直覺判斷斷風塵絕非外表看起來的良善，那眉宇之間隱約透着肅殺氣息。

『水果日報總編輯斷風塵先生，很高興認識你。』 

蒼瞇起眼，他伸手握住斷風塵禮貌性握手的手掌，然後平穩加重施力，斷風塵被迫只好也出力握掌。

普通的寒暄性握手，就這樣硬生生變成握力較勁。 

棄天帝微笑着欣賞蒼過人的本事，翠山行卻看得直冒汗，傷了哪一個他都不樂見。 

最後，翠山行裁判強制介入握力比賽，宣判平手。

『男大不中留啊。』蒼看着翠山行與斷風塵的背影，感慨說道。

棄天帝忍不住笑出聲。

『你在吃斷風塵的醋嗎？怕他搶了弟弟？』 

蒼垂下眼簾。 

『也許我只是害怕寂寞，如果翠山行也離開了，我就只剩一個人而已。』 

身爲玄宗經紀公司大師兄，身旁一直有師兄妹與朋友陪伴，經常給人達觀與堅強形象的蒼，其實最害怕寂寞。

如果沒有了最親的翠山行弟弟，他的生活將只剩他一個人，他不知道該如何面對寂寞。 

棄天帝圈在蒼腰上的手臂圈得更緊，他的氣息吐在蒼耳畔，他的體溫給蒼一種溫暖的感覺。

『你還有我。』 

蒼回眸，他感覺到愛情的確是這個世上最甜蜜的事情，能讓他的心瞬間安定，甜得不知糖蜜是何種滋味。

他放任自己的心相信棄天帝的甜言蜜語，然後，在棄天帝的溫柔目光裏沉淪。

從房間到飯廳，用完餐後他們坐上伏嬰師的休旅車，一路上蒼都沒有再說話，只是看着劇本默揹着動作與對白。

但蒼戴着金色小翅膀戒指的右手緊緊地與棄天帝十指相扣，棄天帝看着身旁正看着劇本的蒼，揚起幸福的笑臉。

他從不知道，心中住着一個人的感覺是如此無法言喻，比起得獎時功成名就的快感幸福幾百萬倍，他很想牢牢握住手中的幸福，不讓它有機會溜走。 


