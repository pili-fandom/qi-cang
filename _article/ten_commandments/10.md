---
title: 十戒
layout: post
lang: zh-Hant
category: ten
depth: 2
date: 2011-03-21 00:00:10
author: 文心玲
---

### 回十
人在舞池中還有拒絕的空間嗎？ 

蒼沉默了一秒，而後他淺笑道： 

『很抱歉，我不會跳女伴的舞步。』

他乾脆地拒絕轉身欲離去，棄天帝戴着黑綢緞薄手套的手卻拉住他的，他的手使勁拉回卻正中棄天帝下懷，順勢回步將他攬入懷中。

ＤＪ以麥克風公告播放曲目：Dolly Parton演唱的Sandy's Song 

舞池中迴盪着柔和的英文歌曲，衆人隨之優雅起舞。 

Sometimes I think of you，and tears fill my eyes 

棄天帝異色雙瞳閃耀着魔魅光彩，他脣角微勾。 

『蒼，你害怕跳舞？』

蒼使力推開他的懷抱，但另一手掌卻被他緊握着，他無奈轉身欲以太極圓以力推力，棄天帝亦反向旋轉引導，一離一合間卻恰似愛侶依依不捨地相聚再暫離，他的圓與他的緣兜兜轉轉終究又轉在同一頻率。 

think of the meaning you've given my life 5

蒼輕仰頭使他與他視線平齊，手臂順音樂舉起巧妙拍掉棄天帝置於他腰上的手掌。 

『害怕？我可不懂。』 

他自信微笑，抽離他的手，在舞池中央展露高難度三連自轉舞技。

You've touched me in places no one ever reached 

棄天帝亦跟上他的腳步，輕輕托住他的手，兩人腳步或柔或慢，一旋轉一拋接，互相較勁着卻又配合得天衣無縫。

相對於棄天帝一身闇黑西裝，蒼穿著簡約風格淺紫合身長襯衫，搭配僅於褲腳繡上象形圖騰的貼身黑長褲，舞蹈時淺紫長衫衣角隨旋轉而飛揚，揚起似夢如幻的優美弧度。 

原本在舞池中慢舞的人們漸漸退開，留下兩人恣意慢舞，蘭漪章袤君索性讓燈光全打在他們身上，爲酒吧裝潢華麗的舞池作免費宣傳。

舞池旁隱隱閃爍紅色光芒，棄天帝注意到光芒來源，他笑着攬住蒼的腰身。

蒼看着他的眼神帶着疑問，棄天帝在他耳邊輕聲道：

『有些小小麻煩，不必擔心還在我能力範圍內，相信我。』

You've given me reason and cause to believe.

他輕聲哼唱着英文歌詞，帶着蒼迴轉再回轉，恰好閃過被音樂蓋過的細微槍聲。

You are my rainbow; you've colored my life.

他壓膝而後靈巧彈起，蒼順着他肩部動作下腰，栗色長髮流瀉而下，在舞廳七彩燈光下映照着虹彩。

眼見暗槍沒有奏效，幾抹黑色人影自舞池旁竄出，快速靠近兩人。

And you are my sunshine; I'm warm in your light. 

『轉圈。』

棄天帝伸手將蒼放開，蒼順勢旋轉到舞池邊上，但黑影瞄準的對象卻不是棄天帝，三人迅速向蒼襲擊而去。 

You are my fountain that never runs dry. 

蒼交叉腳步回身避過槍擊，長腿揚起踢倒兩名狙擊手，另一名已被棄天帝撂倒。

『需要活捉他們審問幕後主使者嗎？』

『不必。』 

蒼淡然答道：

『我想他們大概是找錯人了。』 

You're my inspiration, my reason to try. 

棄天帝看着蒼的紫眸，紫眸坦然而淡定，他微笑。 

『既然阻礙解決了，我們跳完這隻舞吧。』

I'll love you 'til green grass turns lavender blue. 

音樂漸升時他順着音樂悠然舞蹈，置於蒼腰上方的手掌輕柔卻又不放鬆，漸漸拉近兩人間的距離。

And all the stars fall from heaven and vanish like dew. 

蒼眨眨眼，猛然推開他，徑自表演華麗炫目的自轉旋圈。

'Til horses and chariots chase down the wind. 

舞池畔掌聲不絕於耳，卻不知蒼只是以高難度表演爲被擊倒的三名狙擊手爭取時間，他每個循環舞步都擋住棄天帝以槍口瞄準狙擊手的角度，讓狙擊手順利逃出舞池範圍。 

That's when I'll leave you, I'll love you 'til then. 

一曲舞曲將要終止，棄天帝緩步上前，他邀請蒼爲此舞作一個完美的結束。

蒼的手掌託在他的手心上，手臂穩穩置於他的肩上，但重心卻仍在蒼自己身上，正如他的心一般，總在四處旋轉，像是處處留情，卻更像從未有一人能讓他真正上心。 

棄天帝一直都知道這件事，卻由着他放縱，他喜歡他但卻不需束縛他的心，他愛他的自由。

即使終有一天他將要離去，他依然會愛他如昔。

That's when I'll leave you, yes Gazva

I'll leave you then 

I'll need you 'til then 

I'll love you 'til then... 



