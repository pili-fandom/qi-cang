---
title: 十戒
layout: post
lang: zh-Hant
category: ten
depth: 1
date: 2011-03-21 00:00:00
author: 文心玲
subdocs:
  - title: 回一
    link: /ten_commandments/1/
  - title: 回二
    link: /ten_commandments/2/
  - title: 回三
    link: /ten_commandments/3/
  - title: 回四
    link: /ten_commandments/4/
  - title: 回五
    link: /ten_commandments/5/
  - title: 回六
    link: /ten_commandments/6/
  - title: 回七
    link: /ten_commandments/7/
  - title: 回八
    link: /ten_commandments/8/
  - title: 回九
    link: /ten_commandments/9/
  - title: 回十
    link: /ten_commandments/10/
  - title: 回十一
    link: /ten_commandments/11/
  - title: 回十二
    link: /ten_commandments/12/
  - title: 回十三
    link: /ten_commandments/13/
  - title: 回十四
    link: /ten_commandments/14/
  - title: 回十五
    link: /ten_commandments/15/
  - title: 回十六
    link: /ten_commandments/16/
  - title: 回十七
    link: /ten_commandments/17/
  - title: 回十八
    link: /ten_commandments/18/
  - title: 回十九
    link: /ten_commandments/19/
  - title: 回二十
    link: /ten_commandments/20/
  - title: 回二十一
    link: /ten_commandments/21/
  - title: 回二十二
    link: /ten_commandments/22/
  - title: 回二十三
    link: /ten_commandments/23/
  - title: 回二十四
    link: /ten_commandments/24/
  - title: 回二十五
    link: /ten_commandments/25/
  - title: 回二十六
    link: /ten_commandments/26/
  - title: 回二十七
    link: /ten_commandments/27/
  - title: 回二十八
    link: /ten_commandments/28/
  - title: 回二十九
    link: /ten_commandments/29/
  - title: 回三十
    link: /ten_commandments/30/
  - title: 回三十一
    link: /ten_commandments/31/
  - title: 回三十二
    link: /ten_commandments/32/
  - title: 回三十三
    link: /ten_commandments/33/
  - title: 回三十四
    link: /ten_commandments/34/
  - title: 回三十五
    link: /ten_commandments/35/
  - title: 回三十六
    link: /ten_commandments/36/
  - title: 回三十七
    link: /ten_commandments/37/
  - title: 回三十八
    link: /ten_commandments/38/
  - title: 回三十九
    link: /ten_commandments/39/
  - title: 回四十
    link: /ten_commandments/40/
  - title: 回四十一
    link: /ten_commandments/41/
  - title: 回四十二
    link: /ten_commandments/42/
  - title: 回四十三
    link: /ten_commandments/43/
  - title: 回四十四
    link: /ten_commandments/44/
  - title: 回四十五
    link: /ten_commandments/45/
  - title: 回四十六
    link: /ten_commandments/46/
  - title: 回四十七
    link: /ten_commandments/47/
  - title: 回四十八
    link: /ten_commandments/48/
  - title: 回四十九
    link: /ten_commandments/49/
  - title: 回五十
    link: /ten_commandments/50/
  - title: 回五十一
    link: /ten_commandments/51/
  - title: 回五十二
    link: /ten_commandments/52/
  - title: 回五十三
    link: /ten_commandments/53/
  - title: 回五十四
    link: /ten_commandments/54/
  - title: 回五十五
    link: /ten_commandments/55/
---

#### 作者：文心玲

原載於[http://www.36rain.com/read.php?tid=74935](http://www.36rain.com/read.php?tid=74935)

---
