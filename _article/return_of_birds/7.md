---
title: 倦鳥之還
date: 2018-01-01 7:00:01
layout: post
lang: zh-Hant
category: birds
depth: 2
---

### 7.

蒼並未把他們的賭局當真，棄天帝也沒有表現出半分要放他走的意思，此時與神發生衝突不划算，他對自己的處境有清醒而理性的認識。

在他不知曉的另一面，棄天帝卻覺得這人類表達激烈的情緒時有種特別的生動。他很專注，常有意外之舉，認知之外的事物總能引發好奇，對人或對神都是同樣的道理。他休息的時候，棄天帝從黑暗裏看他，蒼疊好外套枕在頸下，從容地跟他道過晚安，闔上雙眼。棄天帝走上前，安靜地坐在他身邊，人類坦坦蕩蕩毫無防備的樣子彷彿某種屏障，就算是神也會覺得怎樣再接近都是逾矩。

棄天帝翻看蒼的筆記本，他的字跡大體流暢雋秀，只有速寫的幾頁潦草得難以辨認。神對語言和文字無師自通，他很快認出日期、時間、種群——不知道蒼是否學過繪畫，鋼筆草圖上的動物只有大體輪廓，特徵卻非常明顯：鸌鳥的鼻管結構、信天翁的尾羽、海鸚巨大的喙。蒼把三色圓珠筆夾在封面上，棄天帝摁出一支筆芯，翻開空白的一頁，凝視片刻，復又合上。

他居然會對人類的東西感興趣，或者說，他居然想知道人類爲什麼對這種東西的興趣超過對自己的興趣。蒼用手機做倒計時，讓自己依舊維持24小時的生物鐘，屏幕閃爍一陣之後熄滅，棄天帝搖了搖頭，將筆記本放回蒼身邊原來的位置，和息屏一起消失了。

無所謂晨昏晝夜之處，維持原有生活的尺度及標準——比如計時——本就是種無聲的對抗。

棄天帝在試探自己，這是毋庸置疑的。固有習慣中的“早晨”，蒼盤膝而坐，新寫的日程表被他放在膝蓋上。先耐不住性子的一方會輸，他也想知道神耐性的底限在哪裏，可惜他以前完全沒有和神打交道的經驗。蒼用了幾天時間充分考慮自己在幽若關要做的事，如果真如棄天帝所說，死亡是唯一的結局，那他也要充分考慮死亡的價值。

“需要我再提醒你嗎，不論是日程還是計劃，你都無法帶出這個空間。”

棄天帝毫無徵兆地出現了，神高大的陰影籠罩在萬年牢盡頭。

“無論你是否提醒，它們都只在這裏才有意義。”

棄天帝無視蒼的回答，他在蒼面前坐下，寬大的袍袖拂過紙面，蒼任由他看那些內容，沒有再說話。

“你的日程裏沒有我。”

是啊是啊，蒼有些無奈地想，對於某些想來就來想走就走行蹤詭譎難以預計的神來說，出現在人類的日程裏可能是一種冒犯。

“如果你認爲神可以被規劃，蒼不介意爲你增添一席之地。”

“嗯？”棄天帝揚起手，呼嘯的風掃過蒼的耳畔。他閉上眼睛，等待神的怒意降臨；但風稍縱即逝，最後什麼都沒有發生。

“怎樣？”蒼有些奇怪地看着棄天帝，後者危險地注視自己，脣角居然扯出一抹笑容。

“算了。”

這樣喜怒無常，着實令人難以應對，或許以不變應萬變是最好的選擇。蒼從口袋裏摸出一塊水果糖，想了想，又摸了一塊遞過去給棄天帝。

“這是什麼意思？”

“甜味可以使人保持冷靜，對你應該也沒有害。”這次是芒果味，蒼看着對面的棄天帝則一臉警惕地剝開糖紙，抓準時機問道，“神和人類擁有同樣的味覺嗎？或者說，神會不會像某些生物一樣，對人類的食物有忌口？”

棄天帝明顯被噎了一下，半晌才說：”蒼，你的問題太多了。”

“對不起，但好奇是人類進步的要素之一啊。”

“人類也有句話叫好奇心殺死貓。”棄天帝嗤笑一聲，“既然你在計時，從現在開始，每個計量單位你只能問一個問題。”

“一個太少了。”蒼果斷表示抗議，“而且你一定會回答？不會刻意引導一個錯誤的答案？”

“那就三個。在遵從內心這一點上，神要比人類坦誠得多。”

“好吧，我下次會斟酌。”蒼伸手撿起被棄天帝扔在地上的糖紙，和自己的那張一起，很快折成兩隻鳥的形狀。玻璃糖紙的構成的鳥靜靜停在兩人中間，一隻看起來更加纖細，另一隻則是振翅欲飛的樣子。

“這一隻的翅膀可以動。”棄天帝指着蒼創造出來的鳥，“但這一隻不行？”

“折法不一樣。”蒼有點好笑，“當然狀態也不一樣。”

“你再演示一遍？”

“不必，區別只在這裏。”蒼拆開纖細的那隻，示意棄天帝看其中一步，“如果把它的翅膀折起來，它會更好看些，但相應地，就不會再動了。”

棄天帝若有所思地對比蒼放在他手上的兩隻摺紙鳥，異色雙瞳中閃過一絲複雜的情緒。

“怎樣，有感而發了？”

“神不會有那種膚淺的東西。”棄天帝矢口否認，“倒是你，試圖影響神是危險的行爲。”

“我有嗎？”蒼轉過身，“這只是你的主觀認知。”

