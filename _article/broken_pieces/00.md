---
title: 斷章三則
longtitle: 《青宮客》同人相關  斷章三則
author: 千狐祭月
category: broken_piecees
layout: post
date: 2009-06-30
lang: zh-Hant
depth: 1
toc: true
---

#### 作者：千狐祭月

---

### 其一 結髮

<br/>

這一天晚上，因爲棄天帝的一時興起，蒼是在天魔殿過夜的。

棄天帝半坐半躺，後背靠上床柱，蒼被他習慣性地摟在自己懷裏。帳幔垂下，近乎封閉的空間內，兩人一時無言。

這種微妙的氛圍卻在下一刻被棄天帝打破，空着的那隻手略動了動，一道氣勁破風而過，棄天帝和蒼的長髮各自斷了一截，落在華美的黑色織錦被面上。

蒼看了他一眼，卻並沒有開口，面上一派平靜。

棄天帝也沒說話，也沒再有任何動作，只是盯着蒼看，蒼卻沒有對這灼人的視線有任何反應。

棄天帝終於先開了口：“你不問朕要做什麼，難不成你那觀測天機的能力連這都能預知到？”

蒼毫不畏懼地與他對視：“修道之人並無許多的好奇心，而且……”

“而且什麼？”棄天帝的聲音中含着危險的意味。

“魔皇是何等任性自我的存在，你是否會說，從來只取決於你想不想，與我問不問根本無關吧！”

“哈！”棄天帝大笑了一聲，對蒼說道，“今天苦境派了人來……”

“這事我知道。”蒼看着他，仍是沒什麼興致的樣子。

棄天帝也不怎麼介意，“那使者無趣得很，不過他腰上配的飾物倒很特別，是用兩種不同顏色的頭髮打成的結。你的頭髮也不錯，你整個人都是朕的，朕拿點頭髮也是理所當然。”，他邊這樣解釋着，邊將自己和蒼的兩束頭髮系在一起打成一個結。

蒼的神色終於有了波動，他的眼中有什麼一閃而過，說不清是痛苦還是無奈。

“原來魔界是沒有這種習俗的嗎？還是單純是你不屑去注意？”蒼還是開了口，而映入他眼中，安置於棄天帝手掌上的，是棄天帝剛剛打好的那個結——歪歪扭扭的，一點也不美觀，卻系得死死的，如他們糾纏不清無法分割的命運。

“什麼習俗？”棄天帝這才發覺事情可能不如他認爲的那麼簡單。

“結髮同枕蓆，黃泉共爲友。道苦兩境都有習俗，在婚禮之上將兩位新人的頭髮系在一起打成結。這個習俗已經流傳了很久，所謂結髮就是結爲夫妻的意思。”

“哦，”棄天帝看着蒼，聲調微微上揚，“這倒是有些意思。”

他這樣說着，將那個髮結牢牢抓在了手裏。

<br/>

<br/>

### 其二 望月

<br/>

滿月如盤，清輝似水，這是蒼在青宮度過的第一個中秋。

蒼坐在石桌旁看着一本古籍，魔國的仲秋之夜其實寒氣頗重，但蒼功體深厚便也不在乎這些，仍是專心致志地翻動書頁。

棄天帝來的時候，看到就是這幅景象。

再怎麼聚精會神，蒼也不會對棄天帝的到來毫無所覺。他放下書，起身：“魔皇駕臨，有什麼事嗎？”

棄天帝將自己帶來的精緻盒子放在石桌上，蒼打開，裏面竟是幾塊精工製作的月餅。

“這是……”

“朕叫人去來焰都做生意的道境商人那裏買的，今日是你們人類的中秋，按照習俗是要吃這個叫月餅的東西的，對吧？”

“你……”蒼頓了頓，“多謝有心了。”

棄天帝卻似乎被噎了一下，冷聲道：“順便而已。倒是你們人類，這亂七八糟的習俗也太多了一些。”

蒼卻沒因他這話有什麼憤怒的表現，只是說道：“節日和習俗，總是有其來歷和原因的。你要聽我講中秋的傳說嗎？”

一聲冷哼， “打發時間也好。”

於是蒼泡了茶，和棄天帝一起坐在石桌旁爲他講故事。

一刻過後，故事講完。

爲棄天帝和自己續了杯，蒼問了一句：“怎麼樣，對后羿和嫦娥的故事有什麼感想？”

（蒼講的是嫦娥爲了不讓惡人逢蒙奪得仙藥而吞下仙藥被迫飛昇的版本）

“弱者無聊的自我安慰而已。朕想要的，一定會牢牢抓在手裏，不會做什麼寄情於物的無用之事。”

“是你的風格，”蒼顯然沒指望這一位此刻能有什麼感悟，“但不是人人都有你那般幾乎壓倒一切的力量。而且……”

“而且什麼？”

“而且即使力量強大如你，也總有不能壓制、扭曲和毀滅的事物。”一如人的信念和情感；一如天道循環，永不停息。

“朕看不到這些東西的存在。”

“你總會有收回這句話的時候。”

棄天帝卻沒有動怒，也沒有就這個問題繼續說下去。再說下去兩人難免僵持，而他此刻卻並不想如此，雖然連他自己也不知道爲什麼。

於是棄天帝說出了在他腦中盤桓許久的疑問。

“中秋對你們人類來說，是團圓之日？”

“是。”

“在你的同門團聚一堂的時候孤身呆在這青宮，不會不平嗎？”棄天帝的聲音中，有着某種惡意。

蒼擡頭，月色溫柔皎潔，彷彿是同門們的目光。

他閉眼，再睜開時目光柔和而堅定。

“至少我們還可以看着同一輪圓月；至少，我在這裏的話，他們能夠安然相聚在這滿月之夜下的機會，能夠大一些。”

“人類所謂的高尚情操嗎？”

蒼不語，銀色月光下，道者愈發顯得飄然如仙，似乎隨時可飛昇而去。

棄天帝看着這樣的蒼，心裏突然就不太舒服，反應過來時，那人已經被他牢牢鎖在懷抱之中。

“怎麼？”

“既然有自我犧牲的決心，就該對未來有清醒的認識。無論生或死，你都不可能離開這座宮殿；即使你說的那種仙藥確實存在，朕也不會讓你有吃下去的機會。”

蒼在棄天帝懷裏露出一個苦笑，“蒼從來不抱不切實際的希望。”

而圈住他的懷抱收緊，帶來輕微的疼痛，宣示着懷抱主人對他的所有權。

隨着這個懷抱加諸蒼身上的，還有棄天帝的體溫。

熾熱的，與棄天帝冷絕無情的性子有着大到近乎荒謬的差別。

“若有一日我必須死去，希望我的死亡能夠換來這個懷抱永遠失去溫度。這不算什麼不切實際的希望吧？”

蒼望了一眼月亮，想起道境那個中秋夜對着滿月許願就會心想事成的傳說，閉上了眼睛。

<br/>

<br/>

### 其三 琴願 上

<br/>

夕陽西下之時，三十多歲的術師終於結束了爲期三月的旅行回到了家中。迎接他的，是父親比往常更急切許多的面容。

“出什麼事了，父親？”

“我兒啊，你也知道，異度魔族民風比較粗獷，琴這種樂器並不流行。但我少年時遊歷道苦兩境，偏偏就被那裏的朋友帶得迷上了琴。”

“這我當然知道，您一向很寶貝珍藏的那張古琴，我記得我小時候調皮，有一次偷偷去撥弄那張古琴，被您逮了個正着後捱了好一頓打。”

做父親的卻沒有什麼回憶兒子童年趣事的興致。

“問題就出在那張琴上。那一天，我突然聽到極優美的琴聲響起，咱們家哪裏來得那麼好的琴師？我以爲是來了什麼雅賊，見了琴好連先偷走都忘了就彈上了。結果進到書房一看，若真是雅賊在彈倒罷了，偏偏什麼人也沒有，就看到琴絃自己在動，好聽是真好聽，但這種情況應該叫做鬧鬼吧？”

“確實……這種事發生了多久了？”術師心理也有些緊張，畢竟他的經驗並不是很足，也是第一次遇上這種情況。

“到今天正好是第七天，那位看不見的樂師每天都來，但只是演奏一首曲子，除此之外就沒有任何異象發生了。今天他彈完的時候，正好是你回來的半個時辰之前。”

“聽起來似乎並不是來作祟的鬼怪……”

“雖然我也覺得能演奏出那樣美妙的音樂的不會是什麼惡鬼，但是鬼怪的想法我又怎麼能確定呢？不管怎麼說兒子你先想辦法弄清彈琴的鬼長什麼樣吧。”

“我明白，父親。雖然那鬼很可能已經離開了，但我自有術法可以捕捉他的影像。”

術師於是取了法器開始作法，嫋嫋升起的煙霧中父子二人摒棄凝神，看着琴桌旁一點點浮現出一個端坐的似實還虛的身影。那“人”有着淺栗色的頭髮，穿着紫色底上面用銀線繡着鳳凰的禮服，卻梳着頗有道門風格的髮髻，白皙的額上一道硃紅蜿蜒。他的氣質高貴而脫俗，不似鬼魂，倒更像謫仙一些。

“父親，我們接下來該怎麼辦？”

“什麼也不用做了。”

“這事若是發生在別人家或許搞不清楚是誰，但是咱們家卻是情況不同。你仔細回憶一下你高祖父留下的手記就該明白了。琴技極爲高超，穿着異度皇后的禮服，卻有着和咱們魔族完全不同的氣質，這是你的高祖父曾經短暫侍奉過的那位皇后啊！”

“若是那位的話，三境史書對他的記載雖然頗有隱晦之處，但就已知的部分來看，也不是會死後作祟的人物。”術師明白了父親的意思，“看來只是來彈琴的啊。就算不是，我也絕對動不了他，不如順其自然。”

“明白就好。這件事也說得上是一件奇妙的經歷了，着實值得記上一筆，流傳後世呢。”

<br/>

<br/>

### 其三 琴願 下

<br/>

時辰將近子夜，生人的世界已經沉入最深沉的黑夜的懷抱，而異度歷史上最強大的魔皇陵墓的地宮裏，一人一魔的兩道魂靈正在對峙。

“你去哪裏了？這幾日每次朕修煉，你都會出去。”棄天帝的語氣很是不快。

“並沒有去哪裏，不過是想試一下能不能夠走出焰都。”蒼的回答很是隨便，並不覺得眼下的狀況需要特意打起精神來應對，“雖然還是沒有成功，但是卻遇上了一件有趣的事情。”

“或許那只是你們人類眼中的有趣。”

“…… ……”

“我路過一戶人家的時候，聽到有聲音呼喚讓我停留。”

“哦？我們不是不能被這座陵墓外的凡人感知到嗎？”

“所以不是凡人，而是古琴，因爲年深日久而有了靈氣產生了自我意識的古琴。若只是普通的琴，我也無法觸摸並彈奏它。”

“彈奏？”

“那琴呼喚我，正是因爲它感應到，我是符合它要求的彈奏者。”

“一張琴而已，要求還挺高。”

“它說它已經很久沒有演奏出足夠美妙的樂音了，現在的主人對他極爲愛惜，但技藝卻只是勉強能稱爲普通而已，所以希望我能彈奏它。”

“所以你不僅這幾日天天出去，以後也要每日去赴一張莫名其妙的琴的約會？”

“你不必如此不滿，”蒼輕笑了一聲，

“那張琴的製作者將它製出時已經病入膏肓，只彈奏了七天就去世了，所以它也只要我連續七天每天彈一首曲子，今天正好是第七天，我明日便不會再去。對了，我在那家遊蕩時發現，那戶人家是葬送千秋的後代。這實在是很巧，不是嗎？”

“哈。”棄天帝意義不明地感嘆了一聲，一人一魔便相對着陷入了沉默。

片刻過後，棄天帝又開了口。

“幫助那張琴完成心願，讓你感到很滿足？”

“那是自然。”

“那你自己的心願呢？會有人替你完成嗎？”

棄天帝的異色雙瞳在夜明珠的幽暗光輝中愈發顯得妖異並充滿惡意。

“當你想方設法封印魔龍並與朕同歸於盡時，一定想要早些投胎轉世擺脫朕吧？可是你如今仍與朕一起陷在這種境地裏不得解脫，蒼你有什麼感想嗎？”

當年魔龍的被封在天地間產生了一些玄妙而微小的影響，於他人並無妨礙，但棄天帝與蒼隕落之時恰逢一個千載難逢的特殊時辰，兩種異況交會作用之下，棄天帝和蒼的魂體被牢牢牽繫在已經失去生命的人世軀體之上，至今不得進入冥界，投入輪迴。

而這，也就是棄天帝所說的“這種境地”。

“我當初的心願，不過是消滅戰亂，保護人類與玄宗平安，這個心願我死之時就已經達到了。至於早些投胎之類的，我生前沒有閒暇去想，死後也沒什麼執念，那一碗孟婆湯，喝不喝都沒什麼要緊。

而且，不能投胎，便等於可以繼續以‘蒼’的身份存在，懷抱着屬於‘蒼’的記憶與感情前行；這樣，或許反倒是上天對我的體諒。”

“也是。”棄天帝冷笑，

“你一直在努力確定我們到底能離開陵墓中的骸骨多遠，看來是想有一天回到你的道境去吧。可朕和你有整整九九八十一年連地宮都出不了，之後雖然能出去了，並隨着時間流逝可以越走越遠，也耗費二十年時間才能在焰都自由行走，卻連一步都跨不出焰都。誰知道還需要幾百幾千年你才能跨過道魔邊境，也或許魂體離開屍身的距離有其極限，你根本永遠都到不了道境。”

他看着蒼，蒼臉上平靜的神色讓他覺得沒有收到預期的成果，於是繼續加大火力。

“就算到了道境又怎麼樣呢？現在才不過一百年，你的親朋故舊就已經死了個乾淨。等你到了道境，只怕幾千年都過去了，別說當初的道士了，連當初的道觀都未必找得到。”

他掃了蒼只有在這座墳墓中才能凝成實體一旦出去就變得半透明的魂體一眼，“到時候你站在陌生的地方，周圍全是不認識的人，他們不但看不見你聽不見你說話，走路時還會從你的身體裏穿過去，你覺得那樣會很快活？”

棄天帝所描述的，實在是很悲慘卻很可能發生的一種情景，然而蒼卻露出了真心的微笑。

“只要能踏上那片土地，我就已經很滿足了，其他一切都不重要。何況事情並不是只有壞的一面。即使故景難重現，故人難重逢；玄宗的精神、道家的精神，依然能夠世世代代傳承下來。而我，也許會在一張或熟悉或陌生的臉上，看到故人精魂。這就已經足夠好了。”

棄天帝看着這樣說着的蒼，看他栗色的長髮，硃紅的額印，蒼藍的眼睛與含笑的淡色雙脣，幽暗的陵墓裏他似乎閃耀着美麗的紫色光芒。他沒有再與蒼爭論什麼，而是上前一步將人摟進懷裏，低下頭用親吻封住那嘴脣。

又深又長的吻後兩人四目相對，棄天帝的眼中閃着某種蒼很熟悉的光，而他的聲音很輕，“你一定希望未來某一日你心願能夠實現時朕不會攔着你吧？那麼，儘量滿足朕如何？”

“哈。”
