---
title: 風姿花傳
layout: post
lang: zh-Hant
category: flowers
depth: 2
date: 2011-03-16 00:08:00
---

### Chapter 8

「不要…」蒼紅着眼眶，伸手碰不到棄天只有穿過棄天的身體的感覺，讓蒼幾乎崩潰，就在棄天要完全消失的時侯，一尾金龍飛過來穿過棄天的身體，穩住了棄天的身形，棄天轉過身，六銖衣飛了下來，他把手貼住棄天的胸口，六銖衣身上的神皇之氣一口氣全灌入了棄天的體內。


棄天張大了眼，六銖衣帶着微笑要棄天閉眼，棄天閉上眼睛之後，奇蹟發生了…一身雪白的棄天大帝，此刻化爲黑色身影，身形再生…。棄天和六銖衣兩人都發了光，當光芒散去，棄天已經恢復，而六銖衣倒入棄天的懷裏。

「棄天…」蒼第一次展現了他的佔有慾，他從棄天的身後伸手緊緊的抱着棄天，棄天成了夾心餅。

「先放開我…」六銖衣說了，棄天放開了六銖衣之後，蒼也放開了棄天。

「這到底是…我覺的我的身體好重…」棄天說着，他閉上運行身上的氣，卻發現…他已經不在是…神了。因爲他現在貨真價實的擁有的是人的體質，而不在是天人的體質，因爲感覺和以前完全不一樣。而眼前的六銖衣也失去了以往的光化，他也從仙人化爲凡人。


棄天覺的他身上的力量都已經回來，卻沒有以前的強大，閉眼想要接近轉輪，卻發現…他已經無法再看到轉輪，自己已經無法再操控這一切，而且終於如了自己這幾千年來的願朢，成了人。


終於入了轉輪，被這個世界所接受，以前…棄天和轉輪共生，卻不被接受，想毀去這一切，卻發現他沒有辦法毀去，只能砍斷兩者的連結，但就算如此無法進入冥界，從這個轉輪所運轉的大千世界降生。


棄天想了一下，也許是因爲自己神格崩毀，化爲虛無的這一刻被輸入了人的因子，加上自己本身就俱有創造再生之能，因爲神皇之氣而重造了肉體，因爲已經不在單純爲天人，所以順利變成了人，而且…不在與轉輪共存。


「我現在是…人了？」棄天眼裏浮起了溼意，這是第一次的奇特感覺。

「…對不起…如果我早一點把身上的神皇之氣灌入你的體內，你就能恢復，修復你的神格。」

「不…我並不想再成爲神只，這才是我要的…最終…我終於溶入你們了。不過…也讓你變成了凡人。」棄天伸手把蒼抱入懷裏：「我不在是神了…和你一樣了…我…」蒼緊緊的回抱着棄天，而六銖衣看着這兩人，不知道爲什麼…心裏不在覺的難受，他笑了出來：「有人來了！」

蒼一聽到有人來了，就推開了棄天，這時侯朱武騎着馬帶了一隊跑了過來，他神形很緊張，看到棄天三人都完好，才鬆了一口氣：「你還沒有死阿！」

「禍害遺千年你沒聽過嗎？怎麼開始關心我了？」棄天笑着說，而朱武明明就是鬆了口氣，但他還是別開臉：「哼！真可惜！」但眼裏的關心還是讓人發現。雖然父子的關係一直不是很好，但父子天性，兩人是用“另類”的方式再關心對方。


另一支軍隊跑過來回報：「太學主戰死了。」


在太學主戰死的消息傳了出去，兩國聯軍大獲全勝，之後朱武解放了被太學主關押的學者和反戰官員，另一邊蒼和學海前國主：佛公子達成共識，讓兩國暫時共管學海的政務，協助學海重建，一同努力讓學海恢復成以前的書香社會的中立國。


在那之後…


蒼帶領着軍隊回到了玄宗國之後，將累計的國事全部處理完成，又和大臣們組成了代管學海的政務組織，來到了鬼之國。

另一邊鬼之國的國主螣邪郎也讓組成了另一組代管組織，雙方合爲一組的會議將要招開。還有學海的代表也會出席這次的會議。會議的舉行以輪流的方式舉行，這次是在鬼之國，下一次玄宗國之後回到學海。


在大廳圓桌會議上，三國討論着之後的重建問題和新的商隊合作方式，棄天和朱武雖然也出席，但也只是掛在一邊，一切都讓螣邪郎去主持。朱武是掛心國家大事，棄天是爲了看蒼。


當會議中場休息的時侯，蒼和螣邪郎一大票官員正在閒聊，棄天就想要插進來，而且目標是蒼，先把人綁出這裏好好溫存一般。


「接來下蒼國主就要忙着競選國主了吧。」

「是的！等這次會議結束之後，接下來就是國主大選了。」蒼笑着迴應，這時侯棄天綠了臉，突然吼了出來，讓全場安靜了下來。


「你要出來選國主！」口氣很差。

「…是的！」蒼迴應。

「我不準！」棄天大叫道，而蒼愣了一下之後就沉下臉。

「你不準個什麼阿！你管這麼多作什麼？」朱武覺的太丟臉了，他老子是那條筋不對了，他是鬼之國的棄天帝，可不是玄宗國的什麼東東吧！

「我當然可以不準了，蒼…經過了這次，你還要出來選國主會不會太對不起我了，我們好不容易能放下這一切四處去遊玩的。」

「…」蒼愣了一下，這是什麼樣子的場合，難不成棄天想要把他們之間的關係招告天下嗎？

「棄天…」蒼用眼神叫棄天閉嘴，而朱武直覺的太丟臉了，丟臉丟到國際上去，他老子竟然想要拐騙對方的國主？」

「你是瘋了嗎？蒼是玄宗國的國主！你在亂說些什麼阿！」朱武想要把棄天拉出這個會議場合，但棄天卻推開了朱武，他說了：「我和蒼是夫妻，這種事爲什麼不能說！」這麼一說完，蒼綠了臉，全部的人都傻了眼。

「誰和你是夫妻阿！」蒼忍不住了吼了出來。

「我們簽過結婚證書的！你看！」棄天從懷裏拿出那張結婚證書，蒼當下差點沒有昏倒，而玄宗國的其他的大臣竟然有人已經昏了過去。

「這有效嗎？那是無效的吧！」蒼急急忙忙的說，那張證書…蒼一直以爲只是棄天的玩笑，所以從來沒有放在心上，棄天怎麼會在這個時侯拿出來。

「找個人驗驗？」棄天讓人看過證書之後，螣邪郎完全無言的對蒼說：「這是鬼之國的證書，我國同性是可以結婚的…但是…爺爺…你有登記了嗎？」

「有阿！戒神老伯！把我們的家譜拿出來！還有戶政的人員，去把資料給我翻出來！」棄天說的很有自信，果然戒神把鬼之國皇室的家譜拿出來一看，在棄天帝的配偶欄上寫着蒼的名字，而且日期還是久以前，而戶政人員也說這張證書有公證，而且登記過了，所以有效。


這下…不只是只有外國人都傻眼了，連鬼之國自家人都快昏倒了，而棄天像是被拋棄的棄婦一般說了：「難不成…你吃了就想跑？」

「我…」也許是蒼已經氣到完全無所謂了，他深深的嘆了一口氣看着棄天，又看自家的官員，他們昏到的、傻掉了一堆，只有自家的兄弟們都忍着笑意。突然間…蒼也覺的無所謂了，因爲他也不想再和棄天分開了。

「好吧！不選就不選…」蒼苦笑道。

<br/>

<br/>

<br/>

<br/>

「國主！」玄宗這邊的人，沒昏倒的也都昏倒了。


朱武氣的衝向了棄天帝，他的老子竟然公然的拐走了玄宗國的國主，有沒有搞錯？所以朱武也公然的在國際上，痛揍他老子。

朱武一拳揮了過去，直擊棄天帝的鼻子，當下！棄天就噴鼻血了。這一噴，不只是朱武傻了，大家也都傻了，而棄天摀着自己的鼻子，看到鼻血直流，神色更是難看到極點。

「你竟然揍你老子揍到噴血！」

「我…你以前怎麼打都不會有事的嘛！」朱武是真的嚇到了，以前他老子是真的怎麼打都不會有事的，怎麼現在一拳就噴血了。

「現在和以前不一樣了，我現在可是和你一樣有血有肉，臭小子！敢揍你老子，找死！」棄天吼完也不客氣的打了過去，而朱武也只有被揍的份，兩人在一邊滾成一團。
螣邪郎嘆了口氣，竟然和蒼異口同聲的說了：「隨便你們鬧吧！」兩人說了出來，還互看了一眼，之後笑了出來。

<br/>

之後會議結束之後，第二天蒼就要回到玄宗國去了，當晚蒼一行人早早就休息了。而蒼還在工作，他還在整理有關退選的相關文件，蒼知道自己的伴侶是個男人，將會在玄宗國引發了不少的風波，所以爲了把傷害壓到最小，蒼正在想辦法。


這時門打開了，走進來的人蒼不用擡頭就知道是誰。

「還不睡嗎？」

「我被你害死了。」蒼放下文件，隨即就被擁入熟悉的強健胸懷裏。

「是嗎？」棄天笑了出來，而蒼推開了棄天就問了：「那張結婚證書…你該不會一開始就設計好了吧。」蒼眯着眼睛問。

「別眯了，你的眼睛夠小了…」棄天取笑着，而蒼拍了棄天的手臂，就轉頭不理會棄天。棄天見蒼似乎又生氣了，只好討好的說道：「…其實我並沒有想過會有拿來用的一天，那張證書也只是我用來滿足我的想法吧了。」

「…你有妻子吧！」

「沒有！」

「那朱武你和誰生的？」蒼一直想要搞清楚這個問題。

「我自己生的。」棄天老實的回答。

「你是男人吧！少開剛玩笑。」蒼很無言。

「我以前是神啊，創造之神…生孩子當然沒有問題，應該說…朱武是我創造出來，在我肚子裏成長，之後像拉大便一樣把他拉出來…」棄天說的很認真，蒼聽了滿頭黑線：「真的還假的…」

「不信你可以去問朱武。」棄天笑。

「你…是蝸牛嗎？無性生殖？」

「喂…什麼蝸牛！不過現在我不行了，我現在只是個凡人。」棄天笑的很邪惡：「不過…要生的話…我們可以努力…」

「努力什麼啦！」蒼紅了臉，就被棄天吻住，他們很久沒有親熱了，兩人脣舌互相的糾纏，棄天把蒼打橫抱了起來，抱上了床。兩人不停的交纏着對方的舌頭，吸取對方口裏的蜜汁，雙手解着自己的衣服，很快的兩人就脫光了衣服，全丟到床下。

棄天放倒了蒼，他吻着蒼的胸口，吸咬着蒼胸口的雙蕾，手也不忘輕捻着另一個，不一會兒雙蕾就堅硬的挺起。同時傳來的麻疼感讓蒼的慾望挺了起來，蒼伸手撫着棄天的，棄天的分身早就爲蒼高高的挺起，兩人很迷戀深吻對方，兩人一邊糾纏，兩雙手就互相的握着對方的分身擼動着、搓揉着。


雙方的分身都因爲對方的動作而吐出了淫液，交溶在一起。

「棄天…」蒼叫喚着棄天的名字，他已經快要忍不住了，就在他要到達頂點的時侯，棄天放開了蒼的分身，這讓蒼很不滿的看着棄天。

「還沒有…」棄天笑的邪惡，他用着雙方的淫液推入了蒼雙股間的密蕾裏，蒼並沒有太吃驚，他張大了他的雙腿，讓棄天的手指更順利的進入他的密蕾裏。棄天一手壓着蒼的一條大腿，壓向了蒼的頭部，蒼整個人毎折了起來。蒼覺的腰很痛，但他的分身高漲的樣子和密蕾因爲被折了起來而盛開，這樣羞恥的姿勢讓蒼難得的害羞了，因爲自己的私處全都展現在棄天的眼前。而棄天的手指順利的擴張着蒼的穴口，蒼的內襞因爲棄天手指的騷刮，變的柔軟而且深處開始收縮着。


「你身下的小口，是越來越棒了。」棄天說完就低頭舔着穴口，這讓蒼一口氣就噴發了熱液，白液撒滿了自己肚子，蒼只能閉眼調息，但身下的快感還是不停的傳來，蒼的分身很快的在棄天的舔弄下又挺起，蒼爲了讓棄天用他的分身把自己填滿，他從來沒有這麼想要棄天插進來，讓棄天好好的折磨自己，但這一刻！蒼就是這麼的想要。

所以他伸手拉住了棄天一手壓着的腳，另一支手撫着自己的分身：「進來…」棄天擡頭，難得的主動吶！棄天直了身子，他把自己的巨大抵上了花蕾，一寸寸慢慢的挺進，蒼的內部又熱又軟的，讓棄天爽到腰支都麻了，頂到深處之後，棄天雙手把蒼的雙腿架到了他的肩上，隨即就瘋狂的抽出，狠狠的推入。

「棄天…慢點…」蒼被強大的慾望和電流弄的求攪着，被狠狠推到了深處，被填滿了後穴，之後瘋狂的抽出像是要把自己內襞的媚肉都翻出來一樣的抽出來，讓蒼又痛又爽的，而棄天的分身被蒼的內襞包覆，那熱感和緊窒的吸入棄天的分身，麻弊神經的感覺，讓棄天陷入了瘋狂的抽撤。


雙方用着最激情的反應來回應着對方，直到兩人筋皮力盡…。

<br/>

第二天，棄天一臉容光喚發的、光明正大的從蒼的房間離開，棄天看到了玄宗國的人，每個人臉上的反應都不一樣，他得時的刻意放大了閃光…。

<br/>

之後蒼回到了國內，以爲會遇到很多的刁難，結果並沒有如他的預期，雖然民調下降了不少，但是基本上這些年來，玄宗國因爲商隊開放來往頻繁，民風也也比較開放，所以對於國主的性向，比起以前的嚴厲看待，到是還好。只要國主當的好，真的有在做事情，到也是無所謂了。

而其他保求派系的官員，雖然也有些動作出來，但大多都假裝不知道國主的性向問題。後來…蒼暗中調查了一下，果然又是棄天的關係。

棄天在玄宗國的產業很多，有些更是直接和許多官員有關係，有些更是私產生意往來，要是得罪了棄天，生意就做不到就完了，所以在蒼宣佈了不再出來選國主的消息之後，大家竟然有鬆口氣的感覺。


當新任國主上任之後沒有多久，棄天就來到了玄宗國，他這次來是來帶走蒼的，而玄宗國的產業已經交接給這些年培養出來的幹部。只要一年過來看一次就好了。


來接走蒼的時侯，六銖衣也前來告別。


「你不和我們一起嗎？」蒼覺得…六銖衣是自己的大恩人，因爲他爲了救棄天，讓自己變成了凡人，所以除了把棄天讓出去之外的事，照顧六銖衣是應該的。

「不了…我打攪你們太久了。」

「銖衣…」棄天看着六銖衣眼裏的笑意，他知道留不住人，所以他讓人拿來了一袋的東西。

「這是我的信物，還有空白的銀票。如果你遇到困難，拿着我的信物到我的商行，我的商行會全力幫助你，還有…在這個世界生存下去是需要錢的，銀票到我的錢莊要領多少錢都沒有關係，用完了用我的信物就可以再拿。」

「謝謝…」六銖衣接過信物之後，就坐上了馬車。

「今後要去什麼地方？」

「等我找到一個可以安養天年的地方，就會聯詻你們…謝謝你們…後會有期。」六銖衣說完就架着馬車離開了，這時蒼注意到六銖衣的馬車上似乎載着一個人…。


「棄天…」

「什麼？」棄天伸手撫着蒼的臉，蒼突然間搖搖頭笑了，管他載了誰呢？所以蒼迴應：「沒有什麼…」


「接下來我們去北方吧！」

「去北方？月之國度嗎？不錯呢…我也想去，聽說是很美的雪之國。不過…你去那邊要做什麼生意？」

「賣珍珠奶茶，我要開泡沫紅茶店。」棄天的眼裏閃着光芒，而蒼皺了皺雙眉：「那裏很冷…賣這個會賺錢嗎？」

「你要逆向思考，就是沒有才有商機，我打算把月姬調過去代言。」

「…」蒼眯起了雙眼：「該不會是因爲奶大吧…」

「你真是太瞭解我了…」棄天放聲大笑，蒼只能搖頭，棄天總有一天會被他的女性職員給劈死。

<br/>

<br/>

完