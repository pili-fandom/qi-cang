---
title: 七罪
layout: post
lang: zh-Hant
category: seven_sins
depth: 1
date: 2011-03-20 00:00:00
author: 文心玲
subdocs:
  - title: 一。相逢之罪
    link: /seven_sins/1/
  - title: 二。欺騙之罪
    link: /seven_sins/2/
  - title: 三。誘惑之罪（限） 
    link: /seven_sins/3/
  - title: 四。囚禁之罪
    link: /seven_sins/4/
  - title: 五。殺戮之罪
    link: /seven_sins/5/
  - title: 六。逆天之罪
    link: /seven_sins/6/
  - title: 七。愛戀之罪
    link: /seven_sins/7/
  - title: 番外-醉拍春衫惜舊香(赭墨)
    link: /seven_sins/8/
---

#### 作者：文心玲

原載於[http://www.36rain.com/read.php?tid=73547](http://www.36rain.com/read.php?tid=73547)

---
