---
author: 刻舟求劍
title: 弱水
longtitle: 弱水 (青宮客同人)
date: 2023-05-02 00:00:03
layout: post
depth: 2
lang: zh-Hant
category: weak_water
---

### 第三部分

<br/>

半夢半醒之間，蒼再次跌入了先前的夢境。這一次他已經沉沒到了深處。四周靜悄悄的，水流宛如一條手臂，輕輕往上託舉着他，又像一道臂彎，在不斷輕柔的擺動中漸漸讓他陷入更深處。 

越是下沉，蒼越感到身體沉滯，知覺彷彿隨水流走了。突然，一道異常明亮的光線穿透了深深的湖水，徑直照射在他的眼皮上。他努力轉動了一下眼珠，接着又轉動了一下。 

<br/>

“已經巳時了，還不起麼。” 

蒼指尖一顫，終於清醒過來。僅僅稍微動一下，就覺得四肢痠痛，渾身都沉甸甸的，沒什麼力氣。床幔早被放了下來，棄天帝站在床邊，掀起幔帳一角，把它向吊鉤上挽。 

他看着倒神清氣爽。沒有穿華麗隆重的朝服，只有一襲漆黑的袍子搭在肩上，墨發披散，形容隨意，一點也看不出昨夜曾喝得酩酊大醉。 

“你今日未去上朝麼。” 

棄天帝不滿：“你不給朕梳頭，朕怎麼去上朝。” 

蒼默了默。“……我看是你不想去吧。” 

棄天帝有潔癖，儘管也會自己動手，但凡是他留宿青宮的日子，更衣梳洗之事都由蒼代勞。蒼通常起得很早，恰巧爲早朝留足了時間。 

今天這樣的情況，是從來沒有發生過的。蒼隱隱有些心驚。 

“沒有什麼大事，他們自己決斷。” 

棄天帝不耐煩地擺擺手，坐到了鏡臺前。 

這座鏡臺是前段時日新妃入宮時，隨其他三座一道賜下來的。青宮裝飾均十分樸素，唯有鏡臺打造奢華，鏡面平整清晰，映照得纖毫畢現。棄天帝的視線從鏡中的自己慢慢轉移到蒼身上。 

蒼正從一旁的衣架上取下一襲淡紫袍子。無事的日子，他總是一副簡樸的打扮。這實際上不合魔界的規矩，不過魔皇不介意他保留一些舊日的習慣，也就再沒人說什麼。 

他的行動過於慢了。棄天帝催促：“還不過來？” 

“……”蒼靜了半晌，“我有些累，陛下靜待一會兒吧。” 

蒼很少說這樣直白示弱的話。棄天帝看他慢吞吞地穿衣，確實是一副不太適宜的樣子，心底忽地翻起一陣煩躁。 

蒼終於收拾停當，走了過來。今日等待的時候比以往都要久，棄天帝的不悅顯而易見。也不知道大清早哪裏來的怨氣。蒼習以爲常，視而不見，拿起檯面上擺放的玉梳，爲他梳頭。 

棄天帝的頭髮柔長順滑，亮如墨緞，縷縷髮絲從梳齒間滑落，在晨光中熠熠發光…… 

倏而聽到一聲輕嘶，蒼松開手，數根頭髮形如斷折，脆弱地倒在齒列間。 

棄天帝的神色像凝在了鏡中。“你走神了。”他道，“在想什麼？” 

“如果我說什麼也沒想，陛下會信嗎。” 

<br/>

蒼重執起梳。棄天帝突然沒頭沒腦地說了一句：“再過半月就是瑞元節。” 

瑞元節是魔界極爲盛大的節日，每逢此時，民間萬衆歡騰，宮中亦要舉辦宴會。蒼參加過一次，這種純爲慶賀新歲的形式，他心中並不排斥。說到底，魔界的子民也只是普通百姓罷了。但畢竟身份尷尬，宴席上只要他在場，場面總顯得很詭異。而現在，身體頻繁的不適也讓他不想再費什麼心神去和異度的高官們虛與委蛇。 

“我……” 

“你必須去。” 

“……” 

“還有賢妃……和德妃。” 

“……在爲昨天的宴會，我和賢妃德妃沒有現面而不滿嗎。” 

“哼，別忘了你的身份。” 

“皇后……嗎？”蒼微哂，“他們出席與否，我並無資格置喙。” 

“沒有資格？那朕去哪一宮，你也沒有資格過問吧。” 

蒼一怔，梳髮的動作亦隨之停下了。他擡起頭，棄天帝正在鏡中冷冷地望着他。 

“你，不就在防着朕，讓朕少去紫霞宮嗎？” 

蒼默然無語。 

很奇怪，儘管棄天帝點名向苦境要來了劍子仙蹟，這段日子卻不見豁然宮有什麼寵愛。他似乎找赭杉軍更多些，這也是蒼憂慮的來由。赭杉軍個性剛硬，不懂變通，入宮不是一個好的選擇。 

“怎麼，被朕說中了嗎？”棄天帝惡質地開口，“蒼，你現在是異度的皇后，若還心心念念着什麼玄宗，朕不介意再次出兵，征討道境。” 

蒼淡淡道：“二妃方和親入宮，陛下就要撕毀和約嗎。” 

“那又如何。停戰或是出征，不過在朕一念之間。” 

絕對的力量，帶來絕對的自信，也是……絕對的傲慢。即使在過去的三年中，已經經歷過無數次這樣的對話，但當再一次聽到，蒼依然感受到一陣難言的憤怒。怒火猶如從身體深處迸發出來，無可控制地往胸腔、往四肢百骸蔓延。正是跟隨着情緒的異常發散，所有力氣像是瞬間被抽空了，蒼眼前一黑，軟軟地跌坐在了床邊。 

棄天帝皺眉。蒼最近總是病懨懨的，讓他心中不快。正想說些什麼，蒼便支着床柱慢慢站了起來，爲他梳全了這個髮髻。 

蒼的沉默激怒了他。棄天帝緊緊抿住雙脣，沒有留下一句話，拂袖離去了。 

<br/>

-

<br/>

朱武正在青宮前徘徊猶豫。棄天帝今早沒有來上朝，宮人們說魔皇夜宿青宮，尚未起床，要各位大人先散了。朝堂自然一片譁然，朱武想到昨夜伏嬰師所說，不由膽戰心驚，忍不住偷偷前來探望蒼的境況。誰想才到了殿門口，不過片刻棄天帝就帶着隨行浩浩蕩蕩地從殿內出來。 

“站住。”棄天帝一出門就見到鬼鬼祟祟的朱武，本就不爽的心情更是雪上加霜，“你來這裏做什麼。” 

朱武躲避不及，迫不得已道：“今日早朝未開，朝上衆說紛紜……衆大臣彙集了幾件要事，要我向魔皇稟告……” 

棄天帝擡手止住他的話：“一些小事！叫襲滅天來去處理。”見朱武仍杵在原地，又道：“有事？” 

朱武偷偷覷他的臉色。 

異色雙眸一眯。“還是，你來找皇后？” 

朱武頭皮一緊，忙否認：“呃不，我……” 

棄天帝打斷他：“再過半月就是瑞元節，你去負責吧。” 

“……什麼？”朱武愣住，好半天才反應過來棄天帝的意思是要他負責瑞元節宴會的安排，這一向是斷風塵的職責。“斷風塵呢？” 

“他另有要事。”棄天帝的眼風涼涼地掃過來，“你在質疑朕的安排？” 

朱武苦笑：“不敢。” 

<br/>

棄天帝的儀仗一如來時，聲勢浩大地離去了。朱武站在寒浸的隆冬晨風中，無限幽怨。 

正要離開，朱武忽然瞥見一個從遠處行走而來的人影。不過數息，那人已行至十丈以內，白衣絕世，飄然出塵。劍子仙蹟站定，微笑道：“太子。” 

苦境名人劍子仙蹟。朱武聽說過他的名號，只是未曾親眼見過。聽聞此人風姿格外出衆，令棄天帝在戰場上一眼相中……此時一見，倒也不算辱沒了傳聞。 

“德妃。” 

“唔。太子也是來找皇后嗎？” 

“哈。青宮是非之地，德妃還是少踏足……爲妙。” 

“太子箴言，劍子定當牢記。”劍子道，“不過我自入宮以來一直避居豁然宮，如今踏出宮門，也應當去各宮拜會才是。” 

劍子垂眸，笑容淡淡斂去：“……其實，今日我來不光爲此。我聽……賢妃說，蒼近日身體一直不適。總是白日嗜睡而夜晚難眠，四肢無力，易感疲乏……” 

“身體不適？” 

朱武少來後宮，對此事竟全然不知；但他知曉對蒼而言此狀並非正常之相，因而更是驚異。 

朱武心中升起強烈的警覺，面前之人曾是苦境正道的中流砥柱，秉性未明，目的未知……他強自剋制了情緒，漠然道：“此事該告知魔皇才是。” 

見拋出的誘餌被無視，劍子也不尷尬，只道：“那是劍子關心則亂了。不過蒼如此，實在令人揪心……我想，不若請了名醫來爲他診治一番，也好解了大家的心懸。” 

“……若要問醫，需稟明魔皇。” 

“哎呀，這等小事，何須叨擾魔皇呢？太子……” 

朱武生硬道：“來日事發，魔皇必問責於蒼。想必德妃也不願見此情形吧。” 

“唔……太子此言有理，是劍子失於考量了。” 

劍子露出一副十分煩惱的模樣。兩人間靜了數息，朱武終不忍，低聲道：“後宮諸事，我在前朝無可插手。他對蒼尚算優遇……你讓蒼自己去說吧。” 

劍子無奈。若蒼有心爲自己求醫，赭杉軍何苦找他來遊說？原聞異度太子同蒼有些來往，說不準會施以援手，現下看來，他終究不是魔界的主人啊…… 

不過，太子也算給他指了一條路。劍子含笑頓首，全了禮數，轉身入了青宮。 

<br/>

-

<br/>

棄天帝帶着一幫內侍一路行至天魔殿，推開殿門，桌上滿摞的公文，山一樣猛地印進眼睛裏。棄天帝心煩意亂，正要指揮侍從將它們全部搬去丞相襲滅天來的府邸，忽然，殿內的陰影中傳出了一道聲音。 

“參見魔皇。” 

棄天帝回頭，伏嬰師數年如一日的藍袍裝扮，臉上的面具像蛇的鱗片，在暗影中沁出幽幽的冷光。 

“怎麼是你？”棄天帝一指滿桌公文，“正好，這些東西搬一半去你宮裏。” 

“……”伏嬰師悠悠道，“身爲后妃，爲陛下分憂是分內之責。只是，斷大人是要偷閒了。” 

“哼，你的消息倒快。” 

棄天帝一甩衣袍，歪靠着椅子坐下。宮女列隊而入，安靜而迅速地清空了桌面上的書摞。 

“說起斷大人……噢，如今該稱他一聲淑妃了。既然添了新人，魔皇可要舉辦冊封典禮？” 

棄天帝想起蒼的封后大典。儘管時間倉促，但在斷風塵的策劃下，大典依然辦得尤爲隆重——很顯然，所有人都沒有想到棄天帝會異想天開到讓一個俘虜來當皇后。 

彼時蒼剛從萬年牢放出來，傷勢尚且未有痊癒，便被迫強撐着走完了全套繁瑣的儀程……雖然他全程神色冷淡，但那樣莊重的場合，卻莫名契合了他的氣質；就連匆忙趕製出來的皇后禮服，穿在身上都毫不違和……這更讓棄天帝覺得自己的決定無比地正確。 

皇后是搶來的特例。反而是自願入宮的貴妃，乃至新進的賢德二妃，都不曾專門舉辦過冊封典禮。 

轉念一想，專長於此的斷風塵預備受禮，朱武被他指去安排瑞元宴……目光下移，伏嬰師正在囑咐捧書而去的宮女：“……放在外邊桌子上……這麼多，內殿恐怕擺不下呀……” 

……罷了！魔，本就不是循規蹈矩的生物。何況，這些人哪是真衝着當寵妃來的。 

“不必辦了！”棄天帝又道，“瑞元節宴會在即，到時候衆人都要出席。” 

伏嬰師稍稍福了一福，而後道：“皇后……也是如此麼？” 

“自然。”棄天帝睨他，“你們一個兩個，怎麼都對皇后這般上心？” 

“我們？魔皇是說……朱武？”伏嬰師尾音上揚，轉而輕輕一笑，“……關心皇后，是妃嬪的本分。” 

“是你的本分，難道也是他的本分？” 

伏嬰師不說話。 

沒有得到確切的回覆，棄天帝面色一沉。 

“說。” 

“是。”伏嬰師慢慢道，“皇后不適，聽聞太子十分掛心，不時前往探望。” 

“他有什麼能探望！” 

“我斗膽猜測，也許是……敘舊。” 

棄天帝冷哼一聲：“如此猶豫……你的消息不是一向靈通嗎？這可不像你的作風。” 

“吾皇恕罪。現今的青宮與往日不同，要我說一句大不敬的話，中宮冊立，朝野本就一片譁然；二妃入宮，異度人心更爲浮動。而賢妃同皇后本是同修舊識，陛下若有心，亦可感知二人感情甚篤，有他在側……” 

面具下的眼睛緊緊盯住棄天帝那張姝麗到驚人的面孔，不錯失它一絲一毫的變化。脣齒開合間，有如毒蛇吐信。 

“皇后……更引舊思了。” 

棄天帝的臉色已經陰沉到不能再看，伏嬰師垂目，剋制地不再看他。天魔殿中，一時竟靜得連呼吸也不聞。 

過了許久，就在伏嬰師都以爲自己這言語挑撥又失敗時，突然頭頂砰一聲脆響，他猝然擡頭，就見棄天帝摔過一個茶杯後仍不解氣，霍然站起一腳踹翻了面前的長桌。 

“那就讓所有人都不準再去青宮！” 

魔皇的怒火引來了成羣的內侍。伏嬰師屈身：“吾皇英明。然而宮中雜事繁多，皇后又一向勤勉內務，恐對療養不利……不若魔皇下旨，增加青宮守衛，除每日送食水的宮人外無干人等不得出入。也好讓皇后安心養病。” 

棄天帝聽出了他的意思，這實際就是變相禁足了。皇后代表的勢力一直爲魔界所不容，伏嬰師更同他勢如水火……短短几刻鐘，從蒼的失魂落魄，到朱武的心不在焉，再到伏嬰師的咄咄逼人，棄天帝那無處排解的躁怒攀至頂峯。他環顧四周，已經沒有什麼可以扔出去的東西。 

“就依你所言！”棄天帝大袖一揮，粗暴地打斷，“其餘的，不準再說了。” 

伏嬰師默了片刻，低頭道：“是。” 

<br/>

-

<br/>

方出殿門，寒風便同刀一般紮了過來。伏嬰師裹緊身上冷藍色的袍子，心中忽然涌起了一陣冰冷的憤怒。朝臣、太子、乃至魔皇，無不被異境道子蠱惑……長此以往，異度何有復興之機！ 

新的守衛很快被挑選出來，親自領受了魔皇的命令，沉默着列隊向青宮而去。宮門前，他們和一個陌生的身影擦肩而過，後者停下腳步，好奇地張望着遠去的隊伍。 

伏嬰師對那人的到來十分詫異，略一思索後快步迎了上去。 

<br/>

劍子仙蹟目視着那羣人，一直到什麼都看不見了才收回目光。他一轉頭，眼裏頓時杵進伏嬰師那張陰惻惻的冰冷麪具。 

“哎呀！貴妃這樣無聲無息地站在人面前，可是會嚇人一大跳。” 

劍子往後退了三兩步，心有餘悸。伏嬰師微微一笑，道：“稀客。德妃如何有興致來天魔殿？” 

“哈哈，我近日偶得了一棋譜，裏面幾處殘局久思不解。聽聞陛下在棋道上頗有心得，特來請教。”劍子揚了揚手中那本封面古樸的書籍。伏嬰師的視線在這之上一滑而過，定在劍子臉上時，後者正巧也用誠懇至極的眼神回望着他。 

“貴妃這是往哪裏去？” 

伏嬰師揚了揚下巴。“去青宮。” 

劍子立刻領會了他的意思：“哦，皇后嗎？皇后現今身有不適，貿然來了這麼多人，小小一間青宮，只怕無法招待。。” 

“德妃有所不知。魔皇剛剛下令，青宮增加守衛，兩班輪換，日夜不離。無關人等不許前往探視。” 

“什麼？！”劍子瞪大眼睛，“……好好的，這是做什麼。” 

伏嬰師意味深長道：“誰知道，許是讓皇后好好養病吧。” 

“養病？養病更應該請大夫才是。貴妃，您同陛下說了嗎？”劍子的表情好像真的什麼都不知道一般，自顧自道，“陛下既然在此，唔……貴妃，恕我失陪了。”說罷，頷了頷首，匆忙離開了。 

<br/>

伏嬰師沒有阻攔，盯着他的背影，一路看他進入殿中。收拾完狼藉後，宮人們推合上天魔殿的殿門。在緩緩閉合的雙門縫隙中，伏嬰師見到劍子的到來同樣給棄天帝帶來了不小的驚訝，然而，劍子不知道耍了什麼小把戲，魔皇竟被逗得露出一絲笑意。 

能讓赭杉軍、乃至劍子仙蹟爲他奔波忙碌，看來，蒼的病比他所想更加嚴重，只是…… 

伏嬰師仰頭，日光微蒙，有烏鵲振翅掠過宮殿的檐角。 

……這其中唯一的變數，魔皇。不知道他又如何作想。
