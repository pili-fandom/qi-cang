---
title: 一千零一夜
author: 深海魚類
date: 2009-01-30
category: 1001nights
layout: post
lang: zh-Hant
depth: 2
---

### 六

“最終，吾兒還是要乖乖為吾所用，哈哈哈……”

伏嬰師報告好消息的聲音消失後，棄天帝當著蒼的面大笑起來。

蒼很久沒見過棄天帝這麼爽快的笑了。此刻天魔像發出的笑聲大概足以震動天魔池的池水，蒼估計。不過這時候就可以看出棄天帝的美麗非常過硬，這樣笑居然也絲毫不損傷形象。

過去道魔大戰的數百年間，玄宗一直不知道魔界戰神銀鍠朱武有這麼一個父親。蒼被抓來後才瞭解到魔界的親屬關係混亂複雜到這種程度，而魔在感情上的強烈和極端也令他有所思。

先用簫中劍逼朱武迴歸，再用九禍逼朱武征戰中原。蒼不確定這是伏嬰師的獻策還是棄天帝的高招，總之他們都是同類。最擅長利用感情，並且無論算計敵人還是自己人都同樣冷酷。

一直看著這些內幕的蒼，對銀鍠朱武這個昔日的死敵不禁謂嘆，甚至惻隱。

“聽了很多，有感想嗎？”棄天帝笑夠了，忽然問蒼。

“魔界素來的風格，不意外。”蒼整理著手稿，語調悠慢。

“但你看來有所感觸”

棄天帝窮追不捨。

“吾不指望你會有身為人父之慈，但如此對待他，是否配得上那一聲吾兒”蒼似無心又似有心地隨口評價道。

棄天帝的神情變得微妙了，他審視著蒼。

“哦，你在同情關心他嗎，你們千百年的仇敵？”

“朱武嗎，那是吾與吾的戰友必須剷除的魔物，無論當年與現在”蒼避開了棄天帝的問題。

“哼。哈哈……”

此刻棄天帝顯然心情很好，雖然他任何時候都可能笑，蒼能看得出他此時的笑容舒展得很快意。

很久沒見到的情況了。朱武的任性出走讓棄天帝一直心情不佳。

在棄天帝因朱武遊蕩不歸而惱火的日子裡，他來蒼這裡的次數反而更頻繁。而且態度很多變。有時會不管蒼想不想聽地拉著蒼說很多話，有時是那些花樣翻新的折磨蒼的遊戲，有時就是一言不發地待在蒼這裡，聽聽琴或者乾坐著。

而蒼始終以不變應萬變地沉靜與淡漠著，棄天帝居然沒有對此膩煩。大概因為這是這位大神現階段唯一可尋的樂趣，蒼想。

“想什麼？敬畏於吾的手段嗎？”棄天帝問。

“是啊，一切終於按照你的劇本上演了”蒼道，輕緩的語氣中帶著“直到這時才”的諷刺。而棄天帝似乎真的是心情太好了，居然沒有留意。或者留意卻沒有追究。

“不提吾兒。”棄天帝看著蒼，似在回味著什麼，忽然問：“彈琴之外，你還有什麼專長？”

“除魔。”蒼淡淡道。

棄天帝眯起了眼睛，但看起來沒有發怒的意思：“哼，哈，你的勇氣真是讓吾惱火又心喜……此外呢？”

“醫術。”

“醫術，是人類面對生老病死微不足道的掙扎，對身為再生之神的吾，毫無意義。還有嗎？”

“煉丹。”

“依賴外物，仍然是人類補償自身弱小的手段。還有嗎？”

“預知。”蒼第四次答道，語調平靜如一

“嗯……”棄天帝沉吟了，帶著品味的神情。

蒼微微一笑：“有興趣的話，可讓蒼為你一佔未來”

棄天帝停了停，也微笑了：“你看得到嗎？”

“你要嘗試嗎？”蒼淡然道。

萬年牢中一片凝滯的寂靜，棄天帝看著蒼，最後說：“這是你的底牌，吾不急於揭開。繼續，你除了這些之外的所長”

“哈。”蒼閉目道，“勉強做算的，還有茶藝。”

“嗯……那就泡茶給吾看。”棄天帝優雅地提襟坐了下來。

“難。”蒼拒絕道。

“嗯……？”棄天帝掃去威壓的一眼，這個蒼到現在都學不乖

“品茶需良辰幽境，”蒼不緊不慢地解釋道，“如清風明月，鬆吟竹韻，梅開雪霽……”

棄天帝聽罷不屑地哼了一聲：“難？”

<br/>

語落時，他們周圍的環境瞬間變化，無盡的雪後梅林取代了萬年牢的鐵欄石牆。

寂寞清冷的美景，枝頭白梅帶雪初綻，甚至可以嗅到清寒中淡淡的梅香。

“吾說了，此地是吾神力幻化的空間。”

蒼向前踏出一步，長久以來只見過萬年牢單一的陰暗，眼前乍現的新鮮的白雪令蒼一陣輕微的目眩。

“夠了嗎？”棄天帝問。

“又如何？仍不脫魔界妖氛。虛假的幻像，是對茶道的褻瀆。何況……”

“什麼？”

“除此之外，尚需佳客。”

蒼微微側頭道，梅枝在他臉上投下優美的素影。

“無良辰，無佳所，無好友。蒼雖然技淺，也有自重。”

“朋友，愚蠢的名詞。”棄天帝嗤之以鼻。

蒼輕輕搖頭：“烹茶待友，這是人類的樂趣，你無緣體會。”

“哼，人類的喜怒哀樂總要依賴於其他個體。弱小。”

蒼聞言默然遠望，白梅映雪，蕊寒枝瘦。

“人，確實需要互相支撐才能活下去。人類因此而弱，也因此而強。”蒼說。

蒼自己就是例子，他之所以是這樣一個強者，不過就是因為他有必須保護的人。

棄天帝不以為然地勾了勾脣角：“那，失去這些依靠的時候呢？比如此時，你的琴沒人來聽，你的茶也沒人來喝。”

棄天帝毫不留情地戳著蒼最痛的傷口，而蒼只是黯然地將目光投向枝頭寂寞的素白。一片寂靜中，可以聽得到枝頭碎雪掉落在雪地中的細微聲音。

換作是當初，蒼必會回敬從容且犀利的反擊，但此刻他唯有默然，帶著憂傷而隱忍的氣息。

所以棄天帝忽然意識到，蒼變了。

彷彿是被剝落掉了外面的一層硬殼，憔悴而無奈地露出了裡面的柔軟。不知為什麼，棄天帝認定這才是更接近蒼本質的樣子。

是的，這麼久了，即使是蒼也不可能不被留下痕跡。

蒼的氣質從來都不尖銳，而是和光同塵，如同海之深柔，所以“磨掉了他的稜角”這種話並不貼切。但棄天帝一時想不出更好的形容。

不管怎樣，棄天帝心情更好了。

也許他並不是想毀掉他，而是想改變他吧。看自己能把這個強得很優美的獵物改變多少，打下多深的烙印。這是一個很有成就感的過程……棄天帝想著這些，忽然又有了另一個念頭

“蒼，吾再問一次，你有影響改變吾的意圖嗎？”

“何以見得？”蒼從容應道。

“你會同吾講話，不可能只是因為無聊。”棄天帝逼視著蒼

“你說無人能影響和改變你，那個唯一繼承你血脈的人也在內嗎？”蒼靜了片刻，問了另一個問題。

“哼……那個不成材的魔界君王嗎？你說呢？”

棄天帝一拍身邊的梅樹枝幹，抖落的積雪掉在蒼的衣袖上，漸漸將淡灰的道袍濡出一塊溼痕。

蒼接住一朵飄落的潔白，竟是真實的梅花。

“吾剛才的問題，回答。”棄天帝追問

“吾只希望影響你對人類的看法”蒼說。

“哼，你是指你自己嗎？個別有趣的例外，也無礙人類汙穢的事實”

蒼不再多言。高傲偏激，任性固執的神，僅靠口舌是無法說服的。

手腕輕翻，蒼讓手中的梅花飄然墜地。

“若有朝一日你真的踏足於你所謂的汙穢的人間，若那時你還記得蒼的話，可以用你的雙眼去驗證，究竟何為真實。”

