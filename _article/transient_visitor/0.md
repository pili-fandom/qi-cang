---
author: 空谷雲客
title: 青宮客
longtitle: 青宮客（修訂版）
lang: zh-Hant
date: 2009-02-28
layout: post
depth: 1
category: transient_visitor
subdocs:
  - title: 第1章
    link: /transient_visitor/1/
  - title: 第2章
    link: /transient_visitor/2/
  - title: 第3章
    link: /transient_visitor/3/
  - title: 第4章
    link: /transient_visitor/4/
  - title: 第5章
    link: /transient_visitor/5/
  - title: 第6章
    link: /transient_visitor/6/
  - title: 第7章
    link: /transient_visitor/7/
  - title: 第8章
    link: /transient_visitor/8/
  - title: 第9章
    link: /transient_visitor/9/
  - title: 第10章
    link: /transient_visitor/10/
  - title: 第11章
    link: /transient_visitor/11/
  - title: 第12章
    link: /transient_visitor/12/
  - title: 第13章
    link: /transient_visitor/13/
  - title: 第14章
    link: /transient_visitor/14/
  - title: 第15章
    link: /transient_visitor/15/
  - title: 第16章
    link: /transient_visitor/16/
  - title: 第17章
    link: /transient_visitor/17/
  - title: 第18章
    link: /transient_visitor/18/
  - title: 第19章
    link: /transient_visitor/19/
  - title: 第20章
    link: /transient_visitor/20/
  - title: 第21章
    link: /transient_visitor/21/
  - title: 番外 賭注
    link: /transient_visitor/26/
  - title: 番外二 海市
    link: /transient_visitor/27/
  - title: 番外三 前塵
    link: /transient_visitor/28/
---

#### 作者：空谷雲客

>##### 作者說明
>
> 曾用ID：深海魚雷，Bluebird，湯伴侶
>
> 現用微博ID：空谷雲客
{: .block-tip}

---
