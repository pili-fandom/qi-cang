---
title: 情意遲遲
layout: post
lang: zh-Hant
category: attachment
depth: 2
date: 2011-03-16 01:09:00
author: 雷爾
---

### 第九章 
 
 
如果說愛情是場遊戲，那我想我是這場遊戲裡的最大輸家。 
 
如果說我承認我愛上了你，那你是否可以原諒我的無知傷害了你。 
 
如果說愛情一定要分輸贏，那我願意承認，我輸給了這世上唯一的你。 
 
*
 
棄天帝快發瘋了，蒼自從那天之後，理都不理他。 
 
如果蒼對他冷言相向或是很生氣甩他一巴掌、打他、罵他，都比現在好過太多了，蒼根本視他於無物。 
 
他打蒼的手機，沒人接，後來蒼甚至設他的號碼為拒接黑名單。 
 
他孤身衝到玄宗，結果被玄宗的保全擋了下來，對方守得滴水不漏，硬是不讓他進去。 
 
好吧，蒼總有離開公司的一天，他在玄宗的門口等了許久，仍是不見蒼的人影，他等在前門，蒼就從後門走，他等在後門，蒼就從前門離開。 
 
送到玄宗的道歉信函連拆都沒拆就被退了回來，蒼這次躲他躲得真是徹底。 
 
想著他、念著他、腦海裡全都是他一個人，這份感情在該死的面子問題中被扼殺了。 
 
他第一次、也是唯一一次，為了一個人如此失魂落魄。 
 
 
*
 
酒吧裡，一名異瞳的俊帥男人桌上堆滿許多喝完的酒杯。 
 
趕到現場的襲滅天來和吞佛童子，看到的就是這麼一副失意人的樣貌。 
 
棄天帝像是極力要用酒精把自己灌醉，卻怎麼喝也喝不醉。 
 
「你還要喝到什麼時候。」毫不留情地，襲滅天來一把搶過棄天帝手上的龍舌蘭酒。 
 
「還給我，我連酒都喝不醉，你說我還能怎麼辦。」苦笑，伸出手想將酒杯拿回。 
 
一旁剛來到現場吞佛童子，掏出數張大鈔，替棄天帝結帳，「這麼喝有用嗎。」 
 
聞言，一股氣升了上來，他說：「不然你說，我還能怎麼辦，他躲我躲得好遠好遠，他不相信我、也不見我，我連喝酒也喝不醉，腦海裡想的全是他，我快瘋了…」 
 
襲滅天來冷顏以對，那杯龍舌酒直接順手往棄天帝頭上一倒，棄天帝全身狼狽，愣在當場。 
 
「你一直在這邊買醉有什麼用，動心了、愛上了，就再去試一次，一次不行，那就再一次，一次又一次，總有一次會成功。」 
 
吞佛聳肩，「如果真的不行，你就放棄吧，如果放不了手，那就再去纏住他，反正你也不是好擺脫的角色。」 
 
真是一語點醒夢中人，棄天帝洩氣般重嘆一聲，他在搞什麼鬼，有時間在這邊半死不活的，還不如把時間放在蒼身上，快點想辦法讓他重新信任他、愛上他。 
 
「謝謝。」良久，棄天帝才釋然一笑，對襲滅和吞佛說道。 
 
「不過，我還是想聽到你親口說出來我才願意相信，你該不會真的對蒼動心了吧？」襲滅好奇問道，和吞佛賊賊對看一眼。 
 
深深的嘆氣，嘴硬的男人這次終於誠實，「我動心了，而且是不可自拔的動心了。」 
 
他喜歡獨佔蒼的那份時光，只有在他面前的蒼才像個孩子一樣，沒有戒心、沒有競爭，只有全心全意的信任。 
 
對他沒有利益、沒有事業、沒有壓力，就像相愛的兩個戀人一樣，他享受著和蒼東奔西跑四處玩鬧的日子。 
 
他好想他，想到快發瘋了，握緊雙拳，棄天帝眼中閃耀著不放棄的光芒。 
 
 
 
我一定會讓你重新信任我、愛上我。 
 
讓你相信我真的愛你。 
 
 
*
 
 
其實蒼的內心很慌亂。 
 
他不知道這要怎麼歸類現在對棄天帝的心情，他應該要痛恨他、討厭他、無視他，可是心底卻怎樣也做不到。 
 
他一直以為棄天帝真的喜歡他，而他也真的被棄天帝打動了。 
 
活了二十幾個年的日子，第一次沒有隱藏的大笑大鬧，結果才發現這只是一場惡劣的遊戲。 
 
眼淚可悲得流不出來，哀莫大於心死。 
 
這幾天，他努力讓自己保持平靜，不讓棄天帝攪亂他的心房，可是他偏偏一次又一次的打擾他。 
 
還口口聲聲說，他真的愛他。 
 
怎麼可能，這也許又是他又一次的玩笑和謊話，別再被騙了。 
 
雖然，他的眼神裡有著無限的痛苦和後悔… 
 
 
「總裁，棄天帝又在玄宗附近徘徊，現在要怎麼辦？」赤雲染匆匆忙忙地走進來。 
 
冷笑，蒼回答：「別理他，我看他能等多久。」 
 
 
他是蒼，玄宗集團的總裁，再傷心難過，他還有很多重要的事情要做。 
 
為了一個男人、甚至只是一個玩笑，不值得他這麼傷神，晃晃頭腦，他將棄天帝從腦海中驅離，讓自己專心於事業上面。 
 
把這些煩人的事情拋在腦後，他不允許自己軟弱。 
 
 
*
 
好多天了，棄天帝還是沒有見到蒼一面。 
 
他依舊躲他躲得兇，不想見到他就是不想見到他。 
 
棄天帝鐵了心一定要見到蒼，好吧，玄宗見不到人，那他到蒼的房子外等他，蒼不可能不回家。 
 
抱著一把玫瑰花，棄天帝痴痴守在蒼的屋外。 
 
他記得蒼曾經嫌棄他的玫瑰花，俗不可耐，想到初識的兩人，棄天帝嘴角忍不住笑了起來。 
 
他的蒼，真的很可愛很特別。 
 
 
司機盡責地開著車，接送他們玄宗的美人總裁。 
 
這不是他驕傲，他能幫美人總裁開車，根本是上上上上上輩子修來的福氣。 
 
他常常透過後視鏡偷看美人總裁的一舉一動、偶爾美人總裁還和和他搭聊幾句，哦哦，這真是太幸福了。 
 
美人總裁在他們玄宗員工的內心世界裡，可是有如神祇一樣的崇高存在！ 
 
不但體卹下屬，也不會對員工發脾氣，常常為大家加薪，而且還沒有架子，這種美麗又迷人又善良的上司，你打著燈籠也找不著啊！ 
 
但是，聽說之前他們的死對頭，異度那間公司的總裁，那個長得像明星，可是他們看來就是不順眼的棄天帝，居然來招惹他們玄宗的美人總裁。 
 
雖然不知道發生什麼事情，可是美人總裁前段時間笑容全沒了，還瘦了不少，看得全公司恨不得把給異度鏟平。 
 
終於過了幾天，美人總裁才漸漸好了許多，他們也鬆了一口氣。 
 
今日，司機開著車，看蒼的臉色和心情似乎不錯，他慢慢開口聊了起來：「總裁您這幾天終於了好了一些。」 
 
蒼愣了愣，沒想到司機會這麼問，「我前幾天狀況很不好嗎？」 
 
「當然很不好，全公司的員工因為您心情不好都快哭了，還想去鏟平異度公司！」 
 
聞言，蒼一笑，「這麼扯，其實我沒有心情不好，只是難免需要調適一下心情，你們別太擔心了。」 
 
「總裁，對我們來說，您很重要，我們大家都很喜歡您，如果您真的…呃…真的被棄天帝那個男人傷了心，我們一定會為您討回公道！」 
 
棄天帝…再聽到這個名字還是會心痛，可是蒼把自己調適得很好，他知道，不能再讓別人為他擔心，他也不想為了棄天帝而難過。 
 
「謝謝你們，不過我沒事，真的沒事。」 
 
語畢，車子漸漸開到蒼的住處，遠處就看到一名偉岸身影定立在那邊。 
 
那個人…蒼睜大雙眼，不可置信那個男人追到這裡來了。 
 
 
緊抿著雙唇、剛毅的眉毛皺成起來、手上抱著一大束他曾說過俗氣的玫瑰花、高人一等的身高佇立在他的門外。 
 
他在等他回來。 
 
蒼覺得時間越來越慢，連那個男人的動作也像慢動作重播一樣緩慢。 
 
棄天帝抬頭，看到不遠處的黑色高級轎車，他的唇形，正告訴蒼一句話。 
 
我、會、一、直、等、你。 
 
 
 
「總裁，怎麼辦，我可以開車撞他嗎？」 
 
蒼被司機無厘頭的話弄得啼笑皆非，「沒關係，我在這邊下車，我倒要看看他想做什麼，你把車子開回車庫吧。」 
 
一說完，蒼打開車門下車，優雅的步伐走近棄天帝苦守在門外的身影。 
 
棄天帝手上抱著一束玫瑰花，看到了彷彿幾世紀沒看到的蒼，他苦苦思念的蒼，他聽見自己有如雷響的心跳聲。 
 
光是看到他一面，他就覺得這些日子的等候是值得的。 
 
心中對自己苦笑，棄天帝想，他真的陷得好深好深了，只要他，只想要他，別人他都不要。 
 
 
蒼不發一語，他美麗的鳳眼盯著眼前的男人。 
 
他不會再被欺騙，他要和男人說清楚。 
 
他想玩，他沒有意見，但是這場遊戲他不想奉陪。 
 
 
 
蒼走向他，棄天帝轉正身體，等不了這一小段路的距離，他邁開長腿，跑向蒼的方向。 
 
他們越來越近、越來越近、直到蒼站在他的眼前，他才發現，自己對他的想念及愛戀，早就超出他能預料的地步。 
 
 
 
千迴百轉所為哪事，千思萬緒只因伊人惱怒。 
 
所以情意遲遲，千辛萬苦情路難行。 
 
 
---------------------------- 
 
我就是那個司機啦XDDD 
 
棄總你自己保重。 
 
我還沒玩夠XDDD 
下一集再繼續虐你(喂) 
 
請大家為棄總的追妻之路默哀謝謝XDDDD
