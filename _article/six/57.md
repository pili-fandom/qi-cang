---
author: 文心玲
title: 六道之外
lang: zh-Hant
date: 2011-03-22 00:57
layout: post
depth: 2
category: six
---

### 六道之外-57(玄幻或者魔幻?現代)(棄天帝X蒼)

<br/>

※

蒼與一步蓮華的背影在夕陽下拖長，方才與魔性抗鬥過的蒼腳步有些虛浮，他走了幾步路後忽然回頭，只看見路燈一個又一個轉亮，並沒有任何帶魔氣的影子。

鬆了口氣，他靠著好友攙扶緩步走遠。

直到看不見蒼的背影後，在街角等待的黑色轎車終於發動，車內太陽神輕拍棄天帝的肩膀，似是安撫也像給他信心。

『怎麼，捨不得嗎？』

『不。』

棄天帝拿出菸盒想抽菸，太陽神搶走菸盒改塞小瓶白蘭地給他。

『喝酒比抽菸好。』

棄天帝一口飲盡小瓶裝白蘭地。

『這可是皇室專用限量版，我好不容易才買到的限量版酒你就這樣牛飲喝乾？你真是不懂品酒。』太陽神搶回酒瓶一臉心疼樣說道。

『下回買大罐點，酒太少無法喝醉。』

太陽神無奈說道：『過幾個小時到了法國再讓你喝個過癮。』

『喝個過癮？哈。我心頭上的癮哪能靠酒經解除，只怕應了句古語，酒入愁腸愁更愁。』

車窗玻璃映照出棄天帝的面容，他仍是笑著卻笑得更像落淚。

太陽神安慰的言語哽在喉頭說不出口，他陪著他的雙胞胎弟弟棄天帝看著窗外飄落纖細雨絲，他沒有嘆息只是看著這從天而降的眼淚，從纖細雨絲到傾盆大雨，傾洩了全世界的無奈。

棄天帝轉頭。『上帝造世界花了七日，那毀滅世界需要幾日？』

太陽神撥弄他自己天生雪白的長髮，回答道：

『一瞬間。』

因為從纖細雨絲到傾盆大雨也只是一瞬間的事情。

<br/>

次日，暴風半徑超過兩個巴黎市的大龍捲風侵襲法國南部，死傷人數過多無法精準估計，同時全世界各地出現短暫地震現象，對很多人來說風災與地震都是短暫的一瞬間，

看完新聞畫面幾分鐘後就將種種異象拋諸腦後，但這一瞬間卻足夠讓世界末日提前降臨。

龍捲風中心眼的空氣謎樣反向旋轉扭曲，一個又一個擁有蝙蝠翅膀的男爵級惡魔自旋渦風眼中飛出，牠們無視於龍捲風可在一秒內拆毀房屋的巨大威力，振翅往世界各地飛去，牠們的目的地是各個國家最主要的權力中心。

惡魔早已在全世界權力核心裡埋下可受控制的定時炸彈，牠們現在要作的是前往按下引爆的按鈕。

惡魔最擅長的就是蠱惑人心，多數善良的人類容易被煽動與愚弄，惡魔在夢境中尋找有野心的人類並給予他們煽動人心的力量，當那些人類站到權力核心時便是收割的時刻。

路西法看著魔界鏡裡人類擁有權力後貪婪的笑容，祂目的快要達成理應該感到開心，但祂皺著眉頭並沒有一絲喜悅。

『不論多清高莊嚴的人類都有可引誘的弱點，權力、財富、名望、情人，蒼的弱點會是哪一樣？究竟哪一樣可以讓你放棄守護可笑的人類，甘願墮落成魔與吾站在同一邊？』

魔界正中央的生命樹仍是枯萎的連一片嫩綠葉芽都長不出來，黑暗帝王路西法卻每日每日走來撫摸已摸到光滑的樹幹，又獨自對著能連通兩界的魔界鏡沉思，他的手指撫摸鏡面，鏡面影像波動後映現出蒼的面容。

忽然，祂在鏡中看到不該出現在蒼身旁的身影，祂激動得捉住鏡面想看得更清楚些。

鏡中，半透明的金髮天使進入了同樣是金髮人類神父軀體，神父按響一步蓮華心靈諮商所的門鈴，蒼開門邀請神父進屋。

天使並不是靈魂體無法控制原本身體主人的意識，但可以影響神父說出來的話語，同時也可以不讓蒼察覺天使獨有的神聖氣息。

然而，對於高高在上的天使長來說，進入汙穢的人類身軀裡是萬不得已的選擇。

路西法玩味地說道：『米伽勒，你終於按耐不住了嗎？你又想用什麼手段令蒼作出抉擇？既然這場遊戲你已經出場，我也不能缺席對嗎？』

黑暗帝王揚手招出巨大龍捲風，魔界天空隨即出現一道裂縫，祂躍進龍捲風中心眼裡隨風捲上直達人間。

<br/>

<br/>

<br/>

<br/>

