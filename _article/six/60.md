---
author: 文心玲
title: 六道之外
lang: zh-Hant
date: 2011-03-22 02:10
layout: post
depth: 2
category: six
---

### 六道之外-60(玄幻或者魔幻?現代)(棄天帝X蒼)H

<br/>

蒼。

蒼。

蒼！

是誰在呼喚我的名字？

蒼詢問著卻沒有人回答，他跨開步伐想尋聲音的來源，卻忽地踏空，身體不斷地往下墜，墜落到無止境的深淵。

他胡亂地捉著空氣，希望能捕捉到救命的繩索，卻什麼也捉不到。

他感到很慌亂，甚至感覺到生命力從自己體內一滴一滴地流逝，他看向自己的手腕，手腕上不知何時被劃開怵目驚心的傷口，鮮血正不斷地流出，

血流彷彿一條繫在他手腕的纖細紅繩，連結到無止境的深淵盡頭。

他想起他自己的抉擇，然後釋然地微笑。

他救了整個人界呢，該驕傲吧，該開心吧，只不過他感覺到的是孤寂，沒有人在身旁真的很寂寞啊。

蒼的心中生起一個莫名其妙的念頭，如果放任自己一直掉下去，會不會掉落到魔界的最深處無間地獄？

記得棄天帝說過要與他一起下地獄，那麼，先到地獄等他也是好的。

蒼。

他聽見聲音在上方呼喚他的名字，他仰頭輕聲回答：

『棄天帝，我先到地獄裡等你。』

愛上惡魔肯定有罪，不過能跟棄天帝一起待在地獄裡想必不會無聊。

他微笑著閉眼準備迎接生命消逝前一刻將來臨的劇痛，痛過了就會是天堂，喔不，是有愛人的地獄。

<br/>

蒼的手指緊握著棄天帝的手臂，雖然已被抓出數道紅痕，棄天帝仍任由蒼抓著，甚至被睡夢中的蒼啃咬他也沒有叫疼。

蒼鬆開口，棄天帝的胸膛遺留清楚的齒印。

他聽見蒼喃喃道：『棄天帝，我先到地獄裡等你。』

心跳得好快好快，就像下一刻會從胸口中跳出一般，他從不知道人類的軀體可以潛藏如此濃烈的感情，烈得令他的神智迷醉。

他親吻蒼緊閉著的眼皮，小心翼翼地不驚擾蒼的睡眠，然後他緊擁著蒼，讓蒼在他的胸膛中沉睡。

『蒼……你說先到地獄裡等我，是不是代表你的心已屬於我？』

棄天帝輕柔吻著蒼的唇瓣，舌尖在蒼的齒前挑逗，蒼唔嗯了聲睜開迷濛的紫眸，唇瓣微啟任他予取予求。

這一刻，棄天帝寧可相信，蒼是屬於他的。

不管有沒有永遠，他擁有蒼的心就已足夠，即使有一天將是仙魔殊途，他也不會後悔。

<br/>

蒼撫摸著棄天帝的胸口，胸口上的齒痕令他雙頰發熱。

他知道他自己的臉一定紅到耳根，但他並不想為了睡夢中作的事道歉，反而將唇靠近齒痕，然後發狠地用力啃咬，這一回啃出淺淺血痕。

棄天帝並沒有叫疼，他笑著在蒼耳畔說道：

『我喜歡你咬我，多咬幾口，最好把肉都咬下來，讓你的體內有我的血與我的肉，你就永遠屬於我。』

永遠，多麼難懂的詞句？蒼淺笑掩蓋住他心中的無奈。

『我不喜歡吃人肉，人類居於食物金字塔的頂端，所有有害的化學物質與抗生素都堆積在人體內，吃人肉對健康有害。』蒼故作嚴肅回答道。

『你真沒有浪漫細胞。』棄天帝攤手作服了你的動作。

『是嗎？』蒼翻身坐在棄天帝身上，緩慢舔了舔食指，濕潤的指腹在棄天帝唇瓣上游移。

『你真的希望我吃掉你？』

蒼的眼中帶著躍躍欲試的興味，棄天帝撫弄下巴，說道：

『你想抱我也不是不可以，只要你能吻贏我就讓你隨心所欲。』

金藍異瞳望著他，棄天帝舔了舔唇，漆黑長髮披散在床上憑添了幾分魔魅般的誘惑。

蒼忽然感覺有些口渴，像是被魔誘惑心智般，他俯身吻上棄天帝的唇瓣。

如同沙漠中渴水已久的旅人放肆地索取對方口中的綠洲。舌與舌互不相讓地纏綿著，他在口腔深處嚐到情濃時的甜味，得逞後他想撤退但後腦卻被按住貼得更緊密，近得沒空隙的深吻令他無法呼吸，他只能憋氣憋到無法再憋，恍然之間像走在懸橋上，無力虛脫的下場就是墮落然後死亡。

他用最後的力氣咬棄天帝的舌頭，血腥氣味在口中蔓延，他也得回喘氣的空間。

棄天帝抹掉唇邊溢出的鮮血，惡意地用沾染鮮血的指腹逗弄蒼胸前的敏感點，蒼虛弱地搥打他抗議，棄天帝卻得寸進尺，翻身又將蒼制於身下。

『蒼，我沒見過比你更適合鮮血的人類，艷紅血液淌在你身上就像是鮮紅欲滴的紅寶石色澤禁果，讓我忍不住想採擷它然後盡情舔弄啃咬，直到它腫脹到不堪玩弄為止……』

『你敢咬……啊……我會咬回來……嗯啊……』

蒼沒想到棄天帝當真啃咬，儘管只是不會留下齒痕的啃弄，仍讓他敏感地輕顫。

『我不介意你咬回來，我喜歡你咬我，最好啃我的肉飲我的血，讓我與你融為一體。在這之前，還是先讓我以另一種方式與你融為一體。』

他分開蒼的長腿，蒼也默許他的進入，挺腰讓他融入得更深更沉。

蒼暗自催動斷情內丹，讓斷情內丹在體內隨著身體律動而浮沉，氣流自四肢匯聚到肚臍，再從肚臍往下行到尾椎處，他等待著攀上高峰的時刻，當身體不自主地因情動高處而收緊顫抖時他瞬間抽離意識，讓斷情內丹旋轉吸取生離魂的陽氣，這就是白狐族雙修術中顛鳳倒龍、採陽入陽元的實行方式。

鳳與龍皆為雄性，意即須顛倒前後，藉由陽入陽元時抽離魂魄讓斷情內丹反過來採擷陽元，這個修練方式非常冒險，一分心就會賠上性命，還好需要抽離的時間非常短，在棄天帝發覺前蒼已回復神識，他先運氣確認斷情內丹已達到第二重，才放心地在棄天帝懷中入眠。

然而，棄天帝根本沒有闔眼。

他吻去蒼額上的細汗，嘆氣道：

『人類根本不值得你為他們折磨自己，蒼，我縱容你修練，但我絕不可能放你走，你別想背著我默默離開，如果你真決定犧牲自己重建封印，我就毀滅這個世界，讓全世界都為你陪葬。』

棄天帝用他的懷抱禁錮住蒼，他的靈魂是由惡魔所生，魔只懂得占有與毀滅，不懂得放手。

<br/>

<br/>

<br/>

<br/>

<br/>

