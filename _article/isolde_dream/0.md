---
title: 伊索爾德之夢
date: 2018-01-01 00:00:01
layout: post
lang: zh-Hant
category: isolde
depth: 1
author: dgrey
---

#### 作者：dgrey

獵魔人paro，法師棄x獵魔人蒼

---

**【被玄宗之術封印的筆記】**

*道境歷1358年 7月5日*

找到了藺無雙最後乘坐的船，它在兩境交界處擱淺了。船長日誌顯示他們遭遇了壞天氣，獵魔人船艙裏的閃光值得注意。根據描述，可能是某種傳送法術。 

會和“那個人”有關係嗎？ 

<br/>

*道境歷1358年 7月14日*

赤雲染帶回了明玥，她很傷心，我能做的也只有安慰。 

星軌變化有時，晦暗有時，明朗有時，也許我該啓程去找他了。 

<br/>

*道境歷1358年 7月21日*

在玄宗故地查閱資料，古老而不死的法師數量不多，現在具有威脅的僅剩一人。推演得出的結論是他目前無法從陰之間離開，這是個令人振奮的消息。 

進行探索之前應做好準備。 

要交代翠山行顧好天波浩渺。 

提前煎藥，劍油也是必不可少的。 

<br/>

*道境歷1358年 8月3日*

…… 

（被水漬打溼的文字）之海（被水漬打溼的文字）第二次（被水漬打溼的文字） 

…… 

<br/>

*道境歷1358年*

不要靠近（塗掉）宅邸 

祂沒有實體。必須讓祂（塗掉） 

按方子煎藥如下： 

多香果，白屈花，精靈之塵。順時針攪拌三次。 

要用銀劍。鋼劍造成的傷害微乎其微。 

會成功嗎？勝敗在此一舉。 

<br/>

<br/>

**【法師的筆記】**

距離上一個玄宗的獵魔人找上門已經很久了，久到我已經忘記那是什麼時候……兩百年？還是三百年？不，那都不重要。 

這個獵魔人顯然有備而來。他的法印和劍舞喚醒了我關於那場戰爭的記憶，他會後悔的，想要挑戰古老的神明要付出代價。 

如果我想，我可以殺死他幾百次，即使有煎藥的作用，他在我的結界裏依舊難以施爲。真可惜啊，人類爲什麼不願意相信神的力量可以主宰一切呢？難道那些典籍沒有教會他何爲敬畏？ 

人類的意志，他居然說什麼人類的意志？妄想以意志戰勝神，還是太狂妄了些。只要我擡一擡手，他就會飛出幾米遠，劇烈的疼痛會讓他痙攣得無法呼吸。 

我知道這草率又無味的結局。我殺死過很多人，獵魔人的徽章可以堆滿一個大展示臺。不過這個確實不大一樣，我試圖扯下那塊金屬的時候他睜開了眼睛——距離這樣近，他淺色睫毛上還掛着凝固的血珠。在我分神注視他的一霎，銀劍貫穿了我的手臂。 

哦，我認識那柄劍，也認識他的法印——明玥搭配亞克席，只是他對效果過於自信。我輕而易舉地拔出劍，抓着徽章提起他。鏈條在他脖頸上勒出一道紅色印記，他似乎想要咳嗽，但脆弱的喉嚨無法發出任何聲音。 
聰明的人類，死亡顯然已經無法滿足他。他說過他的名字，蒼是嗎？道境古老的血脈，獵魔人經受過突變的身體，多麼美妙的組合！ 

啊，我想起來了。太陽神想要我拿出一個配方，但長久以來我不屑於此。現在這個項目可以繼續進行了。讓我們拭目以待吧。 

<br/>

<br/>

**【實驗室手記】**

*第一天*

第二排左起第五瓶……哦，不，不對，不是青草。他已經接受過“青草”的洗禮了，這種煎藥會使人的身體產生突變。我領略過武藝方面的突變，玄宗似乎格外不同。 

實驗對象得保持清醒，又不能完全恢復體力。折衷的辦法之一是金鶯加水母素。哈，這本來是獵魔人用於防止被毒氣侵襲的方法。 

希望他喜歡這份禮物。 

<br/>

*第五天*

他喜歡得過頭了，我的意思是，他對影響心智的法術、對疼痛的忍受力都超出想象，即使是在獵魔人這個梯隊裏。 

實驗暫停了三天，我不得不給幾位助手治療。都是骨折，手臂、小腿、鼻樑……這種烈度的打擊通常來自失去理智的人，但他依舊清醒，可以正常交流，對自己的處境有明確的認識。 

所以這種行爲是在表達不滿？我讀不出他的情緒，有理由相信玄宗的突變消除了情感和相關的其他部分。 

太合適了，太合適了！ 

接下來要加快進度。 

<br/>

*第七天*

加大劑量之後，蘇摩草開始產生作用了。 

這種狀態下的獵魔人真的很美麗，無論是抓着床單的手指還是慢慢癱軟下來的腰肢。他全程閉着眼睛，不過這沒關係，我會把他拖到鏡子前。他應該好好看看自己，被慾望蒸騰，顫抖着吐出微弱嗚咽的自己…… 

精緻又優雅的玩具！承受神的甘霖之後依然聖潔！ 

<br/>

*第十六天*

改良了配方，萬年竹的提取物加一點魔龍鱗片的粉末效果非常棒，我敢說，即使是最無情無慾的獵魔人，在我的藥劑之下都要化身爲比夜魔還要放縱的生物。 

放大敏感……加強神經系統的連接…… 

讚美“青草”，突變讓實驗對象非常適合成爲新試煉的載體。 

<br/>

*第二十一天*

我討厭錯誤，一個錯誤需要用很多行爲去彌補。 

是我低估了人類嗎？他居然藏了一份白蜂蜜，用來抵消魔藥的效果，假意迎合讓我放鬆警惕，並且用蠟複製了我的鑰匙……我的助手裏有人在幫他？我離開的時候他甚至去櫃子裏配出了雷霆和燕子。 

但那不過是毫無意義的掙扎罷了。 

我抓回了他，該死的，他沒有再用亞克席了，可那雙平靜如水的眼睛是怎麼回事？那種想要看到它們盛滿怒意的衝動是怎麼回事？ 

我不再使用魔藥，就只是佔有、佔有、再佔有。 

我差點折斷了他的手臂，擊碎了他的胸骨，他的銀劍差一點就戳穿了我的喉嚨。他一如既往地沒有求饒，沒有屈服，不過他的身體出賣了他……也許征伐才是最好的魔藥？ 

我飲下他的血，他撕咬我的肉，白骨相見，痛感與快感肆意又淋漓。 

他說我瘋了。瘋了又怎樣呢？我扯下一綹他的頭髮，加在我的研究成品裏。 

我吻他發白的脣。他別過頭，只要還能動，他就毫不掩飾敵意。 

這樣更趣味不是嗎？ 

他不知道，那魔藥的名字，我打算叫“青鳥”。 

（被撕掉的紙頁） 

（被撕掉的紙頁） 

（被撕掉的紙頁） 

不……我改變主意了，這個配方只對獵魔人起效，甚至只對蒼起效。 

至於副作用，我會解決的。 

（被撕掉的紙頁） 

（被撕掉的紙頁） 

（非常大的字）我會找到他。蒼是我的。他無法離開我。（塗掉） 

<br/>

<br/>

**【沒有署名的信】**

蓮華之眼在上，你說過的，它會穿過黑暗指引你到海波浪。那裏有你的朋友接應嗎？ 

我的傳送門不會持續很久。請相信不是所有的法師都和祂一樣。 

時間有限，就寫到這裏吧。 

快跑，離開這裏，離開黑暗的一切。 

希望我們不會再見了。 
