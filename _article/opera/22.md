---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:22
layout: post
depth: 2
category: opera
---

### 第二十二場 雙儀舞臺Ｄ

舊曆九月初十，中午左右，昨日從中午下到半夜的一場雨雖已經停了，一場秋雨一層涼，這雨的寒涼的記憶倒還沒從Ｊ城中消退了。

<br/>

“蕭，蕭！打聽到新任市長是誰了！”

因爲消息重大，冷醉忘了拿這公寓的鑰匙，也沒在意爲何敲了半天門，蕭中劍才內裏穿着睡衣，外面披着外套無精打采地來開門。

“是誰？”頭疼地彷彿真地要裂開一樣，被敲醒地蕭中劍其實還沒搞清楚當下的時間，縱使心中再有多少熱情，卻也覺得提不起幹勁來了。

“是叫做閻王鎖的，聽說已經悄悄從北京啓程了，再過個一天，頂多一天半便到了。咱們報社的朋友，剛得到的消息，現在已經要發稿了，估計下午的報紙上便登了。”

“閻王鎖……”蕭中劍眉頭蹙緊——不過有一半是因爲頭疼，“什麼人？”

“嗯，聽說是屯官兵關外的大帥阿修羅的親族，之前是一直在北京做事的。”

“哦”慢慢坐回床邊，“阿修羅曾和天者競爭過政府總理，然而天者得到地者的支持和棄天帝的暗中幫助，終於勝出。阿修羅則被排擠出北京，往關外經營，如今應也是擁兵數十萬了。看他之前作爲政見，也算得是清流了……想來他的親族，應該也是明白事理之人，如今，廣州起兵，又有明理人士代替棄天帝治理Ｄ省，也許能撥開烏雲見月明啊。”

“哈，你這一說……”似乎隱隱在這亂世見到了了希望，冷醉的心緒也疏朗起來。

“……學生聯合會有什麼打算？”當此重大時段，怎可袖手旁觀。

“還沒聯繫過，昨日累了一天，現在想來都還在休息。”說到這裏，冷醉才將呼吸調勻，終於發現蕭中劍臉色不對了，“蕭，你臉色不太好……是不是昨日淋了雨？”

“嗯，有點頭疼……我想……是不是再組織大家，那位閻王鎖市長進城之日，攔車請願？”

“這倒是個好建議……蕭……我看看，你是不是發燒？”眼睛發亮，興奮之餘，卻見蕭中劍似乎是不自覺地靠在身後被子上，瑟瑟而抖，冷醉上前，伸手一摸，果然已是熱了。

“哎呀，這……這，去看醫生吧！不……你稍等，我去你家叫司機來接你！”

“冷醉！”大約是疲勞過度，竟連反應也變慢了，等到將心中策劃如何組織請願之行地念頭放下，想起冷醉的話，赫然一驚，出聲阻止時，人恰恰關門而出了。

<br/>

<br/>

“哈，天草，快來看，今日好多人啊！”

開演在即，伊達我流偷偷扒開臺簾，向外看去，只見沸沸揚揚人頭攢動，都在看着掛在臺口的“病癒復出，重返舞臺，名滿齊魯，天姿國色”的大紅條幅指指點點。而舞臺夥計，正忙着將Ｊ城商會幾家大商行的有償廣告貼在牆上柱下。

“咦？”天草回頭看看，只見師父赭杉軍不在附近，才湊過來向外望去，此時正又夥計端着花籃走過，他剛剛學了些書寫文字，只見花籃上兩條紅布上寫着：【國士無雙，正氣滿歷城；國色天香，美名播泉鄉】他念了幾遍，卻也不知道什麼意思，此時，唱開場【李逵探母】的黃商子和翠山行正在扮戲，叫他過去幫忙，也便趕緊放下臺簾跑去了。

<br/>

在後臺也聽得見前面地喧鬧，蒼竟有些無措，不過念及大約是有段日子沒有登臺地原因，便也釋懷，端起面前花盞，輕輕抿了一口蜜水潤潤嗓子。這時，兩個臨時請來跑龍套的小夥子一面讀着報紙一面閒聊，蒼本無意，然而控制不住聲音飄入頭腦之內，聽着聽着，“啪嚓”一聲，手中的茶碗便落在了地上。

“蒼！”

“師哥！”

聲音刺耳，倒叫後臺的許多人都立刻注目——本來便擔心在意，此時有了變故，更是緊張起來。

“蒼，怎麼了？”彎腰撿起落在腳邊的幾塊大的碎瓷片，隨後丟入天草已經提來的簸箕裏，赭杉軍凝目問道。

“手抖了一下……不礙的。”足尖覺得涼了，才想起自己還站在地下的那灘水裏，趕緊後退一步，卻又撞上旁邊的桌子，若不是墨塵音手快一把接着，便要將茶壺也碰翻了下來。

“小心些……”雖看出反常，卻又不好說什麼，赭杉軍只輕輕說了一句，“這裏弄亂了，讓天草伊達收拾，你換張桌子扮戲好了。”

“久不上臺，有點措手不及了。”蒼垂了眼瞼，看着自己的胸膛，似乎能看見內中心臟跳地格外厲害，只是，……

<br/>

“那個替換棄天帝的市長叫什麼？”

“閻～王～鎖～”

<br/>

短短兩句話，那人走了麼？——原來如此。自已又怎麼辦？看着眼前被前臺座無虛席的場面所鼓舞，幹勁十足忙碌着的師兄弟們，蒼突然覺得竟是如此陌生和不懂了。

<br/>

“胡鬧！”冷霜城重重一拍面前外國茶色玻璃几案，顯然已經渾然忘了自己還在蕭振嶽府上客廳，直接指着冷醉的鼻子，“敗家子！”

“是……”低着頭，冷醉吐了吐舌頭，“敗家子”從十歲就變成了自己的暱稱之一，他斜眼看看剛從樓上下來，坐在一邊的伯父蕭振嶽，知道有人會幫自己說話了。

“我送你去讀政法學校，就是讓你們去學上街鬧事的麼！”

“不是……是叫孩兒開拓眼光和……”後面四個字確然忘記了，總之意思就是，有點見識之後，能夠官商兩道通吃，冷醉情不自禁地撓了撓頭，不感興趣地事情，他總是不記得。

“上街鬧事是不是你的主意！肯定是！還拉上蕭賢侄一起胡鬧啊！啊？現在可好……哼！”又是伴隨着說話節奏和重音的拍桌子。

“是……”冷醉心裏喊了一萬個冤枉，但是他和蕭中劍從小一起長大，在雙方家長心目中的形象早就刻板——反正這種事，在家裏擔下來也沒什麼責任，辯解反而麻煩，何況，“……那個……父親，蕭兄的病情……”

“哦，不礙的……”蕭振嶽終於得到了說話的機會，戴上眼鏡看看新換的茶几確然還是沒有裂縫——卻說，上一個茶几還真是被冷霜城拍壞的，不過，蕭振嶽收留闖禍的醉兒這個習慣還是沒改，“……剛才醫生來過了，說是普通的勞累過度和感冒，發燒而已，並沒有轉成肺炎，只是要好起來，怕要大概一個禮拜了。嗯，他現在應該醒了，你上去陪陪他也好。這麼看，還是醉兒你身體硬朗得多啊！”

“是，謝伯父！”冷醉又情不自禁地吐了吐舌頭，竟然說漏嘴了。轉身跑上樓去和蕭中劍商量幾日後攔車情願的事情了。

……

“蕭兄，你怎麼又把他放走了！”冷霜城一皺眉頭，“這次不比往日，這禍闖下去，自身難保啊！”

“唉……也不是一兩次了，鎮守使公署又把賬單送來了，這次是更換一樓被打碎的玻璃……”蕭振嶽嘆了口氣，“劍兒生病，我可以把他在家裏關上十天，至於醉兒，冷賢弟怎麼打算？”

“隨他去！聽天由命了！”冷霜城說了句氣話，不過其實也知道，自己實在是管不住這個大兒子，“大不了出事了就登報，宣佈脫離父子關係，我好歹也給鎮守使送了不少錢，棄天帝也不會隨便動我。”

“是說……鎮守使被裁撤……繼任市長叫……”

<br/>

“閻王鎖。”任沉浮念着這個名字，情不自禁打個冷戰。

“這個人怎麼樣？”伏嬰師剛剛收拾了一下桌子，“是上次來Ｊ城的總統特使吧，天者的人？”當時自己還在鬱郁不得志中，而進了鎮守使幕下之後，似乎也無人再提起這兩個總統特使了。

“阿修羅嫡系，”任沉浮回答。

“哈，有意思了。”天者和阿修羅在總理府的勾心鬥角，其實也一直是伏嬰師佩服棄天帝的原因——在Ｊ城這種山清水秀氣候宜人的地方做一手遮天封疆大吏，總是好過扯手扯腳的烏煙瘴氣啊。

“麻煩了……”任沉浮吐了口氣，擡手在電話機前動了動，還是剋制住了。

“嗯？”

“你還不知道吧，閻王鎖上次來Ｊ城的最後一天，和朱武少爺打了一架……之後，長官沒有任何反應就把兩人直接趕回去了。”

“原因呢？”

“哪個原因？”

“表兄雖然沒什麼心眼，不過也不是隨便惹事的人啊。”

“當時長官站在窗口應該看見些什麼，嗯……黥武也應該看見了，當時他在三樓，跑下來……哦，對了，應該和蒼先生有關的……”

“哦……”伏嬰師眉頭蹙了蹙，扭頭看着兩個勤務兵將門口的鎮守使公署的牌子收進來，“這事，長官知道麼？”

“怎麼可能不知道，這麼重要的人選，即便是天者也不敢擅自做主吧，否則派來不是找死？”

“嘖……知道了，你忙吧，我下班了。”伏嬰師臉上不動聲色，匆匆收拾了東西，出了鎮守使公署，回自己住着的麟趾巷去了。

<br/>

<br/>

“吞佛真地死了？”天者皺緊眉頭，赫然回頭問道，同時審視地目光又一次打量着對面依舊滿臉悠閒毫不變色的棄天帝。

“不錯。”低頭喝了口茶，看了看方才轉身過來時，眼角餘光掃到的落地窗口的三角鋼琴——以前天者家裏那個會彈鋼琴的女僕水準真是不錯，即便是在類似如今這種風雨交加晚上，竟也能帶給人悠閒地享受——不過現在不知去向了，“我親手打死。”

“好。”緩緩點了點頭，昨天地者帶着朱武去西山打獵，此時還沒回來，總理府內只有他們兩人，對話便更加直接，“那他蒐集的情報呢？”

棄天帝的眼皮擡了擡，又落下，“……簡單看了一下，沒有已知的和你關係太大的人。”

“……我知道了你不驚訝？”不理會對方嚴謹措辭中帶着的嘲弄和警告地意味，天者依舊直截了當。

“把柄這種籌碼，大家都知道了固然失去了價值，無人知道其存在也便基本沒有太大價值了。”

“那你打算怎麼利用？”天者臉上的表情很難說是在和對方一樣享受這談話，步步緊逼，直指目標之下，卻彷彿不只是叫對方越發悠閒，連可選擇的餘地也越大了。

“需用和價值合適的地方。”

“難道不是直接燒掉？”天者的眼睛似乎眨了一下，不過棄天帝沒注意也不在意。

“我不是曹孟德……”半無耐半玩笑地嘆了一聲，嘴角一翹，“世風不古啊，就算當衆處理掉，大家也會覺得是在演戲而已……和現在結果差不多。”

“哈……隨你了。”天者揮了揮手，目的達到也轉換了話題，“總統也快有時間了。”

“Ｊ城、Ｑ城兩市長的人選，你打算什麼時候和我商量？”玩笑結束，放下手裏的杯子，棄天帝也難得露出不耐地神色靠坐在沙發椅中。

“哈，當然是需要和適當的時候。”天者嘴角翹翹，不動聲色地回敬。

此時，外面一聲喇叭響，車燈照亮了諾大的院子。

“天者叔叔，父親，我們回來了！”

朱武的聲音和人幾乎是一起到的，沾滿泥水的獵靴在木質地板上留下一串腳印，若不是棄天帝咳嗽一聲，只怕便要踏上地上潔白如雪的的長絨地毯了，“今日運氣真好啊，打到豹子了！”回程地時候下起了雨，不過好在馬上就進到了車裏，並沒有什麼大礙。

“學長，賢侄的槍法不愧是學長真傳，一槍穿睛，不傷皮毛，內行地很啊。”地者淡淡一笑，其實獵人們早就在山中埋伏了幾天，正好昨天聽說有了獵豹蹤跡，便帶朱武一道去碰碰運氣，不想竟是大獲而歸。

“哈，他自己隨便玩玩，要是我來教，只怕便成廢材了。”棄天帝微微一笑，長身而起，“時候不早，我要去歇息了。”示意之後，便出了客廳上樓往客房去了，而朱武也向兩位長輩道別之後，拖泥帶水地跟上去了。

<br/>

“學長倒是心滿意足了。”換下了微潮的獵裝之後，地者又下到樓下起居室，坐在沙發之上。

“嗯？”難得彎腰給對方倒了一杯熱茶，天者聽到這句話，停了動作，轉頭看了一眼。

“未知夜神和月聲過得如何？”看着放在窗口的雖是一塵不染，卻也許久沒人動過地鋼琴，地者突然又想起自己的養子來。

“很好。”天者靜默了一下，將茶杯端過來，遞在對方手上，慢慢回答。

“你……有他的消息？”

“沒有消息，便是很好了，”天者嗤笑一聲，“追隨在自己崇拜的人身邊，總比呆在家裏天天和長輩吵架快樂吧……”

“可是，阿修羅真的是值得他追隨的人麼……”

“這是他自己的眼光問題，做不出正確的判斷，在哪裏都不過如此。”坐在茶几端頭背對窗口的單人沙發上，天者冷冷一笑，“ 在哪裏都不過如此。”坐在茶几端頭背對窗口的單人沙發上，天者冷冷一笑，“閻王鎖啓程了。”

鎖眉不語，地者緩緩搖了搖頭，將茶杯放在桌上了。

<br/>

<br/>

舊曆九月十三，連演了三日了，上座倒是可觀，不過，如今看來寥寥票錢比起蕭振嶽的資助來，總之是微不足道了。

“咦……師哥！師哥！”

赤雲染從廚房裏追出來，叫住了一隻腳已經跨出院門蒼、赭杉軍和孽角——前日關於資助之事談得倉促，今日蕭振嶽在家設宴，再次款待封雲社幾位大老闆，又是新市長到任的日子，所以便暫停一天演出。

“嗯，什麼事？”赭杉軍回頭問。

“我看看也快沒鹽了，你們回來買米的時候，記着買些回來，不然今晚不夠開飯的了。”

“好！”爽快答應，三人便跨步出門了。

才走出巷口，卻見蕭家的汽車停在路邊，司機正靠在車邊點菸，見到蒼，忙扔了手中洋火，將那根捲菸小心翼翼收好，才走過來招呼。

“嗯？這……曾說我們自行前去，怎敢又如此麻煩貴府來接呢？”蒼輕聲問。

“蒼老闆，您還不知道吧，今是新任市長要來了，再過一會兒幾條大路便都要封了，老爺擔心幾位被攔在路上，便叫我來接了。”司機說着，已經拉開了後面的車門。

“……”

“蒼？蒼？”身邊的人一下子愣着，雖然垂眼瞼看不出表情，然而赭杉軍只聽呼吸聲也知道蒼似乎是有點緊張起來，不由的轉頭問：“怎麼了？”

“……沒……沒什麼。”輕輕搖了搖頭，“……那就有勞了。”蒼說着，低頭彎腰，鑽入了後座。

幸虧蕭振嶽想得周到——新任市長還未到達，已有預備攔車情願地學生提前將其必經之路封鎖，通不了車，只能各種權宜換道而行，本來不到半個鐘點的路途，繞來繞去竟是走了將近兩個小時了。

……

宴席過半之時，外面竟是大亂，蕭振嶽趕緊派人出去打聽，才知道閻王鎖早有準備，帶兵入城，竟是滿街去抓參與情願的學生。街面上太亂，三人只得一直在蕭家坐到下午，等到事態漸漸平靜了，才告辭回家。因爲路上要去買米又不便明說，便堅持婉拒了對方派車相送的好意。本來是想能叫到洋車回去，不過出了巷子見家家閉戶，諾大街上也見不到幾個行人，才知道將方才的事態想得輕鬆了，如今後悔無用，三人只好便挑揀寬敞清淨的大路慢慢走回了。

……

“哎呀！”從米店出來，扛着百斤大米走了一段，眼見就快回去時，孽角轉身看着落在自己身後幾丈的蒼赭兩人，“赭大哥，蒼班主，鹽！”

“啊……確實忘記了！”赭杉軍也有些懊惱，今日一直覺得蒼心神不寧，而在蕭府聽聞了新市長地所作所爲，赭杉軍心中亦有些不平和隱憂掛懷，便也跟着心神不寧起來。

“嗯？”方才蕭振嶽又送了些禮物，雖然不重，體積卻大，一直便在赭杉軍手中拎着，蒼看看只有自己輕鬆，便說：“我便折返去買，師哥和孽大哥先回去吧。”

“……也好，你自己小心，別走太遠，若是不能，便先回來，只是今天做飯，去鄰家借些也可的。”因爲上午那一鬧，街上也不見幾個鋪子開着，這米，還是因爲經常買賣，敲開了店家的偏門扛出來的，若要買鹽，怕是要費番腳程了。

“嗯，我知道。”輕輕點了點頭，蒼便轉身，向着大明湖那邊一向繁華地商街去了。

<br/>

“嗯？出什麼事了？”

跨入自家院門，赭杉軍便覺得氣氛不對，幾步走入後院，卻見衆人滿臉驚慌，皆圍在廳堂，而中間，黃商子鼻青臉腫正在氣喘。

“黃師弟，發生何事？”心中一緊，禮物丟在門口，赭杉軍趕緊走上。

“師哥……是……魔晦王……那廝……”黃商子皮外傷不輕，此時說話還有些斷續，“他不知道從哪裏糾結了一夥流氓，方才我出去買菸，被他們圍上，不由分說便是拳腳齊上……他們說：打得就是封雲社的人，今後在這周圍，見一個打一個……”說話中不停咳嗽，若不是自己皮糙肉厚，只怕便回不來了。

“啊？”

“師哥，”墨塵音皺了皺眉頭，“其實今日師兄弟們都曾出過門，不過只有黃師弟是落單的……這魔晦王怕是專挑……”

“啊！蒼！”腦袋“嗡”了一下，赭杉軍立刻轉身，卻聽在自己之前，院門已經響動，一袋米放在檐下，孽角已經衝了出去。

<br/>

<br/>

當晚，Ｊ城市長那原來是家商號的臨時府邸內，新任市長旅途勞累，進城時又被學生攔着，一番處理，等到終於進了市長官邸，會見等得焦心地前來拜會的各界名流和手下一些官員，終於累到不能，將還在等待召見的幾十人甩在待客廳，自己跑回臥室矇頭大睡，直到晚上十點才終於睡夠起床。揉揉眼睛，命廚子做了晚餐，慢慢享受之後已經是子夜時分。閻王鎖市長擦擦嘴，推開了辦公室大門，一把抓起祕書桌上的電話機，接通了市政府的通訊處，命令說：

“全部集合，市長要辦公了！”

……

“市長閣下……”剛上任的祕書還沒有清醒，顯然是剛剛睡着被叫起來，手中拿着花名冊彙報：“原Ｊ城鎮守使棄天帝閣下的原部署以機要祕書任沉浮、高級幕僚伏嬰師爲首全部請假；高等法院院長殷末蕭和警察廳長斷風塵也沒有來過。”

“嘖……”閻王鎖摸摸光頭，看看面前十數個睡眼惺忪的三級以下文官，“果然啊，因爲在原長官那裏吃了甜頭，更看不起我這年輕有爲前途無量的小市長，所以商量好了，要給我點顏色看看麼？嘖嘖，結黨營私，結黨營私啊，棄天帝實在是野心大得很，大得很啊，這麼看來，這些人是在等他‘Ｄ省’再起的一天了？嘖嘖嘖……”

“閣下，這些人，要不要全都開除公職算了？”小祕書此時已經漸漸清醒，所以沒有先糾正市長究竟是“Ｄ省“還是”東山“了。

“怎麼可以啊！”閻王鎖敲敲光頭，“如果這樣，那我這兩袖清風廉潔守法的小市長，豈不是公報私仇，和棄天帝狼狽爲奸？這怎麼可以，人嘛，都是有善心的，可以感化這些頑固分子，收爲己用啊！跟他們說，帶薪假！想請多長時間都可以！隨時想來上班，我雙手歡迎！這樣不是更好……哈哈哈，棄天帝能用的人，我閻王鎖也能用啊，誰叫他是當年我的老師稱讚過的人，他能做到的事情，我着年輕有爲前途無量的小市長肯定也能做得到啊！你看，他能管Ｊ城，我不是也來了麼……對了，學生抓得怎麼樣了？現在是不是乖多了？嘖嘖，這種事還不簡單，抓幾個就都老實了，看看，今日Ｊ城多太平！”

“市長閣下……其實也……並非如此……”一位書記員終於忍不住插嘴，“今日全城能夠動用地警力都在抓學生，混亂中，便有人趁火打劫啊，開埠區有幾家商戶被砸搶，新市場附近更是出了惡性的傷人事，聽說是將人綁了推進河裏了……”

“啊？死了？”

“倒是沒有，被幾個路人和聞訊而來地同伴救起了。”

“豈有此理！”閻王鎖狠狠一拍桌子，“去查查看，是哪夥人乾的！太笨了！收拾個仇人還能被人救了！太給Ｄ省人丟臉了！啊，哈，雖然我不是Ｄ省人，但是我也覺得丟臉啊！氣死我了，氣死我了……”閻王鎖正如同病人一般自己叨唸地時候一個通訊員跑了進來，稟報說：“市長，警察廳長斷風塵說要見您！”

“咦？”閻王鎖立刻從義憤填膺的表情中恢復過來，摸摸腦門，不耐煩的反問：“那傢伙不是請假了？你告訴他說：

<br/>

“不用客氣，警察廳地事情，過兩天我讓我的表兄閻王祖去處理，你可以去休假了！”

<br/>

從出來報訊的通訊員口中聽到了這句話，斷風塵看了看身邊——被自己在路上看到，索性便拉上車一起過來的伏嬰師，“怎麼辦？”今日下午剛從青島回來，便聽說出了那麼件大事：城裏幾處監獄和拘留所都塞滿了來不及逃跑的學生，其中少不了家境良好的，來說情或者抗議絡繹不絕，終於全都勸走之後，才迫不及待來見現在Ｊ城的當家人。

“我的意見？”伏嬰師臉上倒是沒什麼表情，不過仔細看，似乎在爲什麼其他的事情煩惱着，又似乎只是冷眼旁觀，“警察廳要是還聽你的，那就放人啊。”

“你說的？”斷風塵笑了一聲，兩人開始轉身，慢慢走遠。

“我說的啊。他要是敢造次，大不了這Ｊ城市長你先做兩天！”伏嬰師聳聳肩。

“這麼豪爽？你是不是有什麼別的訊息？”斷風塵繼續笑，拉開了停在路邊的自己專車的客座的車門。

“嗯……兩天是長了一些……”落座之後，伏嬰師才慢慢說出結論。

“……蒼先生怎麼樣了？”斷風塵從另一側鑽入，示意司機開車之後，才猛然問。

聽到這麼問，伏嬰師臉上不太好看了，“性命無憂，不過……事已發生，你就等着長官回來掀你桌子吧。”

“你得幫我啊……”聽到這話，斷風塵臉上表情倒是比剛才閻王鎖要變向撤他的職還嚴重些，“……送二姐夫回青島，是長官的命令啊！我離開前也派了手下人在附近暗中保護，但是誰知道閻王鎖今日調全城的警察都過去對付學生，底下的弟兄們一時不摸門，聽命於他也不是錯啊。”

“我已經幫你把人撈上來，還不夠？否則，就不是掀桌子是挨槍子了吧。”伏嬰師瞥了一眼，用手比劃着，沉着地補充說：“兩槍。舅父一槍，表兄一槍。”

“喂，不過……”斷風塵突然上下打量了一下伏嬰師，“人真是你救的？你這身子板，能救人？”

“……你最好明日一天之內，抓住魔晦王，這樣他能替你擋槍，這樣說明白了？”

“哈……哈哈哈。好，你去哪裏？送你。”

“總來長官家的那個德國醫生，找得到麼？”開了車窗戶，伏嬰師竟是立刻打了個冷戰，這天是越來越涼了啊。

“那醫生不給華人看病……”

“……那就，回公館吧。”伏嬰師撇撇嘴，用睫毛嘆了口氣。

<br/>

伏嬰師在棄家公館的房間在二樓，其實就是以前一間客房而已。推門進屋，倒是不驚訝內中已有人只開了檯燈坐在沙發上看報紙。

“……還不走？”

“我找了個郎中去……”

“……嗯，怎麼樣？”

“戲子氣長，溺水倒是沒什麼大礙；只是他身體一直不好，這種天氣落水，發幾天燒難免的……”將報紙放下，“我來給你送戲票。”吞佛童子的眼神瞥了一下桌上兩張封雲社明晚的戲票。

“你大老遠從賊窩回Ｊ城，就是爲了看戲？”今日是新市長到任的日子，伏嬰師雖然打定了主意不買賬，但是作爲棄天帝的幕僚，倒是出於工作義務關注對方地動向——穿了便衣，躲到附近茶樓，沒想到竟在那裏遇到了這個其實算不上熟稔的熟人。

“奉命看戲。”將報紙往桌上一放，吞佛童子站起身來，走向伏嬰師房間的陽臺。

“哎，從前門出去吧，戒老已經睡了。”

“我知道他睡了，”輕輕晃晃手裏管家隨身的那串黃銅鑰匙，吞佛童子熏熏一笑，說：“正好上樓看看……”

“……做了不該做的 小心長官再槍斃你一次啊！”伏嬰師冷笑一聲，走上前先拉開了窗簾，向外面看看，確認沒有人，才大方的打開了玻璃門，如同賞月散步一般走上了通着的陽臺。

……

“啊？土匪？”

從露臺溜進來，三樓此時也沒有其他人，本想看看就走，卻沒想到床上睡相天真的青年突然睜開了眼睛，吞佛童子愣了一下，只是情急，索性俯下身吻在了正要繼續追問的嘴巴上。

“唔……”本來睜眼就有點暈暈乎乎的，此時嘴巴又被封上，黥武便在一片迷迷糊糊的混沌中又睡着了。 





