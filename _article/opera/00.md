---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-21 23:59:59
layout: post
depth: 1
category: opera
subdocs:
  - title: 引子
    link: /opera/0/
  - title: 第一場 大明湖
    link: /opera/1/
  - title: 第二場 雙儀舞臺
    link: /opera/2/
  - title: 第三場 棄家公館
    link: /opera/3/
  - title: 第四場 棄家公館Ｂ
    link: /opera/4/
  - title: 第五場 棄家公館Ｃ
    link: /opera/5/
  - title: 第六場 棄家公館Ｄ
    link: /opera/6/
  - title: 第七場 東盛客棧   
    link: /opera/7/
  - title: 第八場 舊德國領事館—鎮守使公署
    link: /opera/8/
  - title: 第九場 路邊攤
    link: /opera/9/
  - title: 第十場 棄家公館Ｅ＆東盛客棧Ｂ
    link: /opera/10/
  - title: 第十一場 棄家公館Ｆ
    link: /opera/11/
  - title: 第十二場 雙儀舞臺Ｂ
    link: /opera/12/
  - title: 第十三場 千佛山
    link: /opera/13/
  - title: 第十四場 蓮花山
    link: /opera/14/
  - title: 第十五場 棄家花園
    link: /opera/15/
  - title: 第十六場 棄家花園Ｂ
    link: /opera/16/
  - title: 第十七場 棄家公館Ｇ
    link: /opera/17/
  - title: 第十八場 棄家公館Ｈ
    link: /opera/18/
  - title: 第十九場 封雲社
    link: /opera/19/
  - title: 第二十場 雙儀舞臺Ｃ
    link: /opera/20/
  - title: 第二十一場 Ｊ城 
    link: /opera/21/
  - title: 第二十二場 雙儀舞臺Ｄ
    link: /opera/22/
  - title: 第二十三場 雙儀舞臺Ｅ
    link: /opera/23/
  - title: 第二十四場 天波別業
    link: /opera/24/
  - title: 第二十五場 天波別業Ｂ
    link: /opera/25/
  - title: 第二十六場 封雲社Ｂ  
    link: /opera/26/
  - title: 第二十七場  封雲社Ｃ
    link: /opera/27/
  - title: 第二十八場 路邊攤Ｂ
    link: /opera/28/
  - title: 第二十九場 南崗子
    link: /opera/29/
  - title: 第三十場 泰豐樓
    link: /opera/30/
  - title: 第三十一場 紅樓金店 
    link: /opera/31/
  - title: 第三十二場 封雲社Ｄ
    link: /opera/32/
  - title: 第三十三場 商樂舞臺Ａ
    link: /opera/33/
  - title: 第三十四場 棄家公館Ｉ
    link: /opera/34/
  - title: 第三十五場 封雲社Ｅ
    link: /opera/35/
  - title: 第三十六場 皇華館
    link: /opera/36/
  - title: 第三十七場 封雲社Ｆ
    link: /opera/37/
  - title: 第三十八場 棄家公館Ｊ
    link: /opera/38/
  - title: 第三十九場 街邊公寓
    link: /opera/39/
  - title: 第四十場 棄家公館Ｋ
    link: /opera/40/
  - title: 第四十一場 封雲社Ｇ 
    link: /opera/41/
  - title: 第四十二場 棄家公館Ｌ
    link: /opera/42/
  - title: 第四十三場 泰豐樓Ｂ
    link: /opera/43/
  - title: 第四十四場 天波別業Ｃ
    link: /opera/44/
  - title: 第四十五場 封雲社Ｈ
    link: /opera/45/
  - title: 第四十六場 棄家公館Ｍ
    link: /opera/46/
  - title: 第四十七場 棄家公館Ｎ
    link: /opera/47/
  - title: 第四十八場 封雲社Ｉ
    link: /opera/48/
  - title: 第四十九場 封雲社Ｊ
    link: /opera/49/
  - title: 第五十場 天波別業Ｄ
    link: /opera/50/
  - title: 第五十一場 封雲社Ｋ 
    link: /opera/51/
  - title: 第五十二場 棄家公館Ｏ
    link: /opera/52/
  - title: 第五十三場 Ｊ城車站
    link: /opera/53/
  - title: 第五十四場 棄家公館Ｐ
    link: /opera/54/
  - title: 第五十五場 封雲社Ｌ
    link: /opera/55/
  - title: 第五十六場 棄家公館  
    link: /opera/56/
  - title: 第五十七場 商樂舞臺Ｂ
    link: /opera/57/
  - title: 第五十八場 棄家公館Ｒ
    link: /opera/58/
  - title: 第五十九場 棄家公館Ｓ
    link: /opera/59/
  - title: 第六十場 天波別業Ｅ（正文完）
    link: /opera/60/
  - title: 後記
    link: /opera/61/
  - title: 番外
    link: /opera/62/
  - title: 棄蒼《戲》長評 By 神之殤·寂滅
    link: /opera/63/
---

### 作者：西陵歌者

---
