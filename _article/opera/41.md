---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:41
layout: post
depth: 2
category: opera
---

### 第四十一場 封雲社Ｇ 



舊曆十一月十四，西曆的１２月２９日，送走了朱武，對於其他人來說，邊都是一如往常了。

“伏嬰師！”

跨步走進棄公館的斷風塵，看見那個在走廊裏晃了一下的影子，當即一聲喊，直接追了過去。

“多謝，多謝……”

此時，棄天帝可能還在樓下會議室開會，只有這兩位似乎別有任務，等在三樓的書房之內。而伏嬰師看見斷風塵，立刻連連作揖，滿臉賠笑。

“多謝也沒用，你什麼時候把那村姑送走！”斷風塵揉着肩膀怒道，這幾日，挽月纏上伏嬰師，而伏嬰師只好把她介紹給了斷夫人緋羽，然後以“夫人小姐之間的閒聊，鄙人不便參與”爲由抽身，倒是苦了勞累一天的斷風塵，回到家裏還要再聽憋了一肚子怨氣的夫人抱怨。

“是，是……明日再想別的辦法……”

“挽月還沒走？”

“大帥！”

身後聲音響起，兩人立刻轉身行禮。

“不是隻住三天麼……”其實倒也不太在意，徑直走向書桌後面，“伏嬰師，此事處理好。”

“是。”

棄天帝打開手邊櫃子，取出一隻文件夾，往桌上一放，先是一皺眉頭，不過馬上又嚴肅了。

“朱武不在，咱們把那件骯髒事辦了吧。”

伏嬰師斷風塵兩人神色一凜，同時躬身。

……

“伏嬰師啊……”

安排完畢，已經到了棄天帝可以留他們二人吃飯的時間了，用過午飯，鑽進自己的專車，斷風塵擦擦汗，迫不及待的對還在看着手中文件的伏嬰師說道。

“大帥要除掉恨不逢，從第一樓下手是我早知道的，不過，這份東西，我也是第一次看見……”眼睛發亮，在斷風塵看來，此時那個一直陰森森叫人看不出想法的同僚，這時竟有了那麼一絲絲明顯地歡喜或者說興奮。

“那是誰……不是你不是我，誰給大帥做的這個？”

“如果只有你我，大帥就不是大帥了……若說人選，比如：任沉浮……”

“啊？他？”斷風塵撓撓頭，真是沒想到，自己和任沉浮私交不錯，然而說到公務上的事情，似乎只有交報告或者請求會見的時候才能想起這個似乎永遠都坐在棄天帝辦公室門口的機要祕書。

“哈……別多想了，這種事，不知道比較好。”伏嬰師一笑，“先看看這事怎麼開始着手吧……在少帥回來之前總要了結了啊。”

“不急，既然已經做到了這一步，按照正常程序走就行了。”斷風塵一笑。

“哈，那我也……做自己該做的事情了。”伏嬰師眼睛眨眨，滿意地點了點頭。

看着對方難得一現的志得意滿的神色，斷風塵突然便覺得有些不平起來，猛然想到一事，當即不懷好意地忍笑說：“對了，我想起一事，其實緋羽今天是要出門給咩咩辦入學的手續，所以……”

“啊！”

看着伏嬰師露出狼狽而惶急地神情，斷風塵頓時舒心很多，接下來繼續笑着問：“你想去哪裏？我送你……”

“爪哇國。”伏嬰師白了一眼對方，吐了口氣。

<br/>

<br/>

“你們……”

看着立在院門口的幾個穿西裝的，赭杉軍臉色凝重，不過看着他們手中幾乎高過眼睛的文件，卻也無論如何緊張不起來。

“伏嬰師長官命我們將文書送到這裏，說他稍後會在此辦公。”抱着搖搖晃晃的文件摞，小文書有點吃力地說，“請問，放在哪裏？”

“……跟我來吧。”赭杉軍愣了愣，轉身帶着這一串人走進後院，遲疑了一下，還是無奈地打開了自己的屋門。這時已經是下午，眼看衆人就要出門了。

<br/>“師哥……這是……”

來送公文的一隊人走出院門的時候，正好和送走蕭中劍的蒼迎面碰上，立在院門口，等着這四五個人，每人向着自己一個鞠躬之後，默默離開，蒼臉上也帶着詫異了，擡頭看着最後一個過來的赭杉軍。這時，其實一直在觀望的其餘衆人，也圍攏了過來。

“我也不明白……”赭杉軍搖了搖頭，“他們說，一會兒伏嬰師要來……辦公……”這話音，連自己也說得蹊蹺了。

“這……”

“赭杉，我看，你就留下處理……伏嬰師不是無聊之人，可能以此爲藉口有些什麼動作吧。”

已經穿戴整齊的藺無雙接言。

“好。”答應得有點澀，赭杉軍無奈地搖了搖頭，“正好孽角去斷府給咩咩辦入學的事情，我便先等等吧。”這麼說着，突然覺得自己還是比較安全了。

<br/>

“咦？赭杉，怎麼來得這麼快？”才剛剛開演，就見赭杉軍和孽角雙雙來到後臺，一直覺得伏嬰師不好應付的藺無雙倒有些吃驚了。

“嗯。”赭杉軍點了點頭，似乎是不太想說什麼，而孽角因爲將要登臺了，便趕緊去換衣服了。

“伏嬰師走了？”藺無雙也在上妝，扭頭對着鏡子問道。

“沒有……”將大衣交給門口的打雜，赭杉軍搖了搖頭，走向自己的妝臺。

“啊？”

“他讓我把他鎖在屋裏……”赭杉軍繼續搖頭，實在是捉摸不透這個人的想法，反正他自己帶了杯子和晚餐，屋裏有個取暖的爐子可以熱飯，又存了兩壺開水，關上幾個鐘點倒是也沒什麼，實在急了，也是可以跳窗的。

“……什麼意思？”坐在旁邊的白雪飄也忍不住插嘴了。

“不懂。”皺眉搖了搖頭，赭杉軍接過天草遞上來熱毛巾擦了擦臉。

……

“哎哎，這位小姐，這裏是後臺啊，要找人您去前面啊！”

“我不管，前面沒有就是在後面啦！伏嬰啊～～”

高聲叫嚷，這嗓子的用法讓所有人都皺了皺眉頭。正說着，只見一位粉紅色的小姐已經“騰騰騰騰”地衝了進來。

……

“哎呀，哎呀……真是……”惋惜地看着地上打碎的茶碗和撕碎的戲單子什麼的，黑狗兄又是連嘆了幾聲，讓開一條路，讓伊達和天草慢慢收拾，而後臺忙碌卻也沒有因此而索性停止，“幸虧啊，今天殷二小姐在啊，不然這位帥府的表小姐……哎呀，哎呀……這位小姐究竟是怎麼回事啊？”

後臺亂起來的時候，黑狗兄其實是在前面的包廂裏和幾位名流攀關係，確然不知道是怎麼回事了。不過他也還算是急中生智，請今天來聽戲的殷末簫的次女殷芊嫿出面解圍，兩位小姐倒是投緣，便就這樣走了。

“經理，真的不是我們的錯啊。”這時，蒼、赭杉君和藺無雙都在臺上了，白雪飄雙手一攤，“本來是來找伏嬰師的，後來看見蒼師哥，就喊蒼叔，然後又……然後……其實大家沒得罪她啊，回話還是小心翼翼的……”說着說着，眼角已經向孽角瞥了一眼。

“叔，是我多了句嘴。”孽角此時已經勾完了臉，湊過來說了一句。

“唉，算啦，算啦……”搖了搖頭，“是說，怎麼找伏嬰先生找到咱們這裏了……”

“這……其實，伏嬰先生是藏在赭師哥房間裏的……”

“啊！”白雪飄的話一出口，黑狗兄正在點菸斗的手頓時一抖，差點燙了自己，“這，這……這從何說起啊，難道又……”本想說，難道又是第二個大帥啊，然而孽角“哼”了一聲，轉身去披靠了。

“不過老闆啊，這麼看……”練峨眉有自己的換衣間，此時出來，倒是含笑說了句，“我倒是覺得，那位伏嬰先生，可能真的就是爲了躲這挽月小姐了吧。”

“唉唉，但願吧，就一個月了，可別再出事了……”

<br/>

正在從下場門出來的蒼，聽了這句話，腳步停了一停，下意識地望向掛在門口的日曆：

西曆１２月２９日，舊曆便是十一月十四了吧。

<br/>

“行了，你可以……”

散戲回到封雲社，看到後院一扇窗子亮着燈，赭杉軍才想起屋裏還有個人。心情當然不能說是好的，當即走過去，開了屋門，準備將此人驅逐，卻見屋內，伏嬰師裹着自己的被子縮在床上仍在辦公。

“哦，赭老闆……你沒燒炕。”活動活動凍僵地手指，伏嬰師擡頭——這屋裏裏這麼冷，即便生了爐子，也是已經漸漸住慣了西式洋房的自己所要忘記的了。

“……我，忘了。”一眼即明，赭杉軍也有點愧疚了。

“……挽月小姐……是不是跑去戲園子了。”就是不看着赭杉軍的臉色和他身後黑壓壓的全都帶着逐客表情的衆人，伏嬰師也大概知道。

“嗯……”不願多說，可是看着對方還有點瑟瑟發抖地裹着被子，赭杉軍也有點不忍心立刻就趕他走，“雲染，你給伏嬰先生倒碗熱水……”

“好。”赤雲染忍着笑，“這就去燒水。”

“師姐我幫你生火！”白雪飄看出了什麼，也跑走了。

然後大家也就這麼訕笑着出門，孽角猶豫了一下，還是暫時先去前院小孩子呆的房間去，然而進了屋子，才想起女兒已經住在別人家了。

“伏嬰先生……”蒼和藺無雙倒是沒離開，走進屋子，找地方坐了，屋內狹小，赭杉軍無奈，只得也在自己的床角坐了。

“蒼老闆有何吩咐？”這時大約是人多了些，伏嬰師也覺得漸漸暖和了。

“那挽月小姐……”

“哦，是舅父的遠房侄女，原本只是經過，不過因爲聽說西曆新年Ｊ城有各種遊園，便說要待到那個時候了……”伏嬰師有點頭疼，“因爲當初是我去接的，所以……”胳膊從被子裏探出來一攤手，“有點纏人。”

“哦。”

“所以……”斜眼看了坐在床角的赭杉軍一眼，又往被子裏一縮“我可能還要在這裏躲幾天才行。”

“……這，”藺無雙其實倒是覺得無所謂，“赭師哥你說呢？”

“……畢竟也是女孩子，你這麼晾着她，不怕出事麼？”赭杉軍雙手按在膝頭，說了一句。

“……其實我讓斷風塵安排了兩人暗中人跟着的，這個倒是不必擔心。只是，縱使是位窈窕淑女，現在各位也知道，這個時局，實在是沒有心情風花雪月，何況還是……”伏嬰師吐了口氣。

“……呆到她走，不要打攪其他人的生活。”今天晚上領教了那小姐，赭杉軍想了想，還是點頭了。

“好，我……欠赭老闆一個人情。”伏嬰師微微一笑，看着赭杉軍寬闊的後背，眼神又恢復往常了。

“現在大家要休息了，伏嬰先生請回吧。”似乎是感覺到了後背的視線，赭杉軍起身，一邊走去拉門，一邊說道。

“赭老闆忘記了麼，我在等水開啊。”伏嬰師笑着說。

“那……赭師哥，我和蒼先去休息了……你再陪伏嬰先生坐會兒吧。”藺無雙接過了僵在門口的赭杉軍手中的門環，和蒼走了出去。

<br/>

“無雙師哥……”才將門掩上，又放下了加掛的棉門簾，蒼微微皺眉說道。

“嗯？”

“難道長官，其實有意把挽月小姐和伏嬰師撮合？”

“……我只能說，感覺他沒什麼不良的居心。”藺無雙立在院中，抱肘吐了口氣，表情也稍微嚴峻了一點，“或者，沒有什麼惡意就是了……”

……

伏嬰師喝了熱水，在和孽角睡一屋而赭杉軍去蒼的房間睡，以及自己先回家明日早來兩者之間，做下不甚艱難的選擇之後，便獨自一人走到麟趾巷。剛到巷口，一輛小轎車正好開出來，他倒認得那是法院院長殷末簫的專車——殷末簫是老派又廉潔的人，所以這個時間，車上所坐必是本人了。

從巷口路燈照不到的陰影裏走出來，伏嬰師笑笑，慢慢走回棄家公館去了。

<br/>

“表少爺，您可回來了，老爺還在等您！”

來開門的戒神老者終於看見了悠閒踱回的伏嬰師，趕緊迎了上去。

“舅父等很久了麼？”

慢慢悠悠換着衣服，其實在巷子裏已經看見二樓辦公室一排窗戶的燈光，伏嬰師也知道棄天帝不會半夜不睡等着自己的。

“這倒是沒。晚飯後殷末簫院長親自陪着殷二小姐送了挽月小姐過來，又和老爺在書房談了很久……才送走，老爺便要找您了。”

“……挽月小姐……現在……”

“老爺已經命補劍缺將她送回別墅了。”

“哦。”聽了這句話，伏嬰師這才邁出了走進公館內的第一步，扭頭問關門的戒神老者，“我現在去見舅父？”

“嗯，老爺在辦公室裏處理事情，您自己上去吧。”

……

“舅父，外甥回來了。”伏嬰師是在會議室見到Ｄ省督辦的，臉上立刻掛了嚴肅，雖是開着門，也先停步輕聲告進。  

“嗯。”棄天帝略微點頭，擡頭對着棄天帝略微點頭，擡頭對着屋內不多的幾個人說：“元旦之日，我宴請城內名流，你們準備得如何了？”剛剛應付了殷末簫老先生的諄諄教誨，臉上還有點疲態，說話的聲音也不高。

“屬下明白！”斷風塵立刻起身回答，“魔晦王已被密探誆回Ｊ城數日，不過恨不逢學乖了不再理他，現在被咱們的人藏在城郊，日夜有人監視，不過他本人倒是毫不知情，只以爲是恨不逢派人保護。今天已經聯絡過，讓他們保證元旦之日將之騙往第一樓。”

“嗯……”任沉浮亦點點頭，等斷風塵彙報完畢，也站起身說：“已經確鑿，誠如吞佛所說：半年前，第一樓打死一個叫無豔的姑娘，屍體便埋在他們後院。現在知情人也都控制了，埋屍地點他們已經畫了出來。介時，斷廳長打着追捕通緝犯的旗號去，在這個地點搜查，必有蛛絲馬跡。而且，屬下又查出，這個姑娘是目下廣州軍中一箇中級軍官失散的姐姐……”

“嗯，這件事情，可以等處理完畢，再做文章。”棄天帝冷笑一聲，轉頭看着剛進來的伏嬰師。

“舅父，外甥今日守了半天，封雲社那邊沒有什麼變化，也不見和金鎏影再有什麼往來。”伏嬰師認真說道，“看來……”

“嗯，你便一直守在那裏……”棄天帝點點頭，“只要第一樓別通過金鎏影去給蒼添麻煩便可。”

“舅父，外甥以爲那兩個人對蒼叔來說，只是累贅，不如趁此機會……”提出這個建議，伏嬰師其實也是猶豫了很久，然而只說一半，便被棄天帝揮手製止了。

“……按原計劃。”本來還想再多說幾句作爲解釋或是喝止，只是沉靜了片刻，還是想不到如何表達箇中分寸，索性換了話題，“抓捕時，多加註意，切勿讓恨不逢逃往南方，給朱武和吞佛找麻煩。還要提醒東宮神璽，讓他提防。”

“是！”

“各自準備，明晚再來報告我知。伏嬰師留下，你們走吧。”

斷風塵和任沉浮行禮退出，伏嬰師也大約知道，棄天帝要問他那件事了。

……

“任祕書，剛才殷院長來幹什麼？”斷風塵晚上準備完畢前來彙報，卻被告知殷末簫正在和棄天帝會晤，竟是等了這許久。故此，一走出會議室，伏嬰師被留下，斷風塵便開始向着任沉浮打聽了。

“挽月小姐不知怎麼和殷二小姐認識了，在院長那裏叨擾晚飯。院長約是忍着不能對女士發作，又實在看不過眼，便直接來抗議了。”殷末簫來意不明，棄公館猝不及防，因此談話的前半段，任沉浮是在場的了。

“哈，原來如此。”斷風塵做出一個理解的表情，想想這幾日所見所聞，向着已經甩在身後的會議室指了指，“那……這事……”

“伏嬰的計策吧，他不想開口，就丟給長官。”任沉浮一笑，心中已經在盤算着派誰去送挽月回魔界村了。

“不過，挽月小姐是不是對他有意思啊？”斷風塵撓撓頭，“追的他沒處躲沒處藏的。”

“這……聽說第一天把他錯當成少帥了，然後伏嬰師又禮貌性地請她看戲，不巧那天上的還是【花筵賺】，講的就是溫嶠假借他人之名追求表妹那事……也算是倒黴啊。不過其實看當初大帥的安排，就知道這姑娘沒什麼前途。”任沉浮搖搖頭，“我覺得，本來大帥不想明着管這事，既然是夫人那邊的親戚，也不太好傷了。但是……連殷院長都惹來了，又聽說還砸了封雲社的後臺……二罪並犯，便是大帥也要頭疼了。”攤手無奈，除去公務和軍事上的事情不說，其實棄天帝最頭疼對上的兩個讓他哭笑不得的人，一個曌雲裳已死，碩果僅存的另一個，就是殷末簫了。

“哈，我還想呢，前兩天伏嬰打聽最近都什麼人去雙儀舞臺看戲幹什麼……”

“他是奸猾……不過……”任沉浮和斷風塵此時已經走到公館院門口，看看天色，欲言又止，“斷廳長慢走。”

“咦？你不回家？我送你！”斷風塵倒是奇怪了，扭回頭看，才發現其實任沉浮並沒有離開的意思。

“公事未完，我已經跟戒老說了，今晚加班，不回了，反正一個人，哪裏都一樣，帥府挺好。”

“唉，其實，我說你也該成個家了。”大約是今晚話題的緣故，斷風塵倒是開始多嘴了。

“哈。非常時期，此事別提，太危險。”任沉浮眨眨眼睛，轉身回去了。

走回二層，正要打開客房門進去繼續工作片刻再休息，卻見到表情有點奇怪的伏嬰師也正走過來。

“表少爺……這是遇到什麼難事了？”

“……沒，只是被一件完全沒有可能的事情攪亂了心神和腸胃而已。”伏嬰師臉色還是鐵青，不過說了這句話，似乎也便釋懷很多，手扶上了自己臥室的門扭。

“長官的壞心眼其實不比別人少。”任沉浮不知是今天心情不錯，還是剛才開會或是同斷風塵談話的餘興尚存，竟是難得話多起來。

盯着對方看了好久，伏嬰師突然好像恍然大悟一般，又學着戲臺上的樣子一揖到地，“前輩，晚輩虛心求教，尚有什麼祕訣，請事先教導吧。”

“任沉浮平平庸庸一個祕書，惹到長官又如何收場，其實還是少帥和表少爺您體會更多吧。”笑了笑，“天不早了，表少爺，晚安。”說完，邁步進屋了。

“唉，表兄麼……”伏嬰師嘆了口氣，正要進屋，卻見任沉浮又開門，臉上又恢復了之前的平淡和謹慎的神情，認真問：“伏嬰先生，明日您的公務是送去哪裏？”

“……下午送到封雲社吧……元旦之前，都是如此。”

“好，上午整理完畢，便讓文書送去。”任沉浮點頭，這次是真的關門了。

<br/>

<br/>

西曆１２月３０日，翻了一下，舊曆是十一月十五日，上午。

“蕭！”

冷醉直接衝進了蕭家一衆管家僕人倒是習以爲常，沒有什麼阻攔詢問，紛紛讓路讓他上去三樓，一把推開門走廊盡頭的木門，果然看見蕭中劍立在自己那寬敞簡潔的臥室內，正在換衣。

“嗯？冷醉？”

正立在穿衣鏡前認真地打着領帶，從鏡中見到後面冷醉喘着氣衝進來，蕭中劍一愣轉身。

“怎麼竟回家了……”

先從學校先去公寓，卻連狗不理也不見蹤影，冷醉想了想，應該是預備回家了長住了，然而事情等不得，便又趕緊衝了過來，此時還有些氣喘，也不只是緊張還是剛才一陣疾跑的緣故了。

“嗯……發生了什麼事？”對方的問題，回答起來很複雜，蕭中劍轉身再次整理了一下身上還沒穿過的這身定製洋裝，索性先行跳過，問說：“你今天不是有個考試？”

“沒有什麼考試了！”冷醉這次也難得一本正經嚴肅起來，“今天早晨，考了一半，棄天帝便下令，全省的大學校提前放假。‘只因戰事緊張，體恤學子歸家路途，所以作此決定’。然後先生們可是直接進考場來趕人了。”

“啊？竟有此事？”

“是……門口已經貼了大告示了，我在學校看了一圈，很多人都收拾行李了。”

“戰事緊張……”說到這裏，蕭中劍語氣顫了一下。

“蕭……”頓時明白他擔心朱武，冷醉也實在是不好提醒，其實此時更加嚴重的難題乃是：上京請願，尚無策劃，而學校放假，衆人皆散，今後聯絡不易了。

“算了，事已至此……冷醉，其實我已決定，這便去見棄天帝，答應他在帥府任職的聘請，並且請求即日起便開始實習。”

“啊？！”其實冷醉這時方才喘息平定，才看出來，蕭中劍今天穿得當真是難得的華麗體面了。

“……能第一時間知道前方戰事的所在，只有帥府了吧。”輾轉一夜，總覺得對那個人，不是一句：前方情況不知，唯有聽天由命而已，便能放下的，“況且，明年畢業之後，我身在何方還是未知，先答應下來也沒甚妨礙了。”

“蕭！”

“……我確實應該先了解下棄天帝其人……或許，等蒼日回來……便可說服他了……”

“唉……”冷醉搖了搖頭，“那，上京的事情……”

“……請願是義所當爲。當下提前放假已成事實，那也只能盡力聯絡，努力看看了。”蕭中劍說着，走向書桌，取出了那紙許久不見的聘書，“我需現在出發，聯絡之事，等我回來再去學生聯合會商量吧。”

“好。”皺了下眉頭，冷醉輕輕拍拍蕭中劍肩頭，想勸些什麼亦或寬慰兩句，卻又無論如何不得要領了。

“你放心……我心裏有分寸。”

“……棄天帝不好惹，不過……也不要太緊張。”憋了半天，冷醉自己也納悶爲啥出口的是這一句，“對了，我好像沒看見伯父……”

“父親去郊外看廠子了，我自己去。”再次整理了一下領口和頭髮，蕭中劍似乎是連心中最後一點忐忑也壓下了，邁開步子，走下樓的身形中，帶着那麼一絲興致勃勃了。

<br/>

<br/>

“啊！表叔？”

拎着書箱放假回家的黥武也有那麼點摸不着頭腦，不過剛進門正要去給叔公道聲平安，就被一直守在門口的伏嬰師一把抓着了。

“蕭中劍……在裏面……”比劃了一個手勢，伏嬰師拉着黥武從另一側的樓梯直接去了三樓。

“啊？學長他……”

“嗯……舅父已經安排他在自己的辦公室裏實習做文書了……所以你以後小心……”

“這……這……”

“剛才舅父已經吩咐過了，你暫時搬去別墅住。”

“可是……別墅有挽月小姑……”黥武有點抖，其實也無非是挽月剛來那次，陪着吃了頓晚飯而已。

“我上午剛把她送走，現在別墅空了……”吐了口氣，可以看出，伏嬰師每說一次這句話，都是由衷地快樂的。

“哦，那好，我收拾一下！”

“嗯！正好我要去封雲社辦公，一起從後門走吧。”

“好！”剛剛回來，完全不明就裏的黥武，也沒聽出來“去封雲社辦公”這樣的說法，究竟是哪裏不妥了。

<br/>

<br/>

吃過午飯，大家都開始回屋準備去戲園子上戲了。

藺無雙從前院和衆位師兄弟最後安排回來，正要回屋換衣服的當口，卻見蒼和赭杉軍竟都立在院中而且似乎是毫不相關地發愣。

“呃……”一揚聲，倒不知道該先叫誰好了。

“無雙，我再等等……”總算赭杉軍站得離院門較近，倒是率先打了招呼。

“啊？你是要等……”

“嗯……他不來，我不放心。”赭杉軍一臉正色的煩惱着，倒叫旁人不用誤會這句話的含義了——昨晚，因爲被子上似乎沾了點什麼奇怪的香氣，弄得有點睡不好了，心神恍惚之下，只有一個念頭：跟那人在一起的時候，彷彿沒察覺他身上還有這股氣味的。

“其實，……也許昨天，只是和那個小姐臨時吵架罷了……倒叫咱們做公親……”藺無雙笑了一下。

“……那樣也好。”赭杉軍想了一下，突然扭頭看見，望着原來金、紫兩人住着的屋子發呆的蒼了，“無雙，我沒關係……”說着彼此交換了一個眼色。

“蒼……”

“啊？無雙師哥，要出發了麼？”其實每次看見那屋子，就想起負氣而走的那兩人，可是想要提起，又擔心其他師弟有些什麼想法了。

“……前幾天，又讓塵音去給他們送了點錢……”藺無雙有點無奈， 輕聲說。

“啊？兩位師哥，還是不願回來麼……”其實，這件事蒼是知道的，顏色一正，其實便要將自己的想法說出來了，“這樣下去不是長久之計……”

“這……”其實正要開口，擡眼卻見帶着兩個祕書的伏嬰師走了進來。

“三位老闆，伏嬰師又厚顏前來打攪了。”看來心情是不錯的，隨後輕車熟路，直接推開了赭杉軍的房門，卻是身形猛地一停，讓開陰沉着臉，用高了半頭的眼角瞥了一下自己的孽角，才趕緊吩咐身後的祕書，進屋將火爐和火炕一起燒了起來。

“您不用招呼我了，我自便就好……”扭頭看着跟進來的赭杉軍，伏嬰師眼睛眯了起來。

“……今日還要鎖門麼？”不理會，直接問了一句，不知爲何，突然覺得對這個人，似乎有點坦然了，而且，仔細分辨了一下，才發現面前這位洋裝革履的先生，確然是用了些香水之類吧。

“不用。”被對方難得不帶什麼敵意或者不滿地盯了這麼久，伏嬰師將手裏的幾本冊子放在桌上了。

“雲染今天不去園子，今天不走，我一會兒會託付她給你燒兩壺水。”其實，赭杉軍這麼說，也有些託付的意味了。

“麻煩雲染了。對了，赭老闆，今晚上什麼戲呢？”問這話的時候，伏嬰師已經毫不客氣地坐在床上，身子一歪靠在疊得整齊的被子上。

“【坐樓殺惜】……伏嬰先生有興趣？”其實赭杉軍人已經半身跨在屋外，回頭問了一句。

“唉？這齣戲，不是金老闆和紫老闆的拿手戲，怎地……啊呀，明知故問了，明知故問了。這兩位，還沒消息麼？”

赭杉軍停了腳步，回頭看看伏嬰師，卻也沒說什麼，從門邊拿起帽子和大衣，正要掩門，卻又被伏嬰師叫住，

“明日封雲社休息，今晚大帥應該會去接蒼老闆的。”

“……我該說，多謝告知麼？”赭杉軍本已經有些難看的臉色更加繃緊，冷冷地回答了一句。

伏嬰師聳聳肩，低頭不再說什麼了。

<br/>

“其實……金師弟和紫師弟……一直在躲第一樓追債的啊。”

<br/>

藺無雙其實倒是慶幸伏嬰師的到來了，否則他當時被蒼猛地一問，還沒有想好說辭，墨塵音昨日回來這句，要是全部轉述，真是擔心此時正走在前面，默不作聲的那個人又會露出什麼樣的表情來。

藺無雙囑咐了有點不太舒服，便留在家裏的赤雲染幾句，因爲裏院好歹還有個大男人在，又時不常有幾位送公文的來往，總算也是比較放心。此時看巷子口，蒼跟在衆人的後面慢慢走着，便追上去，接了方才在院內的話。

“蒼，金紫兩位師弟生活只是略微清苦……”

“無雙師哥……我亦想過了，蒼……總是要走的人了，兩位師兄的事情，我不堅持要師哥你做到什麼地步，只是……想至少能再見一面吧。”

“……好，我會留心，找個事宜的機會。”其實，以藺無雙的性急來說，這樣的回答，和拒絕沒什麼兩樣了。

“對了，無雙師哥，第一樓的人是不是沒來要過賬？”蒼沉默一會，路過一間小當鋪，沒有仔細看，然而一眼撇過去，出來的或是進去的都沒有好臉色了。

“是。”蒼的腳步比自己慢，藺無雙壓着的步伐，又因爲這談話的氣氛，倒是覺得有些時走時停的澀味。

“……那定去找金師哥和紫師哥的麻煩了……藺師哥，從我的戲份裏出，縱不能全還，也先給他們一部分，別叫金師哥難受。”

“蒼，便是要還，也是該用他們自己的戲份兒……”其實聽了金紫二人的處境之後，如何還債這事，藺無雙還真想過——直接將存着的金鎏影的錢能給多少就多少，可是用蒼的話說，若不是還有欠錢這一層，與這兩人便真的再難說有什麼非聯繫不可的瓜葛了吧。

“……兩位師哥的錢，便給他們吧……那日說那話，是我昏了頭。若是想靠着這點錢將師哥引回來，是蒼太卑劣……如今他兩人正困難，便給他們，咱們無權處理，更不能雪上加霜啊。”

“可是，那蒼你不需用錢麼……”

“這戲份錢加上錄唱片的收入，到年底我也用不上，用不完……金師哥的戲份，估摸着是不夠還債，倒不如留給他自己應急，衆位師兄弟也是如此……今後若有公共的用度，先從我的賬上出吧。”

“蒼，戲班子的用度，有蕭老資助的款子，怎麼花我心裏有個分寸，不用擔心。只是，就算你要走，也總要留些貼己錢啊，將來……哪怕做個路費救急……”看着蒼慢慢地搖着頭，藺無雙知道不該說了，雖然，其實兩人都知道彼此絕不是在現實中質疑什麼或者執拗地肯定着什麼，因爲更是同知道：這時局之下，最脆弱和不確定的，早已經不是什麼情愛了。

藺無雙靜了靜，想了一下，說：“我一會兒去跟黑狗兄打聲招呼，今晚還是明日，大家算算，提個細賬出來，尋思個兩全的用法吧。”

“多謝師哥。”

“蒼……”其實還是覺得少說了句話的，只是蒼已經自己變了話題，認真討論起今晚將要首演的新劇目來。而沿途街道，似乎是因爲躲避戰亂的災民涌入，竟顯得有點擁擠了和越發混亂了。





