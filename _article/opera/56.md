---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:56
layout: post
depth: 2
category: opera
---

### 第五十六場 棄家公館  



“大帥！！”

<br/>

毫無準備地突然看見起身走出辦公室，卻少有的被門檻稍微絆了一下,踉蹌邁出的東省督辦，還留在屋內的斷風塵以及任沉浮不約而同地向前跨了一步，連一直靠在沙發上的伏嬰師也是挺了挺身子，然而面對那隨隨便便早站穩地高大背影，其實眼神中的詫異多於緊張了。

“散會。”棄天帝穩了穩心思，低低說了一句，至於是不是因爲這偶發地小失誤有些難爲情而沒有回頭，就不得而知了。

……

幾人對望一眼，一直沉默到聽不見大帥回臥室時上樓梯的腳步聲，才長長出了口氣，心中慶幸大帥真的只是一時疏忽。

斷風塵和任沉浮幾乎是同時轉向了早已恢復最舒服的坐姿的伏嬰師，就差指着門口說話：“……咱們，商討出什麼結果了嗎？大帥怎麼莫名其妙就……”

“……我之前看過一本外國人寫的書，”伏嬰師擡了擡眉毛，“……當一個穩重男人日常失誤增多地時候……那麼，不是戀愛就是失戀了吧……”

任沉浮一愣，蹙眉問：“這算什麼道理，而且大帥失誤多麼？”——而況，真的很難將那人同戀愛兩字並論，若是勉強說，父愛還是可以的吧。

斷風塵攤了攤手，說：“之前總是沒有的……”作爲三人之內唯一經歷過戀愛以及成家的男人，倒是沒有懷疑這句話的真實，“……所以，一次就算多了？”於他而言，反倒是此說倒是值得懷疑一下。

“上次大帥有這樣的情況是……”

“……開車撞牆那次，算？”

“……算。”三人都是一副明瞭地樣子，點了點頭， “前兩天燒菸斗燙手那次……算？”

“那次不算吧……那是因爲心煩朱武少爺。”

……

“從撞車到現在……很久了吧……”結束了回憶，似乎能夠確認的也只有這一次，任沉浮又是搖了搖頭，機要祕書對於日期一向是敏感又在意的了。

“以一場戀愛來說，算久麼？”說着，任沉浮和伏嬰師一起看向唯一成家的斷風塵。

“……三四個月，不算。”斷風塵果斷給了否定答案，不過也不算什麼好答案，

<br/>“……奔四十的人了會有……這麼高齡的戀愛麼……”現場的腹誹的三人人都忘了這句話是不是真的有人說出了口，不過此事下了判斷之後，臉色又都嚴肅了起來，“但是……眼下這事？”

“蒼老闆是不是臘月二十三，封雲社封箱之後便要退社了？”伏嬰師收起了看熱鬧地表情，微微沉靜，卻毫不猶豫地問。

“……應該是的。”斷風塵點了點頭，有了方才鋪墊，心中也似乎明瞭，這才是要害了。

“所以……臘月二十三之後，封雲社便和蒼叔，沒有任何關係了。”伏嬰師說完，向着一直站在一旁的看護招了招手，“散會了，扶我回去。”

……

“等吧。”已知非旁人所能用力的關節，任沉浮聳了聳肩，收拾了一下自己的桌子，擡頭看着也準備離開回家的斷風塵，“幾天而已，不出亂子，一晃眼就過去了。”

<br/>

西曆２月5日，舊曆的臘月十八一早。出門去打豆漿的白雪飄回來，繪聲繪色的描述，雖然巷子內東宮神璽及其手下已經撤去，然而換來的監視者只有更多了。

“唉……”

蒼久不在封雲社了，依照性子，現在這院子裏常拿主意的自然而然已經是藺無雙了。此時，藺無雙點了點頭，手裏掰着饅頭，心裏卻是開始思量起來，再也沒動筷子。

“無雙，你看這是什麼意思啊？”先開口的是練峨眉，不過這句雖然是問話，卻也是常年逃亡的時候，夫妻二人常用來商量對策的開場白了。

“要麼就是又有了什麼風吹草動，派正式的人員來是爲了保護蒼，要麼……要麼就是帥府已經確定那幾個逃亡的學子藏在封雲社了。”藺無雙索性放下了手裏的饅頭，端起粥碗來喝了一口，繼續說：“不過，結果都是一樣，就是，咱們被人盯着了。”放下粥碗，藺無雙似有所思的看了看存放行頭道具的前屋。

“藺大哥！”此時，住在巷子對面旅店裏的幽溟興沖沖的過來，“剛收到二哥自成都的電報，催我回家，不如就趁此機會，去一趟帥府吧。”

……

在屋裏已經睡醒起身正要出門的蒼，聽了這句話，靜了靜，便站在門後等着，直至藺無雙同幽溟商議定了，再過一兩個鐘點，帥府上班之後再去，才邁步走出。吃過早飯，各自分手，蒼便同着其他人一起，前往雙儀舞臺準備晚上登臺了。

……然而，一面準備往常登臺，一面卻不見藺無雙同幽溟回來，直至吃過了午飯不久，竟來了一個傳訊官，連黑狗兄也一起找去了。大家面面相覷之下，才覺得事情難道也不是那麼順利了。而臨近傍晚，三人悻悻歸來臉色，都露出爲難了。

“……蒼，”坐在雙儀舞臺後臺的會客室內，隔絕了戲班子內的其他人，藺無雙先開了口，卻又停下，不知如何繼續了。

“藺師哥？此去帥府，可是有了什麼變故？”蒼剛剛落座在沙發上，又向對面的人欠了欠身子。

“是這樣……我的通行證是要下來了，的確如蒼先生你所說，是要乘坐臘月廿三那趟夜車出城的。只是……”幽溟見藺無雙有點爲難，索性便接過話頭一口氣來說了。

“棄帥下令，伏嬰先生親自來傳話，要在大年初一那日下午，請封雲社去帥府唱堂會……”黑狗兄皺了皺眉頭，命令到了頭上，他也只能帶笑應下，回來的路上，藺無雙才有機會向他說明了，然而想想當時情景，卻也仍舊是沒有辦法推脫的。

“伏嬰？他來傳話？”一起進來的赭杉軍楞了一下。

“嗯，赭老闆。伏嬰先生這個人，您大約還是瞭解的……”黑狗兄有點愁眉苦臉——這個人，雖然還拄着柺杖臉色蒼白一瘸一拐的重傷未愈模樣，然而總是透着那麼一股子讓人不放心的感覺。

“嗯……我是想，通知堂會這件事，本不需要他親自來說的……”赭杉軍皺眉。

“對啊，我也覺得蹊蹺，咱們初來Ｊ城的帥府堂會，我記得是任祕書手下的一人來的；後來斷廳長的婚宴，雖然不同，卻也是斷廳長的祕書而已。”黑狗兄細細回憶。

“對……而且，當時我和幽溟正在帥府，卻把黑狗兄單獨叫去……”藺無雙搖了搖頭。

“幾位老闆……我覺得……帥府怕是……”幽溟回來路上一直沉思不語——他家在西南也算政要，雖然之前沒有留意也不參與，然而耳濡目染，總是思路能夠對上的——此時終於是想清楚了，開口卻又有些遲疑，“無論是推知或者查證了，那幾位同學就在咱們院內，如此做，乃是……爲了把我和貴社切割，畢竟我身份特殊，棄帥和羅帥的交情在，若真處理起來有些爲難……而遲遲不動手，也是想等蒼先生退社之後吧。”

“嗯……”藺無雙點頭，聽得明白幽溟這話的另一層含義：因着棄天帝於羅睺的交情，幽溟也是不好在Ｊ城做得太過出格，這幾個學生乃是帥府搜捕的對象，暗中幫助可以，但是這分寸也還是要有，只是，如此說來除非此時將冷醉和那幾名學生送交警察，否則，等到過了廿三，蒼同幽溟離開封雲社，只怕是自己人也未必能夠安然……藺無雙眉頭一緊，眼光再轉。

“既然如此，大家更要在我退社之前離開，才能安全了。”衆人正在遲疑之際，藺無雙也還沒真地將目光落定地時候，蒼卻已經擡頭，看向對方的眼神中也盡是明瞭，不疾不徐地說，“藺師哥，明日不要給我排戲了，我傍晚去一趟帥府，大家還是照原計劃準備……廿三日離開的Ｊ城的機會，我來尋找吧。”

<br/>

西曆臘月十九，新曆２月６日，也許註定將是一個能夠載入歷史的日子。

<br/>

經過了一夜，蒼立在院內樹下，揹着日頭遙遙看着Ｊ城火車站的方向，心中已經有了些個大致的計劃，而吃過午飯臨近將要出門的時候，卻又遲疑——上次這般貿貿然直奔帥府，竟是什麼時候了？這麼一想，心中似乎也就坦然了，去帥府給赭杉軍求情；去帥府要那雙羊玉佩；去帥府送行朱武等等……如今眼下這事，更加緊要，怎會不比當日那樣坦然。

然而事已至此……

<br/>

今日這個拉黃包車的簡直覺得自己自進入黃昏就好像做夢一般——一個清清秀秀的年輕人走來僱了車子，說是要去麟趾巷的時候，他才剛剛好恍然——這位竟是住在封雲社的蒼先生了。而蒼先生要去麟趾巷，只能是去見東省大帥……想明白，心內不知是驚嚇，還是帶着獵奇的榮幸，這一路倒是背對着夕陽跑得格外歡快，深冬的風也沒覺出冷冽來。將到未到時，迎面一輛黑色轎車突然開了出來，車伕也是習慣性地向路邊避讓，直到聽得後面剎車刺耳，才一個機靈，反應過來，那車，竟是從麟趾巷開出來的，那難不成……還沒敢想出來，後面車斗裏的乘客已經輕輕跺腳，叫停了。

車伕回頭，瞥眼就看見方才開過的那車子已經停下，一人慢慢推開後座車門，就那麼一步跨了出來，一身軍裝，外披大衣，站在車旁湖邊，向着這邊招手。

“……棄……帥……”車伕哆嗦了一下，扭頭看看已經下車的僱主，車錢已經遞了過來，愣愣接過來，人家道了一聲：“便到了，您回吧。”就轉身，向着那路邊立着的高大的身影走了過去。

……

看着那人就靜靜立在湖邊，背對夕陽，晃眼得看不清表情，只知道是一徑地招手，蒼開始是走着，後來逐漸快步，再來竟不受控制地跑。

“哈，不急……不急……”眼看臨近自己，蒼竟是跑了起來，棄天帝只覺得有些可笑可愛，卻也有點恍惚——此時夕陽正好，金色柔和，眼睜睜看着蒼迎着夕陽跑進自己的陰影了，竟也一下子就伸臂攬住，順勢轉了半個圈子，讓那金色的光芒繼續照着那熟悉卻又似乎永遠看不夠的面龐。

“長官！”看着少見的一身華麗戎裝，蒼微微吸了口氣。

“找我？”

“是。”

“回家等。”

“……好。”這裏已經是巷子口，走進去不幾步就到了，也便安全，蒼點了點頭，正要轉身，圍着自己腰背的一對手臂卻沒有鬆開的意思。

“讓我……再看看。”捧起了蒼的臉，棄天帝喉嚨動了動，似乎想說什麼，卻還是忍住沒有再說什麼。只是真地戀戀不捨地又看了幾眼，才鬆開了手，“我看你進巷子。”

……

“父親……蒼……蒼叔他來……”

轎車的後座上，其實朱武也在，只是他脖子還套着箍子，行動不便，蒼來了也就沒有下車相見，也免去了些許尷尬。

“嗯。”重新回到車裏，看了看身邊的兒子，棄天帝似乎也沒把心思停留在他的問題上，只是突然腦海中浮現出一個什麼樣的念頭——

剛才那情景，若是朱武同旁的人，大約這小子會說：讓我好好看看你，我在月光下看過你，在晨曦中看過你，在夜色中看過你，在燈燭下看過你，唯獨我，還沒有在夕陽下看過你，所以，這一次便叫我也看夠……這般肉麻的話了吧，輕聲嗤笑，確未發現，說着乃是朱武同旁的人，只是腦海中卻全是蒼的樣貌了。

自嘲一笑，轎車已經穿街過巷，繞着大明湖，直奔焱山議事堂。此時城內的嚴格戒嚴已經結束，百姓總是要出來生活，沿湖賣報的小販所喊的新聞，對於車內人來說，也算不得稀奇，只是看着拿過報紙議論紛紛的百姓，朱武還是忍不住嘆了口氣。

<br/>

“號外，號外，關外阿修羅，跨省歸附廣東政府！號外，號外！”

<br/>

此時，焱山議事堂外應召而來的各路軍政要員也陸續都到齊了，長官下車，便逐級跟進，魚貫而入……

<br/>

……

入夜，蒼自棄公館三層主臥室內的浴室出來的時候，真地是嚇了一跳，不僅是因爲在自己沐浴之時，東省大帥已經悄無聲息的回來，更是因爲，這幾個月相處，從來沒有見過甚至完全沒有想過，那個人是眼下這個樣子——

軍裝只是隨便摔在地上，內裏襯衫的扣子也只解到了第二個而已，兩條套着軍靴的長腿隨隨便便的伸開，用最舒服的姿勢靠躺在壁爐前面的沙發裏——棄天帝的身材，平時端正坐姿還好，此時全都放鬆下來，手臂腿腳都是往最遠處伸展，那本來舒適的單人沙發，瞬間顯得小而擠了。

“……長官。”對面那人的右手放在額頭上，聽見門聲響動地時候，隱在手掌陰影下的雙眸略微動了動，但是也再沒有別的變化。蒼穿好睡衣外袍和拖鞋，蒼穿好睡衣外袍和拖鞋，緩步走過去，棄天帝也只是微微收了收伸直的腿。

蒼也不再出聲，靜靜地在他身邊立定，緩緩蹲下，托起直挺挺的小腿和腳踝，脫掉沉重的靴子……

“你……幾時變得這般乖巧了？”直至累了半夜的雙腳終於輕鬆了些，棄天帝的嘴角終於翹了起來，慢慢恢復了平常的坐姿，只穿着襪子的雙腳平放，順勢拉了正在起身的蒼，攬到了自己的膝頭上坐下，“來找我，有事？”

“嗯。”心不在焉地點了點頭，其實更多的還是擔憂的看着，從不見主這人動露出的疲態。

“難得，蒼老闆以前是寧肯玩命，也不來求我一句呢。”嘴角微微翹了一下，手摸到了蒼胸口正中一個硬物——上次，是爲了救那個小姑娘吧——“不過……如今不同……你有什麼要求，正該向我直說了。”

棄天帝的手摸着那同心指環，蒼微微吐了口氣，說：“本不該再有念想……只是……我……無論如何，也想在封箱退社那日……”

“嗯？”似乎和預想地不同，棄天帝略微坐直了身子，眼神中露出些許認真。

“能在商樂舞臺的大場子上唱上一場。”最後這句，頭已經靠在對方肩上，聲音卻也低得如同私語了。

“好啊，蒼老闆你給我唱上一出，我立刻寫條子。”棄天帝哈哈一笑，倒也沒再勉強，輕輕一推蒼的腰，兩人便順勢站起來，“先睡覺。”

……

換了睡衣後幾乎是一鑽進被子抱着蒼，棄天帝就直接入眠——關外阿修羅歸附廣州之後，中原這塊地方，若再加上東來的日本，便是三面受敵，此等形式，是戰是和，手下衆人爭論一夜，才算暫且有了個姑且折中穩妥的應對，然而心中所知，這一覺醒來，卻又要不得不面對了最終抉擇。

背後抱着自己的人鼾聲已起，蒼也閉了眼睛，只是被子一鼓，蔥花帶着兩隻崽子已經輕車熟路地鑽了進來，大大方方在他懷裏盤了，不一刻也是呼嚕嚕了起來，蒼這一夜，倒是睡得比小時候同師兄弟們一起通鋪的時候，還要熱鬧了……

……

此時，不想棄公館內，還有未熄燈的房間了。

“小叔，表叔……你們說……”黥武此時面對新舊兩個傷號長輩，也顧不得什麼了。

“你想問……舅父他究竟要不要也順勢歸附廣州？”伏嬰師正巧過來隔壁看看帶傷參會的少帥——其實他的傷情已經基本不會再有什麼變化，只是傷處位置特殊，偏偏是不能久坐乘車，沒去議事；倒是朱武一來身體壯實，二來脖子上有個固定，雖是有礙觀瞻，卻是無妨的。

“……嗯”黥武點頭，近日劇變，他又正好在校讀得是政法，這學以致用之下，越想越是緊張不安了。

“以形勢而論，正該如此。”伏嬰師拄着柺杖立在朱武床邊，毫不掩飾地說。

“不會……”朱武沒辦法搖頭表達，只能儘量大聲說，隨後，似乎是又更正，“很難……父親和天者地者兩位叔叔，乃是生死之交……他這個人……”——聽說父親還特意厚葬在車站替他當槍的那口棺材的主人，又推己及人想想那戰場上生死交託的信任和執着——“一素是最在意恩仇二字的。”

“嗯，”伏嬰師不顧黥武臉色，也是點頭，“我也是覺得很難……只是我的理由，又和表兄不同……阿修羅，既和天者地者相爭多年，最終走到這一步，怕也容不下舅父吧……”

“……唉，想也是，我覺得叔公的脾氣也不是那種見到誰勢力大了，便輕易倒向的。”黥武突然認命搬點了點頭，“只是……”

“……就這樣和總統府同生共死？”伏嬰師冷然一笑，“怕是如果大帥一旦宣佈決定，東省倒是失落的要比直隸還快了。”

“啊？”

“別忘了，大帥在北伐中全取守勢，遲遲不動的主因……”

“日本人……”朱武閉眼。

“若要保證東省不失於倭賊之手，舅父唯有儘快同廣州停戰……”伏嬰師抿了抿嘴，卻又微微搖頭。

“……無論父親作何決定，我等此時，也只能全力支持了，國運如此，匹夫奈何啊。”朱武閉眼。

“……若能說動天者地者同廣州和解，也不失爲兩全。”伏嬰師慢慢沉吟，“只要能保證，運作期間，大帥不在，日本人能夠安靜……”

“其實，我此時雖然受傷，心力不足，但若再加上你和……”朱武突然眼睛一亮，只是驟然緘口，看了也一併興奮起來的黥武一眼。

“啊？小叔……？”

“嗯……這倒是個辦法。”伏嬰師的眼睛，確是隨着看到黥武之後，才亮了一亮，“我之前不提，只是覺得那人難得大帥信任，也未必全心輔佐，不過……明日……似乎便可一提了。”

“啊？表叔，你又……”黥武仍是一頭霧水，不過此時，伏嬰師卻已經拉着他走向門口，“少帥還是傷者，此刻累了，咱們走吧。”

<br/>

<br/>

“啊？讓我去犒軍？”

西曆臘月二十，，新曆２月7日，帥府一早，大帥辦公室內一個稚嫩陌生的聲音，滿是驚訝的喊了起來。

“叔公……這……這……”看着坐在書桌後面的棄天帝，黥武少見的又提高了一個調門。

“下午便出發，斷風塵送你，你先去見見人家，犒軍物資隨後運送。”棄天帝今晨心情似乎又已經恢復了以往，隨手翻着一些文件，透過單片眼鏡，又看了看震驚中的侄孫，“哦，對了，你得有個軍銜……年紀還小，之前又沒有資歷，就先做個少尉吧。”

“我……”看着過來送文件的任沉浮隨手從口袋裏掏出準備好的軍銜，遞到自己面前，黥武沒敢擡手接，眼睜睜看着放在面前辦公桌的一角。

“你之前說過的話，不作數了？”棄天帝接了了新的文件，也不再擡頭，將身子轉過去，“不作數的話，把槍還我，立刻送你出國去上學。”

“叔公！我去！”想起生日那天的言辭，黥武頓時挺起了腰桿。

“嗯，那個朱厭，這次……”棄天帝沉吟了一下，把桌上的委任書和軍銜一併推過去的時候，似乎是還在思索，然而，見到黥武確切接了之後，也便做了決定：“……帶上他一起。”

“啊？好！”黥武眨眨眼睛。

“剩下的事，任沉浮替你安排，去收拾隨身物品吧。”揮了揮手，一是驅退，二是將已經落座的任沉浮又叫了過來。

<br/>

“去和商樂舞臺招呼一下，我廿三晚去聽蒼老闆唱戲。” 





