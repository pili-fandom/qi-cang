---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:44
layout: post
depth: 2
category: opera
---

### 第四十四場 天波別業Ｃ



“這是……”已經是半夜了，坐在車上無人相陪時，蒼頓覺的勞累，又想想即將見到的那個人，莫名的放鬆，便將頭靠在後座，小憩了一下，再睜眼，車已經停了，下車望去，滿眼都是漫無邊進的黑暗，颯颯風聲傳來，渾渾噩噩中情不自禁一個寒顫。

“蒼先生，這裏是天波別業啊。”補劍缺亦下了車，走向望着面前大明湖發愣的蒼，提醒道。

“哦……”這才轉身，其實只是心裏默認了，這等大事過後，棄天帝一定在麟趾巷公館，卻不想竟還有閒暇，在別業休息，這恍惚甚是莞爾。轉過身，才慢慢適應了黑暗，藉着那僅有的一窗亮光，看清了面前小樓在夜幕中的剪影。

“蒼先生，我去停車，您請自己上去吧。”說這話，補劍缺其實是又心急着回去看發燒的小兒子了。

“好，您請便吧。”點頭回答一聲，蒼便慢慢步上別墅大門的水磨石臺階，門衛開門之後，立刻便聽到滿耳的自己所唱的【貴妃醉酒】了。

“長官。”

扶着木欄杆慢慢踏上樓去，卻見起居室內，棄天帝坐在沙發上瞑目聽戲，而蔥花和兩隻小貓此時也終於安逸地在他腿上身旁臥着。

聽見蒼的聲音，棄天帝揚了揚眉毛，睜開眼，將在腿上睡得正香的蔥花抱起來放在一邊……

<br/>

雖然主人不在，麟趾巷棄公館內的辦公區還是燈火通明，斷風塵、伏嬰師和任沉浮三人正在會議室內，交換這一晚上的各種情報和成果。

“哎呀，這，這，……”其實，所謂交換，也就是斷風塵將兩位同僚叫來，先商量些對策。“薄紅顏沒有出現，恨不逢宴席中雖有監視，卻還是被他從茅廁窗戶逃了出去，只抓了坐以待斃的賈命公而已。這叫我如何向大帥報告。”

“恨不逢會逃，也在意料之中，況且就在大帥眼皮底下，這事沒有你的責任。”任沉浮淡淡笑道，“蒼老闆出現，他父子應該就明白大勢已去了，不過想來，當時所有人的注意力，全在蒼先生身上，所以才……”

“可是薄紅顏卻是走得蹊蹺了……”伏嬰師其實心中還在盤算，說話也少了些果斷。

“嗯，她的辦公室是在三樓，我們看了，應該是從後面跳窗而走……可是……她一個年長的女流，無人幫助怎會有這樣的身手……你們有聽說過麼？”對此事，斷風塵一直覺得蹊蹺。

“沒，並沒這方面的跡象。”任沉浮搖了搖頭。

“除了薄紅顏，第一樓還有什麼人逃了？”

“我正讓他們用花名冊查對，一會兒應該就能有結果了，嗯，現在還將所有人扣着，只有封雲社那個經理，他不是來嫖娼的，我怕惹麻煩，讓他先走了。”

“他？不會是他。”伏嬰師搖了搖頭，然而心中卻又一動，似乎觸動了什麼了。

這時，外面走廊一陣腳步聲響，應是留在現場的部署終於覈對完畢，來送消息了。

<br/>

<br/>

“便是如此，薄紅顏、五色妖姬在逃，餘者還全部扣押在第一樓，那些嫖客，已經放走了。”

終於來到天波別業，穿過有兩名僕人正在將沙發罩拆下來送洗的起居室，立在書房的時候，斷風塵臉上的汗水一點也不比方才少，而任沉浮和伏嬰師臉上也露出些忐忑了。

“其他情況？”棄天帝是被從床上叫起來的，這時正坐在書房的桌子後面，似乎是心不在焉地低頭整理着還有點凌亂的頭髮，然而只是瞥了一眼面前三名心腹，便知尚有些別的變故了。

“是……有人回憶說，似乎是在薄紅顏逃走的後窗附近，見到了金鎏影……因此，我們懷疑，幫助薄紅顏逃跑的很可能是此人。”努力再三，斷風塵也只能如實上報。

先是看了伏嬰師一眼，棄天帝才緩緩回頭，說了一聲，“繼續查吧，我要確實的信息。”

“是！”三人異口同聲回答。

“……如果證據確鑿，不用顧忌。”

“大帥，那要如何處置魔晦王和賈命公呢？”

“魔晦王三天後槍斃，賈命公……他昨晚放子逃生，獨自留下的氣度我很欣賞，別爲難他，找個地方讓他慢慢養老吧。”沉吟了一下，終於下定了決心，“任沉浮留一下，你們兩人先去吧。”

……

“大帥，還有什麼吩咐？”跟了主子這麼久，被單獨留下的次數自己也記不清了，只是這是最近幾年來頭一次不知道被大帥留下要做什麼了。

“……蕭中劍，你看他如何？”

“嗯？大帥指哪些方面？”

“他來帥府的目的……”

“……蕭少爺只來了一天，尚看不出有甚其他目的，不過，對少帥……他對少帥，似乎很感興趣。”

“嗯？”

“……我覺得可能只是好奇或者，對戰事很緊張吧，畢竟對外來說，這是少帥第一次戰場，不知內情的人心中必然還是會有些忐忑的。不過，他曾在席間問起東宮有關少帥的事情看來他似乎是不知道少帥確切是誰……”朱武化名蒼日接近蕭中劍這種事，雖然不曾聽伏嬰師提起，但對於任沉浮來說，察覺也並無困難。

“哦……你明天去向伏嬰師要‘無人’的資料，以及最近三年Ｄ省省學生聯合會的記錄。仔細看過之後……應該時間也足夠你觀察蕭中劍了，然後再來向我說你的結論。”

“是。”任沉浮不再多說，躬身退出了。

<br/>

走出書房，天還沒有亮，慢慢拉開對面臥室房門，走廊裏的燈光照耀，見蒼還睡在床上，棄天帝也便寬衣坐在床邊，拉開被子躺在蒼的身邊。

……

被裏是溫的，棄天帝一皺眉頭，向前湊了湊，將蒼抱在懷裏。

“醒着？”

“是……”聲音雖小，卻是清楚。

“不舒服？你在發抖……”不甘心地將懷裏的人抱緊，這顫抖似有似無，分外陌生了。

“無……”蒼用了最大的努力剋制，才叫自己沒有真地顫抖起來，本來只是起身解手，卻不知怎地竟就拉開門，聽見了對面房門虛掩的書房內的談話……

<br/>

“唉，其實那天大帥要是聽了伏嬰師你的建議，先將這兩人處理了……”

“噓……既然礙着蒼叔，大帥也只能等到有確鑿實據，才能動手吧。”

“添亂啊……”

<br/>

“起來！”

身後抱着自己的人突然一挺身坐了起來，本已溫暖過來的身軀，又接觸到了冰冷的空氣，縱然壁爐裏燃着的木炭噼啵作響，也絲毫不能增加暖意。檯燈點亮，蒼轉身尋找已經下床的棄天帝時，見他坐在壁爐前的沙發上，拿出了也許不是同一只，卻也沒有什麼區別的酒杯和半支紅酒了。

“來，紅酒是好東西……”臉上也不帶着笑，聲音卻還溫柔，蒼慢慢走過來，瓶口竟撞上了杯子，幸好馬上就被扶穩了。一把攬過只穿着睡衣的蒼，放在腿上，端起面前的酒杯，略微等了一下，才說：“特別是冬天，對身體有好處。這是上次那個要捅你一刀的德國醫生，說得最中聽的一句話了。”

蒼沒說話，玻璃杯的邊沿已經碰到了下脣了，今晚的酒，當真喝得太多了。

“今晚的戲……”本應早就說起的事情，卻到了這個時候才有時間提起，棄天帝看着蒼慢慢喝下那杯美麗顏色的液體，“還真是俏皮了。”

蒼蹙了蹙眉，一時間聽不出來這話的語氣，不過其實卻也不太在意說話的人是什麼心情了。

“哈，不聽話還是不懂我的意思？”這麼說說，已經禁不住又擡手在那還睡得粉嫩的臉頰上捏了一把。

“……封雲社人手不足，只能做此權宜。”腦海中還是一團混亂，方才聽到的隻言片語想問卻又說不出口。

“哈，本應罰你，不過看在那俏皮的小周瑜的份上……剛才在客廳沙發上，就算罰過好了。”

“長官……”

臉上熱熱地發燒，被之前的廝磨消耗和之後的熟睡壓抑的酒興此時又在被這香甜的飲料和對方的手指勾了起來，蒼捧着輕飄飄彷彿不存在的杯子，任由棄天帝慢慢將他的手指從杯腹慢慢掰至下面那細長的頸子，湊在耳邊輕聲說：“喝紅酒，要捏這裏……想說什麼？”

“……沒有。”蒼手中捏着空杯子，不自覺地靠頭在對方肩上。

棄天帝看着對面凌亂的似乎還能見到餘溫的床鋪被褥，眨了眨眼睛，嘴角翹了起來，“我不是不懂徇私的聖人……”感覺到對方的本已經放鬆的身體再次緊張了起來，棄天帝從他手中拿走了酒杯，放在茶几上，看見被那杯子扭曲的陸離的屋內一角的圖影，想起第一次蒼就站在這樣坐着的自己的面前，緊張卻又鎮靜地向起自己請求關照同門的樣子，繼續說：“可是，也不是……看不清大局和利害的蠢人。”

“……蒼，明白。”沉默了半天，也許是走思或者說乾脆心思就全被其他的事情沾滿了，蒼過了許久才恍若回神一樣明白了過來，臉上不自覺的露出了比痛苦還要讓抱着他的人介懷地平靜，簡潔的回答之後，扭回了頭。

棄天帝看着坐在自己懷裏的蒼的側影，不知不覺的又擡起手摸着他臉頰，熱熱的，不知是對方的酒意上來還是體溫升高……棄天帝又看了一眼凌亂的床鋪，打橫抱起懷裏的人，慢慢放了上去……

“長官……”不是厭煩或者是撒嬌，只是有點不解對方的食言——除非是“戲”，否則，即使只是玩笑般的言語，Ｄ省大帥只要是說出口的話，從未有不應驗的。

“剛才是懲罰，現在是獎勵……”——當然，棄天帝自然多地是辦法讓自己保持信用。

<br/>

<br/>

“老爺，少爺電報。”

“拿來我看……”

蒼其實覺得自己應該是睡得不甚安穩的，因爲一直想着要早些起來趕回封雲社去，然而在些些混亂的夢境中，聽到這對話，並沒有覺得奇怪或是什麼其他的反應，只是似乎是在幾秒鐘之後，才猛地醒悟，強迫自己睜開酸澀的眼睛，看着用左臂摟着自己的棄天帝完全沒有起身地意思，僅僅從被子裏伸出赤裸的右臂，接過那單薄的紙，嘴角翹了起來。

……

“長官……”等了不到幾秒鐘，等到棄天帝將電報放在床頭，蒼迫不及待的開口了。

“嗯。”哼了一聲，棄天帝微微側頭。

“蒼想告辭……就不吃早餐了。”

側頭瞄了一眼床頭的鬧鐘，棄天帝竟是爽快的回答：“好啊。”

“……”臉上露出詫異，答應的這麼爽快，反而讓蒼覺得是不是又有什麼別的變故或者打算了。

“起來，該吃午飯了。”棄天帝好像年輕時對兒子惡作劇之後那樣的笑了起來，毫不介意的掀被下地。

“山水極妙，住處不錯，主人盛情，到日即以燉得極爛的豬頭肉款待。聽說明天會有人再送兩頭大肥豬來，主人要帶我去下刀。鄉下豬沒肥肉，就是皮厚。宰殺容易，一捅就死，唯恐柴火不夠，燉不大爛。”

像蕭中劍這樣的富家子，根本是沒有見過殺豬的，但是讀着這份電報，只覺得一陣噁心。

……

聽說今天上午收到了少帥的電報，不過Ｄ省督辦還在別墅，想來在棄公館的辦公室是看不到得了。蕭中劍雖然明白這事理，卻仍是自覺不自覺地一直藉故在任沉浮周邊探問，對方倒也是善解人意地大方，微微一笑，便從那雖然公事繁多，卻仍是整整齊齊地辦公桌上拿起壓在硯臺下的薄紙，遞了過去，“想看？拿去看吧。”

“這是……”蕭中劍看過，捏着電報紙的手在抖，卻也只能儘量很平穩地淡淡問。

“大帥剛剛看過，這是來電話命我備份歸檔，不過，畢竟是少帥的電報，應該還有很多人想看了。”語氣雖然淡淡，只是卻讓人琢磨出一種不能批量油印，全帥府傳閱的遺憾。

蕭中劍無言以對，尷尬地一側頭正看見門口的高大座鐘——他記得電報是早晨九點到的，而現在已經過了十二點了，“……少帥，他……”稍稍平靜了一下，慢慢放下手中的紙，把下半句“是去前線學殺豬的？”生生嚥了下去，感覺卻比吞下一整塊肥肉還要難受了。

“嗯。”任沉浮就算沒有一大堆事情要做，也絕不會向這個才來實習的青年解釋什麼，將電報收好，側頭想了想，說：“蕭少爺，本月４日是瞾雲裳小姐五七，東宮少爺之前應去蓮花山看墓地，您以大帥的名義發函慰問吧。”

“……好。”瞾雲裳過世的慰問函——蕭中劍擡手揉揉額角，來這裏之後，很快就被任沉浮發現了專長，將起草社交函件的工作一式推了過來，蕭中劍只覺得，從評價少帥“業已成年”那句話開始，自己這幾日所說所寫的假話，比之前的二十年都要多了，嗯，好像也不是這樣的。



“補劍缺，”這時，任沉浮自顧抓起桌上電話機的聽筒，接向後面的警衛室，“備車，大帥下午要來，蒼先生也要去封雲社。”

“啪！”的一聲，轉身的蕭中劍碰翻了桌上的紅墨水。

“蕭少爺！沒事吧？”揚了揚眉毛，其實剛才是看見了的那搖搖欲墜的墨水瓶，不過只是那一瞬間講電話佔着嘴，沒有來得及提醒，此時，任沉浮趕緊放了電話聽筒起身探問，

“無……”雖然是文人，蕭中劍畢竟還是年輕，動作和反應都是不慢的，及時用身體一擋，便已經反手接了瓶子，桌上地上倒是沒什麼損失，然而手忙腳亂接過任沉浮遞上的手巾，卻還是不能阻止血紅的顏色染上腰際。

“……您……回去換件衣服吧，大帥已經起身了……應該快過來了。”任沉浮也抓了幾張桌上的棉紙幫忙擦了幾下，不過看着駝色地洋服衣服後襟下襬還有褲子上的痕跡擴散滲透，着實觸目驚心也頗有礙些觀瞻，而且，染色那個位置……只怕哪裏也坐不下去了。

“……好……”突如其來的狼狽加上之前些些信息勾起地怨氣和焦慮，蕭中劍整個人其實已經變得有些恍恍惚惚，看看滿手的紅漬，也沒法收拾什麼東西，只得先去附近的水房簡單衝了一下，便抓起門口的大衣，將後腰上的痕跡蓋了出了公館。



“……無患？”

在洋車上如坐鍼氈的回到自己家，換衣服的時候才發現墨水的滲透遠比自己想的嚴重，又拖延了這麼久，情況越發不可收拾之下，只好把全身內外的衣褲都脫了，去浴室簡單衝擦洗了一下身體，才出來更衣。一面繫着襯衫釦子，同時用腳把已經很難清理的套裝和襯衣裏衣之類簡單歸攏在門口。這時才看見正敲門進來準備取衣服的竟是和自己從小一起長大的管家之子。

“嗯，少爺，我今天才回家的。”因爲受到了督辦放假令的影響，終於在青島開始學西醫的青年也能提前回家了。

“你去給公館去個電話，我有點不太舒服，下午請假。”才只穿了襯衣，髮梢還有點滴水，看着衣櫃裏的收拾得整齊乾淨的外套，蕭中劍突然有種厭煩的關上了櫃門。

“……少爺，哪個公館？”其實才剛剛回家的青年有點疑惑的問。

“……棄公館，Ｄ省大帥……”人已經躺在床上，蕭中劍一隻手無力的放在眼上，另一隻手已經攥緊了拳頭。

其實原來是在一家中醫館做過學徒的，金無患看看少爺的氣色，心中有數之餘也不再多說什麼，只走過來，幫少爺拉開了被子，略微蓋上，也就收拾了髒衣服輕輕關門出去。

“混蛋！”聽到門被關上，蕭中劍哼了一下，竟是嗚咽出聲，這些天衣冠楚楚彬彬有禮其實無非就是在爲一群混蛋工作罷了。

……

“兒！兒啊！”

突然聽到有人狂敲自己的房門，蕭中劍揉揉眼睛才發現自己竟是迷迷糊糊睡着了，而門外傳來的竟是父親急切地聲音。

“父親！”猛地坐起來，覺得頭有點疼，約是冷天溼着頭髮睡着的結果，不過父親叫門甚急，也只好先跌跌撞撞的去開門。

“兒啊，你沒事吧！”

“父親？”眼角還有剛才生氣時擠出的眼淚的痕跡，蕭中劍下意識的用手指蹭了一下，卻突然被父親一把抱住，推進了屋裏。

“父親？出什麼事了？”

“劍兒，你……實說，在……大帥府遇到什麼事了……慢慢和爲父說……別怕……發生了什麼都……不打緊……”蕭振嶽聲音顫抖，先把門關上，才又將兒子摟在懷裏，想起剛才路過洗衣房，現在又見到兒子只穿了一件襯衣眼角有淚的情景，就差老淚縱橫了……

<br/>

<br/>

“哈哈哈哈！”坐在自己的房間裏，看着難得跑來串門，還臉色難看地要命地蕭中劍，冷醉笑地直不起腰來。

原本被冷風吹得有些風寒的先兆，不過被剛才父親一鬧氣出一身冷汗熱汗，蕭中劍尷尬萬分跑來冷家的時候，已經半點難受都沒了。

“小心點嘛！”冷醉給蕭中劍又倒了一杯熱水，“棄天帝素行不良，伯父他那顆心一直放不下來啊！而且，你會碰翻東西，也算是百年不遇了啊，要是我也絕對……哈哈哈哈哈。”

“哼，老子荒唐，兒子飯桶！”蕭中劍又恨恨罵了一聲。

“哈哈哈，不過蕭兄，你因爲這件事罵棄天帝呢，他實實在在是有點冤枉的，罪魁禍首還是那個豬頭少帥吧？”

“唉……”嘆口氣，其實說棄天帝荒唐，倒是指別的事了，不過，蕭中劍不是亂嚼舌根的記者，大帥幾時起身，又和誰一起過夜之類的訊息，實在是不會如同報紙新聞一般隨隨便便捕風捉影地說出口的。

“伯父的反應倒是正常，不過我其實更想知道若是蒼日知道……”

“……我要……做點什麼……”提到那個人名，蕭中劍眼睛有點發直，突然又亮了，“冷醉，學生聯合會那邊，沒什麼動靜麼？”

<br/>

<br/>

“阿嚏！阿嚏！”

趴伏在山間枯草後面的小溝中，吞佛童子無奈地看了一眼身邊還在抽着鼻子的少帥，頭頂的一大群烏鴉剛剛飛過，而遠處的一小隊敵人也緊張地停了下來，“別管什麼時機了，既然少帥已經下令，打吧！”更加無奈地向身邊的部將一揮手。





