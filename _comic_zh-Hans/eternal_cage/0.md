---
author: prayer
title: ETERNAL CAGE
date: 2023-07-20 00:00:11
layout: post
depth: 1
lang: zh-Hans
category: eternal_cage
---

#### By prayer

弃苍初见16周年纪念活动 10:00 【ETERNAL CAGE——4.5弃苍初见纪念日16周年-哔哩哔哩】

提前发一点ooc声明：这只是一个换头，剧情走向和人物行为均保留原作视频

---

<div style="position: relative; width: 100%; padding-top: 56.25%;"> <!-- 16:9 Aspect Ratio -->
  <iframe src="https://player.bilibili.com/player.html?bvid=BV1XZ421q7No" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"></iframe>
</div>
