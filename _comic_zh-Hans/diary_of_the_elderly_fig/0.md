---
author: 三层梳妆台
title: 老年人日记（图）
date: 2023-07-16 00:00:09
layout: post
depth: 1
lang: zh-Hans
category: diary_of_the_elderly_fig
toc: true
---

#### By 三层梳妆台

---

### 1

醉酒小葱和在校外偶遇葱花的老弃

<figure class="responsive-figure">
    <img src="../1712250449.jpg.png" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1712250449.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 2

文字冒险游戏《老年人日记》将于四月32日登陆全平台。 今日解锁实机演示，更多cg敬请期待。 故事发生在风平浪静的异度魔界别墅区。 前黑道弃天帝在钓鱼途中遭遇了神秘的美青年——苍，他的生活从此发生翻天覆地的变化。 真心？假意？莫名消失的资料，围裙上的血迹，看似合理的一切，似乎有哪里不太对？

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455311_856615144-7d1a982b414bd361f447712232d5f6a0-932684568-1-0-wifi-0.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455311_1074916133-4e048d3ceec9b176e4ba955a1f47809b-931761047-2-0-wifi-0.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455311_577730907-ec80c746d61d86522bc7597b75ebaa06-930837526-3-0-wifi-0.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455311_2118311490-8c80cc3db7765cf96ab027069acb1093-925296400-9-0-wifi-0.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 3

cg展示

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455513_5.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455513_4.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455513_.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../diary_of_the_elderly/1713455513_7.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>
