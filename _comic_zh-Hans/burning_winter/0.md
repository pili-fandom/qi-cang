---
author: 三层梳妆台
title: 燃冬
date: 2023-07-16 00:00:06
layout: post
depth: 1
lang: zh-Hans
category: burning_winter
toc: true
---

#### 作者：三层梳妆台

---

燃一下（）

### 一

<figure class="responsive-figure">
    <img src="../../../burning_winter/1708620290_1.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 二

补充①

<figure class="responsive-figure">
    <img src="../../../burning_winter/1708620374_1.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 三

补充②

<figure class="responsive-figure">
    <img src="../../../burning_winter/1708620420_2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>
