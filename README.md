---
layout: home
title: 公開亭
longtitle: 公開亭
permalink: /
lang: zh-Hant
comment: home
---

### 05/04/2024 糧倉週報

棄蒼初見紀念日糧食[匯總頁](april)，持續更新中~~

### 23/03/2024 糧倉日報

- [一千零一夜（番外二條漫）](1001nights_comic/0/) By 三層梳妝臺

- [天命（修订版）](fate/00/) By 終朝采藍

### 19/03/2024 糧倉日報

##### 近期新增：

- [下山·二](down/0/) By 三層梳妝臺
  
- [老年人日記（圖）](diary_of_the_elderly_fig/0/) By 三層梳妝臺

### 13/03/2024 糧倉日報

##### 近期新增：

- [2024白情](white_day/0/) By 三層梳妝臺

- [老年人日記](diary_of_the_elderly/0/) By 三層梳妝臺

### 23/02/2024 糧倉週報

##### 近期新增：

- [遇龍](encounter_dragon/0/) By 三層梳妝臺

- [燃冬](burning_winter/0/) By 三層梳妝臺

- [相愛相殺表格](enemies_to_lovers/0/) By 三層梳妝臺

- [小龍人·三](little_dragon/0/#三) By 三層梳妝臺

- [很草的摸魚（性轉註意）·三/四](gender_reversal/0/#三) By 三層梳妝臺

- [可拆卸嘰嘰（限制級慎入）](cat/0/) By 章魚腿毛

### 14/02/2024 糧倉週報

##### 近期新增：

- [食色性也](food_sex_human_nature/0/) By 惘問

- [弱水](weak_water/0/) By 刻舟求劍

- [伊索爾德之夢](isolde_dream/0/) By dgrey

- [喜宴](banquet/0/) By dgrey

- [空間、系統與宿命](space_system_fate/0/) By 容予

- [爛桃花](peach_blossom/0/) By 三層梳妝臺

- [青宮客相關](transient_visitor_related/0/) By 三層梳妝臺

- [小龍人](little_dragon/0/) By 三層梳妝臺

- [很草的摸魚](gender_reversal/0/) By 三層梳妝臺

- [清醒夢](conscious_dream/0/) By 在崑崙玉虛當咖啡師

### 道境新闻

![cang](./assets/img/cang.jpeg)

**震驚震驚震驚!!!** 數日前一妖道角經過魔界宮外，聽聞宮內花叢中似有異聲，竟發現異度皇後和一狂徒正顛鸞倒鳳不知天地為何物。目擊者表示，被發現時倆人還在花叢中大汗淋漓，異度皇後的紫色鴛鴦肚兜還掛在那狂徒的腰帶上呢!!! 這究竟是道德扭曲，還是人性的淪喪？現在我們來採訪一下當事人٩(✿∂‿∂✿)۶

當事人A：（淡定）子虛烏有。

當事人B：（皺眉）誰造的謠？污穢。

（圖為當事人A日常生活照）
