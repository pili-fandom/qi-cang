---
author: 三層梳妝臺
title: 2024白情
date: 2023-07-16 00:00:08
layout: post
depth: 1
lang: zh-Hant
category: white_day
toc: true
---

#### 畫師：三層梳妝臺

---

### 預熱

扯頭髮 先發一張預熱下，草稿一大堆就是不知道能畫完多少（）

<figure class="responsive-figure">
    <img src="../1709397502.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 性轉現代

畫完了有被迷到……

<figure class="responsive-figure">
    <img src="../1709775366.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 小龍人

<figure class="responsive-figure">
    <img src="../1710341654_1.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 天命

<figure class="responsive-figure">
    <img src="../1710341799_3.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 老年人日記

<figure class="responsive-figure">
    <img src="../1710341944_2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1710341945_2-2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1710341945_2-3.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 異國pa

<figure class="responsive-figure">
    <img src="../1710342021.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 老年人日記（2）

<figure class="responsive-figure">
    <img src="../1710342077.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>
