---
author: 三層梳妝臺
title: 下山
date: 2023-07-16 00:00:01
layout: post
depth: 1
lang: zh-Hant
category: down
---

#### 畫師：三層梳妝台

---

### 一

腦補的天命13章下山那一段

<figure class="responsive-figure">
    <img src="../IMG_20240121_162609.jpg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 二

69章末尾

<figure class="responsive-figure">
    <img src="../1710766779.jpg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1710766779-2.jpg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 三

小蔥:不會是個傻的吧……

<figure class="responsive-figure">
    <img src="../1714141265_4.png" alt="Your Image" style="width: 100%; height: auto;">
</figure>
