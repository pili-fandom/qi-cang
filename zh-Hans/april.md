---
title: 弃苍初见纪念日12h产粮活动
layout: post
date: 2024-03-08
lang: zh-Hant
---

📢📢乡亲们看过来啊！弃苍初见16周年纪念节目单来啰~~

#### 为什么是4月5日呢

1. 因为这一天是弃苍剧中初见的日子！（好吧我也没数过，反正《青宫客》里这么说的）

2. 因为这一天忌结亲、嫁娶、入宅、求嗣，总之不适合恋爱，但是非常适合弃苍这对强取豪夺猫毛乱飞的cp

3. 因为这一天在清明后，谁家正常cp清明节办活动啊！但弃苍不是正常cp啊！

#### 节目单

##### 07:00  [致五千年后的你](../after5000years/0/) By Chaconne

[http://www.36rain.com/read.php?tid=159009](http://www.36rain.com/read.php?tid=159009)

##### 08:00  [道士和亲](../fate/73/) By 终朝采蓝

[http://www.36rain.com/read.php?tid=155858](http://www.36rain.com/read.php?tid=155858)

##### 09:00  [契约诗](../contract_poem/0/) By dgrey

[http://www.36rain.com/read.php?tid=158826](http://www.36rain.com/read.php?tid=158826)

##### 10:00  [ETERNAL CAGE](../eternal_cate/0/) By prayer

[https://www.bilibili.com/video/BV1XZ421q7No/](https://www.bilibili.com/video/BV1XZ421q7No/)

##### 11:00  恋爱试用期 By 寒鸦知我意

[https://weibo.com/5793078975/O8hSMcYV4](https://weibo.com/5793078975/O8hSMcYV4)

##### 12:00  弃苍cp图 By 小雨

[https://xiaoyudongchenglexiaoxue.lofter.com/post/1e8a1970_2bb8cd3a2](https://xiaoyudongchenglexiaoxue.lofter.com/post/1e8a1970_2bb8cd3a2)

##### 13:00  猫想要，猫得到 By 章鱼腿毛

[https://weibo.com/2411235383/O8iFsBExJ](https://weibo.com/2411235383/O8iFsBExJ)

##### 14:00  两相疑 By 蓬山

[http://www.36rain.com/read.php?tid=157199](http://www.36rain.com/read.php?tid=157199)

##### 15:00  堂前燕 By 扶桑

[http://www.36rain.com/read.php?tid=159012](http://www.36rain.com/read.php?tid=159012)

##### 16:00  传说 By 几时修得到海棠

[https://www.bilibili.com/video/BV1px421D7HW/](https://www.bilibili.com/video/BV1px421D7HW/)

##### 17:00  囚徒 By 三千白雪三千尘

[https://weibo.com/5876827836/O8likeqDR](https://weibo.com/5876827836/O8likeqDR)

##### 18:00  1001夜衍生图 By niudanzhi

[https://qicang.ovh/anniversary16/0/#1001夜](https://qicang.ovh/anniversary16/0/#1001夜)

##### 19:00  弃苍cosplay By 玄攸

[https://weibo.com/6451018818/O8l1EBuwy](https://weibo.com/6451018818/O8l1EBuwy)

##### 20:00  弃苍情头 By 十月

[https://weibo.com/7737402078/O8lq7m8Wl](https://weibo.com/7737402078/O8lq7m8Wl)

##### 21:00  天命衍生图 By niudanzhi

[https://qicang.ovh/anniversary16/0/#天命衍生图](https://qicang.ovh/anniversary16/0/#天命衍生图)

##### 22:00  小龙人弃摸鱼页 By niudanzhi

[https://qicang.ovh/anniversary16/0/#小龙人弃摸鱼页](https://qicang.ovh/anniversary16/0/#小龙人弃摸鱼页)

##### 23:00  老年人日记衍生 By niudanzhi

[https://qicang.ovh/anniversary16/0/#老年人日记衍生](https://qicang.ovh/anniversary16/0/#老年人日记衍生)

##### 24:00  睡美人新编 By niudanzhi

[http://www.36rain.com/read.php?tid=159016](http://www.36rain.com/read.php?tid=159016)

##### 彩蛋  问心 By 容予

[http://www.36rain.com/read.php?tid=159018](http://www.36rain.com/read.php?tid=159018)
