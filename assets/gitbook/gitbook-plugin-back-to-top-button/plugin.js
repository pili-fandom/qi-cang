require([
    'gitbook',
    'jquery'
], function(gitbook, $) {
    gitbook.events.on('page.change', function() {
        var back_to_top_button = ['<div class="back-to-top"><i class="fa fa-arrow-down"></i></div>'].join("");
        $(".page-wrapper").append(back_to_top_button)
    
        $(".back-to-top").hide();

        $(".back-to-top").hover(function() {
            $(this).css('cursor', 'pointer').attr('title', 'Scroll to bottom');
        }, function() {
            $(this).css('cursor', 'auto');
        });
    
        $('.book-body,.body-inner,.page-wrapper').on('scroll', function () {
            if ($(this).scrollTop() > 100) { 
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
    
        $('.back-to-top').on('click', function () { 
            var scrollHeight = $('.body-inner')[0].scrollHeight;
            $('.book-body,.body-inner').animate({
                scrollTop: scrollHeight
            }, 800);
            return false;
        });
    });
});
