const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');

gulp.task('minify-html', () => {
  return gulp.src('_site/**/*.html') // Adjust this if your output directory is different
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('_site'));
});
