---
title: 发表内容
author: 终朝采蓝
date: 2022-02-06
category: contact
layout: form
lang: zh-Hans
depth: 1
---

> ##### 注意!
>
> 段落与段落之间请空一行！
{: .block-tip}

如需更改，请直接将更改后的文本再提交一遍即可~

---

<form name="fileForm" action="https://getform.io/f/pamqkzqa" method="POST" enctype="multipart/form-data">
  <label>
    作者:
    <input type="text" name="name" required>
  </label>
  <label>
    标题:
    <input type="text" name="title" required>
  </label>
  <label>
    章节（没有可不填）:
    <input type="text" name="chapter">
  </label>
  <div id="fileInputContainer">
    <label>
      添加文件:
      <input type="file" name="uploads[]">
    </label>
  </div>
  <button type="button" id="addMoreFiles" style="margin-bottom: 10px">添加更多文件</button>
  <label>
    正文:
    <textarea name="message" rows="50"></textarea>
  </label>
  <button type="submit" style="display: block; margin-left: auto; margin-right: auto;">提交</button>
</form>
