---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:09
layout: post
depth: 2
category: opera
---

### 第九場 路邊攤



“唔……”

蒼微微睜了睜眼，天已經全黑了，肩膀和腰腿痠軟地很，讓他更加不想起身，雖是喉嚨乾渴地好像要冒火，然而知道也找不到水，便轉身又睡了。

<br/>

“長官？”

將車停在已經亮起門燈的麟趾巷棄家公館的門口，任沉浮下來打開了車後門，然而一向利索的棄天帝卻坐在後座上若有所思。

“長官，清除匪患也不是一時一刻之功……”

送朱武去了新華院之後，任沉浮和補劍缺回到鎮守府，車子剛開進大門，便見左右士兵列隊，鎮守使滿臉怒容大步出來，兩人跟着上車之後，才知道是隔壁村子出了票綁匪，綁架勒索之後更殺了人質，鎮守使震怒，要親自查問，立即起身，驅車前往當地，如是者忙碌了半天，其實只能說是一無所獲回到麟趾巷的時候，已經是晚上九點了。

“……嗯。”心情不好之餘，棄天帝慢慢走下車，卻總覺得有什麼事情要做，搖了搖頭，正要邁步進入大門，卻一眼瞥見夜色中後花園新建涼亭的剪影……愣了愣，突然繞到汽車的駕駛位置旁，拉開車門坐了上去，還未熄火的汽車又再開動，出了巷子。

<br/>

“唔……”突然覺得一陣風吹進屋子裏，睡得渾身發軟的蒼迷迷糊糊的回手扯了扯身上的坎肩的下襟，勉強蓋住後拉亮了辦公室內的電氣燈，棄天帝看着蜷在沙發上昏睡地蒼，眉毛抖了抖，差點就笑出聲來，走上前去輕輕碰碰那單薄的肩頭，卻見他似乎是嫌亮，如同一隻貓兒一樣，面向沙發靠背座位之間的縫隙鑽得更深，柔軟的沙色頭髮和沙發上的手工刺繡的抱枕互相蹭着早就弄了一團亂。

“起來。“不知不覺就坐在沙發邊，將人扶起來，順手抓起一旁書架上不知多久沒用過的一隻牛角梳子，竟就替他整理頭髮了。

“啊？“有人擺弄自己，睡到缺水渾身發軟地蒼雖不情願還是慢慢清醒了，看看近在咫尺的那張幾乎完美的英俊面龐，竟一時呆住，不知道該用什麼表情面對了，就這樣愣可可注視了半晌，才發現，自己竟是被他抱坐在腿上梳髮的，四下看看，並沒有旁人，才緩緩站起身來。

手中還抓着梳子，只見那纏着幾根久遠的紅色頭髮的梳齒之間又多了几絲沙色，棄天帝心中不知又想起了什麼，“走吧。”順手將梳子放在口袋裏，從沙發上站起來。

“去哪裏？”

“回家……回……公館吧。”慢慢轉身，向門口走去，然而走了幾步又回頭，卻見蒼微微蹙眉，站着不動。

“我今日有公務，沒時間去見曌雲裳，玉佩之事，容後處置。”回身看着並沒有移步的人，棄天帝慢慢說道，然後又轉身去拉門鈕。

“棄長官既然答應，蒼不懷疑……只是，已經出來一天了，能否告辭回客棧了？”

動作停住，微微側頭，眸子滾到眼角，棄天帝立在門邊半晌不語，竟是等不到那人妥協地聲音，只聽到不大的一聲“咕嚕嚕”，纔想起自己也還沒吃晚餐，將頭扭回，打開門，說：“去吃點東西吧。”

<br/>

車子慢慢行駛在已經沒有人跡的路上，寥寥幾盞電氣路燈還比不上棄天帝車燈的亮度。

因爲沒什麼人，棄天帝雖是握着方向盤，卻有足夠的空餘來向左右看看，尋找沒有打烊的店家，不過目光總是會停留在身邊那人垂頭不語的側影上，朦朦朧朧柔和的光線，照得那一條剪影的曲線竟是如此美妙圓潤，精緻的睫毛似乎又將那昏濛濛的光線細細分成無數絲，竟是越發閃亮起來。

“那裏面……”那剪影突然側了頭，閃出一個元寶形的耳廓和几絲柔軟的亂髮。

“啊？”棄天帝覺得應該是許久不曾開車的緣故，竟有些微手忙腳亂地停了車。

“那巷子裏有個宵夜攤子。”其實這裏已經離南崗子很近了，蒼實在是不想就這樣一路開回去。

“好。”下車繞到對側，打開了車門。

<br/>

“老闆。”賣夜宵的老人正要收攤，在一小盞煤油燈的光線下見到這樣兩個穿着的人向自己走來，也不知該如何招呼了，錯愕半晌，才說：“就剩二十個餛飩了。”

“煮了吧！”棄天帝一面說，一面從餛飩挑子上將剛剛摞好的凳子摞上拿了兩個下來，放在挑子支起的小桌板邊，看那老闆重新捅開竈門，將爐子裏的火再生了起來。

有點驚訝於對方的從容熟練，蒼不禁多打量了棄天帝幾眼，纔想起：這個人聽說也是白手起家，從普通士兵做到封疆大吏的傳奇人物了。慢慢坐在他身邊，無處可看，也只好和對方一起，眼巴巴看着鍋內的水變熱起來，越發覺得渴了，蒼悄悄用舌尖溼溼嘴脣。

“老闆，先盛兩碗湯解解渴啊。”

“哦，不過餛飩只有紫菜了啊，蝦米皮和蔥花都沒了。”老闆一面說，一面用勺子撇撇已經熱了的餛飩湯表面薄薄的一層油膩，盛了兩碗清湯放在客人面前。

“無妨。”端起碗一飲而盡，才發現蒼那碗喝了一小口之後，竟是彷彿看着陌生人一樣看着他，“……驚訝什麼？”

既然心中已經明瞭，蒼輕輕搖了搖頭——雖然還是訝異這個人竟有如此不同地兩面，然而卻又不知道如何開口了。

“我以爲你不會訝異的。”說着，棄天帝嘴角微微上翹，又恢復了每天辦公桌前的高傲冷峻，“還是你習慣這樣的我？”

眼中露出淡然，慢慢低下頭——這個人是最好的演員，想到這一點，蒼竟然覺得自己緊貼着他的的左邊身子陣陣發寒了。

平靜地看着對方雖然極力壓抑卻又升起的戒備，棄天帝一笑，“戲子，竟也害怕會演戲的人？不覺得太不公平了麼？”

“……伶人只在臺上做戲。”蒼有些彆扭地將頭微微轉過。

“哦？”棄天帝不禁伸手挑起蒼的下頜，將他的面孔轉回，望定那不滿又無奈地眼神問：“那……上午在辦公室說的話……”

“是真的。”眼神定了定，蒼慢慢回答。

棄天帝聽到這聲回答，眉頭微微抖了抖，卻見夜宵攤老闆戰戰兢兢地將餛飩端了過來，當即鬆手轉身，從桌上缺了口的瓷碗中抓起兩片洋鐵皮彎的湯匙，丟了一個在身邊人碗裏，“吃吧。”

……

小小一碗餛飩，蒼吃尚且不夠，棄天帝自己也沒吃晚飯，幾乎是毫無感覺就吞下去了，意猶未盡舔舔嘴脣，不甘心地瞄了眼空空的已經熄了火的湯鍋，突然見到眼皮低下一隻碗推了過來。

“？”

“師父教誨，過夜不食……我已經吃了兩隻了。”

“……你吃剩的，倒叫我來吃麼？”

“……失禮了。”蒼有些懊惱，這個人又不是天草、伊達，也不是同鍋吃飯那些師兄弟，一時興起，卻被這樣一句反詰，倒真覺地有些唐突尷尬了。慢慢伸手，扶着碗邊將碗撥回自己面前，用那彎彎曲曲的鐵皮勺子晃晃悠悠地將剩下的餛飩盛起來吃了。

“走吧……呃。”等到蒼連碗裏的湯也喝過幾口，終於推碗時，棄天帝站起身，摸摸口袋纔想起自己身上已經多少年沒帶現錢了，擡頭望天，明月尚圓，“老闆……”本想說叫這老闆明日去Ｊ城鎮守府要錢，不過他也不傻，想想對方既不會相信，也沒這個膽量，棄天帝只好把手伸進袖子，要摘手錶了。

“老闆，錢放在碗底了。”蒼捏捏袖口，摸出兩個銅子來，隨後才站起來，“東盛客棧只在前面不遠，小民自己走回去便可……”

……

“蒼？”

“師哥？”

東盛客棧門口，晚歸的兩人竟是對面相逢了。

“蒼，你……”赭杉軍彷彿也懷着些什麼心思，見到對方竟是也同樣有些措手不及的驚慌。

“師哥難得回來晚啊。”蒼吐了口氣，淡淡地說，“進去再說吧。”

“好。”

“師哥……還在看什麼？”一腳跨入院內，有些疑惑地回頭看着有些遲愣的赭杉軍。

“……沒有。”凝目看着巷子口一人轉身慢慢走遠，想想大約也是晚歸之人吧，赭杉軍搖了搖頭，雖然覺得那背影有些眼熟，然而還是跟着蒼進去了。

……

“蒼。”

簡言安撫了等得焦急的衆人，蒼赭二人草草洗漱便各自上牀，熄了燈火，赭杉軍難得輾轉，聽到對方呼吸也不像是睡着地樣子，終於忍不住開口，“蒼……你和那個人之間，究竟……”

“師哥……”先出了一聲，尚未決定如何回答，對方竟就悄無聲息了，蒼想了想，慢慢轉了個身，還是容忍這沉默繼續下去了。

<br/>

<br/>

舊曆八月十八的晨報竟是比平常晚了，直到蒼將要睡醒，報童們嘹亮的吆喝聲纔在巷子裏響起來。

“啊？這……”

已經習慣和幾個師兄弟坐在客棧一樓的大堂一面等着竈下的粥滾，一面看報的墨塵音一眼瞄見頭版鎮守使家的新聞，想想昨夜，“難道昨日，蒼師哥又去……”

“哈。”紫荊衣乾笑一聲，“我看不會，要是待到那麼晚，還回來作甚，又不是沒睡過人家的牀。”

“荊衣，班主他又不是……肯定是有事纔會去的啊。”金鎏影皺着眉頭，看看衆多皺眉不語的師兄弟。

“師兄！師兄！您起來了？”突然看見蒼似乎是睡眼惺忪地從後面的樓梯上下來，衆人都彷彿做錯了事情被發現了一樣，紛紛起身，唯有紫荊衣反倒是將身一轉，後背相向。

“其實，我昨日……”看着大家目光都集中在自己身上，蒼倒是也沒什麼特別地感覺了。

“……咦？大家怎麼了，都不說話？”

一步跨進來的是帶着天草伊達外出練功的赭杉軍，前腳剛剛跨入門檻，便已經察覺氣氛不對，臉色不變，正色說：“蒼昨日下午在茶樓同我遇到，後來便一起吃飯了，如何了？”

“哦，原來如此。”

“嗯，後來和那朋友聊得投機，所以晚了，沒來得及和大家說，抱歉了。”赭杉軍說着便在飯桌邊坐下，看了一眼攤在桌上的報紙，也裝作沒看見了。

<br/>

“師哥……多謝。”吃過早餐，回房更衣要往舞臺練戲之前，在房內，蒼輕輕說了一句。

赭杉軍半天不語，靜了靜心思說：“我不是說過，那玉佩不要再管了？”

“嗯？”蒼臉上倒是露出疑惑，“師哥如何知道……”

赭杉軍不語，蒼昨日究竟做什麼，若非伏嬰師提醒，他其實也是想不明白，又仔細想想昨日伏嬰師在茶樓上教給自己的問話，佩服之餘更加猶豫，下一句話該不該說，然而最終還是說道：“……你和他明明什麼都沒發生，爲什麼做此表現？“

“……師哥，蒼雖是無奈，然而也是自願，除此無他。”

這話說得圓滑，只是蒼自己都沒意識到回答這句話之前，自己究竟沉默了多少時間。

<br/>

<br/>

“表兄，吾回來了。”

伏嬰師拿着一張剛剛買來的晚報，回到新華院內。

“快給我看看！”

天字一號牢房之內，坐立不安地朱武不等他將牢門打開，便已經從柵欄內中伸出手去。下午悶睡朦朧，卻突然被院外報童嘹亮地吆喝吵醒：

“鎮守使半夜撞塌自家院牆，車毀人傷；鎮守使半夜撞塌自家院牆，車毀人傷！”

等到聽清了內容，頓時嚇得從窄窄臥板滾到地上，顧不得渾身摔地生疼，便一把抓着聞聲趕來地伏嬰師，讓他出去問問。

“這……父親究竟傷了如何？”報紙內語焉不詳，模糊照片乃是被撞塌的麟趾巷院牆和有些變形的汽車，至於傷者情況，鎮守使謝絕探傷，便不是這些尋常記者所能知曉，朱武將伏嬰師出去買回來的七八份各種報紙翻來覆去，恨不得看穿過去，然而各家說法不一，實在是無可採信。

“伏嬰師，我要回家！”打定主意猛地擡頭時，纔看見伏嬰師已經打開了牢門。

<br/>

“主人，已經坐了一個鐘點了，您需躺平了。”

下午，戒神老者守在主臥室之內，提醒手腕已經打好石膏一身睡衣半躺半坐看着報紙的棄天帝。

昨夜棄天帝想起蒼還在自己辦公室內而驅車去接只是一瞬間地事情，一頭霧水的補劍缺和任沉浮也不敢回家，便也只得在棄家公館門廳等着，大約過了兩個鐘點，終於聽見外面寂靜的巷子之內響起汽車輪胎壓過馬路的聲音，滿臉喜色起身前去迎接，誰料還未走出建築大門，便又聽到一聲急剎車，隨後，面前的院牆便塌了一半。

好在德國產品着實堅固，當衆人嚇得手腳發軟把埋在磚堆中的駕駛座刨了出來，又將已經變形的車門強行拆下後，終於見到伏身在方向盤之上，碎玻璃落了一身，不過總算還完好的Ｊ城鎮 城鎮守使。

雖然衆人都在暗地佩服鎮守使大人臨危不亂，及時護住要害面孔，所受傷害只有左手尺骨上一條不長不短的裂紋和後腰的挫傷而已；不過，棄天帝不足爲外人道的內心之中，還是相當之難爲情。然而事已至此，也只有謹遵醫囑，臥牀靜養地份了。

“把燈打開。”放下報紙，棄天帝微微蹙眉，等戒神老者把墊在後腰下的軟枕抽出來，扶着自己躺好。

“是，主人，晚餐想吃點什麼？”走到門邊去碰電氣燈的開關，還沒等到開口，身邊虛掩地屋門又是“咣噹”一聲被人撞得大開了。

“……父親！”

拖着和自己銬在一起的伏嬰師衝進臥室，剛剛亮起的燈光照亮了平臥在牀，手臂吊在胸口之人，朱武竟是沒來由地眼睛一熱，怔了怔，三步兩步走到牀邊。

“朱武？”微微側頭，有些驚訝地看着跪在牀邊的看着自己的青年，棄天帝張了張嘴，最後問說：“吃晚飯了麼？”

“啊……沒……”

“哦，正好一起吃……想吃什麼？”

“嗯……父親！”如平常一樣，認真地想了一會兒朱武才察覺不對，“您……究竟傷在哪裏？”

“哈，戒神，吃餛飩吧，多放點蔥花。”棄天帝忍着笑，先吩咐了戒神老者，隨後看看在監獄的臥板上熬了一夜有點憔悴的朱武和在他身後不得不躬着身有些尷尬的青年，“你是……伏嬰師？”

“正是……外甥伏嬰師。”

棄天帝微微一笑，隨機臉色陡然一沉，“新華院管事，私縱犯人，該當何罪？“

“外甥並未釋放表兄。”即使面對棄天帝，伏嬰師臉上也沒什麼變化，緩緩擡手，將銬在自己同朱武手腕上的手銬展示出來。

“哈。打開吧。”棄天帝搖了搖頭，扭頭不看了。

“遵命。”摸摸索索從腰間掏出鑰匙，插入鎖眼，同時說：“表兄提前釋放，還有些手續須同任祕書交接，外甥告退。“

“嗯……吃過飯再走。”

“謝舅父。”伏嬰師略微點頭，轉身出門之後，更將大門關上。

“父親……您……”看着露在衣袖外面的石膏，朱武想去摸摸又不敢。

“手臂撞了個小裂口，腰閃了一下，無礙的。”

“怎麼弄的……”

“許久不開車……手生。”

“大半夜的，您怎麼會親自去開車……”

側頭看看兒子，棄天帝嘴角又翹了起來，絲毫不覺得身爲Ｄ省長官，一直被這麼盤問有何心煩，只慢慢說：“情人幽會，當然是自己開車去。”

“父親！……您放過蒼吧……”朱武愣了下，臉上已經露出氣苦神情。

“我今日不想提此事。”棄天帝將頭轉正，眼睛又慢慢閉上了。

突然一下便又無語，坐在牀邊椅子上，想走卻又捨不得、不忍心，朱武這兩日也被折騰的夠嗆，過了片刻，竟也就彎下腰趴在棄天帝枕邊睡着了。

<br/>

“黥武少爺，老爺和朱武少爺好像都睡着了，您一會兒吃飯的時候再來問候吧。”

在省立圖書館報架上見到當日的晚報，連書包也顧不上提便匆匆趕回的黥武，被戒神老者引着，將門微微打開一個小縫向內中看看，長出口氣，退了出來。

……

晚餐時間，在半坐半靠，扶着在牀上的餐桌上笑眯眯吃餛飩的棄天帝，看看圍在牀邊的幾個小輩，神情竟是格外安詳。

……

“長官……”正在戒神老者收拾碗筷的時候，補劍缺求見，“報告長官，暴風殘道團長已經抵達，正在樓下等候召見。”

“哦？”嘴角翹了起來，“讓他上來。”右手在牀上撐了一下，朱武和戒神老者一左一右扶着棄天帝再坐直了一些，“取外套來。”

“舅父，外甥要不要回避一下……”伏嬰師頷首問道。

“不用，都留下。補劍缺，去接蒼……”

“父親！”“叔公！”

擡起手，制止了臉帶焦急的兩人，棄天帝端起面前茶杯啜了一口，此時戒神老者已把軍服外套拿來，披在他的肩頭。

……

補劍缺下樓，將等在客廳的暴風殘道領至樓上，再次下來，向着還在客廳內無所事事地跟着暴風殘道一起前來的小舅子兼副將魔晦王略一點頭，正要出門，對方卻是自己貼了上來。

“隊長！這麼晚了還要出門？”

“正是。”面前此人，補劍缺略有耳聞，因此並不想與之深交，淡淡回答之後，已經疾步向着門廳走去。

“隊長，您可知道長官這次急召姐夫，是爲了什麼事情麼？”心中說不出是好奇還是惴惴，魔晦王無視補劍缺臉上的冷淡，繼續熱絡地搭話。

“軍務上的事情，我不清楚。”說完這句，補劍缺已經走下門口臺階了，正戴上手套要去車庫開那輛備用轎車出來，卻見自家隔壁鄰居跑了來，“狼大哥啊，你家小狼崽又和人打架啦，腦袋都打破了，快回去看看啊！”

“啊！這臭小子！”一邊是獨子，一邊是任務，補劍缺一時兩難，此時瞅準機會的魔晦王立刻湊上來，“隊長，您這是要去什麼任務？好不好讓小弟代勞，您先回去看看令公子……”

“這……好吧，長官命令，速去南崗子雙儀舞臺，請蒼老闆去樓上長官臥室！”

<br/>

棄天帝臥室之內，隨着暴風殘道慢慢陳述，氣氛也漸漸靜默下來。

“報告長官，當年之事，乃是屬下副將魔晦王所經辦，封雲社原本已經計劃離開津門，應該並未爲難……”額頭上冷汗已經淌下來，偷眼看去坐在牀上的棄天帝並未怎樣，而旁邊的朱武少爺的眼神卻已可怕地很了。

“魔晦王？”棄天帝先看了隨時可能爆發地兒子一眼，微微咳嗽警示之後，纔將眼神挪回愛將身上。

“正是……”

“是你妻舅？”

“正是……他是屬下在津門所納四夫人的哥哥，此人有些能力，是個幹才。”

棄天帝似乎是下意識摸摸額角，才繼續問說：“當年，便是他將封雲社送出津門？什麼也沒發生？”

“這……容屬下回憶……啊，對了，聽說是有一把步槍走火，不過所幸沒有傷人。”

已經能聽見身邊朱武攥緊拳頭地聲音，棄天帝慢慢點頭，“步槍走火也是意外，便是真的傷了人，也不算什麼大事……不過，既是好端端將人送出城，卻不知爲何要荷槍實彈呢？”

“這……屬下……不知……大約是……”冷汗涔涔而下，暴風殘道聲音已經發顫，當年之事，他並未看重，只是一聲吩咐一個回覆而已，眼下卻不知爲何，棄天帝竟是如此用心，偷眼看看一旁的朱武，心中暗道不妙，索性將心一橫，低頭鞠躬，道：“屬下失察，得罪了朱武少爺的朋友，請長官從輕發落。”

“……朱武。”沉默片刻，棄天帝才緩緩開口，“去將暴風團長摻起來……”

“……是。”心中也不知是什麼滋味，然而積鬱四年地怨氣似乎散去了不少，朱武站起身，走到暴風殘道身邊，正要彎腰去扶，卻聽到走廊內一陣大亂。

一直在主人旁邊侍候的戒神老者還沒來得及出去查看，門已被推開。

“報告長官，下官已將您要的人帶來了！”魔晦王興沖沖進來，迫不及待從身後拖過一個人，直接推了進來。

<br/>

蒼在臺上剛剛唱過了《貴妃醉酒》，才穿過下場門回到後臺，只聽清一句“長官召見”便被一羣士兵從臺口強行拖了下來。這時後臺已是一片令人窒息地沉默，所有人的目光皆緊緊注視那立在十幾名士兵之前，不耐煩等待的軍官身上。

“哈，我道是誰，原來是你……”本不認得，然而那憤怒的眼神卻是格外熟悉，魔晦王走上前，推了推壓在眉額的鳳冠，用衣袖擦擦額角戲妝，“嘖，看來這疤留得還是不夠深啊。”

蒼本想躲開，然而兩名士兵牢牢將手臂抓着，肩頭關節已被掰到了極限。

“嘖，嘖，想不到四年不見，竟出落成這般美人了，難怪長官受傷還要讓你去暖牀啊！早知如此，當年就不應該讓你爬出津門，直接爬去長官牀上不是好！”下巴幾乎能揚上天去，看着對方一下子燃燒起來地眼神，撇嘴一笑，隨後聲色俱厲地喊了一聲：“帶走！”

“蒼！”“師哥……”

“別……別動……”一身戲服已被拉扯得歪斜，裙角拖在地上，被幾個人推推搡搡地走出屋去，勉強掙扎着回頭，向着已經追出來的同門蹙眉囑咐，“先演出……”話未說完，額角已經捱了一記，“還不老實！多話！”一塊白布已經塞進了嘴裏，粗大的麻繩也纏在了身上。

……

反剪雙手跌進屋內，魔晦王力道頗大，又被腳下長絨地毯一絆，蒼向前搶了幾步，失了平衡，上身竟是直接摔跌在了臥室正中的牀上。

“放肆！”暴風殘道怒喝一聲，讓纔看清屋內情形的魔晦王一個激靈，雙腿一軟便跪在門口，“長官……小人……小人實在是不知道長官您正在和衆位隊長議事……”

雙眉深鎖，也不作答，棄天帝微微探身，將摔在身邊鳳冠霞披凌亂不堪的人口中的白布扯了出來，硃紅胭脂早被蹭得滿臉都是，如同血痕，倒叫那不住微微顫抖的不見血色的雙脣更顯得慘淡了，“朱武。”已經覺得腰間受傷的地方刺痛起來，棄天帝略微後仰，“替蒼松綁。”不需他吩咐，朱武已經快步過來，旁邊震驚過後的黥武與伏嬰師也都過來幫忙。

“你……認識這個人吧？”看着那一直平靜淡然甚至有些逆來順受的雙眸此時竟露出了失了神智驚恐，棄天帝輕輕地問。

不知是因爲被捆綁的緣故還是緊張過度，蒼甚至覺得有些窒息了，此時胸口劇烈起伏，竟是出不了聲，然而還是依着棄天帝目光所至，勉強回頭看看不復囂張，趴在地上抖若篩糠地魔晦王，再度艱難地點了點頭，穩了穩呼吸，字斟句酌地說：“……四年前，正是他……這位軍爺，送師父和蒼的同門離開津門……”

“他如何送？”

此時，本就捆得潦草的繩索已經解開，蒼本想撐起身體，然而卻渾身發麻使不上力氣，只是他做戲慣了，自然而然慢切優雅都滑坐在牀邊的地毯上，卻又一直迎着棄天帝的目光，漸漸冷靜之餘，心中也在飛速地盤算。終於，露出無法抑制地悲哀神色，一字一字地說：“這位軍爺……將蒼及同門送出津門……一路以禮相待，甚是關照……”

“蒼！”話音未落，朱武已經急地叫出聲來，“蒼，你別怕，父親他會給你做主啊！”

“朱武少爺……蒼……並未說謊……當時未曾向這位軍爺表示感謝，一直有愧。”低着頭，卻羞於對上那熱誠的眼睛，不記得這是三天來第幾次對朱武說謊了……

“蒼！”看那那衣衫凌亂地人蜷在牀邊，微微將頭偏過，朱武恨不能直接抓起牀邊的凳子輪上魔晦王的後腦，“你還怕什麼啊！這個人……他剛纔還那樣對你……怎麼可能……”

“你們都出去！”棄天帝沉沉的一聲，所有人心中一凜。

“父親，此事請您詳查！一定詳查！”朱武轉身拍着蓋在棄天帝身上的被子，“啪啪”直響。

“都出去！”棄天帝一皺眉頭，看着搖着頭緩緩扶起蒼的朱武，突然說：“他留下。”

“父親！”

“表兄，走吧，舅父大約是想單獨開導一下蒼先生。”伏嬰師走過來，一挽震驚當場地朱武的手臂，湊在他耳邊小聲：“舅父腰傷不輕，心有餘力不足，沒事的。”同時已將他拉出房間。

……

“坐牀上來。”腰傷着實不方便，棄天帝不耐煩地皺皺眉頭。

“長官……”慢慢站起身，卻不坐下，只是將頭上已經破壞了大半的鳳冠摘下，大大小小的珠子又“噼噼啪啪”落了幾十顆下來。

“你在等我問你麼？”忍受不了靜默，棄天帝只一個喘息就直接問話。

“……寧得罪十位君子，也不敢得罪一個小人。”

“嗯？”

“棄長官是君子，然而……”

“所以你寧願得罪我，也不敢得罪那小混混？”冷靜下來，這些卑微求生的道理於棄天帝倒也不難明白。

“蒼，亦不敢得罪棄長官……”

“……去洗臉。”蹙蹙眉頭，指了指一旁的浴室，隨後拎起牀頭櫃上的鈴鐺。

一捧水潑在臉上，蒼竟不知自己此時是個什麼心緒，一直以爲被人刻意欺凌，若是如此，反抗申訴也算有些尊嚴；誰料今日才知，竟真如螻蟻一般，在那人根本不曾察覺之時，便差點被隨意踩死；便是因這心緒，方纔蒼也分明知道，棄天帝不可能爲了自己當真處置愛將，然而一頓不痛不癢的訓斥，只能引來越發強烈的報復——雖做如此想，然而心中又何嘗不存着希望，只是最後，棄天帝那淡然明瞭的反應，雖是理解，卻也默認自己的揣測無誤了。

“蒼先生。”身後突然響起了戒神老者的聲音。

“戒老？”

“主人說，請您便在這裏洗澡吧。換洗衣物老兒放在這邊臺子上了。”

“謝……謝您了。”

本想向蒼解釋事情始末，然而地點不對，長官在外，也輪不到自己多言，戒神老者緘口不語，輕輕關了浴室的門。

……

戒神老者準備的，僅是一身單薄的絲綢睡衣，蒼沐浴之後，勉強換了，赤足走了出來，這時，臥室內燈都滅了，只留着牀頭一盞檯燈，地毯頗厚，已經涼了的腳趾陷在毛絨之中，甚是溫暖。

“今晚你睡這裏。”棄天帝指了指身邊空着的半張牀，摘下吊着手臂的帶子，向後一仰，“過來扶我躺下。”

“棄長官……蒼還是去客房睡吧。”口中如此說，然而出於對傷者的照顧，蒼還是漸漸走了過來，依言服務。

“今日朱武在家，你既然進來，又怎能讓你走出去？”平躺之後，棄天帝輕輕一笑，看着立在牀邊的蒼：大約是因爲心神不寧，身體沒有完全擦乾，絲綢睡衣有幾塊被濡溼了，貼在肩頭和腿肚上，乳白色的絲綢彷彿就變得透明瞭，竟是顯出了淡淡的粉紅肌膚。

察覺的出那對異色眸子又開始肆無忌憚地在自己身上游移，蒼眉頭微微蹙蹙，認命低頭，從牀尾繞到對側。

“躺好了？”回想着方纔他轉身時，背後的那一灘水跡，棄天帝閉了眼，用右手摸上了檯燈開關。

一片黑暗之中，想想兩人大約都如挺屍一般躺平不動，棄天帝轉過頭看着從窗口射進來的淡淡月光漸漸將蒼的側影勾勒出來——

今日……本應是散戲的日子，然而……伶人可以只在臺上做戲，世上的大多數人，卻皆是沒有舞臺，也不知何謂散場之時的。

牀鋪突然微微一顫，腳上被什麼熱乎乎的物事壓着，惹得正在有些感慨的棄天帝一皺眉頭，輕輕擡腳，將肥貓踢了下去。  場休：一夢？異夢？



“朱武呢？”

<br/>

這是十年前的津門，棄天帝還不是Ｊ城鎮守使，只是住在租界區邊上一棟小洋樓的中級的軍官而已。

<br/>

這是晚上不到七點，因爲下午有點事務，需去津門武備學堂處理，妥當之後，已經天黑，便直接回家了，看着擺在桌上的還未動過的飯菜，棄天帝將軍裝大衣脫下來交給戒神老者，皺着眉頭問了一句，沒記錯的話，問津學院改成西制之後，便有了寒暑假，棄天帝雖不厭煩，但總也是覺得是個大事了。戒神老者看向自己的眼神有點奇怪，不過勞累了一天的棄天帝到也暫時無感，“不是說了今天只去上午一會兒的？怎麼到現在還不回家？”

“……老爺，”戒神老者聲音有點發顫。

“怎麼？又闖禍了？躲在同學家不敢回來叫你來說情？”坐在餐桌邊，心裏想的是：這小子不主動回來認錯，自己絕不像上次一樣去天者、地者家接他！

“老爺……昨天您不是說，讓少爺散學之後在學校等您，您中午接他去辦公室的……”戒神老者的臉已經泛白了。

“……”愣了那麼幾秒鐘，棄天帝從椅子上蹦了起來，一把搶過戒神老者手裏的大衣，衝出了自家門。場休：一夢？異夢？



<br/>

外面竟開始飄雪了，司機補劍缺送自己回來之後，也就掉頭走了，此時在已經鋪了薄薄的一層雪花的路上，連個車印子都看不見，這裏靠近租界區，連洋車都沒有……棄天帝沿着半凍的海河河沿，仰天長嘆，隨後大步跑了起來……

……

“朱武？”

自己的那間辦公室裏黑漆漆的，棄天帝氣喘吁吁的推開門，還沒摸到電燈開關就叫了一聲。

“唔？”

沙發上傳來了悉悉索索的聲音，燈亮了，紅髮的小男孩蜷成一團，光線突然刺眼，朱武抓起蓋着的毛料子小大衣一下子罩着頭。

“起來了，回家！”坐在沙發邊，一把將不停揉眼的朱武拉起來，讓他坐在自己腿上，看那滿頭紅毛亂七八糟，棄天帝摸着旁邊書架上的一把牛角梳，隨手整理了起來。

“阿爹……”

“嗯？”

“水……”

棄天帝回身，端過書桌上的大茶杯，才發現中午回來沏的茶水早就被喝乾了，只剩些茶葉貼在杯底，菸灰缸裏有些西洋糖果的包裝紙，應該是每天上學前戒神老者塞在朱武口袋裏的小零食。

“回家吃飯去。”

“嗯！”朱武從爹的腿上滑落到地上，很聽話的穿好自己的小大衣，又抱了書包。

<br/>

“……冷麼？”

一手幫兒子拿着書包，一手拉着他的手，走出大門，鵝毛大雪撲面而來，大街上空蕩蕩的，還是沒半個人。

朱武仰頭看着父親，認真努力的搖了搖頭。

“哈，走吧。”

“阿爹，下午在你辦公室乖乖的溫書來着。”

穿着棄天帝的軍裝大衣，甩着長長的袖子，衣服的下襬就拖在地上，朱武吸吸鼻子，仰頭說。

“好。”

只穿裏面的外套還真有點冷，棄天帝看看路程已經走了一半，街上也沒人，便將兒子那小風衣披在肩上。

“哈，阿爹穿我的衣服好帥！”

“小點聲……”

“朱武，別睡覺啊。”

大約就剩十多分鐘的路了，棄天帝輕輕掂掂背後裹着大衣的朱武。

“不睡！”

“快到家了……”

“嗯，阿爹，你走不動了我揹你吧！”

“等你長得和我一樣高吧。”

“阿爹怎麼長這麼高啊？”

“……每天早晨跑步就能長這麼高。”

“……”

“明天跟爹一起出去跑步吧！”

“阿爹，我睡會兒啊。”

“臭小子……”

<br/>

s第二天，朱武到底還是被棄天帝叫起來一起出門跑步了。

不過只跑了一半的路程，就再也跑不動，當然，棄天帝覺得，體力是一方面，而最重要的一方面是路邊那個賣麻花的小店。

<br/>

<br/>

這是十年前的津門，蒼還不是什麼國色天香的梨園新秀，只是一個剛剛被師傅收養不久的孤兒而已。

早早的起來練功，昨天半夜洗衣服手指凍得像蘿蔔一樣，蜷在炕上在心口焐了半宿，剛剛有點知覺，便又要起來練功了。

<br/>

蒼喜歡自己找地方喊嗓子，冬季的海河邊，清清靜靜，雖然冷的要命，卻是叫他莫名喜歡。

“阿爹啊～”

在他練功不遠處，坐着一對說漢語的外國父子——至少在蒼的記憶中是這樣的。

“嗯？”黑髮的高大父親心不在焉的抽着煙，那紅髮的小男孩，一手一根金黃色夾着餡撒着青紅絲和芝麻的大麻花，正在努力啃着。那麻花的香氣，讓蒼練唱越來越心不在焉了——父親，不打孩子的父親……

“吃不下了……”其實只啃了一小半。

低頭看看兒子，麻花已經高高的舉了上來，棄天帝赫然瞥見不遠處一個瘦弱的孩子正在用一種淡然地卻又專注的目光看着自己這邊。他記得這個孩子，每天清晨，無論自己出門多麼早，總是能看見他一個人，穿着永遠與季節不相吻合的舊衣服，孤零零卻又努力的喊嗓子練功。棄天帝熄滅了手中的香菸，蹲下身，將被朱武啃過的那一頭掰了下來，隨後悄悄說：“去，把剩下的送給那邊那個小妹妹吧。”

<br/>

……

天矇矇亮了，棄天帝睜開眼睛，有點安靜的回想自己剛纔的夢，想到朱武只跑了一天，第二天便抱着被子枕頭說什麼也不肯起牀的時候，嘴角翹了起來，這麼靜靜的回味了良久，想翻身的時候纔想起自己還是傷者，慢慢轉頭，卻見身邊之人竟還如昨夜入睡前一般仰天躺着，彷彿都沒有動過。對面從大窗透進來的晨曦，清晰的勾勒出那完美的側影，突然那睫毛動了一下，棄天帝才發現，對方竟也是睜着眼的。

“心裏難受麼？”

<br/>不知爲什麼，便開了口，卻見那側影受了驚嚇一般，猛得顫抖了一下。

沒回答，被推推搡搡的走在巷子裏的時候，肯定是沒有想到現在竟能如此安詳的躺在如此舒適的牀上。不是沒有察覺事情的蹊蹺，只是，根本不確定身邊這個人會對自己做出什麼樣的事情，“……莫名其妙的傷害，蒼已經習慣了。”說出來便後悔了，和這個人這麼說，是在撒嬌麼？

“嗯？什麼樣的事情呢？”

“……不值一提的小事。”

就像還在天津的時候，那對在河邊的父子施捨給自己的一根半麻花，他拿回去給每個師兄弟都掰了一點吃，大家吃得正香甜的時候，師父卻鐵青着臉進來……師父丟了兩文錢，問起衆人嘴邊和指尖的油光時，所有人的回答都是：“蒼給的麻花……”

“你去找，找到他們來給你作證！找不到就別回來！”

<br/>

……沿着海河不停地走，不停地走……其實，找不找得到那對父子，真的也不會改變什麼吧。

<br/>

當蔥花舔着自己的鼻尖的時候，蒼才從不能解脫的夢中醒來，想不到自己竟然又睡着了，此時天已經大亮了。





