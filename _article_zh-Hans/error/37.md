---
author: kasana
title: The Error
lang: zh-Hans
date: 2008-02-25 00:47
layout: post
depth: 2
category: error
---

### 2. Castle on a cloud (6)

伏婴师怎么也打不通弃天帝的电话，急得团团转。实在不得以，他只能试着打了朱武的电话。自己出差期间，应该由这小子来负责弃天帝的起居和杂务。

<br/>

朱武本来已经要睡着了，因为怀里缺个东西，只能抱着抱枕睡在沙发上。正在做着一个春梦，就要成功圆满上演大结局的时候，忽然被伏婴师的电话吵醒了。

<br/>

朱武坐起来揉了揉头发，看着震得要跳起来的手机，一脸地沮丧。

<br/>

今天真是什么都不顺利……连把梦做个痛快都不行。

<br/>

接起电话，他没好气地说：“喂？”

“朱武，我找不到弃先生，我有重要的事情要告诉他。”

本来是伏婴师的电话就够让他恼得了，还要提弃天帝他就更恼了。

“你知道现在几点吗？”

“非常重要！我好容易才到了有信号的地方给你打这个电话，请你一定要让弃先生接电话。”

朱武本来想直接按了闷头睡觉。

可是仔细一想，就自己的美梦被搅和了太吃亏，凭什么弃天帝就可以在楼上逍遥快活？

“我找到他让他给你打电话。”

<br/>

朱武上了楼，发现卧室里是没人，但是书房门外有件浴衣。

<br/>

朱武垂着眼睛，捡起那件衣服，伴随着沐浴露的味道他已经从头到尾把故事想全了：首先应该会这样……然后应该是那样……

<br/>

正在他构思高潮和结尾的时候，手上的电话忽然又响了起来。

<br/>

“喂？”

“大少爷，小祖宗！十万火急，弃总找找没？”

朱武脑子里正想着乱七八糟的事情呢，张口就说：“他忙着呢！什么大不了的事情？明天再说！”

“千载难逢！永道和万圣集团一起空巢啊！”

朱武听得半懂不懂得，不过听伏婴师这么说似乎是发大财的事情，这可不能错过了。

“有钱赚？”

“有很好的买入机会——不过你先帮我找到弃总。”

“收益的百分之十归我。”

“百分之十根本不算什么。”

“成交！”

<br/>

朱武电话都没有挂，直接去开门，刚一转把手就看见一块巨大的毯子从眼前飞过。

<br/>

阿拉丁？

<br/>

他本能地转过头去，目光却被一片黑色挡住。

<br/>

“你怎么还在这里？”

朱武还是不知死活地想越过他的肩膀往里面看。

“伏婴师找你，工作上的事情。”

弃天帝直接拿了他的电话。

“没你的事儿了。”

朱武见他要关门，直接往里卡了进去。

“外头冷，我进来烤烤火。”

<br/>

冷个鬼，烤个魂。

这里是中控恒温的。

<br/>

弃天帝瞪着他，可是他已经进来了。

<br/>

朱武一过门槛就往里张望，就看见一只白白的脚踝露在书架和书桌构成的影子外面，上面还锻带一般闪闪烁烁地泛着一行水光。再仔细一看，苍沙色的头发从毯子里露了出来，地毯上都是液体的深色印记。

<br/>

苍没有看这里，脸色看上去不太好，奄奄一息。

<br/>

朱武的心一时不能平静。

<br/>

弃天帝没再搭理他，反而回到了书桌后的座位上。

<br/>

伏婴师又一次打电话进来，这次终于被弃天帝接到了。在伏婴师跟弃天帝说话的时候，朱武跑过去，蹲在地上端详起躺在地上的苍。后者见到朱武有些恍惚，眼神涣散，一时没有反应。

<br/>

弃天帝什么也没有说，只是冷淡地看着他们。

<br/>

从头到尾他只和伏婴师说了一句话：“知道了。”便没有了下文。

<br/>

他站起来走到朱武和苍的旁边。苍落在他巨大的阴影里。

<br/>

“我还有点事儿，明天继续吧。”

<br/>

他这句话明显是对苍说的。

苍看着弃天帝，冷汗滑进毯子里。	

弃天帝也看着苍，很快避开了他的目光。

<br/>

朱武眼睛一转，心里想着先走在说，正要扛起苍溜之大吉，手上却一空——

<br/>

苍被弃天帝抢先连人带毯子抱了起来。

<br/>

“你的飞机票我已经让人订好了，明天7点的飞机，现在你还有5个小时。”

“7点？”

朱武简直要跳起来了，这么早他起不来。

<br/>

弃天帝不理睬他，转身坐到书桌后面，让苍坐在他腿上，靠着他的肩。

<br/>

弃天帝一手抱着苍，单手打字也打得飞快，给伏婴师发送了一封简短的邮件，内容很简单，只有一句话：“完成第一次增持之后，请替我安排和对方的董事喝下午茶。”然后关闭了电脑。

<br/>

“我要休息了，你是要在书房睡觉么？”

弃天帝向朱武发出了最后通牒，然后抱着苍离朱武而去。

<br/>

朱武站在房间里，用力嗅了嗅，他的直觉告诉他，刚才一定有什么激烈的事情发生了。

===============================================================

冷水划过玻璃，留下一段扭曲的视野。

<br/>

弃天帝冲完澡出来，长发来不及擦干，就一缕缕地挂在浴衣外面。

<br/>

他俯趴在床上，却并无睡意。

<br/>

他在想这一整天，这围着苍度过的一整天。

<br/>

唉……冷水也没有用，他还是觉得无法平息。弃天帝翻了个身，躺在床上，伸手把天花板调成了透明——天亮之前，整片天空都还在混沌中。

<br/>

被朱武打断之后他没有和苍做下去。

<br/>

把浑身瘫软，精神恍惚的苍安置在卧室的床上，弃天帝转身去冲了个凉水澡，以平息自己的欲火。

<br/>

如果苍不愿意，那他也不愿意——这可算不上是在怄气。

<br/>

反正弃天帝没有一点这样的主观。

<br/>

朱武的意外到来，让悬崖勒马的前戏变成了真正的虐待。苍已经喷薄欲出，却受制于弃天帝锁住他身体的物件，紧锁着，令他求生不能，求死不得。

<br/>

苍难耐地在床上滚着，手下又想轻又想重，心里也是又想要又不想要。恍惚间叫唤得也不知道是谁的名字，只觉得有许多人影晃过，抚摸他，亲吻他，撩拨他，然后将他递给下一个人。

<br/>

苍无望地看着那些认识又不认识的影子，无助地被这个弄完，又被推给下一个。

<br/>

金鎏影不会在意他，他只顾着享用他被迫的痛苦；赭杉军也不会帮他，他不知道他也有可耻的软弱。

<br/>

谁才能解放他……谁才能救他……

<br/>

被欲火折磨得死去活来的苍开始无意识地流泪。这不能说是哭泣，更像是受到伤害时的生理反应。

<br/>

湿润的眼睛，这个世界都似乎被看得柔软，但是并没有人在这个时候来感受它的依赖和温顺。

<br/>

他眼睛周围薄薄的皮肤都泛着红色，渗出的泪水给它们润上诱惑的光泽，紧绷的肌肉让它们可怜地吹弹可破。

<br/>

“嗯……唉……唉……”

<br/>

苍在呜咽中，渐渐失去了意识。他柔软敏感的身体和精神，就这样蜷缩着沉入了黑暗的意识。

<br/>

不由自主。

<br/>

当他沉沉昏睡过去，门外的人却还在想着刚才的动静。

<br/>

在哭吗？

<br/>

弃天帝垂着眼睛，手指搭在门上，却没有打开。

<br/>

怎么会哭呢？他明明没有放纵自己的欲望做下去。

<br/>

他想起来那次在车里，苍泪眼迷蒙地望着他，已经流泪了，嘴角却还是带着那种憎恶的弧度。

<br/>

那种复杂的味道，泪水的味道，苦的，咸的。想象着舔一口，味蕾上电波般的麻痹和舒爽，让人觉得迷恋。

<br/>

有人说过眼泪里有某种致瘾或者致幻成份么？

<br/>

他为什么会哭呢？

<br/>

弃天帝不太能理解，因为他的床伴即使罕见地有不喜欢他的，也只不过嘲讽几句他的莽撞，但是他很快能让他们闭嘴。

<br/>

但是苍却会哭泣，他明明……明明……

<br/>

弃天帝推开门，苍蜷缩着躺在床角，几乎就要掉下去。空出来的巨大的地方，也不知道是不是在等着别人睡上来。

<br/>

弃天帝把他往中间推了推，苍只是本能地抖了抖，也没有醒来。他拨了拨苍的头发，看着他潮红退去后苍白的脸。

<br/>

他应该是累得崩溃了。不过弃天帝已经睡意全无了，也不打算睡了，干脆帮苍清理一下。

<br/>

伸手摸进薄薄的丝被里，苍的身躯就在他的手掌下微微地哆嗦。弃天帝在昏暗的光里，注意到苍的两腿之间的被褥不寻常的形状。

<br/>

他伸手探了进去，里面湿润一片，却空空地舒张着，什么也没有。弃天帝想抽出来，却忽然感受到了挤压。

<br/>

大概是因为本能，苍紧紧地收缩了起来。

<br/>

“唔……”

<br/>

弃天帝没有继续抽出，他怕他的动作太猛烈，会惊醒苍。

<br/>

一只手被紧紧地咬着，他只能伸出另一只手，摸到了前面。

<br/>

那里也是火热地——僵硬着——还戴着他的玩具。

<br/>

弃天帝这才想起什么来。

<br/>

他刚要动作，苍却开始扭动起来，大概是因为梦到了什么可怕的事情。

<br/>

弃天帝前面的那只手被他牢牢地揪住，既不能放开，也不能动弹。

<br/>

“傻……”

<br/>

弃天帝做了个口形，然后用嘴叼住被褥往下掀开，像掀开鸡蛋的薄裔一般，露出苍的整个身体。他的发丝扫过苍的身体，引来一阵阵地颤抖。

<br/>

“……不……不要……”

<br/>

苍含糊地说着。

<br/>

弃天帝张嘴，丝被落在苍的腰下，虚虚地盖住了两人交叠的手，和那不可说的悸动。

<br/>

弃天帝调整了一下姿态，尽量不要牵动被苍抓住和含住的双手，弯腰去吻那根只露出半截的手指。

<br/>

大概是动作还是太大，后面被搔动的苍剧烈地打了个哆嗦，手又往被褥下缩了一点。弃天帝笑了笑，干脆吻了那僵直红肿的地方。

<br/>

“啊……”

<br/>

苍轻喘着呜咽出声。

<br/>

可是弃天帝才不管，他吻了下去，然后用舌头轻轻地舔，轻轻地顺着肌理游走。

<br/>

苍已经缩成了一团，身后也紧张地缩得更加用力。

<br/>

弃天帝也闷哼一声，因为苍的臀部已经顶到了他的跨部。

<br/>

他喉结动了动，整个吞下了唇下的东西。

<br/>

苍慌忙放开他的手，两只手都要来捂住那里。弃天帝只闭着眼睛，就抓住了他的双手，不让他乱动。

<br/>

苍难受地在弃天帝的怀里扭动着，嘴里发出迷乱的单音节。

<br/>

弃天帝则专注地品尝着，直到那个冰冷的环也开始变得滚烫，他才停了下来，抓着苍的手捂住那里，然后亲吻了那枚戒指。

<br/>

“啊……”

苍颤抖的余音伴着低不可闻的喷射声消失，然后整个身体终于散架一般松懈下来。

<br/>

弃天帝抽回了双手，去洗了洗，又拿来了温热的毛巾，把苍的手擦干净。

<br/>

他幽幽地看了苍再次变得苍白的脸颊，替他盖好被子。

<br/>

——也许，是时候离开一阵子，他太过为他神魂颠倒了。

<br/>

弃天帝感觉着自己的兴奋，却冷静地这么想着。

<br/>

转身去开门，却意想不到地差点撞到——

<br/>

“……朱武？你在这里干什么？”

=============================================================

<br/>

“你这样假都不请就飞出国，一个高层的劳动纪律在哪里？一个高层的责任担当在哪里？一个……”

<br/>

万圣向来是个管理严格的机构，因此一步莲华这次出了事，少不了被高层说教。但是这说教说到医院病房里来的，也是挺少见的。

<br/>

一步莲华脚上打着石膏，高高地挂着，奈何不得他们，只能乖乖听着。

<br/>

正巧有人来，他终于等到了救兵一般急切地望向门口。

<br/>

“一步莲华……”

<br/>

门口来的人还不少，不过看到里面也这么多人，也没敢贸然进来。

<br/>

“素先生，书先生。”

“呃……我们来得不是时候吗？”

“当然……”

“是永道的人。”

万圣集团的高层似乎不太欢迎来着。

“说起来，一步莲华这次出事，多少也和赭杉军有关。”

一步莲华忙插话：“也不能这么说……”

“那要怎么说？要不是你不在，会给魔亘这种机会运作收购么？这下可好了，永道也没讨到什么便宜吧？”

素还真见对方语气不善，也不多辩解，只是顺着对方的话说下去。

“贵集团的变动，我们也很意外，另外赭杉军也受了伤，不便过来，我们替他过来看看，也表达我们的歉意。”

“不用歉意，不用歉意……”

一步莲华说着，又被领导瞪了一眼，声音低了下去。

“说起来，这件事情还是发端于你们。赭杉军怎么会贸然跑去魔国我们管不了，但是拉着一步莲华去就是他不对。”

“我是自愿去的……”

“你还说！弃天帝说不定就等着你们往套里跳呢！他就知道赭杉军会为了那个谁去魔国，扣下赭杉军，就知道你也会跟着去，就知道你们都会擅自离岗。他可省力了，按下了一个就能把我们连根拔起。”

一步莲华没话说，只好尴尬地坐着。

素还真也知道人家没讲错，弃天帝看似随意，其实思虑深远。他不是刻意制造机会，他是知道迟早会有这么一天。

“先生说得是，我们以后会注意管理下属，避免这种事情再发生。”

一页书是没有说话，其实他知道赭杉军去魔国的事情——如今的事情，也只不过是过去一直拖延至此的一次爆发。

<br/>

两个人出了一步莲华的病房，心里也很沉重。

<br/>

苍走后，管理层进行了一些变动，几个部门合并了一下，在没有外招的情况下也得以运行。但是赭杉军这一住院，事情就又要堆起来了。

<br/>

“你打算怎么办？”

“这要看弃天帝打算怎么办。”

“这么说来，万圣之后，就是我们了么？”

“有消息说，魔亘已经开始零新抛售魔国的矿业资产，你觉得这意味着什么？”

素还真眨了眨眼睛。

一页书似乎也没想，反问道：“意味着什么？”

“意味着，他们正在回拢资金——准备做点什么。”

“我知道，所以其实苍是为我们争取了这一年喘息的机会。”

“可以这么说吧……”

<br/>

=============================================================

<br/>

朱武百无聊赖地坐在座位上打瞌睡。

<br/>

由于大雾的原因，他的飞机延误了，而他昨晚上又没好好睡觉，现在正是又困倦又焦躁。

<br/>

正在这个时候，忽然有人拍了一下他的肩膀。

<br/>

“先生，靠窗的座位是我的。”

<br/>

朱武眼睛都没睁，嚷嚷道：“不可能！我爸怎么可能不给我买靠窗的座位。”

<br/>

那人沉默了一下，补充道：“我想他就是没有给你买靠窗的座位。”

<br/>

朱武正困倦不堪，为了昨天的事情心情不好，遇到这么不识时务的，简直要炸了。

<br/>

他睁开眼跳了起来，正要把飞机票摔对方脸上，瞬间停了手。

<br/>

“……爸爸……”

<br/>

弃天帝看了他一眼，示意他让开。

<br/>

朱武立即乖乖地让了出来。

<br/>

弃天帝久不坐商务舱，一屁股坐下去感觉有些不太习惯。

<br/>

“爸爸……你……你怎么也来了？”

“嗯？临时有些事情，临时决定去一趟，所以让飞机等了一下。”

<br/>

原来延误就是因为他。

<br/>

朱武立即想用鄙视的眼光看着他，但是他忍住了，以免被扔下飞机。

<br/>

他也坐了下来，感觉这个座位明显不太舒适。

<br/>

“嗯……你不在，那苍怎么办？”

弃天帝听了，莫无表情地转过头来。

“你应该问的是，我不在，魔国的生意怎么办。”

朱武看了他一眼，吞了吞口水。

“你满脑子都是他。”

弃天帝说这句话的时候明显是在生气。

“……我只是看他身体不太好……”

“他是成年人，住处什么都有，他会自己照顾自己的。”

“哦……感觉你还是不太关心他……”

弃天帝觉得朱武真的是好烦，转过头来瞪着他。

“像关心你一样关心他吗？”

说着，把一只手机拿了出来：“你的手机忘在他床边了。”

<br/>

朱武看着他，没想到自己的小伎俩会被发现，但是现在承认就完蛋了。于是厚着脸皮，笑逐颜开：“啊！还是爸爸好！我还说我怎么找不到手机了呢！”

<br/>

弃天帝冷笑了一下，说道：“你放心吧，即使有手机，他也休想打出去任何电话——网络也一样。”

<br/>

朱武僵硬着笑容，看着他。

<br/>

“乘客们，你们好！欢迎乘坐xxxx次号航班，我们的航班即将起飞……”
