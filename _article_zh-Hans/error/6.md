---
author: kasana
title: The Error
lang: zh-Hans
date: 2008-02-25 00:16
layout: post
depth: 2
category: error
---

### 1.First Error (6)

苍放下电话，揉了揉眉心。他心里没有底，弃天帝是个危险的人，但是他的要求也不算过分，没有拒绝的立场。 

<br/>

弃天帝很聪明，他不仅要求永道将已有的信用证和货款质押给魔亘，而且要求其未来三年的所有信用证和货款也由魔亘操作。贷款协议和合作协议一同签署。而魔亘则在未来三年，仍然有权利选择贷款或者不贷款给永道。 

<br/>

这样一来，永道在资金链上，会很大程度受制于魔亘。 

<br/>

苍当然不打算签署这样的合作协议，但是贷款协议已经迫在眉睫，非拿下不可。 

<br/>

他叹了口气。 

<br/>

弃天帝的条件，还包括另一件让他觉得蹊跷的事——要求苍本人飞往魔国，参加下周的集团董事会议。按照弃天帝的说法，那是为了显示永道的诚意，方便以后争取更有利的融资筹码。 

<br/>

苍却觉得，那完全只是弃天帝的表现欲在作祟。让对方公司的CEO上门借钱，简直就像抓着敌人俘虏凯旋回国一般。 

他能够感觉得到，弃天帝那浓密卷翘的睫毛间所飘出来的兴奋和得意。 

<br/>

可是目前，也只能走一步看一步。 

<br/>

他立刻给几位董事打了电话，说了赭杉的事，也说了贷款的事，得到首肯之后，着手安排手上的事情。 

<br/>

“喂，翠山行。能麻烦你今天晚上加个班么？我明天开始出差，请你把比较紧急的文件和相关事务整理一下，我今天晚上处理掉。” 

“这么急？” 

“是的。非常抱歉……要不晚饭我请你吧。” 

“啊！不不不。老板，可是你租房子的合同……” 

“哦，那个……要不这样吧，我的私人先寄存在你那里。如果你觉得不方便，也可以放在赭杉军那里。” 

翠山行大脑里迅速过了一遍苍的那几个箱子，放在他家里显然是很挤的，可是他却鬼使神差地没有拒绝。 

“啊……不要紧的，我替你收着好了。你还有什么不放心的，尽管交待我就是了。” 

<br/>

翠山行忽然觉得电话那头沉默了一下。 

<br/>

“老板？” 

<br/>

他是不是表现得太热心了？应该没有什么不自然吧，他们已经同事四年了。苍一般只在很有限的情况下会让他代为处理私事，但是也并不刻意回避他。生活上的很多事，他们闲聊也会说起，并没有什么见外的。 

<br/>

没有什么不自然的地方……他觉得他一直表现得很自然，苍应该不会感觉到什么…… 

<br/>

“老……老板？” 

“……抱歉，我……刚才有点走神……对了，还有一件事情可能要麻烦你马上去办一下。我的两个手机还在警局，我给你一个联系人，麻烦你替我向他取回来吧。” 

<br/>

翠山行听着，心里又踏实了。 

“好的，我马上去。” 

<br/>

记了号码，挂了电话，他风风火火地就走了。 

<br/>

这时候，苍才从自己的办公室摸了出来。 

他脸色苍白，微微地弯着腰。 

刚才打电话的时候忽然胃部开始抽痛，痛得他都没法说话。 

他这才想起来，自己没有吃午饭也没有吃晚饭——这是要修仙么？ 

可是男人的办公桌不像女人的办公桌，抽屉里总有各色零嘴。而苍唯一一张外卖卡也用于打掩护，给了弃天帝。 

<br/>

走到茶水间倒了杯热水，喝了半杯却不见好转，他简直痛得要蹲到地上去了。 

<br/>

“嗯……”苍呲着牙，靠在水吧吧台边，喘着气，用乘着半杯热水的马克杯贴在腹部疼痛的地方。 

<br/>

过了好一会儿，疼痛感终于慢慢有所缓解。 

他把冷水倒进旁边的一盆绿萝里，又倒了杯开水，慢慢地走回办公室看文件。 

<br/>

============================================================================================== 

<br/>

<br/>

次日清晨，苍回到了自己的家，解下了领带，换掉了西装，看着到处被白布包裹的家具，以及已经整齐地打包好的物品，有一瞬的茫然。 

<br/>

他没有茫然太久，就累得趴在没有被褥枕头的空床上睡着了。如果不是被电话铃声吵醒，他可能能一直睡到晚上。 

<br/>

迷迷糊糊地摸到家里的座机，“喂”了一声，却没有人讲话。 

<br/>

“我是苍……请讲……” 

“喂？谁呀？” 

打错了么？ 

<br/>

苍已经不是十年前的小伙子了，一晚上通宵已经是他的极限了。如今困意浓浓，也无力再对这个无头电话多计较，顺手就挂了。 

<br/>

等到他第二次醒来的时候，刚才的事情就好像是一个梦。 

<br/>

他没有枕枕头，醒过来的事后脖子疼得要命。撇到刚刚取回来的手机一看，天，已经下午3点了。 

<br/>

苍连忙开始收拾，把一些日用品和换洗衣服从打包的箱子里拿出来，分类放进行李箱。带上充电器和电源转换器，还有最重要的商务文件和证件。晚上6点整，准时离开了公寓。 

<br/>

到达机场的时间是7点29分，离飞往魔国的飞机起飞还有2个小时。他也没有急着check in，因为他在等人。 

<br/>

7点59分，那个人出现了。 

<br/>

“你来的真早。” 

“签证好了？” 

“我说过，签证像发邮件一样轻松。” 

<br/>

弃天帝笑眯眯地把已经签好的护照和机票交给苍。 

苍打开，看了一眼收进了自己的包里。 

“你怎么做到的？” 

苍其实并不怀疑弃天帝搞到签证的能力。哪怕是一天之内，他也觉得在意料之中。甚至，他也觉得，如果有一天弃天帝买下了霹雳联储，也不是一件不可能的事情。 

<br/>

从第一天看到他，苍就知道，这是一个不能够用常理来衡量的人。因此，他也清楚地知道弃天帝的威险性。所以，第一次见面他递上了一张外卖卡。 

<br/>

“你对我怎么做到的有兴趣？”弃天帝挑了挑眉毛，然后凑近苍，居高临下地看着他：“告诉你有什么好处？” 

<br/>

苍眯起眼睛，别过了头。他对这种过分靠近的距离有些不适。 

<br/>

“很抱歉，我以为这个问题是free的。” 

<br/>

弃天帝笑了笑，他今天穿了件休闲连帽衫和牛仔裤，于是看上去更加像一个大男孩而不是一个大男孩的父亲。 

“哦，其实代价不会很高。” 

<br/>

苍看着他，礼貌地笑了笑，转身排队去check in。 

弃天帝却忽然拉住了他。 

“嗯？” 

“我们的票是vip通道，那边走。”

<br/>

“我已顺利到达。保重身体，不必担心。” 

苍点击了一下确认发送。

屏幕弹出了对话框：

发送至“Plustri”的邮件已成功。

<br/>

酒店的大堂有一点好处，就是可以使用免费的无线上网。因此，等待房间确认的十几分钟也不会太过无聊。苍回完邮件，抬起头看了看满头大汗的大堂经理。弃天帝的秘书正在同他交涉，看上去，这位秘书先生不好对付。

“Mr.Falling...”

“Fuying...伏婴，谢谢！”

“额，抱歉。我已经查过了，这里确实没有这位苍先生预定的房间。您看要不我现在另外给苍先生安排别的房间？”

伏婴师耸了耸肩，转过头来，用一种刚好能够让苍听清楚的音量说：“这不可能。我特意提前两天到这里订的房间。我们总裁和这位苍先生的房间是毗邻的。弃先生，你看怎么办？”

<br/>

他这句话是对着坐在苍对面的弃天帝说的。

弃天帝正在打计时小游戏，没工夫操心那事情，随口就问了句：“查查还有什么房间。”

大堂经理赶紧去系统里查。

<br/>

“Mr.Seven,还有一间房。不过不是商务套房，是东楼的Jupiter Hall。”

伏婴师听了皱起了眉头。他是这里的常客，知道Jupiter Hall是这家酒店最贵的总统套房。他的手指神经质地开始敲击假象中的小键盘— —整整要多付80000魔币。

最麻烦的是————他怎么可能让自己的老板住商务套间，而让苍住在这里最豪华的Jupiter Hall。

心里想着，就偷偷瞥了一眼还在打游戏的弃天帝。

后者刚刚又破纪录地完成了一个游戏的大满贯，得意地把PAD顶在手上转圈。

<br/>

“弃先生，您看要不我们给苍先生就近安排另一家酒店？”

弃天帝一面惊险刺激地转着平板电脑，一面漫不经心地问道：“为什么要换？”

苍挑了挑眉角没有作声。

“你把我原来那间退了。我和苍先生Share Jupiter Hall。”

说着，他双掌一夹停下了手上的小把戏，转过头来对着苍微微笑。

“苍先生不介意吧？我们此财政年度需要Cost Central。”

苍看了他一眼。

弃天帝花异度的钱，他又能说什么？

苍合上笔记本：“客随主便。” 

============================================================= 

坐了一天的飞机，浑身都不舒服。苍整理好了自己的物品，抱着自己的换洗衣物去自己的浴室洗澡。结果发现，弃天帝正在里面整理仪表——具体来说，在修面。从镜子里看到苍抱着衣服站在外面，惊讶地回过头。

“你要用这里？”

苍快速地扫了一眼他的进度：“没关系。你先用。”

说完，转身要离开。

结果，弃天帝出人意料地说了句：“我们可以一起用啊。”

苍有一点点地震惊，回头看了他一眼。

弃天帝用那个还沾着泡沫的下巴，冲着浴室里面扬了扬，进一步补充道：“你看，干区和湿区有门隔着的。” 

苍笑了笑：“不用了。”

“可是我们1小时后就出发了。”

“嗯？”

“晚上有欢迎晚宴啊。”

<br/>

苍顿了一秒，“噢”了一声，转身走了。 

“你不用了么？”

“卧室还有一个卫生间。” 

<br/>

一小时后。

黑色的酒店贵宾车已经候在楼下。

弃天帝和伏婴师已经穿戴整齐，等在车里。这个时候，旋转门的光斑开始移动，随着门童招牌式的弯腰，身着黑色燕尾服的苍走了出来。伏婴师上前为他开了车门，苍说了声谢谢，坐进了后排的座位。 

<br/>

“你这套礼服的腰封和领结是蓝紫色的？”

弃天帝看着包裹着自己双手的白手套，漫不经心地问道。

苍看着前面挡风玻璃中间的的后视镜，里面的弃天帝似乎看着自己的手在隐隐微笑。 

“很多人都以为是蓝色的。弃先生眼力很好。”

“哪里，我觉得这套燕尾服很适合你，挑得不错。”

“多谢。我也仅此一套。” 

两个人互相看了一眼，礼貌地笑了笑，一路无话。

<br/>

晚宴其实就在酒店的宴会厅和花园里举行。

弃天帝在这里似乎很受欢迎，一出现就被各界名流和闪光灯围在中间。不过，能够发现亮点的才是社交圈的先锋，才是新闻界的奇才。

很快，苍就可以用余光发现那些放弃弃天帝转而向他涌过来的人群和长枪短炮。保安先前的部署原本没有考虑到这种情况，显然大队人马都去保护弃天帝了，苍身边只有一个矮个的保安，立刻就被人群中的各种明拖暗掐挤到了一边。

当苍发现情况有些失控的时候，一个套着PNN牌子的话筒已经几乎顶到了他的下巴。 

“请问您和弃天帝是什么关系？”

“请问您和弃天帝是什么时候认识的？”

“请问您觉得弃天帝先生在休假期间带您出席这种社交酒会是否有特别含义？”

“请问您来到弃天帝先生的家乡有什么感受？”

……

苍和弃天帝在红毯上被隔成两截，各自被盘问着各自的问题，仿佛一场众目睽睽下的隔离审查。

一扭头，发现情况有异的弃天帝，不顾伏婴师的阻拦，硬是挤到苍那里，自作主张地挽起了他的手。 

“我们是partner。” 

他抓着最近的一个话筒微笑着，留给摄影师一个清晰完美的笑容。 

<br/>

“暂时的，生意上的。谢谢！”

苍不动声色地凑上去补充道。

<br/>

弃天帝和苍在保安们的保护下终于进入了秩序井然的主会场。 “等等！”

在黑色铸铁正门的一边，弃天帝忽然贴上了苍的耳际。

苍刚要转头看他闹什么花样，却觉得腰上一紧。

“你的腰封松了，我替你紧了紧。”

苍背上一僵，愣了一下，正遇上弃天帝那张写满了“我做好事不留名”、“千万别谢我”的表情的脸。

他就这样，超过了苍，迎向那些缓缓围拢来的香奈儿、阿玛尼、迪奥们。

<br/>

苍哼了一声，拉了拉领结，从侍应的手上取了一杯开胃酒。

不管弃天帝要做什么，他要完成他的使命。

<br/>

<br/>

回酒店客房的路上，弃天帝一路上都在哼着Pachelbel的Canon。

苍则默不作声地看着窗外，他喝了点酒，但是头脑仍然很清醒。

弃天帝自己哼了一会儿，忽然又把注意力转了过来，赫然发现苍和自己坐得很开，似乎也没有要靠近的意思。

“苍先生，今天晚宴上有什么收获？”。

苍放下托着下巴的手，转过来看他。

“收获是，发现弃先生酒量很好，两瓶白兰地下去，也没有任何不适。”

弃天帝的脸一下子僵了，他知道苍在说之前自己醉酒回家的事情。

“苍先生过奖了，这会儿也许酒劲儿还没有上来……其实……苍先生也让我很惊讶。”

“哦？”。

“从下飞机到宴会，几乎没有什么休整的时间。一般人都会受不了，而你，竟然能面面俱到，面对媒体也不惊慌。”。

弃天帝于是不知是崇敬还是探究地向苍靠了过来：“你觉得，我们是否有一点相似？”

苍就保持着这种鼻尖几乎要碰到对方的距离，笑了笑：“恐怕至多也只有那么一点相似。”

弃天帝挑了挑眉毛还想说什么，车速却忽然降了下来。

门童过来拉开了车门，苍垂下眼睑毫不犹豫地跨出了车。决绝的仿佛他从来没有注视过对方，那短暂的对视只是他在回头的时候，视线恰好掠过那个名为弃天帝的人。

一切都只是他旅途的风景。

<br/>

两人一起坐电梯进了套房。弃天帝先说自己要睡了，熄灯关门。

苍连晚安都没有说，直接进了自己的房间。

过了没多久，一直在黑暗中睁着眼睛“睡觉”的弃天帝，听到抽水马桶的声音以及吧台传来的玻璃杯砸碎的声音。他一个激灵坐了起来，。

在房间里听了一会儿却听不到进一步的动静，终于慢慢地转开门把手，穿着羊毛拖和睡袍跑到了吧台那里。

却看见一排的平底玻璃杯少了一个，射灯下，花岗岩地面上有些闪烁着的细小颗粒。

这时，洗手间又传来了响动。他安静地听着，才能分辨似乎是有人在呕吐。还没等他迈开步，又听到抽水马桶的抽水声和关门的响动。

弃天帝抱着手臂，原地站了一会儿，很快就走到套房的另一边去叩夫人房的门。

“苍先生？”。

他又敲了敲。

“苍先生睡了么？”。

这么问着，手已经伸下去转门把。果然，门并没有锁。

房间里一片漆黑，弃天帝打开的门是唯一的光源。他应约看到了华丽庞大的卧床，以及在卧床上一团黑色的影子。

他放轻了脚步走到床边，然后慢慢坐到了床沿，一只手探到了被褥里。

冷的？。

“啪！”。

水晶吊灯忽然亮了。

苍穿着自己带的睡衣，抱着手臂，靠在门口，仿佛是一尊大理石雕像。

“弃先生，你到底想干什么？”。
