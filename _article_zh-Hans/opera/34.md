---
author: 西陵歌者
title: 戲
lang: zh-Hans
date: 2010-06-22 00:00:34
layout: post
depth: 2
category: opera
---

### 第三十四场 弃家公馆Ｉ



旧历十一月初七，也就是西历１２月２２日。

朱武陪同弃天帝参加了Ｄ省武备学堂的开学礼，在回来的车上便已经向父亲请假：说是有位朋友有点急事，需要自己帮忙到大约西历１２月２５日左右。弃天帝冷冷一笑，没有反对，也算答应了。

抵达公馆，沐浴之后已经是临近黄昏的午后了。晚上原本有个同Ｊ城各界名流的酒会，只怕再稍喘口气便要动身，不过在这个间隙听着断风尘、任沉浮以及伏婴师三人的汇报，弃天帝坐在一楼客厅的沙发上，看看时间，脸上也没什表情，淡淡地说：“接他来，我等着。”

<br/>

接近久违的弃公馆的时候，日头刚刚沉下。补剑缺将车停在门口，戒神老者赶紧跑下台阶来接。

走进大厅，慢慢沿着楼梯而上，苍恍恍惚惚地没有什么想法，踏了几步后才猛地反应出：这飘荡在整个公馆的腔调，竟是自己所唱的“太真外传”了。这时，灰暗的建筑之内突然一下子灯火辉煌了起来，倒叫他吓了一跳，仿佛是猛醒了过来。

“苍先生，苍先生，稍等，稍等！”戒神老者适才是去开灯了，这时又追了上来，拦住了自己一味向上走的苍，“适才几位官长来了，老爷在会议室商议军情，您稍候片刻了吧。”

“嗯？啊！抱歉。”苍愣愣，才意识到自己竟是第一次在这房间中擅自行动——大约是最近的事情太多太乱，叫人疲惫不堪，否则是断不至于如此不谨慎的了。也许——苍低下头慢慢思忖——只是想念那人的紧了，迫不及待想要见到——这念头一冒出来，便觉得有些肉麻又好笑了。

“苍先生，有什么高兴的事呢？”

即便是现在这样浅浅地笑容，戒神老者也从未见过，倒不是没见过他笑，只是没见过这样发自内心的真的觉得什么值得一笑的表情了。

“啊？高兴的事？”苍摇了摇头，嘴唇动了动，神色又黯淡下来，没出声，慢慢走下了楼梯。

看着那脸上又露出了一如往常的因为需要随时思考对策而显得凄苦却又因为实在觉得无能为力而又有些心不在焉地表情，戒神老者真是后悔：最近的事情难道自己知道的还不够多？难得能见到他进门后，似乎稍微开怀了些，为什么还要出声提醒他：真的没有什么值得高兴的事情。不过，事已至此，懊恼无用，戒神老者拼命想找点其他话题，却是突然一下子不得要领了。直至走进一层小客厅，看见房间正中不停转动的唱片机，才终于抓着了一个能够转移他注意的玩意来。

“苍先生您的这几张唱片，老爷颇为喜爱的。”方才一直便坐在这里等着苍，直至几位中级军官奉了自己上官之命前来觐见之后，弃天帝才上楼去。走前却不知是有意还是无意，便任由留声机在这里继续。戒神老者其实也是喜欢听戏的，只是做了管家之后，很少出门，所以也乐得能够一饱耳福了。

“多谢。”慢慢点了点头，苍顺着戒神老者地手指，坐在了距离留声机不远的沙发上。

初次唱片其实录得仓促，苍自己也完全没有静心听过。如今坐在屋中，戒神老者去端茶，他无所事事便索性闭了眼睛，细细听着。那唱片慢慢转至马嵬坡一段，苍终于察觉并非是声音不一样，而是唱得不够好了。那【峨眉辗转马前死】，是伤心抑或愤怒？抑或……解脱与了然呢？这出戏，是师父临终前说给自己听，而后自己慢慢琢磨，唱腔身段与舞姿都是极美地，却又觉得总是唱不到最妙处，更悟不出人物的真性情来，原本此次不想灌制唱片，却又想起自己登台不久了，总想留些纪念了。

“苍先生，您还没吃饭吧？”

端了茶点来，戒神老者看苍在思忖中不停地调整坐姿，心中一动，轻声问道。

“……确然还没。”这个时候，同门们应该在吃饭了吧。

“……唉，您也要注意身体啊。”

苍脸上一红，轻声说：“上午出去办事，回来的晚了……”

“哦哦，那老头儿去给你做口吃的？还是葱花面荷包蛋可好？”戒神老者今天莫名来了精神：自从搬到Ｊ城，便不用自己下厨，偶尔想起在津门小楼中，那爷俩捧着饭碗狼吞虎咽地半是玩闹争食地模样，也会想看着别人尝尝自己手艺的念想来了。

“我……我去帮您吧。”

“呵呵，来吧，不用帮忙，说说话也好。”知道苍心情抑郁，也觉得不好放他一人独处了。

<br/>

弃公馆的厨房很干净——厨师伙计每日都将各种辅料备好，实在是没有什么要苍帮忙的地方。因为今晚弃天帝赴宴，这时间做其余佣人们的吃食还早，因此厨房无人。苍便坐在外面佣人的餐厅里隔门看着戒神老者一个人忙活着。

“给您打几个鸡蛋？两个？”

旁边的锅里煮着面条，戒神老者又架了口锅，烧了白开水，要打荷包蛋了。

“一个就好……多了，胃口吃不消。”平日很少吃荤腥鸡蛋，苍说得乃是实话。

<br/>

“打两个。”

<br/>

“啊？”门口多了一个声音，戒神老者和苍同时回头，见弃天帝立在门口，应是结束了公务，循声找了过来。

“老爷……您……”

“嗯，给我也盛一碗。出来等，这里不是你坐的地方。”弃天帝点了点头，后面一句话已经是对着站起身的苍说了。

“长官……”苍此时已经站直了，在想着如何启齿的时候，弃天帝已经转身，同时向他抬手示意，退了出来。

走回方才坐过的客厅，弃天帝停了步，伸手一拉将苍扯在自己身边，坐在沙发上。

屋里的留声机还在自顾自地转着。两个人都不说话，静静地听着那似乎是不属于任何人的声音。慢慢地，苍不知不觉已经靠在了那人身上。

“有什么要求么？”等到唱片转完，只剩唱针触碰胶片的声音之后，弃天帝终于问道。

苍怔怔，本来想要抬头，不过最后还是放松了脖子，摇了摇头，头发蹭着弃天帝的肩头，沙沙轻响。

“那就先吃饭吧。”弃天帝的眸子转向眼角，戒神老者已将两碗热汤面端了进来。

<br/>

“我晚上赴宴……”看着苍坐在对面慢慢地用餐，弃天帝用餐巾擦了擦嘴。

苍放下了筷子，抬头看看，“长官有事，我先……”

“既然来了，便不用回去。”将碗里没动过地荷包蛋夹进对方碗里，嘴角翘了一下，“难道要我回来再等他们去接你？”坐着不动，只是看着苍认真地一口一口地吃面——这社会最底层的挣扎求生的人们，因为太过艰难，而无法真地享受食物；而这社会最上层的享受一切的人们，又因为太过轻易，而忘了这用餐的单纯的乐趣。弃天帝感慨之余，竟是又想起朱武来——那个在津门小楼里坐在餐桌边狼吞虎咽吃饭的小子。

“可是，明日还有演出……”苍皱了皱眉头，说地犹豫了。

弃天帝静静坐着，沉默了一会儿，说：“双仪舞台和近期的骚乱我都知道了，有些决定……须跟你说。”

“啊？”心中蓦然一紧——其实自己本来的目的也就是为近日混乱来见大帅，只是进来之后的温馨安静，叫苍竟也是暂时将这些烦恼忘掉了。

“……回来再说。”时间差不多了，能听得到补剑缺在后院发动汽车的声音，弃天帝站起身。

“……其实，苍懂得。”

“回来再说……”弃天帝吐了口气，少有的不带着任何烦躁的重复，然后轻轻拍拍苍的肩头，“应该……没你想得那么糟。”

公馆大门关上了，苍坐在餐桌前，肩头的余温似乎还在，突然生出点迟迟疑疑的后悔来——不知道该不该在他离开之前，转过身，给他一个拥抱。

<br/>

<br/>

“目前，已经调查清楚的是……”事情紧急，断风尘却是有点迟疑，“根据目击者的描述，派人调查……这几个死者确实都曾在双仪舞台……那个……有点小冲突。”

<br/>

赴宴回来，已经是半夜。

弃天帝带着些醉意，正要上楼睡觉，却看见了在客厅里等了一段时间的警察厅长。两人一前一后来到楼上，弃天帝将等在书房里睡着地苍抱进卧室，才又回来，坐在桌后问话。

断风尘是亲自查办近来双仪舞台的纠纷的。不过，戏园子的斗殴事件还没能有个大家都满意的说法，下午又接到报案：有两人被围殴致死。巡捕调查之下，案发现场有人隐隐听到什么“得罪苍老板，就是得罪弃大帅”之类的言辞，是以落雁孤行抢在记者的大队人马到来之间，将案卷抓起，连带报案者和各路证人，一起塞进了厅长办公室。

“什么冲突……”弃天帝的声音略有些沙哑——

今晚宴会来宾甚多，战事逼近，各方利益，各怀心思，几乎每人都要周旋。和贾命公和恨不逢父子等黑道人物，酒桌上一番讨价还价，明争暗斗尚不觉得什么，不过随后被强压着怒气地法院院长殷末箫抓着谆谆告诫要检点个人生活，约束亲族等等，弃天帝又无奈又好笑地应对良久方才脱身，此时也觉得有点累了。

“其实也就是比如嫌弃茶水不好，园子太挤之类，和伙计们口角了几句。”

“啧，这盆污水扣得实在啊……”端起桌脚的大茶杯喝了一口，弃天帝嘴角露出了冷笑。

“大帅，能动用这么多人，卑职认为只有贾命公那一票人，不过……想不出来他为什么突然针对苍先生。倒是薄红颜，听闻她和曌云裳的交情倒是不错。只是她自从曌云裳死后，声称受了惊吓，一直在城郊养病，听说连第一楼的生意都不怎么过问。现在看来，是不是故作姿态……”

“他不是在针对苍，是在针对我……这是在要我妥协，继续从他们的渠道购买武器……”吐了口气，弃天帝只觉得头疼得要裂开了，便站起身，“……你们继续调查，看看有没有什么蛛丝马迹。”走到门边的时候，突然又一回身，说：“既然这样，那么曌云裳的死也不单纯，你的身份，深入调查不会打草惊蛇。”

“是。”既然长官已经给出了暗示，断风尘自然心领神会。虽然经过之前的一些事件，对自己这位挂名的大姨子并没有什么好感，不过无论何时，亲戚关系都是个不依不饶一查到底的好借口，“不过，双仪舞台所提供的茶叶已经证实是以次充好，有欺诈之嫌。”

“……停业整顿……三日吧。”心烦得很了，弃天帝沉沉地说了一声，拉开门，“你去吧，明天直接执行就好。”

断风尘有点迟疑——方才进入书房之后，见到弃天帝轻轻将苍抱走的样子，更是不自觉的想起自己的新婚之夜来。

“不用等苍回去，这是戏园子方面的错处，与他无关。”

“是。”

断风尘低头，弃天帝已经出了书房，向着房门虚掩的起居室而去。

其实，在弃天帝走入书房抱起自己的时候，苍便醒了。虽然一直想要询问，然而朦胧中听到还有别人的声音，觉得有些难为情了，只有先装睡下去。当身体被放在床上的时候，才迫不及待地睁开眼，大抵是因为卧室内没有开灯，弃天帝又有些心不在焉，竟是没有看见，直接替他盖好被子，便起身，将房门从外面轻轻掩上了。

苍睁了眼，因为一直便是入睡，所以此时并不觉得屋内漆黑难辨，然而，也没有什么值得看的事物了，便又将眼睛闭上。深吸口气，竟有些不敢相信，园子的骚动已经发生了一日；同时班内的争执却又好像是很久远之前的事情了。有些事，不愿想起，原因只是很难忘记吧……迷迷糊糊的，似乎听到有人来开门了……

……

虽然尽量将门开小，瘦弱的身体挤进屋里，不过看到挤在炕上的师兄弟们在那一小阵冷风中不自觉的又缩紧了几分，苍还是觉得愧疚了。十一月的夜晚冷得好像冰窖，所有人都挤在大屋内，围着一个彻夜不息地火炉搭床。小心翼翼爬上最靠门口的木板搭成的自己的床铺，尽量不弄出任何声响地钻进被窝，只有靠近炉火的脚尖有那么一点点温暖的感觉，不过，苍已经很满足或者说没有多余的时间浪费和挑剔了。

躺下之前，又看看床头那个缺了一角的大碗，里面那条今天从阴沟了捡出来的小金鱼大约也睡了，沉在碗底动也不动。也不知道他会不会冷，苍搓搓自己被冻红了的手——他知道冬天的水有多冷。躺了一会儿，终于还是不放心，将枕边冰凉的瓷碗拉到怀里，用手小心翼翼地护着就这样蜷着身子睡了。

……

早晨起来，苍的姿势一动都没动。可是，小金鱼还是平躺着漂在了水面上。

“苍？怎么了？”

每天都是班子里起得最早最勤快的孩子，今天却坐在床上发呆。

“师叔……”

“……唉，冬天的小鱼，这么浅的水，活不长的。”低头看看，师叔摸着苍的头，大概是在后悔：昨天就不应该帮他把这条注定会死的小鱼带回身边，“别哭了，苍。师叔今天发戏份钱，一会儿带你出去买糖吃！”

“噢噢，有糖吃！师叔，我也要！”紫荆衣突然爬了起来，大声一叫唤，本来都在赖床的孩子们都跳了起来，好像过年一样又叫又笑。

“好啊，都有，都有！”师叔脸上没有什么为难或者心疼，笑眯眯地答应。

“师弟，又乱许愿……”班主师父皱皱眉头，不知道是嫌吵还是心疼。

“没事，没事……苍，好不好？”低头看着仍然慢慢摇头的苍。

……这个时候，小鱼拧了拧身子，又游动了起来。

“咦？鱼，鱼又活了！”

“哼，既然活了，赶紧去练功，别瞎闹了。”师父已经起身，沉下了脸。

“是。”惊喜非常，苍抹了抹眼角，破涕为笑。

“……怎么又活了！”紫荆衣皱了皱眉头，小声嘟囔一句：“那还有没有糖……”

“鱼都活了，还要什么糖！”师父又哼了一声，师叔吐了吐舌头。

……

“讨厌！”紫荆衣拉着金鎏影嘟嘟囔囔，“那小鱼死了多好，这样就有糖吃了！”

“……那是苍捡回来的。”金鎏影瞥了半天，说了这么一句。

“咱们再把它弄死吧！”

“你们……”其实一直就站在旁边，大家失望的表情让一直专心的苍今天也不能专心了。

“怎么了，小鱼死了，大家都能吃糖，现在只有你一个人开心了！”紫荆衣撅着小嘴。

苍的脸涨得通红，却是一句话也说不出来，虽然觉得哪里不对，但是，却不知怎么反驳了，只是单纯的觉得，不能弄死小鱼，更不能为了吃糖弄死小鱼……“我没开心，你们不……不能这么自私……”

“你才自私呢！你就是不要大家吃糖！”

苍无措地向周围看看，却一直没有别人说话。

“我没有……”

“那你给大家吃糖啊！明明师叔都答应了……”紫荆衣说着，竟先哭了起来，“就怪你！”

“我没有……”紫荆衣的哭声越来越大，大家都围过去哄他，倒是把苍隔在外面了。

“怎么了？”

陪着师父出门的赭衫军和蔺无双两位师哥回来，见到这情景，赶紧跑过来安抚。

“苍把小紫说哭了……”金鎏影头也不回地回答了一句。

“我没有……”颤抖地话音里带着震惊。

……

当天晚上，苍小心翼翼的将那碗放在炉台上，小鱼很活泼，摇晃着尾巴，在不大的碗里游动。看着这小鱼，苍心里默默地说：怎样都没关系……

……

“你个小冤家！”睡得迷迷糊糊，腿上已经隔着被子被狠狠地抽了一笤帚柄。

“啊！师母……”立刻就吓醒了，苍看着一只温柔的师母满脸怒容，才坐起身，就又挨了一下，打在肩膀上……

“你是巴不得害死师父是不是！”

“……我，没有……”劈头盖脸地打下来，苍声音吓得发抖，莫名中已经带了委屈。

“哎呀，嫂子，这是干什么啊！”师叔披着衣服跑了过来，一把将苍抱在怀里，“师哥昨天喝醉了，没看清才把那条鱼喝下去了啊，怎么怪得了孩子？再说了，郎中刚才说了，就是泻肚子，没什么事啊。”

“怎么不怪他！”师母说了一句，眼泪已经流下来，“人都养不活了，还捡条鱼回来添乱……都在挣命呢，谁生得起病啊！”

“……”

“还不如昨天弄死了，大家还有糖吃……”在师叔怀里抽噎的时候，听见早就躲到屋外的紫荆衣这么嘟囔。

“我……我没有……”

<br/>

“苍，苍……！”

被叫醒了，看着昏黄的灯光中弃天帝的脸，苍的耳边却还是不停回荡着那些不知从哪个时空中飘出来的各种声音。

<br/>

刚刚推开卧室的门准备休息，通讯兵已经追了过来。

“玄貘战败，两广之地归属广州政府。”

连夜召集身边要员，大致安排商讨一番后，时间已经过去了快两个钟点。弃天帝才从二楼的会议室出来，再次走进卧室。

看见苍，才又想起区区Ｊ城内小小封云社的事情来——如果不是这个人躺在床上，这种事，自己本应该只在明天或者后天的报纸上看到一个处理结果而已吧；不过，如果苍不躺在自己的床上，也完全不会发生这些事吧。

“我……我没有……”

立在床边解开衣扣，身后的人突然不停的重复这几个字……

<br/>

“梦见什么了？”

“……小时候的事情。”脑海中那些声音还没有消失，不过，无论何等噪杂，弃天帝低沉的嗓音一直都是那么清晰。

“……你心思太重。”衬衣还有一半披在身上，这是第几次把噩梦缠身的苍叫醒了？弃天帝坐在床边，轻轻摸摸苍沁着冷汗的额头。

渐渐清醒了过来，苍脸上露出淡淡地苦笑。

“……什么样的梦，才叫做噩梦？”弃天帝突然好奇了，在认识苍之前，他只见过做梦狂笑直到笑醒的朱武。

“那种……不想发生的事情发生了，或者不想回忆的事情……”

“无能为力的情况么？”

“嗯，全然地……无能为力，也，没有道理。”明明觉得不对，可是却又明明对大家都好……

弃天帝沉默了一下，“……昨天下午，有两个人被打死了。”

“啊？”苍浑身一震，半撑着支起身子，“长官……难道是……”脑海中竟是刷刷地出现了一个个熟悉的面容。

“是几天前在戏园子闹事的观众……被打死了……”吐了口气，看出了他的误会，赶紧解释说，“……目击者说，是打着你的名义。”

“我……我没有……”这话又说出口了，苍竟是一个寒颤，“我没有”这几个字对苍来说，最是无力，从小到大，这便是一句仿佛没有人曾相信过的话。

“我知道啊。”弃天帝有点讶异的回头，理所当然地回答——这事，根本是不需要对方来否认的，“背后的主使……”看着那还在震惊中的脸，以为他是被这卑鄙的阴谋吓到，正要再多说些什么安抚的话，苍却已经一下子扑进怀里。

柔软的发丝拂过露在外面的半边胸膛，弃天帝不经意深吸了口气，意识中觉得仿佛被军情案情冲淡地晚宴酒意一下子涌到了头顶。身体渐渐热了，才想起把另外半边衬衣也脱了下来。赤膊坐在床边，由着苍抱住自己，却不太明白，对方这突如其来的感动来自何方——不过，不用在意这些小事吧。弃天帝轻轻抚摸着苍的脊背，有点犹豫，不过既然开了头，还是要继续说完。

“这事应是有人为了对付我……后面的事情交我。”

苍愣了一下，点了点头，慢慢地直起身。

“不过……暂时，双仪舞台需要停业整顿三天，我也会下一道让你闭门自省的训令。”

苍不说话，继续点了点头——虽然结果一样，但是这个人真的是相信自己的。

“……天亮，断风尘就会去封园子。”

“……我回去。”

“嗯……不急。”扭头看看床头的闹钟，凌晨三点而已，距离定好的军事会议还有五六个小时，“还有时间……陪我睡。”说着站起身，也懒得换睡衣，就直接将长裤一脱，钻进了被中。

其实坦诚相对也很多次了，不过苍这是第一次在灯光下看见弃天帝赤裸的上身，虽说不上密布，然而匀称的身体上伤疤的数目也算得上惊心了。苍下意识摸摸额头深入发迹中的那短短的一条伤痕，突然觉得真的是微不足道了。

“睡了。”弃天帝闭上眼睛，其实刚才还在想趁着兴致做点别的更有意义的事情，不过头一沾到枕头，困意就一刻也不停地袭来，只来得及将身边的人抱进怀里，便入眠了。

<br/>

“萧，你去睡吧，剩下的有我和黥武做就好了。”

朱武抱着一箱子颜料纸张走进道具间，向着眼中已有红丝的萧中剑说道。

其实现在已经是旧历十一月初八，西历１２月２３日的凌晨了，距离文明戏正式开演的西历１２月２４日晚上，也就一天多一点的准备时间了。

“没事，我还可以的。”萧中剑正在认真地描绘着最后的一块场景。这个钟点，刚才还干劲十足的文学社和请来帮忙的艺术社的众人都已经纷纷告辞回去休息，只剩下寥寥几个专心敬业的社员各自忙碌。

“赶紧睡觉去。苍说过，这叫‘美人觉’。睡不好，登台那日脸色需不太好看的！”朱武说着将箱子放下，“还有时间啊。”

“嗯，我明日定会按时睡下，今天便多干些吧。不然明天还是要赶工的。黥武，你看看，这里的颜色够深了么？”萧中剑头也不抬地继续描绘一片花树。

“应是差不多了吧……”黥武倒还是认真，只是站起来后退时，也已经哈欠连天了。

“恩恩，相当好了！”朱武说得迫不及待。

“来来来，大家先休息下，我买了包子！”

“冷醉？你不是回家了？”记得接近凌晨，冷家人来接，便把这位少爷劝了回去。

“家里人都睡了，我放不下心，便又跳窗户出来了。”冷醉一笑，怀里抱着沿途所得的两大口袋夜宵，“……你们进展很快啊。”

“大家先歇歇，吃包子吧。”萧中剑起身，还不忘招呼在一边安装和试用幕布的几个社员。

……

“都走了？”黥武去扔垃圾再回来，发现偌大的后台道具间就剩下三个熟人了。

“是啊。”冷醉笑眯眯地回答，“吃饱了更困吧。”

“嗯……回去睡一觉，清醒了再来也好。”萧中剑是那种一旦来了兴致便不愿停下的性格，不过对认识的别人倒也宽容得紧。

“刚才明明还说要一起熬个通宵的……”黥武回想刚才一起吃夜宵时候的热闹，又看看现在骤然冷清的道具室：地上还有散落的各种工具，人走得仿佛逃难一般，竟不想先将其收好。

“总要回去吧，想也知道不是所有人都能陪你一起到天亮的啊。”朱武倒是说得轻松，对他而言，无论几个人都是足够。

“嗯……”脑海中不知道怎么竟出现了一张脸，黥武愣愣，打了个呵欠，看来确实是到了往日都在梦中的时间了。

“不过……还是人多一点好，否则黎明之前会很寂寞……”萧中剑回想起每每通宵写作，等到窗外渐亮，拉开窗帘，看着楼下清冷的街道和城外的旭日慢慢出现的场景——若是看见那场景，怕是往后的一天，无论这世界多么热闹，也还是会觉得寂寞吧。





