---
author: 西陵歌者
title: 戲
lang: zh-Hans
date: 2010-06-22 00:00:50
layout: post
depth: 2
category: opera
---

### 第五十场 天波别业Ｄ



几个月来，双仪舞台的票，卖得一素是很好，只是最近又似乎好上加好。

黑狗兄坐在后台姑且闲置的会客室里，点上了烟袋，闭着眼睛咋么了几口——虽然，最近买不到关外的好烟土了，然而毕竟是心情颇佳，倒是惬意了。连着几日票房都在看涨，一来是因为前阵子让萧中剑改写的剧本已经排演完成，粉墨登场，虽说生书熟戏，然而寻常看客也要新鲜，而一些文化名流，倒是对这改编过多了那么点点“文明”意味的老戏新唱怀着嘉许，便也都纷纷来捧场了。一时间，封云社双仪舞台倒是成了雅俗共聚之处；二来……看看门楣上的日历，约莫大家也都猜出了苍要退社的时间，Ｊ城小贵妃的戏嘛，倒也是看一出少一出了。

“唉……”想到这里，黑狗兄不由得有些陈烦泛起，吐了口烟气——自己在Ｊ城是有家有业的，这成都倒是去也不去呢？面前烟雾遮眼，黑狗兄情不自禁挥了挥，宽心道：时局如此，也许根本由不得自己，万一乱了，想不走也难。

此时，鸣锣开戏，【刺虎】这出，黑狗兄也是只见过几段台下的排练，因此就也出了客室，踱步到前面看着了。

<br/>

<br/>

“伏婴，先别睡！”

“啊？还有什么事情？”

“趴床”静养也有一段时日，大约是臀部伤口实在是太疼，伏婴师也是认命，如今天刚擦黑，便叫那护理给上了半粒安眠药剂，和任沉浮聊着聊着，眼看药力发挥，就要入睡了。

“……我看萧少爷神情有些奇怪，说他知道了少帅身份倒也不像，然而却似乎又对战事分外关心。”

“恩……他知道，也不知道。”困意上来，伏婴师头已经埋上了枕头，“无缘无故告诉别人要上战场，我想，朱武表兄纵使鲁莽，也必编不出既能隐瞒身份，又能说服萧中剑的理由啊。”

“哦。”看着伏婴师连话都说不清楚，任沉浮也只能站起身，心里盘算着，这西医大夫开的药片，都是小小一粒，倒是看不出有啥效用，喝下去也无感，唯有这安眠药，看着颇有用，可以要几片来，留待自己头疼难眠的时候应急了。

<br/>

<br/>

【俺切着齿点绛唇，搵着泪施脂粉，故意儿花簇簇巧梳着云鬓。层层穿着这衫裙，怀儿里冷飕飕匕首寒光喷，俺佯娇假媚装痴蠢，巧语花言陷佞人。】

<br/>

“嘶……这……”听了半宿，大轴终于上场，黑狗兄站着听了一会儿，终是一皱眉头，自言自语道：“苍老板怎么上这出啊……”偷眼看去，却见场内素有几个毒舌机智之人，倒是已经开始掩口窃语了。然而他环顾全场，赫然看见那二楼天字第一号的包厢，那西洋纱布的帘子竟是落下了，倒是吓得一身冷汗。扭头便往楼上跑，却见而二楼转角处，几个穿皮鞋的已经站起来要拦着了。

<br/>

“孽老板……”

“啊？叔，有事说！”孽角已经穿戴停当就要上场，却见叔叔满脸是汗跑了进来。

“老板啊，这个……”黑狗兄用大拇指比了比，压低了声音：“一会儿上台，瞅机会提醒苍老板一下，大帅在包厢里看着呢……叫他悠着点，别唱过了火儿。”

“这是啥话，苍老板的戏，几时曾过了，放心吧！”孽角说着，一起身，腾腾腾便循着上场门去了。

“哎呀，傻话傻话啊！”

便是这一片惴惴中，听到散戏，总算松了口气，前台却又是一片哗然。等到后台同卸了妆，正在客室中闲聊等待的赭杉军蔺无双等一起追去看时，却见孽角一人站在台上看着入口，听见人声便扭头说：“大帅传令，已将苍老板接走了。”

<br/>

<br/>

“长官……”还穿着戏装，用身边人递来的手帕擦着脸，苍突然低声说道。

“嗯？”一直在饶有兴致的看着面前之人，这身青黑衣衫，在剧中便是赤身露体之意了，然而竟还是从头至尾穿得严实，之前不觉得什么，今日倒觉得京戏中如此含蓄却又明白的表现，和眼前之人很是贴合了。

“长官即来看戏，那前线……”

“呵，朱武还活着，如此而已。”弃天帝坐正了身子，心思也似乎从一个登徒子真正变回了Ｄ省督办。

“哦。”茶馆本就是各路传言荟萃之地，身在江湖，时局之事，想不听也是不行得了，然而，苍虽然忐忑对于真相如何，倒是一直不急了。

“……这一下应该也够他教训了。”少帅的爹吐了口气说。

侧头看了看，却似读出了些什么，苍想了想，提高了些声音问：“……今日这出戏，长官您看了如何？”

“哈？”混没想到会被这样一问，“这……一会儿下车我再告诉你。”这么说着，倒是已经遥遥看见对岸天波别业的灯光倒映在大明湖的冰面上了。

……

“嗯！”

卸妆沐浴出来，刚刚走出浴室大门，竟不曾想，已被拦腰抱住。

“……呵呵，那出戏倒是提醒我，以后你进我房间，我倒是需要先搜搜身，看看有没有贴身藏着利刃了……”

……

“嗯？”

翌日清晨，苍还睡着，公事在身的弃天帝也没叫醒他，自己起床略作收拾，补剑缺已经驾车在院外等着了。回到了麟趾巷公馆，在办公室内一坐，弃天帝环视屋内，莫名冷清。

“大帅有何吩咐？”一早便在工作的任沉浮听到上司似乎有什么疑问，赶紧站起来问道。

“……今天，几人缺席？”

“哦，今早萧府来电，萧秘书身体不适，先请假一周，视病情发展，再做汇报。”

“……，准。”

“……至于萧秘书未完城的工作，我昨晚问过医生，他说伏婴伤口愈合地不错，再过几日应该能够卧床办公……”

“别指望……”弃天帝低头开始拿起桌上的公文，表示不用需要再继续此话题下去。

“啊？”

“……你，一会儿再去看看他 。”

“是。”

其实没过多久，任沉浮一是因着大帅吩咐，二是因着实在好奇，便抽个空子直奔同层的伏婴师的病房了。

“咦？小孟，今天是你？朱厌呢？”开门的是之前那个小看护而不是才来上班的勤务兵。

“他……”

“别理他！”埋头在被子里的伏婴师哼了一声。

“恩？”侧头看看，只见床脚还堆着带血的纱布和棉花，，似乎开门前小看护正在收拾残局，“怎么换个药还流这么多血？医生不是说快好了？”

“……之前是快好了……但是今天早晨……”小看护带着满脸尴尬，说几句就瞄一眼床上的伏婴师。

“怎么？”

“伏婴先生这几天愈合地不错，医生也说可以下床走走，锻炼下腿脚……今天早晨，朱厌便扶着他在走廊里走了两步，谁知道，在楼梯口正好看见大帅上来，朱厌……他是军人，见到长官马上立正抬手敬礼，伏婴先生一下子没站住，就摔地上了……”

“嘶～”亲眼见过那伤口，任沉浮听着看护的描述，都情不自禁用手摸摸后胯，吸了一口冷气，觉得甚疼了。

“恩……幸亏大帅拉了一把，不然伏婴先生说不定就滚楼下了……”

“……那……医生怎么说？”

“医生说：那也……只能先就这样了……之前就算白养了……没落下什么严重后果也算万幸了。”

“……那朱厌……”

“大帅让他去厨房帮忙了，不用照顾伏婴先生了。”

“……好了，你先出去吧……我和伏婴先生有点事情要说。”把后面的话忍住，任沉浮正色吩咐那个小看护。

“……啊？”趴在床上的病人，听他说的严肃，也精神了几分。

“……你……就当是替吞佛看儿子吧……”等看护从外面将门关好，任沉浮才不那么压抑，坦率而言，“谁叫吞佛救少帅的时候挨了一枪呢，这现世报，总不能应在大帅身上啊。而且，你看朱厌这架势，吞佛要看着少帅，又要看着儿子，你让不让他打仗了。”

“……出去！”伏婴师嘟囔了一句，随后似乎又想起了什么，“……有别的事情没有？”

“哈。”任沉浮拉把椅子坐下，“天者的部队已经开进淮河北岸……我刚奉命给吞佛发了电报过去。”

“电文是？”

“务求一胜。”

<br/>

终于还是睡醒了，苍睁开眼睛——上次朦朦胧胧睁眼地时候，还看见躺在脸边的葱花和床帐外穿上军装的弃天帝，而真正清醒的时候，卧室里已经是空空如也了。

“……”

<br/>

“哈，不差。”

记得昨夜，弃天帝“搜身”完毕，将自己抱起来放在床上的时候，脸上露出了得意，“终于胖了一点点。”

“……长官。”

“你之前太瘦……。”继续笑嘻嘻地上下其手，信心满满在验证自己的判断。

一蹙眉头，虽然其实能够登台的日子只有不到一个月了，但是苍乍一听闻身材变化，却无暇或是刻意忘记这大大的烦恼，却又忧愁起了其他。

“哈，别怕，送你的戏服，让他们做宽松了。”轻轻一笑，把身上所剩无几的衣服也“宽松”了。

……

苍猛然惊醒自己是躺在床上发呆的时候，才发现自己其实已经在下意识的捏量着腰身和手臂等等要紧的部位，顿时有些难为情，赶紧坐起身，拿起衣物之前，已经习惯伸手去床头柜上拾取昨夜被弃天帝摘下的白玉戒指的挂坠，不料却是拿了个空……

……

那戒指，最后也是没有找到。苍回到封云社之后，其实也是在心不在焉地思忖此事，然而昨夜两人的行动历历在目，分明是有摘下来的。

然而饭前和照顾别墅的两名仆人找了约莫半个终点——今日乃是歇演，正要下午继续找的，却有戒神老者前来来，言说Ｄ省大帅临时决定前往水屯，请苍不必相候，自行安排几日。看看日历，想到和同门相处时间日少，苍顿时又归心似箭了。故此，也只能听了戒神的宽慰：“大概是被葱花碰掉，或是叼走玩了。”烦劳老人家好好留心，便急匆匆赶回封云社。

然而，那同心戒指何等重要，寓意又是……这才叫苍心里有了片藏着大风雪的乌云一般了，直到从轿车上下来，立在封云社院门口的时候，才强令自己先将此事放放。

<br/>

“唉，上午刚去了趟萧府，萧少爷病了。”

每日排练新戏，黑狗兄也是时时前来通报一些经营状况和明年计划，今日收的早，等着云染等女眷做饭的时候，又开始闲聊。

“啊？萧少爷怎地……”苍此时已经全然便是戏班成员的心思，听到这事，脸上也不由得动容了。

“不知啊，听说是昨日中午便不舒服，从帅府请了假回家，府上也没在意，以为休息一晚便好了，不料今天早晨便病倒了，我去的时候，医生刚走，倒是给人家添乱了。”黑狗兄抽着烟，他虽有些事故，但是却也觉得萧中剑不错，此时，倒是真有些担心了。

“……想是前些日子太过担心，这一下放松了。”苍低头，默默嘟囔一句，其实具体情形不知，但是总算也知道朱武平安的消息，也算松了口气。

“啊，对了……”其实，少帅安危，乃是大家都关心的话题，蔺无双也恰恰想到，要开口问了。只不过才问几句，就被外面号外的叫声：

“帅府厨房失火，帅府厨房失火”这种真的是大新闻的声音给打断了。

……

厨房失火，只是开端，这几日帅府倒是事故不断，见诸报端的便已不下三起，而帅府内部真正掰着手指数起来，脸色越来越难看的，倒是会计算天河了。

然而，重修厨房、更换灯泡，清理盆景等等，终归还是小小花销，眼见曾经的J城名嫒曌云裳下葬之期已至，这开销才算真的大了。

<br/>

<br/>

旧历腊月初三。

曌云裳的七七之日，是腊月初四，西历次年的1月17日。准备的工作倒是早已开始，而入葬前夕，亲友中最后抵达的，便是柳生剑影和楼无痕了。

<br/>

“柳生兄……今次，轮到你心不在焉了。”

<br/>

柳生剑影身份特殊，又同东省大帅交往甚密，故此，在Ｊ城竟是直接留居在了天波别业。

而到达之日的下午，书房之内，技痒的两人便迫不及待的对坐手谈了起来。

此时，其实也只是刚刚开局而已，弃天帝便已经一笑，将上次收到的评语，原封不动的送回了。

“……手续已经办过，等到葬礼结束，我送无痕往城外无常庵落发。”过了几个月，柳生剑影的话，汉语也不知是有所进步，还是这句话是刻意学过，总之，说地毫无歧义了。

“我是想请问弃天君，解决这件事，在你们中国的文化里还有没有其他的办法？”

“……没有。”弃天帝垂下了眼睑——祖父早年被清廷派遣留学德国，同德国的祖母相恋，后来归国，短暂分离之后，祖母竟携子来投，直至德国出兵占了胶东，两人才又分开，祖母独自回国，从此断了联系，祖父也郁郁一生，空有一身学识本事４５岁上便亡故了——日本人既然觊觎Ｄ省已久，肯定对自己的底细查得清楚，柳生剑影就算无心军务，也难免耳熟能详，这话倒真不是随便问问。

“好吧，我尊重她的选择。”柳生剑影点了点头，似乎是最后一点希望破灭，也似乎明知不会有结果，只是再尽人事罢了。

弃天帝没出声，静默了一会儿，轻轻将手中的棋子放在桌子的边缘，换了一副表情问：“要开始了么？”

“……等这些事情了结，我将回国了。”

“嗯？”弃天帝一愣，若是如此的安排，其实……大可没有必要……

“既然这些事，无可回避，又无能为力，而……”柳生剑影脸上没有什么变化，拳头握了一下，“却也……迈不过去，这结果，于她于我……这是最不让彼此为难，或者还抱着什么歉疚的选择。”

“懂。”弃天帝点点头——国家的立场便是如此，是不是亲自来执行，已不重要，“我会照顾她……只要我还在Ｄ省……”话出口，却是涩笑——虽不为难，然而……却也尴尬了,“不过，这些年，还是要感谢柳生君你。”

“我国还未同广州方面达成利益交换，不过也快了……”柳生剑影神色不变，仿佛在谈论棋局一样淡淡地说。

弃天帝沉默了一下，却是翘起嘴角，问说：“……柳生君，你告诉我这些，可是有叛国嫌疑啊，不担心有损本国利益么？”

柳生剑影的手还是放在棋篓内轻捻棋子，“短期来看，符合我国利益的事情很多。”

“比如？”

“北据朝鲜，东吞支那，南占马六甲……”

“短期……”

“中国有句话，叫多行不义必自毙，世界各国也不会坐视不理的。”

“哈，透彻了。”

“可惜，掌权者多看不透……我实在看不出，这样的目标，对我国有什么好处。”柳生剑影摇了摇头，几乎是嗫嚅地说了一句“有些事，不知是我错了，还是国家错了……”

“……至少，也算是有个国家吧。”弃天帝说着，向着柳生剑影示意一翻，已经拿起了烟斗。

“小心我的继任，京极鬼彦，他……”柳生剑影摇了摇头还要再说，却被弃天帝打断：“谢，剩下的，我自己去查。”

柳生剑影点了点头，换了话题：“……我听说，天者前几日，在淮北吃了败仗？”

“嗯，战略罢了，不算败仗。”

“……嗯。”柳生剑影点了点头，“我陪你等。”说着，低头捻起了棋子。

“哈。”弃天帝心领神会，也终于开始专注于棋盘了。

……两小时过去，任沉浮终于捏着电报走入。

<br/>

弃天帝看罢，抬头，正看见柳生剑影又一次投子认负。

“你赢了。”

“不是我，是朱武赢了。”

<br/>

旧历腊月初四，虽然是曌云裳下葬的日子，然而，即使是在葬礼现场，全员也是沉浸在一片少帅在鲁南打破广州军的欢愉气氛之中了。

而与之相比之下，红楼金店二小姐同柳生剑影离婚，前往无常庵出家的新闻就更是微不足道，徒增一些意在春花秋月的文人心中，对那四姐妹家族的离合唏嘘罢了。

<br/>

“少帅大捷！少帅大捷！”窗外的巷子里响彻云霄的号外，纵有飞雪，却总是时不时传进萧中剑的卧室之内，而渐渐康复的萧中剑，却仿佛浑然未觉一般，坐在桌前，将一份写好的文字小心翼翼的折叠起来，放入上衣口袋，穿戴整齐走出了房间。

“父亲，今日文学社会办了茶会，我可能晚些回来。”依旧满面春风，同萧振岳打过招呼，便迈步出门了。

“嗯，早去早回。”萧振岳正和几家工厂的厂主对账，只是答应一声，便继续安排大帅订购，近日便要送去前线的一批物资了。只过得片刻，管家来报：冷氏父子来访。

萧振岳一愣，也就出迎，心中一闪而过的念头：文学社茶会，醉儿竟没用同去么？不过，他一向不甚干涉小辈的活动，也不再多想了。

<br/>

“我已向帅府递了辞呈，来此就是为了向大家坦言，我求统一之心一直未变。”

萧中剑知道在这些面露惊愕的同学眼中算是不速之客，然而心中决心已定，直接大声将自己的想法说了出来。

“这……”学生联合会会长，犹豫了一下，眼角却不由自主微微撇了撇这包厢一角的屏风，随后抬眼，却看见他派出去望风的几人之一上来，立在门口处向他摆了摆手，示意确然冲上来的只有萧中剑一人而已。

“……这是会长您前几日吩咐叫我结算的会费，以及将要分配的名单。”萧中剑从口袋里慢慢掏出了一张纸。

“名单……”会长心中暗叫不好——萧中剑是兼职的会计，众人集资，日常花销，大家来往的支出必定是有账目记录，此时所见也不过是薄薄的一张纸，那之前的账本，却还留在对方手里。

“哈……萧同学，我们自然知道，你为国为民的决心。”会长有点涩涩的开口，“之前只是因为你在帅府……”

“萧少爷，我倒是觉得，你既然有这个决心，那向帅府递辞呈的事情，就做的有点草率了……”屏风后的人终于转了出来，萧中剑愣愣，不过环顾大家的表情，也大概猜得出，这人怕才是最近真的在运作学生会的高手了。

<br/>

“嗯？辞呈？”

下午临近下班时，萧中剑再次出现在了帅府。大帅仍是不在，只有任沉浮依旧坚守在门口的办公桌后。

“是，任秘书，我想了又想，觉得我那辞呈，递得冲动了。未知……”

“哈。”任沉浮一笑，“正好，我还没有将这辞呈交给大帅，萧少爷若是收回，拿去便是。”说着，从桌角砚台下面将那装着辞呈的大信封抽了出来。

……

“他果然拿回去了。”

送走了萧中剑，任沉浮立刻起身，走到同层走廊的另一边，伏婴师的房间，推门走入，反身关门，还没等到转身，就直接说，只是等转过身来，才更惊讶地说：“你，能下床了？”

伏婴师此时正在那看护帮助之下，扶着床头缓缓走动，看见任沉浮进来，也是苦笑一声，说：“若再不能，便要废在床上了。”他这几日倒是真真正正得了安心静养，伤口也水到渠成地好了很多。

“小孟，你把伏婴先生扶回去，便去外面歇歇吧。”任沉浮也不再客气，拉了把椅子坐在一旁不碍事的地方。

“萧中剑又把辞呈拿回去了？”又在床上趴好，伏婴师理了理心思，“快，太快了。”

“嗯？”

“难道学生联合会又有什么安排了？”

“这个……他们今日似乎有个茶话，我的人本意要混入，不想他们倒是老练的多了，防备甚严，不过当时萧中剑也有参加，应该无非是谈论上京之事了。”

“……任秘书，你学这活儿，学了几年？”伏婴师沉吟了一下，突然问道。

“啊？”任沉浮一愣，想了半天才明白伏婴师口中的“活儿”是指什么，略有些愧赧的一笑，说：“学了，两三年吧。”

“手下人呢？”

“哈，少则数月，多则，连我也不知了。”

“嗯……所以，这套活儿，那些斯文学生，必然是不会无师自通，短短几日就老练得很了？”

任沉浮不傻，这一提点，当即心中一惊，说：“你是说，他们之内已经有人指点？”

“恐怕不是指点，而是指挥了。”伏婴师将头放在枕头上，一脸疲惫。

“指挥他们……这……有何意义？”脑海中如同打字机一样刷刷闪过一排一排动机与能力都足够的人物，只是再一转念却也无从确认。

“……嗯，总有意义。我现在也没想透，只是提醒你，从今以后莫要再把那些学生们的计划往单纯幼稚的方面去想了。”

“……目的计划如何，手段也不过是请愿罢课而已。”任沉浮摇摇头，不是轻蔑，而是费解了。

“请愿罢课，也不是小手段啊，比如那年……”

“似那样闹得那般大的，怕是要天时地利了。”

“这种事，没有人和，闹不起来的。”伏婴师摇摇头，“如要一团散沙，怎么也是聚不成塔，表面上只是自发，而事前准备，一路下来各方补给调度，事后收尾，直至获利，若无人策划，便应无人得利，然而结果并非如此，事后政局变化，可见一斑啊。”

“……懂了。”任沉浮深吸一口气，“做最不利想，萧中剑再入帅府，便不是单纯的关心战局了吧。”

“嗯，来日方长，他如此急急回来，定是有件什么事情，急于知晓。”

“……难道……”任沉浮沉思片刻，突然便是一抬眼神，然而随即又黯了下去，“……不对啊，少帅回Ｊ城的日子地点，只怕过不了几天便要见报，这消息实在是没什么急急特意回来打探的价值吧。”

“一旦见报，便证明咱们也开始防范，便是早这一两天，也是个先机啊。”伏婴师倒是不以为然，沉着嗓音说，“是了，表兄凯旋的日子，究竟定了没有？”

“……尚无，今日大帅仍在会晤柳生剑影，我尚未见到。”

“可以适当提醒，越早越好。”

“嗯，我明白。”心中有些紧张，任沉浮话说到此，便要起身了。

“哦，对了……今日……封云社的戏单是……真的【千里走单骑】？”

“……哈，不是。”任沉浮撇了撇床头桌边的一堆印刷品，笑了笑，“我去问问大帅，给你请个堂会啊？”

“……好啊。”伏婴师说着，将盖在腰间的被子往头上一蒙，算作送客了。

……

因为知道向大帅请示了，也不会得到批准，任沉浮晚上倒是没有提慰问表少爷伤体的堂会的事情，而是直接请示少帅旗开得胜之后的安排。

而得到的回答是：

“致电朱武：七日后回家”。







