---
author: 西陵歌者
title: 戏
lang: zh-Hans
date: 2010-06-22 00:00:0
layout: post
depth: 1
category: opera
subdocs:
  - title: 引子
    link: /opera/0/
  - title: 第一场 大明湖
    link: /opera/1/
  - title: 第二场 双仪舞台
    link: /opera/2/
  - title: 第三场 弃家公馆
    link: /opera/3/
  - title: 第四场 弃家公馆Ｂ
    link: /opera/4/
  - title: 第五场 弃家公馆Ｃ
    link: /opera/5/
  - title: 第六场 弃家公馆Ｄ
    link: /opera/6/
  - title: 第七场 东盛客栈   
    link: /opera/7/
  - title: 第八场 旧德国领事馆—镇守使公署
    link: /opera/8/
  - title: 第九场 路边摊
    link: /opera/9/
  - title: 第十场 弃家公馆Ｅ＆东盛客栈Ｂ
    link: /opera/10/
  - title: 第十一场 弃家公馆Ｆ
    link: /opera/11/
  - title: 第十二场 双仪舞台Ｂ
    link: /opera/12/
  - title: 第十三场 千佛山
    link: /opera/13/
  - title: 第十四场 莲花山
    link: /opera/14/
  - title: 第十五场 弃家花园
    link: /opera/15/
  - title: 第十六场 弃家花园Ｂ
    link: /opera/16/
  - title: 第十七场 弃家公馆Ｇ
    link: /opera/17/
  - title: 第十八场 弃家公馆Ｈ
    link: /opera/18/
  - title: 第十九场 封云社
    link: /opera/19/
  - title: 第二十场 双仪舞台Ｃ
    link: /opera/20/
  - title: 第二十一场 Ｊ城 
    link: /opera/21/
  - title: 第二十二场 双仪舞台Ｄ
    link: /opera/22/
  - title: 第二十三场 双仪舞台Ｅ
    link: /opera/23/
  - title: 第二十四场 天波别业
    link: /opera/24/
  - title: 第二十五场 天波别业Ｂ
    link: /opera/25/
  - title: 第二十六场 封云社Ｂ  
    link: /opera/26/
  - title: 第二十七场  封云社Ｃ
    link: /opera/27/
  - title: 第二十八场 路边摊Ｂ
    link: /opera/28/
  - title: 第二十九场 南岗子
    link: /opera/29/
  - title: 第三十场 泰丰楼
    link: /opera/30/
  - title: 第三十一场 红楼金店 
    link: /opera/31/
  - title: 第三十二场 封云社Ｄ
    link: /opera/32/
  - title: 第三十三场 商乐舞台Ａ
    link: /opera/33/
  - title: 第三十四场 弃家公馆Ｉ
    link: /opera/34/
  - title: 第三十五场 封云社Ｅ
    link: /opera/35/
  - title: 第三十六场 皇华馆
    link: /opera/36/
  - title: 第三十七场 封云社Ｆ
    link: /opera/37/
  - title: 第三十八场 弃家公馆Ｊ
    link: /opera/38/
  - title: 第三十九场 街边公寓
    link: /opera/39/
  - title: 第四十场 弃家公馆Ｋ
    link: /opera/40/
  - title: 第四十一场 封云社Ｇ 
    link: /opera/41/
  - title: 第四十二场 弃家公馆Ｌ
    link: /opera/42/
  - title: 第四十三场 泰丰楼Ｂ
    link: /opera/43/
  - title: 第四十四场 天波别业Ｃ
    link: /opera/44/
  - title: 第四十五场 封云社Ｈ
    link: /opera/45/
  - title: 第四十六场 弃家公馆Ｍ
    link: /opera/46/
  - title: 第四十七场 弃家公馆Ｎ
    link: /opera/47/
  - title: 第四十八场 封云社Ｉ
    link: /opera/48/
  - title: 第四十九场 封云社Ｊ
    link: /opera/49/
  - title: 第五十场 天波别业Ｄ
    link: /opera/50/
  - title: 第五十一场 封云社Ｋ 
    link: /opera/51/
  - title: 第五十二场 弃家公馆Ｏ
    link: /opera/52/
  - title: 第五十三场 Ｊ城车站
    link: /opera/53/
  - title: 第五十四场 弃家公馆Ｐ
    link: /opera/54/
  - title: 第五十五场 封云社Ｌ
    link: /opera/55/
  - title: 第五十六场 弃家公馆  
    link: /opera/56/
  - title: 第五十七场 商乐舞台Ｂ
    link: /opera/57/
  - title: 第五十八场 弃家公馆Ｒ
    link: /opera/58/
  - title: 第五十九场 弃家公馆Ｓ
    link: /opera/59/
  - title: 第六十场 天波别业Ｅ（正文完）
    link: /opera/60/
  - title: 后记
    link: /opera/61/
  - title: 番外
    link: /opera/62/
  - title: 弃苍《戏》长评 By 神之殇·寂灭
    link: /opera/63/
---

### 作者：西陵歌者

---
