---
author: requiem
title: 色相红尘
date: 2023-05-07 00:00:01
layout: post
depth: 1
lang: zh-Hans
category: red_dust
subdocs:
  - link: /red_dust/1/
    title: 沉香亭北
  - link: /red_dust/2/
    title: 孤舟一系故园心
---

#### 作者：requiem

---

每一期应该都是不同设定，性格会有所不同。只为搞黄，不要深究。
