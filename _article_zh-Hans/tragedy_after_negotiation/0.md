---
title: 谈判桌下的悲剧
author: 雾恋
layout: post
date: 2012-12-30 00:00:00
category: tragedy_after_negotiation
lang: zh-Hans
depth: 1
subdocs:
  - title: 第1章
    link: /tragedy_after_negotiation/1/
  - title: 第2章
    link: /tragedy_after_negotiation/2/
  - title: 第3章
    link: /tragedy_after_negotiation/3/
  - title: 第4章
    link: /tragedy_after_negotiation/4/
  - title: 第5章
    link: /tragedy_after_negotiation/5/
  - title: 第6章
    link: /tragedy_after_negotiation/6/
  - title: 第7章
    link: /tragedy_after_negotiation/7/
---

#### 作者：雾恋

---
