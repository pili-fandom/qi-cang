---
title: 談判桌下的悲劇
layout: post
lang: zh-Hans
category: tragedy_after_negotiation
depth: 2
date: 2012-12-30 00:00:02
---

### 2. 

<br/>

“蠢货！废物！”

听完断风尘的汇报，弃天帝大发雷霆。他大声叱喝自己不中用的下属，“开多少价码都可以…你想了一晚上，就拿出这种没技巧没档次没品位的谈判方式？实在太丢我的脸了！”

断风尘一直不敢抬头，他后悔太早就向弃天帝保证万无一失，结果现在落到这么凄惨的境地。

“弃总……”

“你还想解释什么？”

“玄宗的道士很顽固很难说服，我看也不必非要玄宗那块地皮，封云山这么大……”

“……给我滚出去！”

<br/>

断风尘刚跨出总裁办公室的大门，一阵熟悉的、令人讨厌的轻慢笑声就飘了过来。他的心中顿时悲凉了起来，真是屋漏偏逢落雨，落魄的时候他最不想见到的人，把他的狼狈尽收眼底。

伏婴师，断风尘大学时代的同学兼死对头，大学一毕业就进了异度集团。当断风尘念完研究生，也进了同一家公司从基层干起的时候，伏婴师早就是总裁身边的高级秘书了。

<br/>

“你是来看我笑话的？”断风尘没好气地说。

“我可没这么悠闲……”伏婴师拍拍手中的文件夹，“其实也是托你的福啊！增加了额外的工作量。”

“……”

“啊啊、对了，说起来你也真了不起。我很少见到弃总这么生气。”

“……”

“那么请再接再厉吧，在某些方面，你还真挺有才能的。”

<br/>

此时，弃天帝正背着双手站在窗前俯视窗外风景。异度集团的本部大楼是本市最高的摩天大厦，总裁办公室又是位于大楼最高层。

弃天帝非常喜欢这种一览众山小的感觉。

所以当他想要建造一所私人庄园的时候，唯一想到的理想地点就是在本市辖区内海拔至高、风景秀丽又清净的封云山。

只是没想到还没开工就遇到了这么大的麻烦。他越想越觉得烦躁。

<br/>

“弃总。”是伏婴师的声音。

“把文件放我桌上就行，去忙你自己的工作吧。”弃天帝没转身，他现在懒得和任何人说话。

“封云山的事情，还是交给我来办吧。”伏婴师小心翼翼地说。

弃天帝挥挥手，口气生硬地说：“我亲自去处理。”

“……伏婴师不是那种办事不靠谱的人，弃总大可以放心。”

“同样的话，不要让我说两次。”

于是伏婴师顿时会意，弃天帝是想亲自去向那个令他心情不爽的源头施加压力，顺便挽回断风尘留下的“没技巧没档次没品位”的形象…

他擅长揣摩老板的心思，这是他比断风尘高明的关键之一。

“明白了，预祝弃总一切顺利。”

伏婴师向弃天帝的背影鞠了一躬，退出了办公室。

他很期待事态的发展，期待弃天帝一发狠，就不择手段把那群不知好歹的玄宗道士都从自己的地盘上撵走。再说明白点，幸福地看着他人的不幸，这是伏婴师一点小小的乐趣。

<br/>

<br/>

当弃天帝那玄黑色的特别定制款宾利轿车出现在玄宗道观的山门口的时候，引起了一阵小小的骚动。

玄宗的道士们也都有觉悟，异度集团不会这么轻易就死心。但他们也没想到，居然才过了一天就又找上门来，而且看这架势，……来了个麻烦人物。

<br/>

弃天帝很满意他的登场方式，聚焦在玄宗众人饱含着不安的目光之下，这令他心情大好。那么接下来，是找到那个三言两语就把断风尘打发走的家伙，狠狠地还击回去，让他知道自己的厉害。目光扫过不远处三三两两聚集在一起的道士，却没有他要找的目标。

<br/>

正当弃天帝疑惑的时候，他感觉身后有人出现。

转身一看，正是他要找的人，苍。像断风尘说的那样，穿着青灰色的道袍，淡茶色的长发用一支紫檀木簪挽起，眉心一点鲜艳的朱砂，“看起来很温和但其实很棘手”，其实也就是这个描述，让弃天帝颇有几分兴趣。

<br/>

“弃天帝。”

“你知道我是谁？哈、很少有人直接这么叫我，真是新鲜。”

苍却避开了这一问题，“玄宗方面的态度很明确，其实你没必要来白跑一趟。”

“哈！你以为这是在平等地商量谈判么？”弃天帝要比苍高出半头，这么近的距离之下，给人十分的压迫感。

话音刚落，旁边围观的五弦一片哗然，但也不敢贸然说什么，生怕一下子惹恼这个“传说中的”弃天帝，演变成不可收拾的事态。

见此情此景，苍无奈地叹了口气。

“这里不是说话的地方，你跟我来吧。”

<br/>

苍与弃天帝一前一后地走远，五弦们才义愤填膺起来。

“那种嚣张的态度是怎么回事啊？真想在那张笑得很恶心的脸上踩两脚！”白雪飘朝着弃天帝离开的方向吼道，“不就是卖便当起家的么！”

另外几个人都捏了把汗，还好弦首他们已经走远，否则说不定真就捅了马蜂窝。

“异度集团不单单只是财力雄厚。”翠山行一脸严峻。

“相信弦首吧，他一定能应付那个弃天帝。”九方墀为大家宽心。

对于苍，大家当然都是百分百地信赖，但与这种叱咤商界的传奇人物打交道，谁心里都是忐忑不安，这关系着玄宗的人们，是否必须离开待了几百年的封云山。

打破压抑的沉默的又是白雪飘。

“真是叫人生气！我可不想和那种讨人厌的家伙待在同一个空间。……黄商子车钥匙借我一下。”

“呃，怎么了，要下山么。”黄商子一边把钥匙递给他一边问。

“昨天我发现米缸快见底了，下山去买点，顺便散散心。”

“还是让黄商子给你陪驾吧。”赤云染很不放心。

白雪飘只是摆摆手，“哈！总要自己学会开的！别担心了。”

要不是黄商子的那辆二手小面包车，玄宗道士们要采购食材和生活用品就会变得异常艰难。虽说是道士，但生活在现代，也还是没办法太过于超凡脱俗的……

至于白雪飘，十八岁的他刚刚得到驾驶执照，正是对开车兴致最浓的时候。
