---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:09:50
lang: zh-Hans
depth: 2
---

### 第50章

<br/>

苍幽幽醒来时，发现自己置身在一间阴暗的石室里。他正躺在一张石床上，身下垫著厚厚的草席，散发著草木刚烘烤过的特有香气。身上的伤势已经恢复了，沈重的倦意仍未散去。

回想起闭上双眼之前所见最后一幕，温暖的胸膛再无心跳的韵律⋯⋯苍猛地坐起上身，寻找著那个熟悉的白衣身影。

这是哪里？

<br/>

「醒了？」武神正站在石床的另一侧，双臂环抱，居高临下地打量著他，金蓝异瞳慵懒而嘲讽。

望著优美黑发如瀑布般流泻在肩头，苍黯然神伤地垂下眼睫。他很清楚，它们再也无法恢复往日的色彩了。

<br/>

「你又回来做什么？」武神傲慢地问。

「⋯⋯你还在这里，不是吗。」

「哼，这棵树，吾中意。懒得换地方。」

「⋯⋯」苍淡淡地笑了一下，「所以，吾回来找你。」

<br/>

「哼。」武神抬了抬下巴，傲慢地冷哼一声，「吾说过了。苍，吾不想再见到你。」

<br/>

苍歉意地望著他，「吾这次可以一直陪你。你希望多久，吾就留下来陪你多久。」

武神皱著眉「哼，吾不稀罕。吾早就不稀罕了！」

「⋯⋯」

「怎么受这么重的伤？是那群人类把你伤成这样？」

「不是。」苍摇摇头。「上次吾离开后，发生了什么？」

武神冷冷地问：「那是谁伤你？！」

「⋯⋯吾叮嘱过你，不要靠近昆仑山。」

「问你话呢！」

「苍也在问你话。」

「吾先问的！到底是谁伤你？！」

「⋯⋯」

「哦～～又不能说，是吧。」武神嘲讽道，冷冰冰地扫了道者一眼，「又是那套，『总有一天，吾会告诉你』。吾，听腻了，也无所谓。你以为，你是死是活，对吾有影响吗？休息够了没有？赶紧滚。」

「吾会告诉你⋯⋯」苍对著冷冰冰的金蓝异瞳，释出一个悲伤的笑意，「你有什么想做的事吗，吾陪你。在那之后，吾会把一切告诉你，绝不食言。若你当真⋯⋯再也不想见到吾，那苍不会再出现在你面前。」

「哼！」武神气鼓鼓地重重一哼。

<br/>

这个『哼』又是什么意思？⋯⋯

苍思考了一会，转而小心翼翼地问道：「倒是你⋯⋯吾离开后，发生了什么？你不是答应过，不会靠近昆仑山吗。」

「你怎么知道吾去了昆仑山？」武神皱了皱眉。

一百年间，他苦思无果，那块玄苍珀为什么不在苍手里，苍和那些人类又是怎样的关系。

那是一段他不愿提及的往事，稍有回忆便会撕扯开胸口的陈伤，让他再次体会那深可见骨的剜心之痛。

多可笑啊。高傲如他，竟然会被他鄙夷的人类造成弱点，由著自己的心被践踏，在泥淖中浮沈。多可笑啊。

「因为，吾来自未来⋯⋯」苍道，「吾知道⋯⋯你去了昆仑山，受伤了。」

「⋯⋯哼，污秽世间的人类。」

「告诉吾，发生了什么？」苍直起身子。武神似乎陷入了某种痛苦的回忆，金蓝异瞳竟然一时有些失神。苍抬起手，指尖微微颤抖著靠近武神的心口。

三寸，两寸，一寸⋯⋯武神心脏的位置近在咫尺了。

一尘不染的白衣之下，苍知道，他已经没有心了。

<br/>

武神怔怔地望著向他伸来的手。就在那只手即将触碰到他心口的一刹那，他猛地回过神来，一把捉住了道者的手腕。

手腕上的力度之大瞬间留下了狰狞的红痕，苍微微一愣。

武神将道者的手生硬地扳开，「别乱摸。」

苍抽回了手，黯然道，「抱歉。」

察觉到自己反应有些过激，武神有点磕巴地补充道，「⋯⋯别，别碰这里。吾不喜欢。」

「⋯⋯他们怎么可能伤到你？他们怎么可能伤到你？」

「哼。罢了，都过去了。你没事就好⋯⋯」武神漫不经心地说。话甫一出口，他忽然发现自己不小心露了馅，欲盖弥彰地找补，「哈哈哈！反正，吾，无所谓。」

⋯⋯听起来更刻意了。

武神假装优雅地转过头，煞有介事地再次强调，「你的死活，吾也无所谓。此事揭过，不需再提。」

⋯⋯还不如不说。

黑著脸，他背过身去，避开了道者的目光。

<br/>

「⋯⋯」苍隐隐有了一种猜测。见武神没有继续说下去的意思，苍最终还是把话吞了回去，转移了话题。「吾这次离开了多久？」

武神回过头，讥讽地一笑，「不到一百年，有进步了！」

「⋯⋯对不起。」苍低声道，指尖无力地扣紧身下的草席。

一百年，那说明武神在凡尘的生命即将走到尽头。相比于武神五百年承受的痛楚，自己所能弥补的，实在太过渺小。

「这次，吾不走了⋯⋯只要你愿意，吾会一直留下来陪你。」

「吾不稀罕！」

「⋯⋯」

⋯⋯气氛陷入了难堪的沉默。

<br/>

⋯⋯

<br/>

「罢了。」

漫长的沉默后，武神叹了口气，再一次背过身去，像是刻意避开道者的目光。「苍，你吾第一次下山，你曾说，你会满足吾一个愿望。这句话，还作数吗。」

「作数的。」苍连忙道，「吾说过，长期有效。」

「那你听好了，吾的心愿⋯⋯」

「嗯⋯⋯」

<br/>

「别再让自己受这么重的伤了。」背对著道者，武神平静地说。

<br/>

长睫下的深蓝眼眸微微一颤，苍抬起头。

<br/>

「⋯⋯能做到吗？」

「吾⋯⋯」苍艰难地开口。

「吾没有再生之力了。下次再受这么重的伤，吾也救不了你了。苍，别再让自己受伤了。」

「吾⋯⋯尽力。」道者哀伤地闭上眼睛。

<br/>

「你走的这一百年，吾陆续想起了很多事。」

武神依旧背对著他，双手缓缓抬起复上额头，伴随著窸窸窣窣金属碰撞的清脆响声。

苍安静地望著落寞如雪的背影，「嗯？」

「比如⋯⋯吾终于想起，想起吾头上这个东西到底是什么。」

沉静的眼眸再次微微一颤。

「想起它来自吾原来的世界，和吾一同来到人间⋯⋯」

「吾明白了⋯⋯」苍喃喃道。

<br/>

原来如此，原来如此。至此，逻辑链条的最后一环最终合上。

原来⋯⋯这就是他的天命吗？

<br/>

武神似乎没有注意到道者的低声自语。他转过身来，那条额链已经被他取下，放在手心。

苍这才发现，他的眉间有一条很好看的金色额纹。那是与弃天帝一模一样的眉眼，他见过无数次，然而取下额链的样貌还是第一次见。

天神俊美的容颜，没有了华饰的映衬，依旧神圣而庄严。

<br/>

武神伸开手掌，金色流苏在空中缓缓上升，华光四溢，流彩翩翩。他的指尖向著苍轻轻一勾，两道光晕在空中交汇，没入鲜艳的红宝石内芯。

那是他们灵识的碎片，一道来自他的眉间，另一道来自苍的眉间。

光芒散去，金色流苏再次落入他的手中。武神走上前来，将它系在了道者微微颤抖的手腕上。

「带著它。别再让自己受伤了⋯⋯」

道者抬起低垂的眼睫，金蓝异瞳沉静地望著他，与他四目相对。空气安静得似乎凝固了。只闻武神缓缓道：

<br/>

「未来，只要你想起吾，想起你吾在一起的时光⋯⋯无论你吾相距多远，它都能带著你穿越所有时空的距离，把你带回吾的身边。」

<br/>

> ##### 小剧场
>
> 武猫：葱花宝贝说要陪著吾，好高兴！不行不行，本猫要保持形象。哼，不能那么轻易原谅他
>
> 某蓝：哟，当初是谁说要苍再也别出现的？
>
> 武猫：（黯然）就算不爱吾⋯⋯也不要和苍做陌路人。
>
> 某蓝：啊，气氛突然沈重起来了。还是赶紧下一章吧
{: .block-tip}
