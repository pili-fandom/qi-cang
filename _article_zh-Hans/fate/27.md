---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:03:27
lang: zh-Hans
depth: 2
---

### 第27章

> 继续魔改神话...把苍和武神融合进了颛顼作《承云曲》这段神话里，请不要深究...（捂脸逃走）

<br/>

不周山一行之后，赭杉军忽然察觉，苍变了。

苍大多喜怒不形于色，即便是那段暗无天日、神州倾颓的日子，清淡的脸上只有郁结在眉间的一点似有似无的忧愁。而如今，这份忧愁似乎更浓烈了。只是赭杉军不知，这是否只是他的错觉。

与之相比，那位「白衣武神」也变了——也不知到底遇上了什么开心事，这位大神心情像是比以前愉快了不少，像个小孩似的黏在苍身前身后。甚至，某些行为，在后世看来甚至算得上「撒娇」⋯⋯

撒娇⋯⋯

这个想法让赭杉军不禁打了个寒颤，连忙制止了自己漫无边际的胡思乱想。

<br/>

<br/>

因危险解除，苍便也不再强求赭杉军的魂体留在瓶中了。被圈禁在逼仄的空间中并不好受，苍对此深有体会。然而赭杉军在瓶外飘了一会儿，发现武神总是有意无意地耀武扬威，在自己面前动辄对苍摸摸抱抱。在武神充满敌意的目光下，在三双眼睛两两相觑的诡异气氛中，尽管苍看起来对武神的动手动脚并不情愿，赭杉军还是感觉到自己像个明亮的电灯泡。

就这么过去了两三天，为了让苍的日子能好过些，赭杉军还是很好脾气地自愿回到瓶子中去了。

对此，苍深感歉意。道者苦言相劝，试图让这位大神的脾气收敛一些，然而后者却只是冷哼一声，全然当做了耳边风。至于赭杉军，说什么也不肯再从瓶子里出来了。

鉴于这次穿越时间已所剩无几，苍也就作罢，顺其自然了。

他们此刻正在前往北方三十六州的都城——帝丘——的路上。不周山一战后，颛顼为表谢意，在数日后的重阳于帝丘设秋日宴，款待苍与武神。苍本欲婉拒，但考虑到帝丘恰在返程途中，便答应了下来。

道者更重要的考量是，共工口中的「圣人」是谁。他有种敏锐的直觉——暗中一定有一双翻云覆雨的手，运筹帷幄间挑起多方战争，而神话中有人间五帝之称的颛顼，主管北方的「天帝」，也不过是任凭摆弄的棋子罢了。

不周山一战也许只是起点；针对武神的更加庞大复杂的阴谋，也许正在缓缓开启。如今敌暗我明，与其被动地见招拆招，倒不如主动出击，苍想。

草蛇灰线，羚羊挂角，道者始终坚信，一切阴谋都是有迹可循的。

若判断无误，那么「圣人」与颛顼一定有所渊源。前往颛顼所在的北方都城——帝丘——也许能寻到些蛛丝马迹。

<br/>

***

<br/>

帝丘为北方重镇，与南方的都城「朝歌」相比，少了几分小桥流水的和美，却多了几分大漠苍林的粗旷。宴席设在一场秋日围猎之中，半圆形的城墙上十丈高台，旗帜纷飞，王公贵族旒缨冠冕，依次盘膝而坐，俯瞰著城墙外连绵起伏的广袤山野。从高处放眼望去，天也茫茫，地也莽莽，广袤红尘尽收眼底，辽阔之景难免让人心生渺小之感。

古往今来，放眼山川，当有限的生命置身于永恒的天地，有人看到了自身的渺小，有人却想要将天下尽收囊中；有人居于一隅，寻仙问道，也有人贪恋权力，妄想著永垂不朽。历史的脉络，就在这样的反复变动中曲折向前。任人书写、华丽光鲜的神话背后，又埋藏了怎样的真正的故事。苍漫漫地想著，垂下长睫，缜密地观察著依次席地而坐的王公贵族们。若所谓「圣人」与颛顼当真有渊源，那么他会不会就在这一行人中呢？

他会是谁呢？又为什么要针对武神？

更重要的问题——他是人类吗？

这样的想法让苍不禁吸了口冷气。一个弃天帝已经是极难对付；而种种迹象表明，这位「圣人」，始终隐于幕后却挑动风起云涌，只会是更强大的对手。

<br/>

颛顼此刻坐在十丈高台正中的席位。苍与武神坐在颛顼右侧，与其隔开一丈左右的空地。再往右列坐的是北方三十六州的部分首领，而颛顼左侧是帝丘的王公贵戚，包括不久前在大荒山所见的颛顼之子祝融。不动声色地扫过列坐的众人，一双冷眼探查过所有人的命星后，道者却不免有些失望——在座之人都能在阅读的神话典籍中找到对应，然而不过是身分显赫的普通人类罢了，并非能一手挑起风云万变的人物。

所谓「圣人」，始终隐于幕后，像是刻意地在神话传说中擦去了其存在的痕迹。他到底会是谁呢？

苍无声地叹了口气，按下心中的隐忧。这次来到帝丘，他小心地避免武神与颛顼再起冲突。幸运的是，自从不周山一战之后，武神心情始终不错，此刻正轻快地哼著小曲儿，不仅对席位的排布全然不以为意，态度也变得宽宏大度了许多，连众人向道者望来的目光都不计较了。

<br/>

正值重阳，秋日的风带来些许凉意，围猎的号角一声长鸣，林间的群鸟被纷纷惊起。北方三十六州训练有素的精锐，在王公贵族的兴味注目下，策马冲入山野。颛顼站起身，挽起一张大弓，一支朱红利箭缓缓抬起，冠冕上的垂珠因手臂的蓄力而不安地晃动起来。沿著利箭瞄准的方向，苍放眼望去，不觉眉头一紧。

——那只箭瞄准的并非猎物，而是一名围猎的士兵。

「这是何意？」苍淡淡道，一贯云淡风轻的调子此时却有些冷。

「围猎的乐趣。」颛顼笑道，弦骤然绷紧，「并不在于围猎本身。武神大人，你说是吗？」

「喔～」武神心不在焉地应了一声，注意却完全被席间的美味珍馐吸引，戳了戳道者的腰际。「苍，这些好吃吗？吾能开始吃了吗？」

「哦？吾等愚昧，可否请颛顼帝指教？」忽略了武神的小动作，苍面无表情地反问。

「⋯⋯而在于站在高处，俯瞰天下。」颛顼有意无意地拉长了声线，「武神大人想必深有体会，这众望所归，天下共主的滋味。」

「天下共主，说的是吾吗？吾怎么不知道。」武神百无聊赖地扯下一只鸡腿，正要咬上一口，道者突然不动声色地按住了他的手腕。

「逐恶气，立神柱，补天倾——这哪一项不是千秋万代的功绩？天神降世，人心所仰，武神大人自然担得起『天下共主』这四字。」颛顼笑道，手中的弓却越崩越近了，箭在弦上，一触即发。

「这些吾会在意吗？」在道者手心的施压下，武神悻悻地放下手中的鸡腿，有些闷闷地说。和苍周游列国的日子，人间百味虽然品尝了不少，然而吃的品的都是些蔬食茶饮，太清淡了。他虽然没有进食的需求，当野味的香气扑鼻而来，也不免跃跃欲试。「苍，吾想吃这个。」

「等等。」苍压低声线，随后不著痕迹地接过了话题，对著颛顼淡淡一笑，「说来也巧，在下所来的地方，有句俗话。」

「喔？」颛顼饶有兴致地反问。

「螳螂捕蝉，黄雀在后。」

「何解？」

「那些自以为是捕猎者的人，到头来往往发现，自己不过是他人口中的猎物。」苍不急不徐道。「只不过，谁是螳螂，谁又是黄雀⋯⋯谁又能说得清呢。自以为是黄雀的，到头来⋯⋯」

道者话音一顿，席间的气氛骤然紧张起来。

绷紧的弦突然释放，利箭破空而去，堪堪掠过方才那名士兵的头顶，钉在了数丈之外的树干上。

风吹草动，林鸟惊起，席间顿时鸦雀无声。一片静默中，众人大气不敢出，只有武神兴高采烈地依旧拿拿这个，拿拿那个。「苍，怎么都愣著？现在能吃了吗？」

「开个玩笑，还请武神大人与阁下不必认真。」颛顼缓缓垂下了弓。

苍不急不慢地话锋一转。「在下自然也是开个玩笑，还请颛顼帝不必认真。」

「哈哈哈⋯⋯」颛顼大笑几声，再次盘膝而坐，双掌重重拍了几下。「长琴，给武神大人斟酒。」

随著颛顼话音落下，一名身著银丝绸的垂髫少年一路小跑来到苍与武神面前，肩上落著三只五彩鸟，叽叽喳喳地鸣叫著。少年看起来不过十五岁的年纪，手里拎著一只不大的青铜壶，给武神面前的酒樽里斟了酒。

这一声「长琴」让道者倏忽抬起低垂的眼眸。修琴乐之人必读的《古琴疏》曾记载，祝融生太子长琴，始作乐风。因缘际会，没想到来到这个神话世界竟然能遇到祖师，也算是一段奇遇。只是此时的太子长琴尚且年幼，不似熟稔乐声，不知在他的未来是因何种缘分，与琴结下渊源。

「可算倒上了。」武神不满地嘀咕一声，兴致勃勃地举起青铜酒樽，将醇厚美酒一饮而尽。「现在能吃了吗？」

「哈哈⋯⋯能！来，再给武神大人斟酒！」颛顼爽朗地大笑几声。

苍无声地叹了口气。高台上的众人心思迥异，各有盘算，唯一的例外，只有全无心事的武神了。

「苍。吃这个，这个好吃，吾试过了。」武神兴高采烈地享用著宴席上的美食，顺手将一只熟透的浆果塞在道者的嘴里。「不许和别人讲那么多话。吾今日已经很宽容了。」

「唔⋯⋯」猝不及防被塞住嘴，苍有点无奈地咀嚼几下，将浆果吞咽下去，只闻武神有点闷闷地小声说，「『帝』是什么？你为什么要称他为『帝』？」

「一方之主为帝，他是北方联盟的共主，也是人间的帝王。」苍压低声音解释道。

「人间的帝王，人间的帝王⋯⋯哼。」武神小声地嘟囔著，「有什么了不起？」

「怎么？」

「要吾说，称神为『帝』才差不多。人类，不够格。」武神颇为傲慢地耳语道。

「⋯⋯」

武神亲暱地咬了咬道者的耳朵，在道者耳边小声道：「反正，吾也要做一回帝王，让你这样唤吾。」

「⋯⋯」苍面无表情地别开了头。「帝王，可不是什么好差事。」苍正了正色，趁著众人注目围猎的间隙，压低声音道，「在苍看来，驰骋人间，未必比闲云野鹤更自在。」

「怎么，苍，你是不想让吾称帝吗？」武神闷闷地说。

「⋯⋯现在这样的你，也很好。」苍很轻地叹了口气。

「你不在的时候，吾的日子真的很无聊。」

「⋯⋯抱歉。」

「说起来，二位如何相识的？」颛顼突然提高声线，打断了一人一神的窃窃私语。

颛顼此言一出，武神不由得一怔。顿了顿，他颇为傲慢道，「在苍原来的世界，吾与苍早就相识了。」金蓝异瞳慵懒地眯起，显得有些得意洋洋。「吾是他的天命，懂吗？就是命中注定的意思。」

「喔？」颛顼将意味深长的目光投向与武神紧邻的道者，「那阁下在原来的世界，与武神大人又是如何相识的呢？」

「吾与他相识在一片海域。」苍面色无波道。

「所以是看海认识的？」武神插嘴道。

「⋯⋯」苍面无表情地垂著眼睛，既没有点头，也没有摇头。

「喔？那阁下又是出于何种原因，要来到久远前的世界寻找现在的武神大人呢？」颛顼犀利的目光似乎更加意味深长了。

苍面色罕见地一僵。

<br/>

一直以来，和武神的相处中，苍一直小心翼翼地回避著这个问题。幸运的是，武神心思不经世事，又对一切大多漫不经心，也忽视了这一至关重要的问题。他们之间始终保持著微妙的平衡，然而这平衡却像是在万丈悬崖之上，稍有不慎便会跌得粉身碎骨。

<br/>

武神忽视的问题，人间的帝王却未必会忽视。在犀利的目光中，苍镇定地缓缓道，「天命。」

「哦？怎样的天命？」

「注定相遇的天命。」

「哈哈哈⋯⋯」颛顼意味深长地笑了几声，若有所思地不再追问。

苍极轻地叹了口气，心口高悬的巨石愈发沈重，压得他几乎无法喘息。自从来到这个世界，对于武神堕魔的症结不仅毫无头绪，还欠下一条手臂的情意——更准确的说法，欠下了一屁股情债——一切都在向著不可控的方向急驰而去了。

自己真的在改变未来吗？还是成为同一个结局的推动者，见证曾经心思纯粹的天神终有一日成为翻云覆雨、降罪人间的毁灭之神，在命运的风起云涌中，再一次走向覆水难收。

<br/>

既然「圣人」不在此地，这场围猎也没有围观的必要了，徒增变数。想到这里，苍对武神小声道：「咱们走吧。」

「吾还没吃够呢⋯⋯」武神明显有点闷闷不乐。苍用力拖拽了几下，高大的身影纹丝不动。

「走吧。」苍无奈地小声道，「你想吃，吾买给你。」

「长琴，再给武神大人斟酒！」颛顼再次拍了拍手掌。

道者正欲起身，方才斟酒的少年应声再次上前。正要为武神斟酒时，少年手中的动作不知为何停了，恍神的一瞬间，清冽的酒已经洒了一地。

「你背的是什么？」长琴好奇地指了指苍背后的怒沧，乌黑的瞳孔透著隐隐兴奋的神采。

颛顼脸色一沉，「长琴，不得无礼。还不快给武神大人道歉！」

「是一把七弦的残筝。」在少年求知的目光中，苍微微一笑。拂尘轻挥，怒沧应声而出，落在了少年面前。「名为『怒沧』。」

「为什么叫『怒沧』？」少年不依不挠地问，有些新奇地抚上怒沧生冷的琴弦。「你为什么要用一把残筝？」

「你想听听关于它的故事？」苍温和地问。

「长琴，退下！」颛顼不悦地命令道。「回去抄书！」

「嗯。吾想听。」压抑住怯意，少年认真地点了点头。连武神的目光也变得认真了几分，饶有兴致地望著苍，罕见地大有一幅认真听故事的架势。「苍，吾也要听。」

见武神发话，颛顼也只好由著太子长琴去了。

「吾是在一处海边的古战场拾获这把残筝的。」苍对少年温和地一笑，娓娓道来，「那里的海怒浪滔天，潮舔断崖，吾想，不如就叫它『怒沧』吧。当时这把筝已经残破不堪了，像是经过一场浴血之战，连断了的琴弦都被鲜血浸透了。」

「那你为什么你不把它修好，只留下七根琴弦呢？」

「过犹不及，盈满则亏。吾当时觉得，这把琴就应当如此。」苍解释道。

更重要的原因，苍却并未说出口。海浪翻涌，惊涛拍岸，当双手抚摸上琴弦的那一刹那，当奏响势如山海的第一音，似有似无的熟悉感蔓上心头。琴声厚重凝远，似海幽深，不知是否是一种错觉，那时他仿佛觉得，这把琴从来如此，就也应当如此，七弦，便已足够了。

那时他有种恍惚的直觉——也许，他与这把琴的缘分，似乎在遥远的过去就已经开始了，远得几乎超过了这把琴应有的年岁。

「『怒沧』，好名字。」武神品味著。

「不思进取，偏爱著靡靡之音，给吾下去。」颛顼沉著脸，对著太子长琴一声厉喝。「完全没有太子该有的样子。」

长琴恋恋不舍地从怒沧上收回手，却依旧未挪动脚步。「我不喜欢骑马打仗，我就喜欢这个。」

「还不回去面壁思过？！」颛顼恼怒道。

「靡靡之音？」金蓝异瞳闻声一凛，武神冷冷道，「怎么，苍的琴声也是靡靡之音了？解释你的话。」

见武神不快，颛顼话锋一转，对苍道：「⋯⋯方才的话并非针对阁下。只是长琴身为太子，本应戎马倥偬，却不思国事，偏爱乐声，未免让人贻笑大方。」

「无妨。」苍淡淡道，「夫乐者，乐也，人情之所不能免也。少年心性，何必苛责。」

「欲戴王冠，必承其重，谈何苛责？」

「苍，这话吾倒是同意。」武神插嘴道，戳了戳道者的腰际。

苍面无表情地拿开了武神的手，对颛顼从容道：「不知在下可否斗胆一言？」

「哦？」

「『夫民有血气心知之性，而无哀乐喜怒之常，应感起物而动，然后心术形焉。是故治世之音安以乐，其政和；乱世之音怨以怒，其政乖；亡国之音哀以思，其民困 。声音之道，与政通矣』。既有以武治天下，为何不能有以乐治天下？」

武神拖起了腮，转过头，懒洋洋地瞇起眼睛看著道者。

「乐者，音之所由生也，其本在人心。以乐教民平好恶，而返人道之正。」苍道，「何逊于以武治人心？力量，不应该是绝对。」

「又来了，苍，其实你是说给吾听的吧。」武神不满地小声嘀咕道。

「⋯⋯」

「我可以听你弹琴吗。」少年怯怯地问。

「放肆！他只能给吾弹琴听。」武神低声喝道，金蓝异瞳的寒意吓得太子长琴一个寒颤，连退几步。少年强抑住心中的怯意，再一次迟疑地向前迈了一步。「⋯⋯可以吗？」

「他只是个孩子。」苍对武神无奈道，随即转头望向眼中满是希冀的少年，温和一笑。「当然可以。」

「罢了。」武神不悦地摆摆手，「看在吾今日心情不错，姑且允你了。」

「⋯⋯」

「⋯⋯再说你有哪次真的听了吾的话？反正吾说什么你也不听，随便你吧。」

「哈哈，哈哈。」颛顼干巴巴地笑了几声，略显尴尬。「长琴，还不赶快谢过武神大人？」

苍闻言只是淡淡一笑，凌空而起，在众人的注目下广袖铺开，缓缓降落在十丈高台正前。秋日的凉风吹起紫色绣带翻飞，仿佛世外仙人，从九霄云端暂落人间。

望著天际云卷云舒，道者瞬间思如泉涌。琴弦在指尖如有万钧之力，轻轻一拨，扰动天际云海呈现怒海之象，如巨浪般翻涌席卷。琴音渐渐转急，道者忽闻八方如有钟鼓之声，雄浑浩荡，厚重凝敛。他抬起头，只见武神不知何时已经起身，金蓝异瞳正与他遥相对望，手掌轻抬，微微蓄力，似有召唤之意的一点灵光没入云端。

刹那间，八条飞龙在天地的八个方位已承云而来，在云海的万丈波涛之中飞舞盘桓，鼓之以雷霆，奋之以风雨，伴著风声对空长吟，与琴声的云海翻涌之象相和而歌。

苍微微一怔。武神召来飞龙，作八风之音，是为他伴奏吗？

在琴声与风声的牵引下，阴阳相摩，天地相荡，风雨飞动，日月光照，天地呈现百化兴焉之象。在场之人无不为眼前这罕见的宏大壮观之景而震摄，纷纷屏住了呼吸。在一片无言的注目中，苍望著武神，感到更加痛苦而茫然了。

一曲终了，余音回荡在天地间，八条飞龙依然首尾相接，在道者周身低低地盘旋。直到金蓝异瞳凌厉生冷地瞪了一眼，才恋恋不舍地向八方散去，重回天地之间。

「它们很喜欢你。」武神闷闷不乐地低声说，把道者拉回自己身前。「看在吾今日心情不错，就不和它们计较了。」

「⋯⋯」

「今日听君一曲，大开眼界，阁下果然非凡人也。」颛顼赞道。

「废话。吾看上的人，能差吗？」武神气定神闲地背起手，转过身去。

「⋯⋯」苍自动略过了武神的后半句。「今日时候不早，多番叨扰，吾等是时候告辞了。」

「这首曲子，有名字吗。」颛顼问道。

苍微微一顿，略一迟疑，最终还是道：「承云。」

<br/>

史书《古乐》所载，『帝颛顼生自若水，实处空桑，乃登为帝。惟天之合，正风乃行，其音若熙熙凄凄锵锵。帝颛顼好其音，乃令飞龙作，效八风之音，命之曰《承云》』。

无论历史究竟是何种真实⋯⋯他再一次推动了同一个结局。

<br/>

> ##### 小剧场
>
> 武神猫猫：（骄傲）（叉腰）哼，本猫看中的人，能差吗？！
>
> 苍：（淡定）（喝茶）
>
> 某蓝：看把你得瑟的，不吃飞醋了？
>
> 武神猫猫：吾的人在外面演奏，面子要给足！⋯⋯至于别的，回去再算帐。
>
> 苍：？
>
> 某蓝：算什么帐啊？人家葱花怎么得罪你啦？
>
> 武神猫猫：拈花惹草，招蜂引蝶，哼（猫爪拍飞一个路人）回去再好好教训♂️一下！
{: .block-tip}
