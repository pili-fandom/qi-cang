---
title: 十戒
layout: post
lang: zh-Hans
category: ten
depth: 2
date: 2011-03-21 00:00:08
author: 文心玲
---

### 回八
苍是无限可能的令人难以捉摸，自从认识苍以后，弃天帝曾阅读过的苍个人档案资料形同废纸，除了身世与朋友资料正确外其它完全错误。

方才在车上还睡得很深沉，估计连喊火烧车也不会醒来的苍，一到酒吧就生龙活虎地到处聊天外加猛喝各种酒类，更神奇的是他喝再多种酒也没醉，倒是混酒想灌醉他的人全都醉倒了。 

弃天帝执起玻璃杯，轻浅地饮着马丁尼加冰块的鸡尾酒，对于调酒他并不懂所以不会装懂，除了他知道的酒类以外一概不点。

苍是千杯不醉，不代表弃天帝也是，如果两人都醉倒那谁开车送苍回家？

或许是一个眼睛直盯着另一个帅哥的帅哥太显眼，或者是弃天帝异色眼瞳太引人注目，他身旁早已被一个个想寻找一夜情或男伴的女孩坐满，却没有人有勇气上前向他搭讪。 

令人意外的是敢搭讪弃天帝的第一人也是个帅哥，还是兰花影酒吧老板兼首席调酒师―兰漪章袤君。

『一个人喝闷酒，被女朋友甩啦？』

猜错了，是男朋友，只不过还没交往算的上被甩掉吗？ 

弃天帝大笑。『你的名字？』 

『初次见面，我是这里的老板兼调酒师，兰漪章袤君，你可以叫我兰漪。』

『喔，幸会了， 兰漪章袤君。』

随意回完招呼，弃天帝仍是看着正在舞台区唱歌的苍，他从未看过苍如此放纵的样子，也许苍已经醉了也说不定。

放纵的苍正握着麦克风，聚光灯照在他的身上使他的栗色长发异常炫目，他忘情地唱着情歌使酒吧小小的舞台顿时让人有演唱会舞台的错觉。

这是一首简单的小情歌，唱着正道便当的曲折。

我想我很豁达，当有你的神岚，脚边的魔气转了。

> 这是一首简单的小情歌，唱着正道便当的不甘。  
> 我想我很适合，当一个歌颂者，生命在风中飘着。  
> 你知道，就算便当让整个苦境颠倒，我会给你白虹。  
> 受不了，看见你背影来到，写下我，度秒如年难捱的心声。    
> 就算整个世界被弃天帝绑票，我也不会奔跑。  
> 逃不了，最后谁也都便当，写下我，时间和琴声交错的阵法。  
>  
> 你知道，就算便当让整个苦境痛哭，我会给你白虹。  
> 受不了，看见你背影来到，写下我，度秒如年难捱的愤怒。   
> 就算整个世界被弃天帝毁灭，我也不会奔跑。  
> 最后谁也都便当，写下我，剑阵和琴声交错的结尾。（注：改编自小情歌歌词，版权归原传播媒体所有。）

台上的苍正唱着歌，台下的弃天帝轻声合唱，唱完歌曲的一瞬间紫眸往他的方向望一眼，弃天帝立刻回以微笑。 

尽管他知道苍已经醉了，可能也看不到他，但他还是放下酒杯为他鼓掌。

若是要醉到极点才可以放纵，那让他放纵一回也无妨。

从头到尾弃天帝只是静静欣赏，做个称职的观众与支持者，适时的给与微笑与掌声。

『苍唱的怎样？哎呀看来我也是多问的，你从头到尾都看着苍，铁定觉得他唱的很好。』

『不错。』他的确值得称赞。

『只是不错而已？我还以为你会夸他夸的天上有地下无，不错两字太少了吧？』兰漪章袤君惊讶的表情夸张得很刻意。

『歌唱只是他才华的一部分，夸得少点代表还有进步的空间。』

弃天帝微笑，作势欲招服务生再点酒品，兰漪章袤君却阻止他。 

『难得有这么张狂的客人，老板自要尽地主之谊啰。不介意尝试我亲手调的酒吧？苍的同事，大明星弃天帝先生。』

『送上门来的美人我从未拒绝过，况且酒吧老板亲手调制的鸡尾酒，即使有毒我也会荣幸喝下。』 


