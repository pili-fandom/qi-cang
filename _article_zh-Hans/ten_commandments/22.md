---
title: 十戒
layout: post
lang: zh-Hans
category: ten
depth: 2
date: 2011-03-21 00:00:22
author: 文心玲
---

### 回二十二
弃天帝开着车漫无目的地奔驰，仪表板显示目前车速一百二十公里，远远超过省道规定的六十公里时速，反正罚单伏婴师会帮他缴，一切都无所谓。 

太阳神的跑车性能真好，如果开上国道高速公路也许能飙到时速两百公里，从台北到高雄只需要两个多小时，那从云林出发到台北市应该不用两小时。 

他一时心血来潮，转个弯将车子开上交流道。

虽然跟踪失败，但弃天帝并不担心苍的安全问题，他相信苍不会有事。

与其说他相信苍有能力自保，不如说他相信对于某政客而言苍仍有很高的利用价值，因此苍不可能出事。

他打开伏婴师帮天生路痴的太阳神安装的ＧＰＳ导航系统，输入 立 法 院 
三个字，ＧＰＳ立刻搜寻出最快的行车路线与指引，弃天帝看了一眼然后关掉ＧＰＳ导航系统。

只要记住地址便够，他始终相信人脑优于计算机，而且他不喜欢依照计算机指示开车，将自己生命操纵在无生命系统手中的感觉非常之差。

他让车子慢慢加速，他很喜爱在非上下班时间的国道高速公路上驱车奔驰，飞逝而过的景物让他有一种飞翔的快感。

只可惜有人跟踪妨碍到他难得的飚车时间。

后照镜映照出灿金车头，弃天帝故意将车速放慢，后方金色车子随即放慢车速，看他放慢车速弃天帝忽地踩油门加速，瞬间拉长两部车子的车距。 

金鎏影拿下已抽到快尽头的烟头，连串国骂脱口而出。

他边踩油门边按下车头大灯，打开又熄灯，再打开又再熄灯。 

弃天帝感觉到青筋从额上浮出，后方的车子故意开大灯扰乱他的照后镜，又熄灯又开灯令人心绪不定。

他索性将车开到外车道，放慢车速让金色轿车超过他，然后他看到金色轿车车主拉下车窗，大吼道：停车！

雪白跑车与金色轿车一前一后停到了路肩，金鎏影递给弃天帝一根烟。

『我不抽烟。』 

弃天帝并未摇头或是做拒绝手势，金鎏影将摆到他眼前的烟收回，以中指与食指夹住烟身，点燃后深吸一口。

『你不好奇我为何跟踪你？』

弃天帝看着他抽烟，微笑道：『谁说我不好奇？我只是等你自动说出你的目的。』

他的笑容彷佛正说着询问你是浪费我的时间。

金鎏影很想将烟蒂弹到他脸上，然而他忍住这股冲动。

『你想让苍自由对吧？我可以帮你。』

弃天帝点头。

『好，你需要多少报酬？这张空白支票给你，你可以随意填个数目，但是支票只有瑞 士 银行可以兑现，你得先办好护照，事成之后到瑞士兑现支票。如果你不方便用原本的身分去，我可以让伏婴师帮你办 阿 根 廷 的国际护照证件。』

金鎏影手上的烟抖了抖，他惊讶地看着弃天帝。

『你不怀疑我的来历吗？就这么信任我？你不怕我转过身拿了钱就背叛你？』 

弃天帝仍是维持他一贯的笑容。

『你不会。你是苍的朋友，所以我信任你。』

是不会还是不敢？

长年为金家做事，金鎏影用演员身分来掩饰他的真实身分，实际上金鎏影纵横黑白两道，他做事既狠且绝，完事后干净漂亮不留破绽，这是姓金的老头为何会信任他，留他在身边的原因。 

只要金鎏影想做的事，没有什么事他不敢作。 

『你不怕你信错人？』 

随手乱丢烟蒂，金鎏影点燃另一支烟。

与苍总是淡泊实际上却很温暖的紫眼不同，弃天帝带笑的异色眼瞳眼神异常锐利，金鎏影想到三个字来形容他对弃天帝的感觉：笑面虎。

『伏婴师提过你，我不会怀疑伏婴师推荐的人选，除非你怀疑你自己的能力。』

金鎏影呼出白蒙烟圈。

『我能力如何你很快就能得知。』

他踹弃天帝一脚，弃天帝反应闪过，却发觉子弹正巧掠过金鎏影的脸颊，划出浅浅的血痕。

弃天帝皱眉，他居然没发现狙击手的无声子弹，太反常了。

路肩除了雪白跑车与金色轿车外，没有任何车辆，而狙击手绝不可能在高速移动的车辆中对准开枪射击，唯一有可能的地方只有五百公尺外的大看板上。

弃天帝掏出随身的微型手枪，瞄准看板上一个移动中的黑影。

金鎏影自车上拿出长枪，与他同时击出子弹。

黑影瞬间掉落，弃天帝的翅膀型蓝芽耳机响起卡通歌，他打开手机看到伏婴师传来的简讯：活捉杀手。

弃天帝本想回简讯：太晚了，我杀了他。 

手机却被金鎏影抢走，金鎏影很讨厌用手机传简讯，所以他只简单回复两个英文字：ＯＫ。

『你的微型手枪射程只有十公尺，看来他是被我的麻醉枪打中。』 

弃天帝拿回手机，微笑道：『伏婴师说的不错，金影杀手确实价值不斐。』

什么不好提，又提紫荆衣随意取的没品绰号！

金鎏影暗骂了句国骂。

『你先准备好十亿元现钞，支票上我会填五百万欧元，支票是给我的报酬，现钞我别有用途。』 

『我所持有的异度集团股份刚好价值十二亿元，准备出这笔钱我不就倾家荡产？』虽是疑问句，弃天帝的表情却一点也不在意。

金鎏影听这句话的感觉像是贵妇买名牌包时的抱怨一样，似是心痛实则不痛不痒。
他真心讨厌这种不知民间疾苦的人。

若有一天让你穷到去麦当劳打工，哄小孩子让家长买儿童餐还加购全套玩具，或者带小丑面具帮小朋友办庆生会然后被奶油涂满整头，还没休息又被店长叫去送外送阿，包准你会崩溃到连嘻皮笑脸都不知道怎么笑！ 

『给你一周时间准备，一周后让伏婴师拿给我。』

金鎏影一边腹诽弃天帝，一边打开车门。

弃天帝敲他的车窗，他按下车窗。

『弃老板有何贵干？』他特别强调最后一个字。

弃天帝的笑容却让金鎏影全身起了鸡母皮。

『你能不能顺便帮我一个忙？帮我买束玫瑰花送给苍，苍现在生我的气一定不收我送的花，但是他不会拒绝你。』

弃天帝害羞腼腆地塞了张粉红卡片到金鎏影手上，卡片上画了很大的爱心。

他可不可以不答应帮忙？

『记得要买九十九朵红玫瑰......』 

金鎏影拉上车窗，猛踩油门绝尘而去。

他终于体会到什么是王八配绿豆，一个是表面淡泊严肃实际上温暖而热情，另一个是看起来笑嘻嘻但是性格分裂得有够夸张，平常时像笑面虎，提到苍时又变成智商零分的花痴笨蛋，难怪苍的自我防备会被这个人瓦解...... 

谁能对一个真挚的傻子设下心防？

金鎏影不禁叹气，他与紫荆衣之间能再坦诚点该多好，可惜他就学不来笨蛋的表白法。

他看着弃天帝写的粉红卡片，拿起手机按下花店号码。

『喂，我是昭穆尊，妳帮我准备两束红玫瑰花，都要九十九朵红玫瑰，一小时后送去片场，钱跟一位叫伏婴师的人收。』

电话挂断后金鎏影又再打一通。

『还是我，其中一束要加一只小熊，恩，跟以前一样要蓝色的生日小熊，对啦就是要送给尹秋君，妳只是花店老板娘问东问西做啥？别逼我揍人，莎萝蔓妳知道我没把妳当过女人，再废话我就开车去妳店里砸店！』 

挂断电话后，金鎏影抽完最后一根烟，将空的蓝色烟盒又收回上衣口袋。


