---
title: 十戒
layout: post
lang: zh-Hans
category: ten
depth: 2
date: 2011-03-21 00:00:12
author: 文心玲
---

### 回十二
苍开车门的动作有些迟疑。

有些人的脸皮的确是比城墙还厚，即使拒绝他还是会硬跟吧。 

苍本来想问弃天帝怎么会知道他家地址，转念一想他可能早已调查过，怕是连他喜欢什么讨厌什么都一清二楚。

所以，何必白费力气呢？ 

他推开车门，弃天帝跟着一同下车。

家门前，苍停下脚步。

『这栋是两人住的公司宿舍，翠山行去高雄办签名会不在家。』 

意即今晚这栋房子并不会有别人存在。 

如果苍开门让他进入，那便代表着今晚只有苍与他两人独处。

俩人在一间房子里可以做很多事，他可以亲吻苍的脸颊，可以握住他的手，可以…… 
弃天帝忽然觉得心跳加速，但他还是以微笑掩盖住他的紧张。 

希望苍不会发觉他狂乱的心跳。

苍的手里握着钥匙，明明开门的动作相当简单，这一瞬间他却忘了该如何开门。 
他的内心其实相当犹豫，该不该为他开启这道门扉？ 

弃天帝看见苍犹豫的表情，他暗自叹息又领到一张好人卡。

『唉，看来我没机会喝你亲手泡的茶了。等过几天翠山行回来，我再与剧组的朋友们一起来你家作客，你可不能拒绝。』 

为他找个台阶下吧，在他还没赶人之前先离去也好，至少下次见面不会尴尬。

他希望能保有喜欢他的机会，若是太过逼迫他，也许反而会弄巧成拙。 

正想不着痕迹后退离去的时刻，苍却打开了门扉，他站在门后方等他进入。

『先把鞋子脱下放在玄关，客用托鞋已摆好了请自便。翠山行有洁癖，如果你没脱鞋子就进屋，他会逼我与他一起将全屋子的地板抹干净。』

事情转折得太突然，弃天帝正思考着是否该换上拖鞋入内，还是该保持本来的距离？
他进了这扇门之后，还能保持原本与他的关系吗？

苍却一副不以为意的样子，他拿着茶罐对他微笑道：

『你真的很想喝我亲手泡的茶？』

房间里温暖的晕黄光线流泻而下，照在苍的身上却引起他的无限遐想。 

他可以放任他的心前进吗？

如果过了今晚，是否会再也没有接近他的机会？

弃天帝深吸口气，又恢复他一贯的笑脸模样。

『坦白说我很想，如果你不想，我也不会逼你。』

苍看了他一眼，然后打开电视。 

『如果我让你陪我看一晚电视，你能接受吗？』

四十二吋电浆电视屏幕上播放着之前苍曾参与演出的剧集，他递给弃天帝一个大抱枕与很多包零食。

弃天帝叹口气，认命道：

『在你泡茶之前先给我一大杯冰水吧，我需要冷静一下。』

苍依言到冰箱倒出翠山行出门前冰好的冰块，把冰块全倒到大啤酒杯内，再冲入已泡好的春茶。 

再热的水遇到冰块也会冰凉，但他并不是冰块，他只是还没准备好。

虽然他已开启门扉让他进入，但他还无法克服他心底对于同性之爱的迟疑。

他能感受到弃天帝对自己的体贴，他并不是木头人，也不是万年不溶化的冰山，他只是还需要一点时间罢了。

将冰茶放到弃天帝面前，苍却惊讶地发现弃天帝已然睡着。

他拿起平常搁在沙发上头的毯子，盖在弃天帝身上。 

弃天帝熟睡的脸庞离他如此近，近的可以感受到他平稳的呼吸频率。

以男人而论，弃天帝的眼睫毛很长，总是微勾的薄唇带着健康的唇色，下巴清理的很干净不留下一点胡渣，脸颊亦是相当光滑好摸。

苍的唇瓣不自觉地掠过弃天帝的脸颊，他的心跳忽地快了几拍。 

他将毯子放下，故作镇定地回到房间。

关上房门，苍在房门后喘息，他不知道该如何控制已失控的情绪。

心若已不属于自己该如何是好？ 

想不出解决方法，苍索性蒙头大睡。


