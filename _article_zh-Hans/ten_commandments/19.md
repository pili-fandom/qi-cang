---
title: 十戒
layout: post
lang: zh-Hans
category: ten
depth: 2
date: 2011-03-21 00:00:19
author: 文心玲
---

### 回十九
弃天帝在车子里看着苍走入银行，弃天帝下午的戏份并不多，因此他比苍还早拍完戏份，苍步出片场的时候弃天帝也走出片场，苍坐上出租车后他开太阳神的纯白跑车一路跟随，他很有信心苍不会发觉他正在跟踪他。 

下午的那场戏，弃天帝站在棚内的沙漠景中，他表面上是看着摄影机，实际上却是看着正在摄影机后方休息区坐着休息的苍。

苍闭着眼睛像是睡的很熟。 

他想起苍当时似是提起很大勇气才说出的话语，话语中的矛盾与挣扎是否意味着苍曾因为天生的淡漠性格而受过伤害？ 

那，放映室内他的一时冲动是否已伤害到苍？

他其实并不想逼迫苍，但当时他并没有克制住他自己，也许他始终无法摆脱骨子里的征服欲与劣根性，当遇到苍的淡泊时，那劣根性就会不受控制地爆发出来。 

弃天帝并不希望因此而伤害到苍。

导演要弃天帝作的动作并不多，他只需站着任风吹起他的黑色长袍，暗紫花纹在灯光下隐约反射魔幻光华，他的手负于身后，眼神却是一直追着苍的身影，他看着苍起身自公用饮水的厚纸箱里拿出矿泉水打开后猛灌，矿泉水从苍的唇角溢出，透明水痕顺着颈部线条流淌入内袍衣领。

弃天帝忽然感到有些燥热，而苍的眼神正巧与他的对上，他赶紧别过头看向正等着对戏的先天三人组。 

摄影机镜头自弃天帝转到剑子仙迹，剑子仙迹一扬拂尘，照着后方工作人员提示的大字报念出台词。

『恩，是弃天帝。』

剑子仙迹脸虽然是朝着弃天帝方向，但并没有看着他。

他已从蔺无双那里得知苍与弃天帝离开休息室后发生的事情，若不是龙宿拦住他，他其实挺想不顾剧本冲上去用古尘好好教训弃天帝一顿。

疏楼龙宿以珍珠团扇半掩面，悄声提醒剑子不可冲动，但提醒了这个却来不及阻止另一个。

『杀生为护生，斩业非斩人。斩魔除业不由分说！』 

下午才听说中午有架打但没找他去，已憋了一肚子火的佛剑分说立刻不由分说地冲上前准备打架。

『佛剑汝不可冲动啊！』剧本没有写不要自己找架打啊！

『走吧，我们去帮佛剑！』

剑子仙迹见机不可失，也拉着龙宿一齐去围殴弃天帝，三个人怎么可能打不过一个？就算是再厉害的魔王也该受死，哈哈哈。

说时迟那时快，佛碟与古尘配合无间直向弃天帝杀去，三台摄影机不知道该关机还是该忠实记录下每一个分镜镜头？

摄影师困惑地看着导演，导演却示意让他们即兴演出，副导指示Ａ摄影机拉弃天帝大特写，Ｂ摄影机捕捉佛剑分说与剑子仙迹的武打动作，Ｃ摄影机装雾化带疏楼龙宿转圈镜头，务求剪辑出闪亮唯美与热血兼备的绝佳画面！ 

这段戏没写在剧本里。

并没有预料到要即兴演出的弃天帝愣了一秒，但剑锋已然迫面，他瞬间反应想掏出贴身的小型手枪，然而戏服里并没有装置手枪，他只得转身闪过剑锋，空手夺白刃接下佛碟，再抬手一掌撃向佛剑分说。 

『神之岚！』 

依照之前剧本指示，只要弃天帝小抬手念神之焰或神之岚台词，众人便得后退多步作出吐血以及被击倒的样子。 

剑子仙迹虽照样后退，但却拋出小金剑射向弃天帝胸口部位，演戏用的小金剑刺中人时刀面会自动缩回，所以被刺到的人并不会受伤，然而只要扔中他胸口也能造成瘀青，够让弃天帝疼个好一阵子。 

谁让他使好友伤心呢？打不到他也要让他尝尝被捅的滋味，哈哈哈。

剑子仙迹笑得太过腹黑，被疏楼龙宿以珍珠团扇拍头提醒他要保持先天形象。

弃天帝察觉到三部镜头皆转向三先天，他微笑以右手背挡住小金剑，只听到喀擦一声，小金剑便断成两半。

『ＣＵＴ！』 

导演下达收工指示，弃天帝走到休息区时却已不见苍身影，伏婴师凉凉指向他方才所在的沙漠戏棚，他才知道苍为了不跟他撞面，特地绕了个圈子走到沙漠戏棚内拍下一场戏。 

他坐在苍曾坐过的椅子上，他看着苍在沙漠中一步一步往前行，休息区与棚内的距离只有不到十公尺，但是他却觉得心的距离隔了整个浩瀚无垠的冰河。 

他相当懊恼地抱住头，然而后悔又如何？他还能作什么事才能得到苍的原谅？ 

忽然，弃天帝想到他可能忽略了一条很重要的线索。

他心中一直想着苍，只想如何让苍能自由地放纵，却忽略了还有妨碍苍自由的可能。

『伏婴师，太阳神去拍周边商品需要的写真照，那他的车钥匙在你身上吗？』 

『的确在我这里，喏，你要就拿去，开坏了你负责。』 

弃天帝接过车钥匙，正巧苍的戏份刚拍完，他跟着苍的脚步离开戏棚。


