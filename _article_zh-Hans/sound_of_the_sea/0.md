---
title: 时之岚·海之音
longtitle: 时之岚·海之音
author: 雾恋
layout: post
category: sound
date: 2012-12-31
lang: zh-Hans
depth: 1
toc: true
---

#### 作者：雾恋

原载于[https://tieba.baidu.com/p/758846326](https://tieba.baidu.com/p/758846326)

（关键词：水晶球、双重人格、人鱼）

---

苍站在海边，干涩的海风吹乱了他的长发。

在他的面前，是一块明显地和周边有植被覆盖的地面区分开的，规整的矩形空地，松软的地表，显然是新近刚刚翻整过的。他长久地待在这里，凝视著面前的地面。在薄薄的土壤层下，埋著一副鲸鱼的骨架，一年以后，这句骨架将被重新组装，安置在博物馆里供人观赏。除了简单的新闻报道，没人会在意这具标本过去的故事。而苍在这里，凭吊著一个逝去的生命。

“是同伴啊，一定得去呢。”

那孩子留给他的最后一句话，回响在耳畔。

最后，苍长叹了一口气，转身离开。

科学院下属海洋生物研究所的怒海实验站，坐落在这座海上孤岛上。除了以白色小楼为主建筑的一系列实验设施，岛上什么都没有。向四周望去，都只有茫茫大海，一成不变的单调景致。

岛上的生活是这样乏味，每发生一点不同的事情，都会令站里年轻的员工们兴奋很久，比如五天前刚来怒海实验站报道的研究员、苍。

作为研究所下派到这里的新站长，苍谦和的态度与柔和整洁的印象广受大家好评，不过除了工作时间，他基本都是独来独往，时间一久，大家都已经习惯，这个新来的研究员从来没有对融入团体做任何尝试。他经常只是在一边看著大家的热闹，或者干脆地不见踪影,大家已经习以为常，没事不会去打搅他。在大家眼里，苍是一个让人颇有距离感的学者。

这天却是不同。

当苍回到实验站，推开大门的时候，所有的站内员工，齐刷刷地向他投来视线，七嘴八舌地打招呼。

“苍你总算是回来了！”“到哪里去了？等你等了好久啊！”“快点过来啊！”…………

“抱歉…有点事情。”除了报道的第一天，苍还没受到过这么热情的招呼，于是他有些不解地朝众人聚集在一起的地方走去。

大家纷纷向两边闪开，人群的空挡间露出张桌子，一个水盆摆在桌子的中央。

看见盆子里的东西，苍平静的脸上，刹那间流露出动摇的神色。

那是一只有硬壳的小动物，圆溜溜的，大约直径10厘米左右， 光滑平整的壳体向内卷得很紧，由中心向外辐射的细密的生长纹。无视周围一群人的目光，这个小东西正在水盆里缓慢地倒退游动，数目众多的触手轻轻摆动。

“…………”苍盯著它看了大约五秒钟，转过头问道，“这是…怎么来的？”

“刚才去采集贝类的样品，结果居然捞上来这玩意儿！”一位显然处于亢奋状态的实验员大声嚷道。

“别说那些了！”另一位实验员把他推搡开，抢白道，“苍研究员、快说啊！这难道…、真的是……”因为激动、他脸涨得通红，已经无法连贯地说话。

苍走到桌边，伸出去触碰盆里的那只螺，周围一下子鸦雀无声。

“……是棱菊石，应该没错。”苍语调平稳地向众人宣告。

一时之间无人说话，再接著，不大的房间里像炸开了锅一样地热闹。

“天啊！这、这简直是历史性的时刻啊！”

“这是做梦么…怎么可能！真是不敢相信！”

“我去把水族箱和氧气泵搬来，不能让它死了！”

大家手忙脚乱地散开，而水盆里的焦点“人物”，依然在悠闲地戏水，完全不知道自己已经掀起了一场轩然大波。

<br/>

菊石不是罕见的东西，在野外甚至可能随手捡到——嵌在土层里的圆盘状的、或平整或密布各种纹路的不起眼的石块。在久远得无法想像的一段漫长的时间里，从世界所有的陆地都是完全死寂的荒漠的时代，这个庞大而荣耀的家族就已游曳在海洋之中。

当地球从古生代走向中生代的时候，在一场毁灭了世界90%以上物种的巨大浩劫之中，绝大多数种类的菊石消失了，包括这种名为"棱菊石"的品种，残存下的来的迅速发展壮大，很快又恢复了生机。但是，它们最终还是在另一场标志一个时代终结的灾难里，和那些最著名的远古生物一起，被彻底的抹杀了，只剩下坚硬的壳，无言地见证著沧海桑田的巨变。

人们一直都是这么坚信的:那是一种，早已不复存在的生物。

今天，在这个不起眼的小小实验站里，这一定论，却被无法否认的事实推翻了。

在恐龙时代来临以前就已灭绝的棱菊石，现在正在大家的注视之下，在一个塑料盆里，舒展著它的腕足。

看著大家忙得不亦乐乎的样子，苍笑了笑，悄无声息地上楼回自己的房间去了。

“……比预计的更加糟糕啊。”收敛了笑容，他长叹一口气。

桌上摆放著一颗镶嵌在黄金上的足球大小的水晶球。

这么大的完美无瑕的天然水晶球本来就很罕见，更加奇妙的是，它底座的表面上那些手法古拙的雕刻。那不是单纯的纹饰，更像是一些被时间长河湮灭的某种神秘文字。

花了不菲的代价，入手这样一件价值连城的珠宝，弃天帝此时竟是满脸的嫌恶，他带著那样的表情，长久地端详著桌上的水晶球。

“来历这么污秽的东西…哼！你什么时候有了种嗜好？”弃天帝问道。

没有人回答他，这是自然。在这间书房里，除了他以外并无其他人。

瞥了眼望著敞开的窗户，他补充了一句：“再不说，我就让它从那里来回哪里去吧。”

他像是欲搬动那颗水晶球，结果却只将它的底座略微抬起。金托的底面上有三个很像是卯眼的小孔，相互形成正三角形的模样，这样看来，这件珠宝，先前应该是被固定在某个底座之上。

“这究竟是什么？告诉我！”他没预兆地突然发火。“谁允许你在我不知情的情况下擅自行动？”

又是沉默了良久，弃天帝伸出手，爱怜地轻轻摩挲那之前被他称为“污秽之物”的晶体。

嘴角勾起微微的弧度：“何必这么心急呢。谜底什么的，慢慢揭晓才更有趣味啊。”

电话铃响的时候，弃天帝正无视满桌等他签字同意的文件，若有所思地站在窗口眺望风景。但他对打扰到他的电话不悦的心情不会改变。

“不见！这种事不要再向我请示，以后自己判断！”

“但是，那位客人是海洋研究所的，说是有重要的……”

“就算是海王星来的也不见！”

切断通话，还没来得及做什么就觉得有异样，转身一看，有个穿著白色制服的人正站在离他一米开外的地方看著他。

“你是…你怎么进来的？”

“这不是重点，我是为了……”

“我应该在电话里说了，我没不想会客。”

“通报一声是礼仪需要，之后…我必须来见到你。”

“那么你现在见到了，快出去。”

“……那么在那之前，你能先把那颗水晶球还给我么？”

“‘他’告诉你水晶球的事情？！”反射性地脱口而出，同时，头一次抬头看那不速之客。

沉静的目光定格在自己的身上，淡漠从容的神情之中做不出任何的解读。似乎两人的立场错了位，弃天帝才是突然的闯入者一样，于是他后悔自己先前这么激动。

“你怎么会在这里？”用审问的语气说道，这才是该有的态度。

“之前在电话里已经说过了，我的名字是苍，是海洋生物研究所的……”

弃天帝不耐烦地打断了苍的话，“这些我都知道了。”

“又是”这种答非所问的态度！这个念头刚刚闪现，弃天帝就暗自诧异，为何自己会这么想？刚才的一幕似乎是过去某个场景的重现，要仔细追究，却又毫无头绪。

苍看了眼弃天帝办公桌的状态，“抱歉打搅阁下的工作时间了。但我必须拿回那个水晶球。”重申一遍被对方忽视了的此行的目的。

弃天帝眯起眼睛，“…它怎么可能是你的？难道你想说，是鲸鱼把你的收藏品当作点心吞下去了？”

三天前，一头抹香鲸在怒海海域误入渔网窒息而死，这颗水晶球就是在那头鲸鱼的胃里发现的。弃天帝不知出于什么理由，花了高昂的代价将它购得。现在，突然出现的这个名叫苍的人，自称是水晶球的真正所有者。

苍的说辞太荒诞，完全没有可信度。但这个眉清目秀的科学家，看起来实在不像是会撒谎的人，弃天帝相信自己的直觉，换句话说，他想听他说下去。

<br/>

“确实是很无理的请求。不过……”

弃天帝打断了苍的话，“同样的话就不用再重复了。你坚称那东西是你的，却说不出任何理由，我怎么能相信你？”

“如果不能及时送放回去的话，恐怕…”苍有无力地轻叹一口气，“整个世界都会因此被毁灭……”

说完这些话有些艰难，倒不是因为别的，只是，说出来也不会有人会相信吧。

果然，弃天帝愣了几秒钟，突然爆发出一阵大笑：“我从没听过编得这么拙劣的故事！”

苍耐心等待他笑完，平静地补充了一句：“……差不多就是这种程度的重要性吧。可以把它给我了么？当然，我会设法补偿你的损失。”

“你还是老样子，一点都没变！”

“…是你？”

“哈！截止刚才为止不是。”

“嗯。”

“完全没有重逢的喜悦么？你这种冷淡真是叫人扫兴。……庆幸吧，换成别人，谁会耐下心听你说这些荒唐的内容。”

“确实如此，刚才我已经深刻领教了。” 苍就近找了张椅子坐下，“不过我并不意外。持有那颗水晶球的人名字和你一样，不像是简单的巧合。”

“不是名字一样，我和他是同一个人。”弃天帝纠正道。

“所谓的双重人格么？”

“有点区别。”

“嗯。”

见苍并没有询问自己情况的意向，弃天帝有些失望，“……你不打算问我些什么？”还是忍不住问了。

“总会有原因。”

“哼！还是这种态度。别忘了现在是你有求于我。”

“是啊、所以切入正题吧。”

“也是，那么就请你向我详细说明一下吧，关于这个……”弃天帝打开身后的保险箱，把那颗苍目标所指的水晶球取出来摆在写字台上。

苍看著弃天帝的行动，他要拿回来的东西就在眼前。

“……那是时间装置的能量中枢。没有中枢控制的装置很快就会失控。事实上比预计的更短，现在紊乱的情况已经产生，在能量中枢消失的地点，发现了二叠纪末期的海洋生物。”

“那又怎么样？不是很有趣么？”

“……并非是因为时空扭曲，把远古生物送到现代。”苍语速很慢，像是在寻找合适的表达方式，“而是整个环境，都被过去时代的环境的置换了……而且这种情况，在海底正在逐步扩散……”

“说得明白一点。”弃天帝打断苍的话。

房间内很安静，能听见远处潮水有节奏地拍打岸礁的声音。

“整个地质状况正逐渐地倒退到二叠纪末期。也就是说，窗外的海……很快就不属于这个时代…不是人们熟悉的海洋了……”

弃天帝开始觉得，认真听著这种天方夜谭一般的话的自己也很荒唐，“那又如何？”

“弃天帝。二叠纪末期发生过的事……你知道么？”

“想给我上地理课么？那好吧…”弃天帝用手托著下巴，看著苍继续说道，“P-Tr灭绝事件，90%以上的物种彻底消失，成因还没有定论。我的回答正确么？”

“整个地质状态都倒退到那个时期，真是这样的话，当时发生过的、导致大灭绝（Great Dying）发生的种种灾难，可能会一一重演。”

“火山爆发、地震、海平面升高、缺氧。”弃天帝替苍补充道，意外地，他知道得很详细。

“是的，如果现在让装置恢复稳定的话，就能阻止地质置换的进一步发展……”

看到这番对话像是快要有了结果，苍一贯平稳的语调，因为焦急微微地颤抖著。

弃天帝却不想让他顺心，索性撇开了话题：“既然是这么重要的东西，怎么现在会到了我的手里？你总得解释得完整一些，才有足够的可信度。我说的对么、苍？”

“那颗水晶蕴含的的能量可以瞬间净化水质，那孩子…那头鲸鱼为了让它的一群同类通过受污染严重的水域不至于因为迷失方向搁浅，问我借走了能量中枢。短期之内的话没事，可是……”

“你总算是向我承认你能和动物沟通了。”弃天帝感慨地插嘴，“何必呢，以前就坦白不是挺好，这一点我怀疑了很久啊。”

<br/>

“这和今天的主题无关。”

“你为何来找我，其实我并不怎么感兴趣。你只需要让我满意，觉得把这东西给你心甘情愿就好。”

“哈、也是。……那么你还想问什么？”

无论问什么都会回答？弃天帝冷笑一声，他所认识的苍本来是个把什么都藏在心里的人，会逼得那样的人有问必答，看起来的确事态严重。不过，这种对话却并不令他觉得很有成就感。

“我好像是有听说，几天前有一群鲸鱼在北越天海搁浅，也和你说的这件事有关吧？要阻止的事情没有阻止，反而搭上了自己的性命，顺便造成了更大的隐患。……这种没有效率的事情，还真亏你会同意。”

苍不愿理会弃天帝的讥讽。想要挽救同伴的生命的心情，他完全能够理会。因此不忍心拒绝那孩子的请求，即使是现在，也不后悔当时做的决定，只是，如果当时一起行动的话，就不会……

“你还有一个问题没解释清楚。”弃天帝的声音再次响起，打断了他的回忆，“所谓的装置啊…怎么会有那种东西存在？能够替换地质状态的装置……我从来都不知道，现代科技已经发展到这个程度了啊～～”

“……”苍半晌无语，“……你这是明知故问。”

弃天帝支著头观察对方的表情，“我就是想听你亲口证实我的猜想呢。苍、你说，有这样的机会，我会错过么？”

“……确实它不属于现在的这个人类文明。能够替换地质状态的装置…那时候，我们的大陆即将沉入海底的时候，为了避免灾难的发生，我们制这个装置…希望能借此使大陆深处正在酝酿著的强烈的地质活动平息下来。但是在最终调试完成之前……灾难就发生了。然后我们
的大陆，沉入了海底，但这个装置却并未失效。”

“我明白了。当时你们的人想通过‘替换’将强烈的地质运动彻底消除，但没有成功。结果这个装置到现在才运转起来，却即将把现今的地质‘替换’成为最不稳定的年代的地质，这种理解正确么？”

“……是的。”苍苦笑了一下。“这种毁灭性的后果，不应该由现在的人类来承担，我必须制止装置继续作用下去。”

“…就算没有你这个莫名其妙的装置，‘第六次生物大灭绝’不也是在进行当中么？唯一一次因为人类活动的缘故而造成的生物大规模灭绝。……不过是进程快慢的区别罢了。” 

苍无法反驳这种说法，但他的微微皱起的眉头，却显示了他内心的想法。

“我来替你说吧，养尊处优惯了的人类，怕是无法熬过去，会彻底地消失吧。说到这个，每一次大规模灭绝事件当中，那个时代最优势的物种都会被彻底地抹消呢——这种安排真是很公平。有什么生物能在大灭绝中存活下来？真是令人拭目以待呢。” 

听弃天帝的口吻，像是在讨论完全和自己无关的事情。

“你……是认真的么？

“对于和自己无关的事情，人们不都是抱著这种饶有趣味的旁观著的态度，很正常么。”

“你一样也会死。”

“哈哈！用这种事威胁我，苍，你真是令我失望了。对我来说，生或死没多少区别。我在这个世界上存在了很久，并且会继续存在下去。至于其他人，这种结局对于他们来说，是最彻底、最公正的。能亲眼见证这一幕，这是我存在至今的目的之一。”

“…………”

“啊啊、对了，差点忘记。”弃天帝打开桌角摆放的香炉的盖子，用打火机点燃里面的东西。还没等他将盖子合上，房间里就弥漫著一股浓烈的芳香，“这是我送给你的，久别重逢的礼物。也是你那个制造麻烦的小伙伴留下的最后的纪念哪。合意么？”

“…………”

抹香鲸的肠道之中分泌的这种灰黑色的物质，外观上毫不起眼。点燃之后却有奇香扑鼻，是制造香水的重要原来，价格及其昂贵，被人们叫做“龙涎香”。

苍的脸上，看不出有什么表情变化。不过弃天帝并不奢望能这么轻易就激怒他——正是因为这样，撩拨他的情绪，才更加有趣，更加地富有挑战性。

<br/>

苍出现的时候，弃天帝正斜靠在卧室的大床上看电视。

“我不知道你有不打招呼就擅闯私宅的习惯。”眉毛一挑，他故作惊讶地说道。

“我也不知道、原来你其实是很矜持的……”

“哈！”弃天帝没接他的话，又专注于电视节目了。

“最近发生的一系列3级以下的小地震，属于正常的地壳运动，不会引起更大强度的地震发生，广大市民可以放心广大市民不必恐慌，有关方面会密切监测……”正好是新闻节目的时间。

“自以为是真可悲。”弃天帝顺口评论道，“对于近在眼前的危机丝毫没有认识。…你说是不是、苍？”

苍不吭声，现在不是和人抬杠的时候。

“你真是爱藏招。”于是弃天帝很有默契地换了话题。

“怎么说？”

“科学院的人虽然一口认定你是在那里工作的研究员，但对你去实验站报道之前的情况一无所知。原来你连人的记忆都能轻而易举地改动么。”

“为了不给他们带去不必要的困扰。”苍淡淡地说，“我必须找个最便捷的方式，实验站的监测仪器对于我了解海底正在产生的变化非常有帮助。”

“我只是惊讶，……这种像是反派才会干的事，原来你能做得这么心安理得。”

“在你的印象里，我原来很死板么，真遗憾…”苍走近了几步，“我只是不愿他们增添无谓的困扰。只要是切实有效的手段…我从来不拘泥于解决问题的方式。”

这么说著的时候，苍已经坐到了床沿上，弃天帝维持著之前的姿势，没有更多的行动。

苍深吸一口气，直视著对方的眼睛继续说道：“……那么，另一位的你，现在在哪里？”

听到这里，弃天帝笑出声来。

“真遗憾，他刚才和我说，他不想和你打交道。”

“…………”对于这种没有丝毫诚意的态度，苍无言以对。

“只要你能说服他，水晶球就还给你。那玩意儿就锁在书房的保险柜里，你看见过的。只是他不想看见你，我也爱莫能助啊。”

明白苍的焦虑，弃天帝反而更加悠哉了，摆明了就是要享受别人的不安。他不厌其烦地把前一天曾经说过的话复述了一次。

“弃天帝。”

“嗯？”苍很少直接叫他的名字，当他这么做的时候，通常就代表了接下去他将说一些更重要的话，于是弃天帝换了个更舒服的姿势，饶有趣味地等待著。

“我前面说过，我不会拘泥于达成目的的方式。”

“哦？没意义地强调，反而显示了你现在内心的不安。”

“确实，也许吧……”

苍缓缓向自己靠近，他的双眸之中仿佛有水光流转，看著这样的情形，弃天帝觉得心情大好，非常新鲜，他从没见过这样的苍。

他想著就算苍此时别有用心也值回来了。

于是很快他的猜测就被证实：一股无形的巨大压力自上而下向他袭来，整个人被压著陷进床里动弹不得，仅仅想要举起手也变成为不可能。但那力量却很柔和的，没有伤害性的，并未觉得有多么痛苦，那力量拿捏的很好，目的仅仅是为了限制他的行动。

<br/>

对话到这里戛然而止，然后，一阵急促的电话铃声打破了僵局。

犹豫了一下，弃天帝用空闲的左手向身后摸索著，按下了电话的免提键，他右手的使之维持著搭在扳机上的状态。苍可是很会钻空子的人，他不愿意提供这种便利。

“弃总，还有一个小时，酒会就开始了……”是秘书的声音。

“取消。”回答得干脆利落。

“可、可是，现在去发出通知已经来不及了，与会的各位贵宾都已经陆续到场了。”

“……”

听出弃天帝也在权衡，秘书赶忙补充道，“弃总您可不能缺席啊！这次的宴会的重要性……”

但让弃天帝立刻决定去留的，其实是苍那万年淡定的表情。

“我知道了。”说话他就切断了通话。

“看来，我暂时没法陪你了。”他笑著对苍说道，“来吧、我准备了房间给你，在我回来之前，就待在那里打发时间怎样？”

“那么就请带路吧。”

这原本只是一间很普通的客间，于是装了铁栅栏的窗户就显得格外显眼，栅栏细密的空隙把窗外的天空切得支离破碎。

“如何，是不是非常适合你？”

“真是多此一举……你的消除装置，在这里也是同样有效。在这种情况下，我和普通人并没有区别。”

“哈！这虽然是事实，但对于你，我不得不多加留神一些啊。”

“……多谢。”苍觉得很无力。

“在我回来之前，就乖乖待在这里吧，想吃什么直接加热一下就好。”说著，弃天帝还特地拉开了冰箱的门展示给苍看。

冰箱里塞得很满，大概是不清楚苍的口味的缘故，食物的种类格外地丰富。果真想得很周到，看来弃天帝是蓄谋已久啊……

“怎么，还有什么疑问么？”

苍犹豫了一下，说：“你什么时候回来？”

“哦？你是不舍得我离开么？”当然他很清楚苍不是这个意思，不过还是忍不住说出口了。

“…没什么，我只是随便问一下。”

“哼！无论你在计划什么，放弃那些无谓的努力。你唯一可以努力的，只有取悦我……不过放心吧，你的时间还很充裕，你心心念念的，所谓的‘大灭绝’又不是在一天之内发生的。”

门锁的声音响动了几下，看来是被反锁了。听到弃天帝的脚步声渐远，苍才起身观察用来软禁他的这个不大的空间。电视网络一应俱全，分明就是打算让他清楚地了解外界的动向，却只能眼睁睁地看著，真是恶趣味。

<br/>

“你似乎有很多感想要抒发。”

“只是单纯地觉得你闲得无聊。要令他绝望，直接毁了那颗水晶球不好么，何必这么麻烦。”

“留给一线的希望给他。哈，苍究竟能做到何种程度？这样才更有趣味。”

“说得太多，只会显得你心虚。”

“…………”

作为共有一个身体的两个意志，他们早就习惯了彼此的存在，直接在精神层面对话方便又省力。在旁人看来，现在的弃天帝像是闭目养神的状态。

“那个叫做苍的…来历不明的家伙，他究竟是谁？”

“对于他的事情，我知道的并不比你多多少。”

“但你保留著你的时代的记忆，那部分是你独有的。我不喜欢一头雾水的状态……”这番话摆脱不了抱怨发牢骚的嫌疑。

另一位的弃天帝笑了笑，时间太久，已经记不真切了。当年的自己真是这种样子的？

“还是等他自己说比较有成就感。”

“哼！我对他没什么兴趣……”

“哦？……不过昨天，一直旁观的你，也没有提出异议啊？我本来以为那时候你会像前天那样突然取代我呢。”

“……”沉默不语，是在想著如何反驳，另一位却是不依不饶。

“我记得你有洁癖，不喜欢被人触碰的？……哎，这种别扭的态度。原来当我还年轻的时候，还是非常可爱的么……”

“住口！”听到这里，终于忍不住大声喝止。

每当这样的时候，弃天帝就会觉得，那个所谓的另一个的自己，简直就是终日徘徊不肯散去的恶灵啊。

老板突然怒气冲冲地大喊，让正在全神灌注地开车的司机冷不防被吓了一大跳，差点没让车冲上了绿化隔离带；一旁坐著的秘书掏出手绢擦拭冷汗：“弃总，这是怎么了……”

“只是梦见了讨厌的东西，无视吧。”弃天帝面无表情地说。

“哈、哈、哈！”

“我就是你，这样嘲笑有意思么。”

“没什么。……那个所谓的大灭绝又不是在一天之内发生的，就算是灭亡高峰期也持续了几万年……远远比不上现代人类灭绝其他物种的效率啊。就算没有他那个神叨叨的装置，谁知再过一百万年会怎么样？人类的历史根本就是短得不知一提啊，苍总是那样，成天在担心一些无谓的事情……哼，不过这样也好，对我来说倒是意外的收获。”

弃天帝默默听著另一个弃天帝在那里自言自语，他心里想著，活了太久果真是免不了变得太过于唠叨，哪怕是自己也不能免俗。

“……我觉得，那个苍有句话说得倒是不错。”

“什么？”

“执念深重，真是可怕。”

“哈！那个人，和其他的人比起来，确实还是有意思了不少，不会令我觉得腻烦，但也仅此而已罢了。”

“…他是人类么……”

“…这个我也不知道。”

<br/>

中午12点，苍正坐在电脑前享用他的午餐。既然有人帮他准备的这么周到，也没必要多客气。

在这段时间里，苍写完了前几天一直没空完成的程序：以10万年为时间单位的陆地板块的运动模型。屏幕上，代表著陆地的绿色色块不断地变形、聚合、解体、分离，亮度不同的红色光点则是2.6亿年前活跃的那些火山的具体位置。

通过运行这个程序，即将重演的过去的灾难的发生地点与时间能够被精确地查明。

依照弃天帝的性格，恐怕也只有令他满意了，才能达成目标吧…

等待演算结果的过程漫长而枯燥，苍渐渐觉得困乏。

就在这个时候，显示器里弹出的窗口，让他的困倦顿时消失得无影无踪。他的脸上流露著难以置信的神色，他放下刚才打开盖子的矿泉水瓶，凑到显示前前仔细查看详细的分析报告。

糟糕，之前想得太轻巧了！

古代生物的出现、轻微地震频发，都集中地出现在怒海一带，造成这一系列反常情况的缘由，并非像苍原先想的那样，是因为巨大的能量中枢在那里消失造成了时间和空间的稳定。

二叠纪-三叠纪灭绝事件的罪魁祸首之一，古峨眉山火山那巨大的岩浆库，现在就在怒海海底之下翻腾著。

使得这片地区被轻微地震困扰的真正缘由是……海底火山即将爆发。

他逐字逐句地阅读那份分析结果，面色变得越来越凝重。

古峨眉山火山…或者称之为怒海火山，一旦爆发，首先遭殃的就是沉没在海底的他的故乡，整个城市被迸发出的岩浆掩埋的话，那个“装置”自然就会被……

只要重现的灾难拉开了帷幕，就再也无法停止它的脚步。唯一的办法，就是在怒海火山爆发之前，将能量中枢送回去，阻止装置进一步失控。

距离留给苍的时间，只有最后的24小时。

这场酒会的主办人、弃天帝，正带著高傲而神秘的微笑，独自一人坐在角落里，却是仍是格外地引人注意。此时，他在用他的长指转动一只高脚杯，看著杯子里的红葡萄酒在灯光下折射出漂亮的色彩，对弃天帝来说这要比眼前那些衣著光鲜的男女要来的有意思的多。

酒会什么的，早就厌烦了。几百年的时间里，时代在变，这种无聊的社交模式却是顽固地一成不变。一大群人装作亲切地相互攀谈著，不过是为了攀比的珠宝华服，或者是身边的美人。无聊、虚伪……将身外物作为体现自我价值的资本，这些所谓的上流人士，还真是肤浅之极。

然后他又想到了苍，呵，要是苍说的那种灾难发生了……看到这群人惊慌失措逃难的模样，大概会很有趣？

“…………”

弃天帝没来由地面色一沉，闭上眼睛，周围的音乐，众人的谈笑风生，各种声音迅速退去，只余下一片寂静。

这里是弃天帝的意识空间，看不到边际的巨大空间，空无一物。

处在“待机状态”的另一个人独自待在那里。

“怎么了？”

“是苍。”另一个的自己，眼中闪烁的是不安的光彩。

“哦？……原来他还有强制接通他人意识世界的能力，果真不能掉以轻心……”

“…他非常虚弱。”

“他说了什么？”

“什么都没说，也许是强行入侵意识空间就耗费了所有的精力。”

“快回去！”

空气像是变成了某种粘稠的物质，从中取得氧气显得异常的苦难。他不得不大口地喘息著，但每一次呼吸的过程都是如此地痛苦，五脏六腑像被灼伤了一样。不知道过了多久，也不知道还要忍耐多久。

苍觉得自己可能太过冒险，他所掌握的唯一筹码显太过脆弱，但却别无选择，只有不到一天的时间……

朦胧之间，似乎听到了门被打开的声音。

“苍！！”是有人在喊他的名字么，听不太清楚啊……

勉强露出虚弱的笑容，随即，苍失去了意识。
