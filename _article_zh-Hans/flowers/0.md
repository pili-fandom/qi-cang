---
title: 風姿花傳
layout: post
lang: zh-Hans
category: flowers
depth: 2
date: 2011-03-16 00:00:00
---

### 起

弃天看着那个少年！心中的火焰烧很大，他这里可是便当店也！竟然给我只叫白饭一碗？肉燥还不用钱，汤可以喝通海！

有没有搞错！

「少年也！你好歹也叫个便当吧！」

「有阿！白饭便当！」


弃天额头上青茎暴走…是阿！白饭便当！


气的掀桌还扎到自己脚…


＊＊＊＊＊


「你要出来选国主？」弃天放下手上帐本，抬眼看着眼前的俊毅男子。

「是的！」

「…」


过了几天！苍气呼呼的拿着宣传单跑到弃天的帐房，把传单拍着弃总的案上怒道：「这是什麽！」

「宣传单阿！」

「宣传单！我是竞选国主，不是在选牛郎！」

伟大的弃总带着微笑说道：「要得到选票！用美色就对了…」


苍气的掀桌，来不及闪躲的弃总被桌子和文件给埋了…

「原来你是个“神”！」苍苦笑着。

「我已经不是了！」弃天帝把苍转过来面对着自己：「因为你…我已经成为凡人…苍…」

「是吗？」苍眼里的苦涩让弃天帝放开了苍。


两人安静的好一会儿之後说了：「回去吧!回去鬼之国…今後…我们应该不会再见面了…」

<br/>

<br/>

玄宗国是个民主国家，会形成民主国家的主要原因是因为，建国时期时国主是用禅让制度，随着每代国主的改革，已经变成国主由人民票选产生。当然了…不是人人都可以出来选国主，而是人民经由国家考试成为公务员之後，才能开始有资选村长，当过村长的人，才有资格选区长…以此类推。要出来选国主，当然是要当过县长的人才有资格。国主每十年选一次，能连选连任。

虽然…玄宗国是个老黄思想派的古国，但国主的产生方式到是很顺应民意，当然了…国主当不好也有可能是会被罢免的。


玄宗国是个什麽样的国家，并不是重点！重点是有一支旅行各国的有钱商队来到了玄宗国，查探了玄宗国的投资环境之後，决定要定居下来，投资这个国家的餐饮事业。


今天在省城一闹街上，一家闪亮亮响当当的便当店开幕了，因为便当店是采用前所未有的新的经营方式，采用中央厨房控制菜色水准，一个饭盒里包了一人份的饭菜，还有各样菜色，又快速不用等很久，加肉燥不用钱，汤随你喝到饱，价格便宜又实在，很快的就在省城引发话题，并且生意长红。


此便当店名为“弃天排骨大王”，因为总裁个人长的还算人模人样，所以招牌上就用了自家总裁的肖像，就画了咱们弃总左手排骨饭右手烧肉饭的肖像，更是引发众少女的话题。


因为那家卖便当的老板是个大帅哥阿～


为此…弃天排骨大王为此写了首歌，让大家歌颂他的便当店开幕


便当之歌（原案：叉烧包）

<br/>

排骨饭　谁爱吃刚才包的排骨饭

谁爱吃刚才包的排骨饭

还有那鸡腿饭阿鱼排饭阿三宝饭

烧鸭饭　应有尽有　烧肉饭

假使你说你不爱吃烧肉饭

还有各式各样的便当菜

让我来告诉你有海陆全餐大便当

满汉全席便当菜阿

好人客　你到底爱吃那一种　你到底爱吃那一种

那一样排骨饭阿鸡腿饭阿　三宝饭　烧肉饭阿　烧鸭饭阿

有的人他们爱吃烧鸭饭　有的人他们爱吃烧肉饭阿

人客阿到底爱吃三宝饭阿鸡腿饭

烧肉饭阿烧鸭饭　我最爱吃是排骨饭

<br/>

总之～弃天排骨大王就此轰轰烈烈的开幕了。

省城边缘…那里既是这个区域的边缘地带，也就是一些穷苦人家，边缘人所居住的地方，虽不至於变成所谓的贫民窟，但比起繁华的省城中心，那里就显的很破旧了。


在这里的某一间平房里，一名棕发的少年正准备要出门，这少年今年才十六岁，正值发育期，但是少年看来很纤细，有点发育不良的样子。

「苍！」一个中年人叫住了少年，少年回过头之後，他停下脚步。

「老师…」

「怎麽又没有来上课！再这样下去…你会被退学的。」

「我…」苍神色苍白，却无法回答老师的问题。

「我知道你的家世…但…」中年人是中等教育书院的夫子，他很关心苍，因为苍是百年难得的好学生，但苍是个孤儿，他还带了一群同为孤儿院的孩子，因为之前孤儿院的院长死了之後，孤儿院就倒了，苍身为比较年长的孩子，不忍其他的孩子被带走，虽然他已经满十五岁，已经离开了孤儿院，他还是回头去带走了剩下的孩子。因为如此…苍就必须养五个孩子，所以他打了三份工，常常不能到书院上课。

孩子中比苍少一岁的九方鍚，他本应该独立了，但他并不放心苍一个人，所以他留下来，帮忙照顾其他孩子，接下来是翠山行和黄商子这两人还在低等书院上课，但白雪飘和赤云染就不行了，完全是需要照顾的孩子呐。

「老师明白你的苦，也帮你申请了奖学金，你只要成绩保持第一名，就不用担心其他的问题，总之…先放弃一份到二份的工作，到书院来上课吧！」

「…让我想想吧…」苍很感动…但目前还是要考虑的是房租的钱，奖学金是无法在第一时间就拿到手的，所以苍和老师点了头之後，就先去打工了。


晚上打完工之後，苍经过了那家业积长红的便当店，他想起了今天他只吃了一餐，但算了算身上可以花的钱，基本上…他什麽也买不起…。这时他抬头看到点菜的菜单上写着“白饭五钱”，这是他可买的起了，又看到放在外头任人取用的肉燥和菜汤，於是…


「白饭！」苍点了白饭，因为已经接近收店的时侯，所以那伙计也不会计较苍叫了什麽，因为若苍点了其他的，要是缺菜了，他们还要再开火煮也麻烦，就这麽一个客人，就算了吧！给他一个便当包了白饭。

於是苍打包了一大袋的肉燥和全部的菜汤…工作伙记傻了眼，但因为标榜着不出过夜菜的原则，而且要收店了…就…算了吧。


因此…苍就开始了每到收店前，就去买白饭顺便包肉燥和汤，有时见他们要丢剩菜，就问能不能包走？伙计也很大方，他看的出苍是穷苦人家的孩子，反正都要丢了…给他也没差了。

而苍因为有了这麽便宜的一餐还可以给义弟义妹吃，他就少打一份工，总算能回书院顺利上课了。


这一天…咱们伟大的弃总来巡店了，他就坐镇在店内的某一角落，监看着所有人事物，盘算着展店的计画。


要收店前，他注意到了那个棕发的少年。


「白饭！」苍开口，而伙记也见怪不怪的给了苍饭盒，还杀必思的多给了一个，让苍去装肉燥，之後打扫的欧巴桑还帮忙苍打包了汤，苍要离开前，在厨房的大厨还丢了包已经发黄的地瓜叶给苍。

苍那满足的笑容，像针一样的直中弃天的心头，弃天一时之间竟也傻愣住。


但弃天很快就回过神了，但苍已经走远了，所以他走了出来，就叫住伙计来问话了。因为那一大包肉燥也是成本阿…虽然说，即然开放给客人自取，就不怕客人吃，但可没见过只叫一个白饭就包着比白饭还要多的肉燥回去吧。

伙记就说了：这个学生总是在收店的时侯来买白饭，也曾看过那学生在省城边缘出现，估计是住在那边的苦命孩子吧。因为那孩子并没有违反便当店的规定，所以伙计也没有阻止那孩子。

弃天听完之後心里实在有够囧，他是开便当店也，又不是慈善事业！不过对方也有付钱买饭阿！可是弃天心里觉的那里怪怪的。

後来的几天，因为弃天忙着展店，所以就把这个包肉燥的少年给忘了。之後…第二家分店开幕，打出促销策略：凡来店内消费就送便当一个。当天苍也来排队了，他在弃天的面前第一个买五钱白饭又给他包走便当的人，後面的人竟有样学样…大家都买白饭送一个便当。於是开幕的第一天，弃天的便当二店营业额低到让弃天的青茎暴走。

因为活动进行有三天，所以这三天苍都去白饭送便当，连三天的业绩都很惨的便当二店，让弃天气的想撞墙。

「再这样下去不行啦！」弃天的特助伏婴师拿着财务报表，向弃天抱怨着。而弃天只是想了一下，也许这三天的业积很难看，不过估算来客率，店家的知名度和市场调查，竟然是居高不下，所以这一次就绕了这个少年一次吧。


之後…促销结束了，苍又恢复了以前要收店的时侯来包饭，这几天弃天又来巡店，再次见到了苍，他开始认真的注意起这个少年来。

少年看来有点乾扁，一副就是营养不良的样子，每天包那麽多肉燥是吃到那里去了？他心里实在疑问。不过…一次就算了…每天都来会不会太扯？於是他决定亲自上场，把这个会包走他的肉燥的少年也给赶走。

第二天，弃天也在店内镇守，果然到了要收店的时侯，苍走进来了。苍一走进来，伙计就被拉到一边去，弃天亲自站柜台。


「白饭…」苍抬头看到不是往常的那个伙计时，心里觉的那里怪怪的。

弃天看着苍！心中的火焰烧很大，便当二店的仇他还没有报呢，而且…这里可是便当店也！竟然给我只叫白饭一碗？肉燥还不用钱，汤可以喝通海！有没有搞错

「少年也！你好歹也叫个便当吧！」弃天忍着头上的青茎，带着让人觉不舒服的微笑，这麽对苍说。

苍知道这该是便当店的高层干部，（苍还没有意识到弃天是老板，因为他从来没有注意看那个闪亮的招牌。）估计是要来阻止他包肉燥的吧。所以苍带着微笑这麽回应到：「有阿！白饭便当！」这麽一说完，弃天额头上青茎暴走…是阿！白饭便当！

苍才不会理会弃天，他拿了便当就去包肉燥，这时侯弃天又开口了：「你包的肉燥…」弃天指着苍手上的大包肉燥。

「怎麽？你有规定要包多少吗？这可是我一餐所用的…难不成…你怕客人吃喔…不是以客为尊吗？」苍指着墙上的标语。弃天的脸都绿了，而苍带着胜利的微笑离开了便当店，不但如此…不知道从那里碰出来的伙计，塞了包剩菜给苍带走。

弃天气的掀桌还扎到自己脚…


第一战…弃天以失败做收。


之後又过两人又数度交锋，老实说…弃天还真的没赢过，因为苍真的没有违反店内规定，加上剩菜本来要丢的，给了苍又不会怎麽样，但因为弃天曾说不能用店内的盒子包剩菜，之後苍竟然自己带一个饭盒来装，让弃天更是无言。

之後…弃天也算是半放弃了，就由苍去吧。不过…弃天真的很好奇，苍包回去的肉燥都到那里去了呢？他一个人真的能吃的了这麽多？


在好奇心的驱使下，加上吃饱没事干的弃天，就跟踪苍回家。才知道…那些饭菜没有一样是进了苍的肚子，苍都分给了他的弟妹们吃。加上有两个正值发育期，那点饭菜还是不够的。

然後…像是在演乡土剧一样…有房东来要房租，苍只能拜托再给一段时间，要等他的奖学金核发下来，但房东似乎失去了耐心，只能再等三天，要不然就苍搬家。
看完了这一段，弃天心里觉的那里又怪怪的，带着怪异的心情回到了自家的豪宅。


「这很正常阿！」优秀的特助伏婴师说。

「一般人…都是这样子的吗？」

「基本上住在那边缘的人都是这样子没错！弃总…你关心这个做什麽呢！这是这一期的报告，我们可以再开第三家分店了，要开放加盟吗？」

「现在暂时不开放…」弃天似乎被这样的心情给伴住了，因为他以前“身份特殊”，基本上没有想过也没有遇过这种问题。所以他对这个少年起了很浓厚的兴趣，而且他也想知道…少年怎麽度过这个难关。


过了二天之後的半夜，弃天和其他的谈生意的客户从酒家走出来，见到了苍…大半夜苍和一个看来是他弟弟的人，两人一起在做搬货工人，这是很辛苦的工作，他问了客户，才知道这是一份薪资很高可以现领的临时工，但很辛苦！因为都是半夜工作。看着弃天眼里…苍的身影已经占住了他的全部。


弃天从来没有感受过所谓的平凡和贫穷，看到苍这样辛苦的工作，还要养一群弟妹。是苍只有这样吗？弃天不知道，他只知道数钞票很累，所以他都请伏婴师来数。真的是穷的只剩下钱阿…。


後来弃天就让人调查了一下苍的一切，更让弃天吃惊了，苍竟然是中等书院里的高材生，今年要毕业的他一直维持着全校第一名的好成绩。打了二份工的他还能这麽强，真是个好人材阿。基於爱材的心理，还有心里另一种让弃天觉的怪异的感觉，让弃天想要帮助这个少年。


这一天…弃天让厨师做了菜，弃天亲自在店里等着。苍一项准时，收店前他就来报到了。

「过来…孩子！」弃天指了指他前面的坐位，这让苍有点吓一点，这位弃天大老板是想要怎样？（苍後来有注意到招牌，才发现弃天是老板）

「过来阿！难不成我会吃了你喔！来！和我一同吃看看，这菜好不好吃…我在试新菜色！不用钱啦。」

「…」苍觉的那里怪怪的，但他受不了食物的引诱，於是就…上勾了。


每天，弃天总是用着许多名义让苍和他一同吃这麽一餐，之後的剩菜都让苍包回去。有时侯弃天还会要厨师特别作一份营养满点的便当，当然是六人份让苍带回去。每天都吃肉燥来维持蛋白质也太可怜了，不都正在发育吗？


苍不是笨蛋，弃天对他的好，他很快就发现了。
